if (NOT MYHANDBOX_STYLE AND ENV{MYHANDBOX_STYLE})
    set(MYHANDBOX_STYLE $ENV{MYHANDBOX_STYLE})
endif()

if(MYHANDBOX_STYLE)

    message("-- Custom handbox style called.. ${MYHANDBOX_STYLE}")
    set(MYHANDBOX_STYLE ${MYHANDBOX_STYLE})

    if(EXISTS "${MYHANDBOX_STYLE}/Modern2.cc")
        message("-- \"${MYHANDBOX_STYLE}/Modern2.cc\" found !")
    else()
        message("-- \"${MYHANDBOX_STYLE}/Modern2.cc\" not found..")
        set(MYHANDBOX_STYLE "")
        set(MYHANDBOX_STYLE "" PARENT_SCOPE)
    endif()

    if(MYHANDBOX_STYLE)
        if(EXISTS "${MYHANDBOX_STYLE}/Modern2.h")
            message("-- \"${MYHANDBOX_STYLE}/Modern2.h\" found !")
        else()
            message("-- \"${MYHANDBOX_STYLE}/Modern2.h\" not found..")
            set(MYHANDBOX_STYLE "" )
            set(MYHANDBOX_STYLE "" PARENT_SCOPE)
        endif()
    endif()

    if(MYHANDBOX_STYLE)
        message("-- Custom handbox style set.. ${MYHANDBOX_STYLE}")
    else()
        message("-- Cannot set the custom handbox style..")
    endif()
endif()

if(NOT MYHANDBOX_STYLE)

    message("-- Default handbox style has been set..")
    set(MYHANDBOX_STYLE ${CMAKE_CURRENT_SOURCE_DIR}/include/HandboxStyle)
    set(ENV{MYHANDBOX_STYLE} "${MYHANDBOX_STYLE}")

    get_directory_property(HasParent PARENT_DIRECTORY)
    if(HasParent)
        set(MYHANDBOX_STYLE ${CMAKE_CURRENT_SOURCE_DIR}/include/HandboxStyle PARENT_SCOPE)
        set(ENV{MYHANDBOX_STYLE} "${MYHANDBOX_STYLE}")
    endif()

    message("-- Handbox style loaded from here: ${MYHANDBOX_STYLE}")
endif()
