/**
 * @Author: Marco Meyer <meyerma>
 * @Date:   2019-03-27T01:29:31-05:00
 * @Email:  marco.meyer@cern.ch
 * @Last modified by:   meyerma
 * @Last modified time: 2019-03-30T15:40:51-05:00
 */



/**
 * *********************************************
 *
 * \file TFactory.cc
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \brief Source code of the TFactory class
 *
 * ********************************************* */

#include <time.h>
#include <UIUC/TFactory.h>
ClassImp(UIUC::TFactory)
#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

using namespace std;

const int UIUC::TFactory::kSimpleAxis = 0;
const int UIUC::TFactory::kRepetitionAxis = 1; // Used by normalization()
const int UIUC::TFactory::kUseSimpleAverage = 0; // Average taking into account zeros values
const int UIUC::TFactory::kUseSimpleAverageNoZero = 1; // Average without taking into account zeros
const int UIUC::TFactory::kUseWeightedAverage = 2;
const int UIUC::TFactory::kUseWeightedAverageAndStdDevError = 3;
const int UIUC::TFactory::kUseSimpleAverageAndStdDevError = 4;
const int UIUC::TFactory::kUseSum = 5;

const int UIUC::TFactory::kScaleUnit = 1.;

const bool UIUC::TFactory::kUseWeight = true;
const bool UIUC::TFactory::kNotUseWeight = !kUseWeight;
const bool UIUC::TFactory::kUseWidth = true; // df/dx1 <-- dx1 = width
const bool UIUC::TFactory::kNotUseWidth = !kUseWidth;
const bool UIUC::TFactory::kModulo = true;
const bool UIUC::TFactory::kNoModulo = false;
const bool UIUC::TFactory::kRenormalizeFFT = true;


void UIUC::TFactory::RemoveAmbiguousBins(TH1* h)
{
        if(h == NULL) return;
        for(int i = 0, N = h->GetNcells(); i < N; i++) {

                if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;

                double content = h->GetBinContent(i);
                double content_err = h->GetBinError(i);
                if(content_err/content > 1) {

                        h->SetBinContent(i,0);
                        h->SetBinError(i,0);
                }
        }
}

vector<double> UIUC::TFactory::GetVectorAxis(TH1* h, Option_t *axis) 
{
        TString opt = axis;
                opt.ToLower();

        int iAxis;
        if(opt == "x") iAxis = 0;
        else if(opt == "y") iAxis = 1;
        else if(opt == "z") iAxis = 2;
        else iAxis = -1; // invalid value on purpose

        return UIUC::TFactory::GetVectorAxis(h, axis);
}

vector<double> UIUC::TFactory::GetVectorAxis(TH1* h, int iAxis)
{
        std::vector<double> v;
        if(h == NULL) return v;

        TAxis *axis = NULL;
        if( iAxis == 0 ) axis = h->GetXaxis();
        else if( iAxis == 1 ) axis = h->GetYaxis();
        else if( iAxis == 2 ) axis = h->GetZaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
                return v;
        }
        
        for(int i = 1, N = axis->GetNbins(); i <= N; i++)
                v.push_back(axis->GetBinLowEdge(i));

        return v;
}

vector<double> UIUC::TFactory::GetVectorContent(TH1D* h) { return GetVectorContent((TH1*) h); }
vector<float> UIUC::TFactory::GetVectorContent(TH1F* h) 
{ 
        std::vector<double> v = GetVectorContent((TH1*) h);
        std::vector<float> vv(v.begin(), v.end());

        return vv;
}
vector<double> UIUC::TFactory::GetVectorContent(TH1* h)
{
        std::vector<double> v;

        if(h == NULL) return v;
        for(int i = 0, N = h->GetNcells(); i < N; i++) {

                if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                v.push_back(h->GetBinContent(i));
        }

        return v;
}

vector<double> UIUC::TFactory::GetVectorError(TH1D* h) { return GetVectorError((TH1*) h); }
vector<float> UIUC::TFactory::GetVectorError(TH1F* h) 
{ 
        std::vector<double> v = GetVectorError((TH1*) h);
        std::vector<float> vv(v.begin(), v.end());

        return vv;
}

vector<double> UIUC::TFactory::GetVectorError(TH1* h)
{
        std::vector<double> v;

        if(h == NULL) return v;
        for(int i = 0, N = h->GetNcells(); i < N; i++) {

                if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                v.push_back(h->GetBinError(i));
        }

        return v;
}

void UIUC::TFactory::AddBinError(TH1* h, double err, Option_t *option)
{
        TString opt = option;
        opt.ToLower();

        if(h == NULL) return;
        for(int i = 0, N = h->GetNcells(); i < N; i++) {

                if(h->GetBinContent(i)) {

                        double w = err;
                        if(opt.Contains("%")) w = h->GetBinContent(i) * sqrt(err); // divide by err to compensate the option (math trick)

                        h->SetBinError(i, TMath::Sqrt(w*w + h->GetBinError(i)*h->GetBinError(i)));
                }
        }
}

double UIUC::TFactory::Integral(TGraph *g, double *err_min, double *err_max, double xmin, double xmax)
{
        double x0 = 0, x1 = 0;
        if(UIUC::IsNaN(xmin)) x0 = g->GetXaxis()->GetXmin();
        if(UIUC::IsNaN(xmax)) x1 = g->GetXaxis()->GetXmax();

        double y = 0, ylow = 0, yhigh = 0;
        UNUSED(x0);
        UNUSED(x1);

        UNUSED(ylow);
        UNUSED(yhigh);

        UNUSED(g);
        UNUSED(err_min);
        UNUSED(err_max);
        UNUSED(xmin);
        UNUSED(xmax);

        //for() {
        //
        //   x0 = "...";
        //   x1 = "...";
        //
        //   if(err_min != 0)
        //   if(err_max != 0)
        //}

        //TODO..
        throw std::invalid_argument("Not implemented..");
        return y;
}

bool UIUC::TFactory::Filter(TH1* h, double content_min, double content_max)
{
        if(h == NULL) return 0;

        for(int i = 1; i <= h->GetNbinsX(); i++) {

                for(int j = 1; j <= h->GetNbinsY(); j++) {

                        for(int k = 1; k <= h->GetNbinsZ(); k++) {

                                double ibin = h->GetBin(i,j,k);
                                if(h->IsBinUnderflow(ibin) || h->IsBinOverflow(ibin)) continue;
                                if(h->GetBinContent(ibin) < content_min && !UIUC::IsNaN(content_min)) {
                                        h->SetBinContent(ibin, 0);
                                        h->SetBinError(ibin, 0);
                                }

                                if(h->GetBinContent(ibin) > content_max && !UIUC::IsNaN(content_max)) {
                                        h->SetBinContent(ibin, 0);
                                        h->SetBinError(ibin, 0);
                                }

                        }
                }
        }

        return 1;
}

pair<double,double> UIUC::TFactory::Integral(TH1* h, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax)
{
        pair<double,double> d;
        if(h == NULL) return make_pair(NAN,NAN);

        double x0 = 1;
        if(UIUC::IsNaN(xmin)) x0 = 1;
        else x0 = h->FindBin(xmin);
        double x1 = 0;
        if(UIUC::IsNaN(xmax)) x1 = h->GetNbinsX();
        else x1 = h->FindBin(xmax);

        double y0 = 1;
        if(UIUC::IsNaN(ymin)) y0 = 1;
        else y0 = h->FindBin(ymin);
        double y1 = 0;
        if(UIUC::IsNaN(ymax)) y1 = h->GetNbinsY();
        else y1 = h->FindBin(ymax);

        double z0 = 1;
        if(UIUC::IsNaN(zmin)) z0 = 1;
        else z0 = h->FindBin(zmin);

        double z1 = 0;
        if(UIUC::IsNaN(zmax)) z1 = h->GetNbinsZ();
        else z1 = h->FindBin(zmax);

        for(int i = x0; i <= x1; i++) {

                for(int j = y0; j <= y1; j++) {

                        for(int k = z0; k <= z1; k++) {

                                double ibin = h->GetBin(i,j,k);
                                if(h->IsBinUnderflow(ibin) || h->IsBinOverflow(ibin)) continue;

                                double width = h->GetXaxis()->GetBinWidth(i)*h->GetYaxis()->GetBinWidth(j)*h->GetZaxis()->GetBinWidth(k);

                                if(h->GetBinContent(ibin) != 0) {
                                        d.first += h->GetBinContent(ibin)*width;
                                        d.second = TMath::Sqrt(
                                                d.second*d.second +
                                                TMath::Power(width*h->GetBinError(ibin),2)
                                                );
                                }
                        }
                }
        }

        return d;
}

TGraphErrors* UIUC::TFactory::HistToGraphErrors(TString name, TString title, TH1D* h)
{
        if(h == NULL) return NULL;

        TGraphErrors *g = new TGraphErrors(h);
        g->SetName(name);
        g->SetTitle(title);
        g->GetXaxis()->SetTitle(h->GetXaxis()->GetTitle());
        g->GetYaxis()->SetTitle(h->GetYaxis()->GetTitle());

        return g;
}

TGraphAsymmErrors* UIUC::TFactory::HistToGraphAsymmErrors(TString name, TString title, TH1D* h)
{
        if(h == NULL) return NULL;

        TGraphAsymmErrors *g = new TGraphAsymmErrors(h);
        g->SetName(name);
        g->SetTitle(title);
        g->GetXaxis()->SetTitle(h->GetXaxis()->GetTitle());
        g->GetYaxis()->SetTitle(h->GetYaxis()->GetTitle());

        return g;
}

TGraph* UIUC::TFactory::HistToGraph(TString name, TString title, TH1D* h)
{
        if(h == NULL) return NULL;

        TGraph *g = new TGraph(h);
        g->SetName(name);
        g->SetTitle(title);
        g->GetXaxis()->SetTitle(h->GetXaxis()->GetTitle());
        g->GetYaxis()->SetTitle(h->GetYaxis()->GetTitle());

        return g;
}

TH1D* UIUC::TFactory::GraphToHist(TString name, TString title, TGraph* g)
{
        if(g == NULL) return NULL;

        TH1D *h = (TH1D*) g->GetHistogram()->Clone(name);
        h->SetTitle(title);

        for(int i = 0, N = g->GetN(); i < N; i++) {
                if(g->GetY()) h->SetBinContent(i+1, g->GetY()[i]);
                if(g->GetEY() != NULL) h->SetBinError(i+1, g->GetEY()[i]);
        }

        return h;
}

TGraphAsymmErrors* UIUC::TFactory::GetGraphWithHessianErrors(TGraphAsymmErrors *gCentralPDF0, vector<TGraphAsymmErrors*> gReplicaPDF, int taylor_order) {

        if(gCentralPDF0 == NULL) return NULL;

        TGraphAsymmErrors *gCentralPDF = (TGraphAsymmErrors*) gCentralPDF0->Clone();
        for(int i = 0, N = gCentralPDF->GetN(); i < N; i++) {

                double x0, y0;
                gCentralPDF->GetPoint(i, x0, y0);
                double y0low = gCentralPDF->GetErrorYlow(i);
                double y0high = gCentralPDF->GetErrorYhigh(i);

                double y = y0;
                vector<double> yhigh, ylow;
                for(int j = 0, J = gReplicaPDF.size(); j < J; j++) {

                        double xi, yi;
                        gReplicaPDF[j]->GetPoint(i, xi, yi);
                        y += yi;
                        if(taylor_order == 1) {

                                ylow.push_back(yi);
                                yhigh.push_back(yi);

                        } else if(taylor_order == 2) {

                                if(yi > y0) yhigh.push_back(yi);
                                else ylow.push_back(yi);

                        } else {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__,
                                                             "Cannot compute with taylor order %d", taylor_order
                                                             );
                        }
                }

                gCentralPDF->GetY()[i] = y/(gReplicaPDF.size()+1);

                double ylow_stddev = 0;
                if(ylow.size() == 0) ylow_stddev = 0;
                else if(ylow.size() == 1) ylow_stddev = ylow[0];
                else {

                        for(int j = 0, J = ylow.size(); j < J; j++) ylow_stddev += TMath::Power(ylow[j]-y0, 2)/(J-1);
                        ylow_stddev = TMath::Sqrt(ylow_stddev + y0low*y0low);
                        gCentralPDF->SetPointEYlow(i, ylow_stddev);
                }

                double yhigh_stddev = 0;
                if(yhigh.size() == 0) yhigh_stddev = 0;
                else if(yhigh.size() == 1) yhigh_stddev = yhigh[0];
                else {
                        for(int j = 0, J = yhigh.size(); j < J; j++) yhigh_stddev += TMath::Power(yhigh[j]-y0, 2)/(J-1);
                        yhigh_stddev = TMath::Sqrt(yhigh_stddev + y0high*y0high);
                        gCentralPDF->SetPointEYhigh(i, yhigh_stddev);
                }
        }

        return gCentralPDF;
}

TGraphAsymmErrors* UIUC::TFactory::GetGraphWithMinMaxErrors(TGraphAsymmErrors *gCentralPDF0, vector<TGraphAsymmErrors*> gReplicaPDF) {

        if(gCentralPDF0 == NULL) return NULL;

        TGraphAsymmErrors *gCentralPDF = (TGraphAsymmErrors*) gCentralPDF0->Clone();
        for(int i = 0, N = gCentralPDF->GetN(); i < N; i++) {

                double x0, y0;
                gCentralPDF->GetPoint(i, x0, y0);

                double yhigh = 0, ylow = 0;
                for(int j = 0, J = gReplicaPDF.size(); j < J; j++) {

                        double xi, yi;
                        gReplicaPDF[j]->GetPoint(i, xi, yi);

                        double y = TMath::Abs(yi-y0);
                        int sign = (yi < y0) ? -1 : 1;
                        if(sign < 0 && y > ylow) ylow = TMath::Abs(yi-y0);
                        if(sign > 0 && y > yhigh) yhigh = TMath::Abs(yi-y0);
                }

                gCentralPDF->SetPointEYhigh(i, yhigh);
                gCentralPDF->SetPointEYlow(i, ylow);
        }

        return gCentralPDF;
}


TGraphAsymmErrors* UIUC::TFactory::GetGraphWithSumMaxErrors(TGraphAsymmErrors *gCentralPDF0, vector<TGraphAsymmErrors*> gNegativeReplicaPDF, vector<TGraphAsymmErrors*> gPositiveReplicaPDF, int taylor_order) {

        if(gCentralPDF0 == NULL) return NULL;
        if(gNegativeReplicaPDF.size() != gPositiveReplicaPDF.size()) return NULL;

        TGraphAsymmErrors *gCentralPDF = (TGraphAsymmErrors*) gCentralPDF0->Clone();
        for(int i = 0, N = gCentralPDF->GetN(); i < N; i++) {

                double x0, y0;
                gCentralPDF->GetPoint(i, x0, y0);

                double yhigh = 0, ylow = 0;
                for(int j = 0, J = gNegativeReplicaPDF.size(); j < J; j++) {

                        double xi_n, yi_n;
                        gNegativeReplicaPDF[j]->GetPoint(i, xi_n, yi_n);
                        double xi_p, yi_p;
                        gPositiveReplicaPDF[j]->GetPoint(i, xi_p, yi_p);

                        if(taylor_order == 1) {

                                yhigh += 1/4 * TMath::Power(yi_p-yi_n,2);
                                yhigh = ylow;

                        } else if(taylor_order == 2) {

                                yhigh += TMath::Power(TMath::Max(TMath::Max(yi_p-y0,yi_n-y0),0.),2);
                                ylow += TMath::Power(TMath::Max(TMath::Max(y0-yi_p,y0-yi_n),0.),2);
                                //        yhigh += TMath::Power(TMath::Max(TMath::Max(TMath::Abs(yi_p-y0),TMath::Abs(yi_n-y0)),0.),2);
                                //        ylow += TMath::Power(TMath::Max(TMath::Max(TMath::Abs(y0-yi_p),TMath::Abs(y0-yi_n)),0.),2);
                        }
                }

                gCentralPDF->SetPointEYhigh(i, TMath::Sqrt(yhigh));
                gCentralPDF->SetPointEYlow(i, TMath::Sqrt(ylow));
        }

        return gCentralPDF;
}

TGraphAsymmErrors* UIUC::TFactory::GetGraphWithWeightedSamplesErrors(TGraphAsymmErrors *gCentralPDF0, vector<TGraphAsymmErrors*> gReplicaPDF) {

        if(gCentralPDF0 == NULL) return NULL;
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Weighted samples error PDF.. has to be verified..");

        TGraphAsymmErrors *gCentralPDF = (TGraphAsymmErrors*) gCentralPDF0->Clone();
        for(int i = 0, N = gCentralPDF->GetN(); i < N; i++) {

                double x0, y0;
                gCentralPDF->GetPoint(i, x0, y0);

                vector<double> ey;
                for(int j = 0, J = gReplicaPDF.size(); j < J; j++) {

                        double xi, yi;
                        gReplicaPDF[j]->GetPoint(i, xi, yi);
                        ey.push_back(yi);
                }

                vector<double> stdv;
                if(!ComputeMeanAndSV(stdv, "", ey)) return NULL;
                gCentralPDF->SetPointEYhigh(i, stdv[3]);
                gCentralPDF->SetPointEYlow(i, stdv[3]);

        }

        return gCentralPDF;
}

TVectorD UIUC::TFactory::GetFqfn(TH1D *h) {

        TVectorD v(h->GetNbinsX());
        for(int i = 1, I = h->GetNbinsX(); i <= I; i++)
                v[i] = h->GetBinContent(i);

        return v;
}

TMatrixD UIUC::TFactory::GetMatrix(TH1 *h) {

        TMatrixD m(h->GetNbinsX(), h->GetNbinsY());
        for(int i = 1, I = h->GetNbinsX(); i <= I; i++) {
                for(int j = 1, J = h->GetNbinsY(); j <= J; j++) {

                        m[i][j] = h->GetBinContent(i,j);
                }
        }

        return m;
}

pair<double,double> UIUC::TFactory::GetRangeContent(TList *l, bool bLog)
{
        vector<TObject*> vObj;
        for(int i = 0, N = l->GetSize(); i < N; i++) vObj.push_back(l->At(i));

        return UIUC::TFactory::GetRangeContent(vObj, bLog);
}

pair<double,double> UIUC::TFactory::GetRangeContent(vector<TObject*> vObject, bool bLog)
{
        double min = NAN, max = NAN;

        for(int k = 0, K = vObject.size(); k < K; k++) {

                if(vObject[k] == NULL) continue;
                if(vObject[k]->InheritsFrom("TGraph")) {

                        TGraph *g = (TGraph*) vObject[k];
                        for(int i = 0, N = g->GetN(); i < N; i++) {

                                double content = g->GetY()[i];
                                if(content == 0) continue;

                                if(min > content || UIUC::IsNaN(min)) min = content;
                                if(max < content || UIUC::IsNaN(max)) max = content;
                        }

                } else if(vObject[k]->InheritsFrom("TH1")) {

                        TH1 *h = (TH1*) vObject[k];
                        for(int i = 0, N = h->GetNcells(); i < N; i++) {

                                if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;

                                double content = h->GetBinContent(i);
                                double content_err = h->GetBinError(i);
                                if(content == 0 && content_err == 0) continue;
                                if(content == 0 && bLog) continue;

                                if(min > content || UIUC::IsNaN(min)) min = content;
                                if(max < content || UIUC::IsNaN(max)) max = content;
                        }
                }
        }

        return std::make_pair(min, max);
}

TObject* UIUC::TFactory::SetAutomaticRange(TObject* obj0, std::vector<TObject*> v, pair<double,double> margin, bool bLog)
{
        double xmin = NAN, xmax = NAN;
        double ymin = NAN, ymax = NAN;
        double zmin = NAN, zmax = NAN;

        if(obj0->InheritsFrom("TH1") && ((TH1*) obj0)->GetEntries()) return obj0;
        else if(obj0->InheritsFrom("TGraph") && ((TGraph*) obj0)->GetN() == 0 ) return obj0;

        if(!v.size()) v.push_back(obj0);
        pair<double,double> content0 = UIUC::TFactory::GetRangeContent(v, bLog);

        for(int i = 0, N = v.size(); i < N; i++) {

                if(v[i] == NULL) continue;

                double content = 0;

                //
                // X-axis
                //

                TAxis *x = NULL;
                if(v[i]->InheritsFrom("TGraph")) {

                        x = ((TGraph*) v[i])->GetXaxis();
                        content = x->GetXmin();
                        if(xmin > content || UIUC::IsNaN(xmin)) xmin = content;
                        content = x->GetXmax();
                        if(xmax < content || UIUC::IsNaN(xmax)) xmax = content;

                } else if(v[i]->InheritsFrom("TH1")) {

                        x = ((TH1*) v[i])->GetXaxis();
                        content = x->GetXmin();
                        if(xmin > content || UIUC::IsNaN(xmin)) xmin = content;
                        content = x->GetXmax();
                        if(xmax < content || UIUC::IsNaN(xmax)) xmax = content;
                }

                //
                // Y-axis
                //
                TAxis *y = NULL;
                if(v[i]->InheritsFrom("TGraph")) {

                        if(ymin > content0.first  || UIUC::IsNaN(ymin)) ymin = content0.first;
                        if(ymax < content0.second || UIUC::IsNaN(ymax)) ymax = content0.second;

                } else if(v[i]->InheritsFrom("TGraph2D")) {

                        y = ((TGraph2D*) v[i])->GetYaxis();
                        content = y->GetXmin();
                        if(ymin > content || UIUC::IsNaN(ymin)) ymin = content;
                        content = y->GetXmax();
                        if(ymax < content || UIUC::IsNaN(ymax)) ymax = content;

                } else if(v[i]->InheritsFrom("TH1")) {

                        y = ((TH1*) v[i])->GetYaxis();

                        TAxis *z = ((TH1*) v[i])->GetZaxis();
                        if(y->GetNbins() == 1 && z->GetNbins() == 1) {

                                if(ymin > content0.first  || UIUC::IsNaN(ymin)) ymin = content0.first;
                                if(ymax < content0.second || UIUC::IsNaN(ymax)) ymax = content0.second;

                        } else {
                                content = y->GetXmin();
                                if(ymin > content || UIUC::IsNaN(ymin)) ymin = content;
                                content = y->GetXmax();
                                if(ymax < content || UIUC::IsNaN(ymax)) ymax = content;
                        }
                }

                TAxis *z = NULL;
                if(v[i]->InheritsFrom("TH1")) {

                        z = ((TH1*) v[i])->GetZaxis();
                        if(z->GetNbins() == 1) {

                                if(zmin > content0.first  || UIUC::IsNaN(zmin)) zmin = content0.first;
                                if(zmax < content0.second || UIUC::IsNaN(zmax)) zmax = content0.second;

                        } else {

                                content = z->GetXmin();
                                if(zmin > content || UIUC::IsNaN(zmin)) zmin = content;
                                content = z->GetXmax();
                                if(zmax < content || UIUC::IsNaN(zmax)) zmax = content;
                        }
                }
        }

        if(UIUC::IsNaN(xmin) && UIUC::IsNaN(xmax)) {
                xmin = 0;
                xmax = 1;
        }

        if(UIUC::IsNaN(ymin) && UIUC::IsNaN(ymax)) {
                ymin = 0;
                ymax = 1;
        }

        if(UIUC::IsNaN(zmin) && UIUC::IsNaN(zmax)) {
                zmin = 0;
                zmax = 1;
        }

        if(obj0->InheritsFrom("TH3")) { }
        else if(obj0->InheritsFrom("TH2")) {

                zmin += zmin*margin.first;
                zmax += zmax*margin.second; // Get some marging in the top when computing bin content limits

        } else if(obj0->InheritsFrom("TH1")) {

                ymin += ymin*margin.first;
                ymax += ymax*margin.second; // Get some marging in the top when computing bin content limits

        } else if(obj0->InheritsFrom("TGraph2D")) {

                zmin += zmin*margin.first;
                zmax += zmax*margin.second; // Get some marging in the top when computing bin content limits

        } else if(obj0->InheritsFrom("TGraph"))   {

                ymin += ymin*margin.first;
                ymax += ymax*margin.second; // Get some marging in the top when computing bin content limits
        }
        
        TObject *tmp1 = UIUC::TFactory::SetRange((TH1*) obj0, "x", xmin, xmax);
        if(tmp1 == NULL) tmp1 = obj0->Clone();
        TObject *tmp2 = UIUC::TFactory::SetRange((TH1*) tmp1, "y", ymin, ymax);
        if(tmp2 == NULL) tmp2 = tmp1->Clone();
        TObject *tmp3 = UIUC::TFactory::SetRange((TH1*) tmp2, "z", zmin, zmax);
        if(tmp3 == NULL) tmp3 = tmp2->Clone();

        delete tmp1;
        delete tmp2;

        return tmp3;
}

void UIUC::TFactory::SetAutomaticRangeUser(std::vector<TObject*> v, std::pair<double,double> margin, bool bLog)
{
        double xmin = NAN, xmax = NAN;
        double ymin = NAN, ymax = NAN;
        double zmin = NAN, zmax = NAN;

        pair<double,double> content0 = UIUC::TFactory::GetRangeContent(v, bLog);
        for(int i = 0, N = v.size(); i < N; i++) {

                if(v[i] == NULL) continue;

                double content = 0;

                //
                // X-axis
                //
                TAxis *x = NULL;
                if(v[i]->InheritsFrom("TGraph")) {

                        x = ((TGraph*) v[i])->GetXaxis();
                        content = x->GetXmin();
                        if(xmin > content || UIUC::IsNaN(xmin)) xmin = content;
                        content = x->GetXmax();
                        if(xmax < content || UIUC::IsNaN(xmax)) xmax = content;

                } else if(v[i]->InheritsFrom("TH1")) {

                        x = ((TH1*) v[i])->GetXaxis();
                        content = x->GetXmin();
                        if(xmin > content || UIUC::IsNaN(xmin)) xmin = content;
                        content = x->GetXmax();
                        if(xmax < content || UIUC::IsNaN(xmax)) xmax = content;
                }

                //
                // Y-axis
                //
                TAxis *y = NULL;
                if(v[i]->InheritsFrom("TGraph")) {

                        y = ((TGraph*) v[i])->GetYaxis();
                        if(ymin > content0.first  || UIUC::IsNaN(ymin)) ymin = content0.first;
                        if(ymax < content0.second || UIUC::IsNaN(ymax)) ymax = content0.second;

                } else if(v[i]->InheritsFrom("TGraph2D")) {

                        y = ((TGraph2D*) v[i])->GetYaxis();
                        content = y->GetXmin();
                        if(ymin > content || UIUC::IsNaN(ymin)) ymin = content;
                        content = y->GetXmax();
                        if(ymax < content || UIUC::IsNaN(ymax)) ymax = content;

                } else if(v[i]->InheritsFrom("TH1")) {

                        y = ((TH1*) v[i])->GetYaxis();

                        TAxis *z = ((TH1*) v[i])->GetZaxis();
                        if(y->GetNbins() == 1 && z->GetNbins() == 1) {

                                if(ymin > content0.first  || UIUC::IsNaN(ymin)) ymin = content0.first;
                                if(ymax < content0.second || UIUC::IsNaN(ymax)) ymax = content0.second;

                        } else {

                                content = y->GetXmin();
                                if(ymin > content || UIUC::IsNaN(ymin)) ymin = content;
                                content = y->GetXmax();
                                if(ymax < content || UIUC::IsNaN(ymax)) ymax = content;
                        }
                }

                TAxis *z = NULL;
                if(v[i]->InheritsFrom("TH1")) {

                        z = ((TH1*) v[i])->GetZaxis();
                        if(z->GetNbins() == 1) {

                                if(zmin > content0.first  || UIUC::IsNaN(zmin)) zmin = content0.first;
                                if(zmax < content0.second || UIUC::IsNaN(zmax)) zmax = content0.second;

                        }else {
                                content = z->GetXmin();
                                if(zmin > content || UIUC::IsNaN(zmin)) zmin = content;
                                content = z->GetXmax();
                                if(zmax < content || UIUC::IsNaN(zmax)) zmax = content;
                        }
                }
        }


        if(UIUC::IsNaN(xmin) && UIUC::IsNaN(xmax)) {
                xmin = 0;
                xmax = 1;
        }

        if(UIUC::IsNaN(ymin) && UIUC::IsNaN(ymax)) {
                ymin = 0;
                ymax = 1;
        }

        if(UIUC::IsNaN(zmin) && UIUC::IsNaN(zmax)) {
                zmin = 0;
                zmax = 1;
        }


        for(int i = 0, N = v.size(); i < N; i++) {

                if(v[i] == NULL) continue;

                if(v[i]->InheritsFrom("TH3")) { }
                else if(v[i]->InheritsFrom("TH2")) {

                        zmin += UIUC::EpsilonEqualTo(zmin, 0) ? 0 : -TMath::Abs(zmin)*margin.first;
                        zmax += UIUC::EpsilonEqualTo(zmin, 0) ? 0 :  TMath::Abs(zmax)*margin.second; // Get some marging in the top when computing bin content limits

                } else if(v[i]->InheritsFrom("TH1")) {

                        ymin += UIUC::EpsilonEqualTo(ymin, 0) ? 0 : -TMath::Abs(ymin)*margin.first;
                        ymax += UIUC::EpsilonEqualTo(ymax, 0) ? 0 :  TMath::Abs(ymax)*margin.second; // Get some marging in the top when computing bin content limits

                } else if(v[i]->InheritsFrom("TGraph2D")) {

                        zmin += UIUC::EpsilonEqualTo(zmin, 0) ? 0 : -TMath::Abs(zmin)*margin.first;
                        zmax += UIUC::EpsilonEqualTo(zmax, 0) ? 0 :  TMath::Abs(zmax)*margin.second; // Get some marging in the top when computing bin content limits

                } else if(v[i]->InheritsFrom("TGraph"))   {

                        ymin += UIUC::EpsilonEqualTo(ymin, 0) ? 0 : -TMath::Abs(ymin)*margin.first;
                        ymax += UIUC::EpsilonEqualTo(ymax, 0) ? 0 :  TMath::Abs(ymax)*margin.second; // Get some marging in the top when computing bin content limits
                }
                
                TAxis *x = NULL;
                if(v[i]->InheritsFrom("TGraph2D")   ) x = ((TGraph2D*) v[i])->GetXaxis();
                else if(v[i]->InheritsFrom("TGraph")) x = ((TGraph*) v[i])->GetXaxis();
                else if(v[i]->InheritsFrom("TH1")   ) x = ((TH1*) v[i])->GetXaxis();
                if(x != NULL) x->SetRangeUser(xmin,xmax);

                TAxis *y = NULL;
                if(v[i]->InheritsFrom("TGraph2D")   ) y = ((TGraph2D*) v[i])->GetYaxis();
                else if(v[i]->InheritsFrom("TGraph")) y = ((TGraph*) v[i])->GetYaxis();
                else if(v[i]->InheritsFrom("TH1")   ) y = ((TH1*) v[i])->GetYaxis();
                if(y != NULL) y->SetRangeUser(ymin,ymax);

                TAxis *z = NULL;
                if(v[i]->InheritsFrom("TGraph2D")) z = ((TGraph2D*) v[i])->GetZaxis();
                else if(v[i]->InheritsFrom("TH1")) z = ((TH1*) v[i])->GetZaxis();
                if(z != NULL) z->SetRangeUser(zmin,zmax);
        }
}

TH1* UIUC::TFactory::MakeCorrelation(TH1* h1, TH1* h2, Option_t* option, int x1bins, int x2bins, double x1min, double x1max, double x2min, double x2max)
{
        TString opt = option;
        opt.ToLower();

        TString hname = (TString) h1->GetName() + "__vs__"+ h2->GetName();
        TString htitle = (TString) "Correlation between \""+h1->GetTitle()+"\" and \""+h2->GetTitle()+"\"";

        if(h1 == NULL || h2 == NULL) return NULL;
        if( UIUC::HandboxMsg::Warning(!h1->InheritsFrom("TH1"), __METHOD_NAME__, "Input is not inheriting from TH1") ) return NULL;
        if( UIUC::HandboxMsg::Warning(!h2->InheritsFrom("TH1"), __METHOD_NAME__, "Tempo #0 is not inheriting from TH1") ) return NULL;
        if( UIUC::HandboxMsg::Warning(h1->GetXaxis()->GetBinWidth(1) != 1 || h2->GetXaxis()->GetBinWidth(1) != 1, __METHOD_NAME__, "Not valid TH1 pair : spill number must be on x-axis (= 1)")) return NULL;
        if( UIUC::HandboxMsg::Warning(h1->GetXaxis()->GetBinWidth(1) != h2->GetXaxis()->GetBinWidth(1), __METHOD_NAME__, "Not valid TH1 pair : not same x-axis range")) return NULL;

        if(x1min == x1max) {

                x1min = h1->GetMinimum();
                x1max = h1->GetMaximum();
        }

        if(x2min == x2max) {

                x2min = h2->GetMinimum();
                x2max = h2->GetMaximum();
        }

        if(opt.Contains("YXX")) {

                x2min /= x1min;
                x2max /= x1max;
        }

        TH1* h = new TH2D(hname, htitle, x1bins, x1min, x1max, x2bins, x2min, x2max);
        for(int i = 1, N = h1->GetNbinsX(); i <= N; i++) {

                if(h1->GetBinContent(i) != 0 && h2->GetBinContent(i) != 0) {

                        if(opt.Contains("YXX")) {

                                if(h1->GetBinContent(i) != 0) h->Fill(h1->GetBinContent(i), h2->GetBinContent(i)/h1->GetBinContent(i));
                                else UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Cannot divide by x=0.. for bin #" + TString::Itoa(i,10));

                        } else h->Fill(h1->GetBinContent(i), h2->GetBinContent(i));
                }
        }

        return NULL;
}

TH1* UIUC::TFactory::RemoveBackground(TH1 *h, int niter)
{
        if(h == NULL) return NULL;

        TH1* hBkg = (TH1*)h->ShowBackground(niter,"same");
        h->Add(hBkg, -1.);

        return h;
}

TH1* UIUC::TFactory::FFT(TH1 *h, Option_t *option, vector<double>* re, vector<double>* im)
{
        return FFT(h, option, !UIUC::TFactory::kRenormalizeFFT, re, im);
}

TH1* UIUC::TFactory::FFT(TH1 *h, Option_t *option, bool renormalizeY, vector<double>* re, vector<double>* im)
{
        TString opt = option;
                opt.ToLower();

        if(h == NULL) return NULL;

        double dt = h->GetXaxis()->GetBinWidth(1); // Time resolution
        int N  = h->GetXaxis()->GetNbins();     // Frame size

        double fs = 1./dt;                // Sample frequency
        double T  = N / fs;               // Frame size

        double BW = fs / 2; // Nyquist frequency or Bandwidth 
        int    SL =  N / 2; // Spectral lines total number of frequency sample
        double df = BW/SL;
        
        bool bLocalRe = false;
        if(re != NULL) re->resize(N);
        else {

                re = new vector<double>(N,0);
                bLocalRe = true;
        }

        bool bLocalIm = false;
        if(im != NULL) im->resize(N);
        else {

                im = new vector<double>(N,0);
                bLocalIm = true;
        }

        // Apply FFT
        TVirtualFFT::SetTransform(0);
        TH1 *hFFT = NULL;
        
        hFFT = (TH1*) h->FFT(hFFT, opt); // Arbitrary axis..
        hFFT->SetName((TString) h->GetName() + ":" + opt + "-fft");
        hFFT->SetTitle(Form("Frequency distribution (f_{s} = %.2f Hz); Frequency (Hz)", fs));
        if(hFFT == NULL) {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Cannot compute FFT using \"" + hFFT->GetName() + "\" with \"" + opt + "\"");
                return NULL;
        }

        hFFT->SetDirectory(h->GetDirectory());

        // Get the complex point..
        TVirtualFFT *pFFT = TVirtualFFT::GetCurrentTransform();
	try { pFFT->GetPointsComplex(&(*re)[0], &(*im)[0]); }
        catch (std::exception& e) {

            UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Cannot compute FFT, did you installed+enabled fftw3?");
            delete hFFT;
	    return NULL;
	}

        if(bLocalRe) {
                delete re;
                re = NULL;
        }

        if(bLocalIm) {
                delete im;
                im = NULL;
        }

        delete pFFT;
        pFFT = NULL;

        TH1D* hFFT_tmp = new TH1D(
                (TString) h->GetName() + ":" + opt + "-fft:fft-norm", 
                Form("Corrected Frequency Spectrum (f_{s} = %.2f Hz, nFFT = %d, d_{f} = %.2f); Frequency (Hz)", fs, N, df), 
                (SL+1), 0, (SL+1)*df);
        
        hFFT_tmp->SetDirectory(h->GetDirectory());

        for(int j = 0; j < hFFT->GetNbinsX()/2+1; j++)
                hFFT_tmp->SetBinContent(j+1, renormalizeY ? hFFT->GetBinContent(j+1)/N : hFFT->GetBinContent(j+1) );

        delete hFFT;
        hFFT = NULL;

        return hFFT_tmp;
}

TH1* UIUC::TFactory::BackwardFFT(TString hname, TString htitle, bool renormalizeY, vector<double>& re, vector<double>& im, double tmin, double tmax, TDirectory *dir)
{
        if( UIUC::HandboxMsg::Error(re.size() != im.size(), __METHOD_NAME__, "Non-consistent re/im pair provided.. Not the same size") ) return NULL;

        return BackwardFFT(hname, htitle, renormalizeY, re.size(), &re[0], &im[0], tmin, tmax, dir);
}

TH1* UIUC::TFactory::BackwardFFT(TString hname, TString htitle, bool renormalizeY, int N, double* re, double* im, double tmin, double tmax, TDirectory *dir)
{
        bool renormalizeX = (tmin != tmax);
        if(UIUC::HandboxMsg::Error(N<1, __METHOD_NAME__, "Wrong parameter, N must be greater than 1")) return NULL;

        //Now let's make a backward transform:
        TVirtualFFT *pBackwardFFT = TVirtualFFT::FFT(1, &N, "C2R M K");
        pBackwardFFT->SetPointsComplex(re,im);
        pBackwardFFT->Transform();

        TH1 *hBackwardFFT = NULL;
        hBackwardFFT = TH1::TransformHisto(pBackwardFFT, hBackwardFFT, "RE");
        if(hBackwardFFT == NULL) {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Cannot compute backward FFT..");
                return NULL;
        }

        hBackwardFFT->SetName(hname);
        hBackwardFFT->SetTitle(htitle);
        hBackwardFFT->SetDirectory(dir);

        delete pBackwardFFT;
        pBackwardFFT = NULL;

        if(renormalizeX) {

                tmin = 0;
                tmax = N;
        }

        TH1D* hBackwardFFT_rescaled = new TH1D(hname, htitle, N+1, tmin, tmax);
        hBackwardFFT_rescaled->SetDirectory(dir);

        for(int j = 0; j < hBackwardFFT->GetNbinsX(); j++)
                hBackwardFFT_rescaled->SetBinContent(j, renormalizeY ? hBackwardFFT->GetBinContent(j)/N : hBackwardFFT->GetBinContent(j) );

        delete hBackwardFFT;
        hBackwardFFT = NULL;

        return hBackwardFFT_rescaled;
}

TH1* UIUC::TFactory::FindFirstHistogram(vector<TObject*> vObject) {

        for(int i = 0, N = vObject.size(); i < N; i++) {

                if(vObject[i] == NULL) continue;
                if(vObject[i]->InheritsFrom("TH1")) return (TH1*) vObject[i];
                else if (vObject[i]->InheritsFrom("TGraph")) return ((TGraph*) vObject[i])->GetHistogram();
        }

        return NULL;
}

TSpectrum *UIUC::TFactory::Spectrum(TH1 *h, int npeaks) {

        TSpectrum *s = new TSpectrum(2*npeaks, 0.001);
        s->Search(h, 2, "nodraw", 0);

        return s;
}

TH1* UIUC::TFactory::RemoveEmptySpills(TH1* h0)
{
        if(h0 == NULL) return NULL;
        if(!h0->InheritsFrom("TH1") && !h0->InheritsFrom("TProfile")) {

                cerr << "Object is inheriting from TProfile nor TH1.. cannot apply badspill list.." << endl;
                return NULL;
        }

        TString opt;
        TAxis *x1 = NULL;
        TAxis *x2 = NULL;
        TAxis *x3 = NULL;

        if( TString(h0->GetXaxis()->GetTitle()).EqualTo("spill") ) {

                opt = "x";
                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();
                x3 = h0->GetZaxis();

        } else if( TString(h0->GetYaxis()->GetTitle()).EqualTo("spill") ) {

                opt = "y";
                x2 = h0->GetXaxis();
                x1 = h0->GetYaxis();
                x3 = h0->GetZaxis();

        } else if( TString(h0->GetZaxis()->GetTitle()).EqualTo("spill") ) {

                opt = "z";
                x1 = h0->GetZaxis();
                x2 = h0->GetXaxis();
                x3 = h0->GetYaxis();

        } else {

                cerr << "[Warning] No spill label found for \"" << h0->GetName() << "\".. cannot remove empty spills.." << endl;
                return h0;
        }

        vector<bool> bIsEmpty(x1->GetNbins(), true);

        double content, content_err;
        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        for(int x3_i = 1, x3_N = x3->GetNbins(); x3_i <= x3_N; x3_i++) {

                                if(opt.Contains("x")) content = h0->GetBinContent(x1_i, x2_i, x3_i);
                                else if(opt.Contains("y")) content = h0->GetBinContent(x2_i, x1_i, x3_i);
                                else if(opt.Contains("z")) content = h0->GetBinContent(x2_i, x3_i, x1_i);

                                if( !UIUC::EpsilonEqualTo(content,0) ) {

                                        bIsEmpty[x1_i-1] = false;
                                        break;
                                }

                                if(bIsEmpty[x1_i-1] == false) break;
                        }

                        if(bIsEmpty[x1_i-1] == false) break;
                }
        }

        TString hname = (TString) h0->GetName() + ":" + opt + "-remove-empty-spills";
        TH1 *h = (TH1*) h0->Clone(hname);

        int x1_j = 1;
        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                if(bIsEmpty[x1_i-1] == true) continue;

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        for(int x3_i = 1, x3_N = x3->GetNbins(); x3_i <= x3_N; x3_i++) {

                                if(opt.Contains("x")) {

                                        content = h0->GetBinContent(x1_i, x2_i, x3_i);
                                        content_err = h0->GetBinError(x1_i, x2_i, x3_i);

                                        h->SetBinContent(x1_j, x2_i, x3_i, content);
                                        h->SetBinError(x1_j, x2_i, x3_i, content_err);

                                } else if(opt.Contains("y")) {

                                        content = h0->GetBinContent(x2_i, x1_i, x3_i);
                                        content_err = h0->GetBinError(x2_i, x1_i, x3_i);

                                        h->SetBinContent(x2_i, x1_j, x3_i, content);
                                        h->SetBinError(x2_i, x1_j, x3_i, content_err);

                                } else if(opt.Contains("z")) {

                                        content = h0->GetBinContent(x2_i, x3_i, x1_i);
                                        content_err = h0->GetBinError(x2_i, x3_i, x1_i);

                                        h->SetBinContent(x2_i, x3_i, x1_j, content);
                                        h->SetBinError(x2_i, x3_i, x1_j, content_err);
                                }
                        }
                }

                x1_j++;
        }

        TH1* h_tmp = UIUC::TFactory::SetRange(h, opt, 1, x1_j);

        //delete h; // h is not used anymore..
        if(h_tmp != NULL) {

                h = h_tmp;
                h->SetTitle(hname);
        }

        return h;
}

bool UIUC::TFactory::HasCompatibleBinning(TAxis *a0, double binLowEdge, double binWidth)
{
        double w0 = a0->GetBinWidth(1);
        double w1 = binWidth;

        if(w0/w1 != (int) (w0/w1)) return false;

        if(UIUC::EpsilonEqualTo(a0->GetBinLowEdge(1), binLowEdge)) return true;
        return false;
}

// Check if h2 has a compatible binning with h1
// (not h2..)
bool UIUC::TFactory::HasCompatibleBinning(TH1* h1, TH1* h2)
{
        if(h1 == NULL || h2 == NULL) return 0;
        if( h1->InheritsFrom("TH3") && !h2->InheritsFrom("TH3") ) return 0;
        if( h1->InheritsFrom("TH2") && !h2->InheritsFrom("TH2") ) return 0;
        if( h1->InheritsFrom("TH1") && !h2->InheritsFrom("TH1") ) return 0;

        vector<TAxis*> axis0 = {h1->GetXaxis(), h1->GetYaxis(), h1->GetZaxis()};
        vector<TAxis*> axis1 = {h2->GetXaxis(), h2->GetYaxis(), h2->GetZaxis()};
        for(int i = 0; i < 3; i++) {

                TAxis* a0 = axis0[i];
                TAxis* a1 = axis1[i];
                if(a0->GetBinLowEdge(1) < a1->GetBinLowEdge(1)) {

                        a0 = axis1[i];
                        a1 = axis0[i];
                }

                bool b = false;
                for(int j = 1, J = a1->GetNbins(); j <= J; j++) {

                        b |= HasCompatibleBinning(a0, a1->GetBinLowEdge(j), a1->GetBinWidth(j));
                        if(b) return 1;
                }

                if(!b) return 0;
        }

        return 0;
}

TH1* UIUC::TFactory::RemoveContent(TH1* h)
{
        int nbinsx = h->GetNbinsX();
        int nbinsy = h->GetNbinsY();
        int nbinsz = h->GetNbinsZ();

        for (int binz=1; binz <= nbinsz; binz++) {

                for (int biny=1; biny <= nbinsy; biny++) {

                        for (int binx=1; binx <= nbinsx; binx++) {

                                int bin = h->GetBin(binx,biny,binz);
                                h->SetBinContent(bin, 0);
                        }
                }
        }

        return h;
}

TH1* UIUC::TFactory::RemoveError(TH1* h)
{
        int nbinsx = h->GetNbinsX();
        int nbinsy = h->GetNbinsY();
        int nbinsz = h->GetNbinsZ();

        for (int binz=1; binz <= nbinsz; binz++) {

                for (int biny=1; biny <= nbinsy; biny++) {

                        for (int binx=1; binx <= nbinsx; binx++) {

                                int bin = h->GetBin(binx,biny,binz);
                                h->SetBinError(bin, 0);
                        }
                }
        }

        return h;
}

TGraph* UIUC::TFactory::RemoveContent(TGraph* g0)
{

        for(int i = 0, N = g0->GetN(); i < N; i++) {

                if(g0->GetY() != NULL) g0->GetY()[i] = 0;
        }

        return g0;
}

TGraph* UIUC::TFactory::RemoveError(TGraph* g0)
{
        for(int i = 0, N = g0->GetN(); i < N; i++) {

                if(g0->GetEX() != NULL) g0->GetEX()[i] = 0;
                if(g0->GetEY() != NULL) g0->GetEY()[i] = 0;
                if(g0->GetEYlow() != NULL) g0->GetEYlow()[i] = 0;
                if(g0->GetEYhigh() != NULL) g0->GetEYhigh()[i] = 0;
        }

        for(int i = 0, N = g0->GetN(); i < N; i++) {
                if(UIUC::EpsilonEqualTo(g0->GetY()[i], 0)) g0->RemovePoint(i);
        }

        return g0;
}

TString UIUC::TFactory::MultipleLines(TString title0) {

        TString title;

        TObjArray *array = title0.Tokenize(";");
        for(int i = 0; i < array->GetEntries(); i++) {

                title0 = ((TObjString *)(array->At(i)))->String();

                if(i != 0) title += ";" + title0;
                else {

                        TObjArray *subarray = title0.Tokenize('\\');
                        for (int j = 0; j < subarray->GetEntries(); j++) {

                                TString line = ((TObjString *)(subarray->At(j)))->String();

                                if(title.EqualTo("")) title = line;
                                else title = "#splitline{"+title+"}{"+line+"}";
                        }
                        delete subarray;
                }

        }

        delete array;

        return title;
}


TGraph* UIUC::TFactory::GetFillArea(int nbins, double x0, double x1, TF1 *f1, TF1 *f2)
{
        if(f1 == NULL) return NULL;
        if(nbins < 1) return NULL;
        if(x0 == x1 || x0 > x1) {

                x0 = f1->GetXmin();
                x1 = f1->GetXmax();
        }

        x0 = TMath::Max(x0, f1->GetXmin());
        x1 = TMath::Min(x1, f1->GetXmax());
        if(f2) x0 = TMath::Max(x0, f2->GetXmin());
        if(f2) x1 = TMath::Min(x1, f2->GetXmax());

        //second fill area between f1 and f2
        vector<Double_t> x(2*nbins+1), y(2*nbins+1);
        double dx = (x1-x0)/(nbins-1);
        for (Int_t i=0; i<nbins; i++) {
                x[i] = x0 + dx*i;
                y[i] = (f2) ? f2->Eval(x[i]) : 0;
                x[nbins+i] = x1 - dx*i;
                y[nbins+i] = f1->Eval(x[nbins+i]);
        }
        x[2*nbins] = x[0]; y[2*nbins] = y[0];

        TGraph *g = new TGraph(2*nbins+1, &x[0], &y[0]);
        g->SetFillStyle(3002);
        g->SetFillColorAlpha(f1->GetLineColor(), 0.5);

        return g;
}

TLine*  UIUC::TFactory::GetMeanLine(TF1 *f)
{
        if(f == NULL) return NULL;

        TLine *l = new TLine(f->GetParameter(1), 0, f->GetParameter(1), f->GetMaximum());
        l->SetLineColor(f->GetLineColor());
        l->SetLineWidth(f->GetLineWidth());
        l->SetLineStyle(8);

        return l;
}

TLine*  UIUC::TFactory::GetMeanLine(TH1 *h0)
{
        if(h0 == NULL) return NULL;
        TLine *l = new TLine(h0->GetMean(), 0, h0->GetMean(), h0->GetMaximum());
        l->SetLineColor(h0->GetLineColor());
        l->SetLineWidth(h0->GetLineWidth());
        l->SetLineStyle(8);

        return l;
}

TLine*  UIUC::TFactory::GetMeanLine(TGraph *g0)
{
        if(g0 == NULL) return NULL;
        TLine *l = new TLine(g0->GetMean(), 0, g0->GetMean(), g0->GetMaximum());
        l->SetLineColor(g0->GetLineColor());
        l->SetLineWidth(g0->GetLineWidth());
        l->SetLineStyle(8);

        return l;
}

TString UIUC::TFactory::GetRandomName(TString prefix, TString suffix){

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        return prefix + gRandom + suffix;
}

TString UIUC::TFactory::GetTitle(TString title0, int i){

        TObjArray *array = title0.Tokenize(";");
        TString title = (i < 0 || i >= array->GetEntries()) ? "" : ((TObjString *)(array->At(i)))->String();

        delete array;
        array = NULL;

        return title;
}

bool UIUC::TFactory::AppendToHistogram(TH1*&h0, TTree *tree, TString var, TString selection)
{
        TH1 *h = (TH1*) TVarexp(var).GetHistogram(h0, tree, selection);
        if(h != NULL) {

                if(h0 == NULL) h0 = (TH1*) h->Clone();
                else {

                        h0->Add(h);
                        delete h;
                        h = NULL;
                }
        }

        return true;
}

TH1* UIUC::TFactory::AppendToHistogram(TString hname, TTree *tree, TString var, TString selection)
{
        TH1 *h0 = (TH1*) TVarexp(var).GetHistogram(hname, tree, selection);

        TH1* h = NULL;
        if(h0 != NULL) {

                if(h == NULL) h = (TH1*)h0->Clone();
                else {

                        h->Add(h0);
                        delete h0;
                        h0 = NULL;
                }
        }

        return h;
}

TH2D* UIUC::TFactory::Transpose(TH2 *h0)
{
        TH2D *h = new TH2D(
                h0->GetName() + TString(":^T"), h0->GetTitle(),
                h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax()
                );

        for(int i = 1; i <= h0->GetNbinsX(); i++) {

                for(int j = 1; j <= h0->GetNbinsY(); j++) {

                        h->SetBinError(j,i, h0->GetBinError(i,j));
                        h->SetBinContent(j,i, h0->GetBinContent(i,j));
                }
        }

        h->SetEntries(h0->GetEntries());
        h->GetXaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetXaxis()->GetTitle());

        return h;
}

TH1* UIUC::TFactory::Smearing(TH1* h0, double sX, double sY, double sZ)
{
        if(h0 == NULL) return NULL;

        double x, y, z, w0, w1, w2;
        const int nX = h0->GetNbinsX();
        const int nY = h0->GetNbinsY();
        const int nZ = h0->GetNbinsZ();

        if(nX == 1) sX = 0;
        if(nY == 1) sY = 0;
        if(nZ == 1) sZ = 0;

        if((h0->InheritsFrom("TH2") || h0->InheritsFrom("TH3")) && (sX < 0 || sY < 0 || sY < 0)) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Narrowing of 2D histogram not implemented");
                return NULL;
        }

        TString option = "";
        if(sX) option += ":x-smearing";
        if(sY) option += ":y-smearing";
        if(sZ) option += ":z-smearing";

        TH1* h = (TH1*) h0->Clone(h0->GetName() + option);
        h->SetDirectory(h0->GetDirectory());
        if(sX == 0 && sY == 0 && sZ == 0) return h;

        int kmin0, kmax0, kmin1, kmax1, kmin2, kmax2;
        double sigma = 1;
        if(sX) sigma *= sX;
        if(sY) sigma *= sY;
        if(sZ) sigma *= sZ;

        if(sX < 0) {

                h0 = UIUC::TFactory::FFT(h, "MAG");
                delete h;
                h = h0;
                h->SetName(h0->GetName() + option);
        }

        for(int i=1; i<=nX; ++i)
        {
                x = h->GetXaxis()->GetBinCenter(i);
                //kmin0 = h->GetXaxis()->FindBin(x-3*sX);//For being faster, we reduce the range to 3-sigma. Use kmin = 1 if you want the whole range
                //kmax0 = h->GetXaxis()->FindBin(x+3*sX);//For being faster, we reduce the range to 3-sigma. Use kmax = nX if you want the whole range
                kmin0 = 1;
                kmax0 = nX;
                for(int j=1; j<=nY; ++j)
                {
                        y = h->GetYaxis()->GetBinCenter(j);
                        //kmin1 = h->GetYaxis()->FindBin(y-3*sY);//For being faster, we reduce the range to 3-sigma. Use lmin = 1 if you want the whole range
                        //kmax1 = h->GetYaxis()->FindBin(y+3*sY);//For being faster, we reduce the range to 3-sigma. Use lmax = nY if you want the whole range
                        kmin1 = 1;
                        kmax1 = nY;
                        for(int k=1; k<=nZ; ++k)
                        {
                                //For bin i,j, we look in the old histogram at all bins in a nearby region and calculate the probability of being smeared in the current one
                                z = h->GetZaxis()->GetBinCenter(k);
                                //kmin2 = h->GetZaxis()->FindBin(z-3*sZ);//For being faster, we reduce the range to 3-sigma. Use lmin = 1 if you want the whole range
                                //kmax2 = h->GetZaxis()->FindBin(z+3*sZ);//For being faster, we reduce the range to 3-sigma. Use lmax = nY if you want the whole range
                                kmin2 = 1;
                                kmax2 = nZ;

                                double content = 0, content_err = 0;
                                for(int k0=kmin0; k0<=kmax0; ++k0) {

                                        w0 = h0->GetXaxis()->GetBinCenter(k0);
                                        for(int k1=kmin1; k1<=kmax1; ++k1) {

                                                w1 = h0->GetYaxis()->GetBinCenter(k1);
                                                for(int k2=kmin2; k2<=kmax2; ++k2) {

                                                        w2 = h0->GetZaxis()->GetBinCenter(k2);
                                                        //We approximate the integral of the gaussian function between binLowEdge and binUpEdge by the integrand times the bin width. This bin width and the normalization is divided after the end of the loop. The Error Function could be used instead.

                                                        double gaus = 1;
                                                        if(sX > 0) gaus *= TMath::Gaus(x,w0,sX,false);
                                                        /*else if(sX < 0) {

                                                                gaus *= TMath::Gaus(x,w0,sX,false);
                                                           }*/

                                                        if(sY > 0) gaus *= TMath::Gaus(y,w1,sY,false);
                                                        /*else if(sY < 0) {

                                                                gaus *= TMath::Gaus(y,w1,sY,false);
                                                           }*/

                                                        if(sZ > 0) gaus *= TMath::Gaus(z,w2,sZ,false);
                                                        /*else if(sZ < 0) {

                                                                gaus *= 1TMath ::Gaus(z,w2,sZ,false);
                                                           }*/

                                                        content += h0->GetBinContent(k0,k1,k2)*gaus;
                                                        content_err = TMath::Sqrt(content_err*content_err + h0->GetBinError(k0,k1,k2)*h0->GetBinError(k0,k1,k2)*gaus*gaus);
                                                }
                                        }
                                }

                                content = content * h->GetXaxis()->GetBinWidth(i) * h->GetYaxis()->GetBinWidth(j)  * h->GetZaxis()->GetBinWidth(k) / 2./TMath::Pi() / sigma;
                                if(!UIUC::EpsilonEqualTo(content,0)) {
                                        h->SetBinContent(i,j,k, content);
                                        h->SetBinError(i,j,k, content_err);
                                } else {

                                        h->SetBinContent(i,j,k, 0);
                                        h->SetBinError(i,j,k, 0);
                                }
                        }
                }
        }

        h->SetEntries(h0->GetEntries());
        return h;
}

TH1* UIUC::TFactory::Empty(TH1 *h0)
{
        for(int i = 0, N = h0->GetNcells(); i < N; i++) {

                h0->SetBinContent(i,0);
                h0->SetBinError(i,0);
        }

        h0->SetEntries(0);
        return h0;
}

TGraph* UIUC::TFactory::Empty(TGraph *g0)
{
        for(int i = 0, N = g0->GetN(); i < N; i++) {

                if(g0->GetY() != NULL) g0->GetY()[i] = 0;
                if(g0->GetEY() != NULL) g0->GetEY()[i] = 0;
                if(g0->GetEYlow() != NULL) g0->GetEYlow()[i] = 0;
                if(g0->GetEYhigh() != NULL) g0->GetEYhigh()[i] = 0;
        }

        return g0;
}

bool UIUC::TFactory::HasVariableBinWidth(TGraph *g0, double epsilon)
{
        if(g0 == NULL) return 0;

        bool x = HasVariableBinWidth(g0->GetXaxis(), epsilon);
        if(x) return 1;
        bool y = HasVariableBinWidth(g0->GetYaxis(), epsilon);
        if(y) return 1;

        return 0;
}

bool UIUC::TFactory::HasVariableBinWidth(TGraph2D *g0, double epsilon)
{
        if(g0 == NULL) return 0;

        bool x = HasVariableBinWidth(g0->GetXaxis(), epsilon);
        if(x) return 1;
        bool y = HasVariableBinWidth(g0->GetYaxis(), epsilon);
        if(y) return 1;
        bool z = HasVariableBinWidth(g0->GetZaxis(), epsilon);
        if(z) return 1;

        return 0;
}

bool UIUC::TFactory::HasVariableBinWidth(TH1 *h0, double epsilon)
{
        if(h0 == NULL) return 0;

        bool x = HasVariableBinWidth(h0->GetXaxis(), epsilon);
        if(x) return 1;

        bool y = HasVariableBinWidth(h0->GetYaxis(), epsilon);
        if(y) return 1;

        bool z = HasVariableBinWidth(h0->GetZaxis(), epsilon);
        if(z) return 1;

        return 0;
}

bool UIUC::TFactory::HasVariableBinWidth(TAxis *a0, double epsilon)
{
        if(a0 == NULL) return 0;

        double delta0 = a0->GetBinUpEdge(1) - a0->GetBinLowEdge(1);
        for(int i = 2, N = a0->GetNbins(); i <= N; i++) {

                double delta = a0->GetBinUpEdge(i) - a0->GetBinLowEdge(i);
                if(!UIUC::EpsilonEqualTo(delta0, delta, epsilon)) return 1;
        }

        return 0;
}

TGraph* UIUC::TFactory::CopyStruct(TGraph *g0, TString name, TString title)
{
        if(name.EqualTo("")) {

                TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
                name = (TString) g0->GetName() +"-" + gRandom;
        }

        int N = g0->GetN();
        vector<double> x, y, ey, eyl, eyh;
        for(int i = 0; i < N; i++) {
                if(g0->GetX() != NULL) x.push_back(g0->GetX()[i]);
                if(g0->GetY() != NULL) y.push_back(g0->GetY()[i]);
                if(g0->GetEY() != NULL) ey.push_back(g0->GetEY()[i]);
                if(g0->GetEYlow() != NULL) eyl.push_back(g0->GetEYlow()[i]);
                if(g0->GetEYhigh() != NULL) eyh.push_back(g0->GetEYhigh()[i]);
        }

        TGraph *g = NULL;
        if(g0->InheritsFrom("TGraphAsymmErrors"))
                g = (TGraph*) new TGraphAsymmErrors(N, &x[0], &y[0], NULL, NULL, &eyl[0], &eyh[0]);
        else if(g0->InheritsFrom("TGraphErrors"))
                g = (TGraph*) new TGraphErrors(N, &x[0], &y[0], NULL, &ey[0]);
        else if(g0->InheritsFrom("TGraph"))
                g = (TGraph*) new TGraph(N, &x[0], &y[0]);

        g->SetName(name);
        g->SetTitle(title);
        g->GetXaxis()->SetTitle(g0->GetXaxis()->GetTitle());
        g->GetYaxis()->SetTitle(g0->GetYaxis()->GetTitle());

        return g;
}

TH1* UIUC::TFactory::CopyStruct(TH1 *h0, TString name, TString title, vector<int> rebin)
{
        name = UIUC::TVarexp::RemoveTFormulaHistogramBinning(name);
        if(name.EqualTo("")) {

                TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
                name = (TString) h0->GetName() +"-" + gRandom;
        }

        int nbinsx = h0->GetNbinsX();
        int nbinsy = h0->GetNbinsY();
        int nbinsz = h0->GetNbinsZ();

        rebin.resize(3);
        if(rebin[0] == 0) rebin[0] = 1;
        if(rebin[1] == 0) rebin[1] = 1;
        if(rebin[2] == 0) rebin[2] = 1;

        if(rebin[0] == -1) rebin[0] = nbinsx;
        if(rebin[1] == -1) rebin[1] = nbinsy;
        if(rebin[2] == -1) rebin[2] = nbinsz;

        TH1 *h = (TH1*) h0->Clone(name);

        h->SetTitle(title);
        if(h->InheritsFrom("TH3")) ((TH3D*)h)->Rebin3D(rebin[0],rebin[1],rebin[2]);
        else if(h->InheritsFrom("TH2")) ((TH2D*)h)->Rebin2D(rebin[0],rebin[1]);
        else if(h->InheritsFrom("TH1")) ((TH1D*)h)->Rebin(rebin[0]);
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown histogram type.. skip");
                return NULL;
        }

        UIUC::TFactory::Empty(h);
        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());

        return h;
}

bool UIUC::TFactory::ComputeMeanAndSV(vector<double> &stdv, vector<double> c, vector<double> c_err, vector<double> w, bool bShowValues)
{
        //definitions:
        //std[0] = mean
        //std[1] = standard uncertainty
        //std[2] = standard deviation
        //std[3] = standard error / standard deviation of the mean

        stdv.clear();
        stdv.resize(4);

        int N = c.size();

        TString eMessage;
        eMessage = Form("Cannot compute values.. size error with c_err (c=%d, c_err=%d, w=%d)", N, (int) c_err.size(), (int) w.size());
        if( UIUC::HandboxMsg::Error(N != (int) c_err.size() && c_err.size(), __METHOD_NAME__, eMessage ) ) return false;
        if(!c_err.size()) c_err.resize(N, 0);

        eMessage = Form("Cannot compute values.. size error with w (c=%d, c_err=%d, w=%d)", N, (int) c_err.size(), (int) w.size());
        if( UIUC::HandboxMsg::Error(N != (int) w.size() && w.size(), __METHOD_NAME__, eMessage) ) return false;
        if(!w.size()) w.resize(N, 1);

        // //////
        // MEAN VALUE
        // ////// ////
        bool bNoWeight = true;
        double content = 0, content_error = 0, weight_tot = 0;
        for (int i = 0; i < N; i++)
        {
                content += c[i]*w[i];
                content_error += c_err[i]*c_err[i]*w[i]*w[i];
                weight_tot += w[i];

                if(w[i] != 1) bNoWeight = false;
        }
        if(weight_tot) content = content/weight_tot;
        if(weight_tot) content_error = TMath::Sqrt(content_error)/weight_tot; //https://physics.stackexchange.com/questions/15197/how-do-you-find-the-uncertainty-of-a-weighted-average
        stdv[0] = content;
        stdv[1] = content_error;

        // //////
        // STANDARD DEVIATION CALCULATION
        // ////// ///////////////////////
        content = 0;
        content_error = 0;
        weight_tot = 0;

        int M = 0;
        double weight2_tot = 0;
        for (int i = 0; i < N; i++) {

                weight_tot += w[i];
                weight2_tot += w[i]*w[i];
                content += (c[i] - stdv[0]) * (c[i] - stdv[0]) * w[i];

                if(w[i] != 0) M++;
        }

        if(M > 1) content = TMath::Sqrt(content / ( ((double) M-1)/((double) M) * weight_tot ));
        if(M > 1) content_error = content*TMath::Sqrt(weight2_tot)/weight_tot; // https://math.stackexchange.com/questions/320441/standard-deviation-of-the-weighted-mean
        stdv[2] = content;
        stdv[3] = content_error;

        TString opt = (bNoWeight) ? "1" : "Wi";
        if(UIUC::IsNaN(stdv[0]) || UIUC::IsNaN(stdv[1]) || UIUC::IsNaN(stdv[2]) || UIUC::IsNaN(stdv[3])) {

                for (int i = 0; i < N; i++)
                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Ci = \"%f +/- %f\" and Wi = \"%f\"", c[i], c_err[i], w.size() ? w[i] : 1);

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__,
                                             Form("Mean(%s) = %.4f +/- %.4f; StdDev(%s) = %.4f; StdErr(%s) = %.4f",
                                                  opt.Data(), stdv[0], stdv[1],
                                                  opt.Data(), stdv[2],
                                                  opt.Data(), stdv[3]
                                                  )
                                             );

                exit(1);
        }

        if(bShowValues && N != 0) {

                for (int i = 0; i < N; i++)
                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Ci = \"%f +/- %f\" and Wi = \"%f\"", c[i], c_err[i], w.size() ? w[i] : 1);

                UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__,
                                               Form("Mean(%s) = %.4f +/- %.4f; StdDev(%s) = %.4f; StdErr(%s) = %.4f",
                                                    opt.Data(), stdv[0], stdv[1],
                                                    opt.Data(), stdv[2],
                                                    opt.Data(), stdv[3]
                                                    )
                                               );
        }

        return 1;
}

bool UIUC::TFactory::ComputeMeanAndSV(vector<double> &stdv, Option_t* option, vector<double> c, vector<double> c_err, vector<double> w, vector<double> w_err)
{
        TString opt = option;
                opt.ToLower();

        bool bShowValues = opt.Contains("vv");
        opt.ReplaceAll("vv", "");
        opt = UIUC::HandboxMsg::Trim(opt);

        bool bWeighted = false;
        for (int i = 0, I = w.size(); i < I; i++) {
                if(w[i] != 1) bWeighted = true;
                if(bWeighted) break;
        }

        if(bWeighted && w.size() && opt.EqualTo("")) {
                UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Weighted computation is enabled by default (Wi != 1 found..)");
        }

        int N = c.size();

        TString eMessage = "You cannot use option C and W at the same time..";
        if( UIUC::HandboxMsg::Error(opt.Contains("c") && opt.Contains("w"), __METHOD_NAME__, eMessage) ) return false;
        eMessage = "You cannot use option W, weight is empty..";
        if( UIUC::HandboxMsg::Error(opt.Contains("w") && !w.size(), __METHOD_NAME__, eMessage) ) return false;
        eMessage = "You cannot use option RW/AW, error on weight is empty..";
        if( UIUC::HandboxMsg::Error((opt.Contains("rw") || opt.Contains("aw")) && w.size() && !w_err.size(), __METHOD_NAME__, eMessage) ) return false;
        eMessage = "You cannot use option RW/AW, content error is empty..";
        if( UIUC::HandboxMsg::Error((opt.Contains("rc") || opt.Contains("ac")) && c.size() && !c_err.size(), __METHOD_NAME__, eMessage) ) return false;

        eMessage = Form("Cannot compute values.. size error with c_err (c=%d, c_err=%d, w=%d, w_err=%d)", N, (int) c_err.size(), (int) w.size(), (int) w_err.size());
        if( UIUC::HandboxMsg::Error(N != (int) c_err.size() && c_err.size(), __METHOD_NAME__, eMessage ) ) return false;
        if(!c_err.size()) c_err.resize(N, 0);

        eMessage = Form("Cannot compute values.. size error with w (c=%d, c_err=%d, w=%d, w_err=%d)", N, (int) c_err.size(), (int) w.size(), (int) w_err.size());
        if( UIUC::HandboxMsg::Error( N != (int) w.size() && w.size(), __METHOD_NAME__, eMessage ) ) return false;
        if(!w.size()) w.resize(N, 1);

        eMessage = Form("Cannot compute values.. size error with w_err (c=%d, c_err=%d, w=%d, w_err=%d)", N, (int) c_err.size(), (int) w.size(), (int) w_err.size());
        if( UIUC::HandboxMsg::Error(N != (int) w_err.size() && w_err.size(), __METHOD_NAME__, eMessage) ) return false;
        if(!w_err.size()) w_err.resize(N, 0);

        // //////
        // PREPARE WEIGHT
        // //////
        int check_ambiguity = -1;
        for (int i = 0, N = c.size(); i < N; i++) {

                if( UIUC::IsNaN(c_err[i]) ) c_err[i] = TMath::Sqrt(c[i]);
                if(opt.Contains("rc")) {

                        // Reciprocal absolute uncertainty
                        if(i == 0) UIUC::HandboxMsg::PrintDebug(1000, __METHOD_NAME__, "Weighted average (rel.unc.): weight=1/(sigmaX=[dX/X])");
                        if(i == 0) check_ambiguity++;

                        if( !UIUC::EpsilonEqualTo(c_err[i], 0) ) w[i] = 1 / TMath::Power(c_err[i], 2);
                        else w[i] = 0;

                }

                if(opt.Contains("rw")) {

                        // Reciprocal absolute uncertainty
                        if(i == 0) UIUC::HandboxMsg::PrintDebug(1000, __METHOD_NAME__, "Weighted average (rel. unc.): weight=1/(sigmaX=[dX/X])");
                        if(i == 0) check_ambiguity++;

                        if( !UIUC::EpsilonEqualTo(w_err[i], 0) ) w[i] = 1 / TMath::Power(w_err[i],2);
                        else w[i] = 0;
                }

                if(opt.Contains("0")) {

                        if(i == 0) UIUC::HandboxMsg::PrintDebug(1000, __METHOD_NAME__, "Simple average: weight=1");
                        if(i == 0) check_ambiguity++;

                        if(c[i] != 0 || c_err[i] != 0) w[i] = 1;
                        else w[i] = 0;
                }

                if(opt.EqualTo("")) {

                        if(i == 0) UIUC::HandboxMsg::PrintDebug(1000, __METHOD_NAME__, "Weighted average: weight=\"custom\"");
                        if(i == 0) check_ambiguity++;
                }

                if( UIUC::HandboxMsg::Error(check_ambiguity > 0, __METHOD_NAME__, "Ambiguous option \""+opt+"\" detected.. ("+TString::Itoa(check_ambiguity,10)+")") )
                        return 0;
                else if( UIUC::HandboxMsg::Error(check_ambiguity == -1, __METHOD_NAME__, "Unknown option \""+opt+"\" detected.. ("+TString::Itoa(check_ambiguity,10)+")") )
                        return 0;
        }

        return ComputeMeanAndSV(stdv, c, c_err, w, bShowValues);
}

double UIUC::TFactory::GetMeanContent(TH1 *h0, Option_t* option)
{
        if(h0 == NULL) return NAN;

        TString opt = option;
        opt.ToLower();

        //loop on all bins and refill by hand because of some issues with over/underflow
        int nbinsx = h0->GetNbinsX();
        int nbinsy = h0->GetNbinsY();
        int nbinsz = h0->GetNbinsZ();

        vector<double> c, w, w_err;
        for (int binz=1; binz <= nbinsz; binz++) {

                for (int biny=1; biny <= nbinsy; biny++) {

                        for (int binx=1; binx <= nbinsx; binx++) {

                                int bin = h0->GetBin(binx,biny,binz);

                                c.push_back(h0->GetBinContent(bin));
                                w.push_back(h0->GetBinContent(bin));
                                w_err.push_back(h0->GetBinError(bin));
                        }
                }
        }

        vector<double> stdv(4,NAN);
        ComputeMeanAndSV(stdv, opt, c, {}, w, w_err);

        return stdv[0];
}

double UIUC::TFactory::GetStdDevContent(TH1 *h0, Option_t* option)
{
        if(h0 == NULL) return NAN;

        TString opt = option;
        opt.ToLower();

        //loop on all bins and refill by hand because of some issues with over/underflow
        int nbinsx = h0->GetNbinsX();
        int nbinsy = h0->GetNbinsY();
        int nbinsz = h0->GetNbinsZ();

        vector<double> c, w, w_err;
        for (int binz=1; binz <= nbinsz; binz++) {

                for (int biny=1; biny <= nbinsy; biny++) {

                        for (int binx=1; binx <= nbinsx; binx++) {

                                int bin = h0->GetBin(binx,biny,binz);

                                c.push_back(h0->GetBinContent(bin));
                                w.push_back(h0->GetBinContent(bin));
                                w_err.push_back(h0->GetBinError(bin));
                        }
                }
        }

        vector<double> stdv(4,NAN);
        ComputeMeanAndSV(stdv, option, c, {}, w, w_err);

        return stdv[2];
}

double UIUC::TFactory::GetMean(TH1 *h0, Option_t* option)
{
        if(h0 == NULL) return NAN;

        TString opt = option;
        opt.ToLower();

        //loop on all bins and refill by hand because of some issues with over/underflow
        int nbinsx = h0->GetNbinsX();
        int nbinsy = h0->GetNbinsY();
        int nbinsz = h0->GetNbinsZ();

        vector<double> c, w, w_err;
        for (int binz=1; binz <= nbinsz; binz++) {

                for (int biny=1; biny <= nbinsy; biny++) {

                        for (int binx=1; binx <= nbinsx; binx++) {

                                int bin = h0->GetBin(binx,biny,binz);

                                c.push_back(h0->GetBinCenter(bin));
                                w.push_back(h0->GetBinContent(bin));
                                w_err.push_back(h0->GetBinError(bin));
                        }
                }
        }

        vector<double> stdv(4,NAN);
        ComputeMeanAndSV(stdv, option, c, {}, w, w_err);

        return stdv[0];
}

double UIUC::TFactory::GetStdDev(TH1 *h0, Option_t* option)
{
        if(h0 == NULL) return NAN;

        TString opt = option;
        opt.ToLower();

        //loop on all bins and refill by hand because of some issues with over/underflow
        int nbinsx = h0->GetNbinsX();
        int nbinsy = h0->GetNbinsY();
        int nbinsz = h0->GetNbinsZ();

        vector<double> c, w, w_err;
        for (int binz=1; binz <= nbinsz; binz++) {

                for (int biny=1; biny <= nbinsy; biny++) {

                        for (int binx=1; binx <= nbinsx; binx++) {

                                int bin = h0->GetBin(binx,biny,binz);

                                c.push_back(h0->GetBinCenter(bin));
                                w.push_back(h0->GetBinContent(bin));
                                w_err.push_back(h0->GetBinError(bin));
                        }
                }
        }

        vector<double> stdv(4,NAN);
        ComputeMeanAndSV(stdv, option, c, {}, w, w_err);

        return stdv[2];
}

double UIUC::TFactory::GetMeanContent(TGraph *g0, Option_t* option)
{
        if(g0 == NULL) return NAN;

        TString opt = option;
        opt.ToLower();

        vector<double> c, w, werr;
        for (int i=1, N = g0->GetN(); i <= N; i++) {

                c.push_back(g0->GetY()[i]);
                w.push_back(g0->GetY()[i]);
                if(g0->GetEY() != NULL) werr.push_back(g0->GetEY()[i]);
                else if(g0->GetEYlow() != NULL && g0->GetEYhigh() != NULL) werr.push_back(g0->GetEYlow()[i]+g0->GetEYhigh()[i]);
        }

        vector<double> stdv(4,NAN);
        ComputeMeanAndSV(stdv, option, c, {}, w, werr);

        return stdv[0];
}


double UIUC::TFactory::GetStdDevContent(TGraph *g0, Option_t* option)
{
        if(g0 == NULL) return NAN;

        TString opt = option;
        opt.ToLower();

        vector<double> c, w, werr;
        for (int i=1, N = g0->GetN(); i <= N; i++) {

                c.push_back(g0->GetY()[i]);
                w.push_back(g0->GetY()[i]);
                if(g0->GetEY() != NULL) werr.push_back(g0->GetEY()[i]);
                else if(g0->GetEYlow() != NULL && g0->GetEYhigh() != NULL) werr.push_back(g0->GetEYlow()[i]+g0->GetEYhigh()[i]);
        }

        vector<double> stdv(4,NAN);
        ComputeMeanAndSV(stdv, option, c, {}, w, werr);

        return stdv[2];
}

double UIUC::TFactory::GetMean(TGraph *g0, Option_t* option)
{
        TString opt = option;
        opt.ToLower();

        vector<double> c, w, werr;
        for (int i=1, N = g0->GetN(); i <= N; i++) {

                c.push_back(g0->GetX()[i]);
                w.push_back(g0->GetY()[i]);
                if(g0->GetEY() != NULL) werr.push_back(g0->GetEY()[i]);
                else if(g0->GetEYlow() != NULL && g0->GetEYhigh() != NULL) werr.push_back(g0->GetEYlow()[i]+g0->GetEYhigh()[i]);
        }

        vector<double> stdv(4,NAN);
        ComputeMeanAndSV(stdv, option, c, {}, w, werr);

        return stdv[0];
}

double UIUC::TFactory::GetStdDev(TGraph *g0, Option_t* option)
{
        TString opt = option;
        opt.ToLower();

        vector<double> c, w, werr;
        for (int i=1, N = g0->GetN(); i <= N; i++) {

                c.push_back(g0->GetX()[i]);
                w.push_back(g0->GetY()[i]);
                if(g0->GetEY() != NULL) werr.push_back(g0->GetEY()[i]);
                else if(g0->GetEYlow() != NULL && g0->GetEYhigh() != NULL) werr.push_back(g0->GetEYlow()[i]+g0->GetEYhigh()[i]);
        }

        vector<double> stdv(4,NAN);
        ComputeMeanAndSV(stdv, option, c, {}, w, werr);

        return stdv[2];
}

void UIUC::TFactory::PrintWithAxis(TH1 *h0)
{
        if(h0 == NULL) {

                cerr << "TH1 Ptr: " << h0 << "; TH1.Name: NULL; TH1.Title: NULL; TH1.Entries = 0" << endl;
                return;
        }

        cout << "TH1 Ptr: " << h0 << "; TH1.Name: " << h0->GetName() << "; TH1.Title: " << h0->GetTitle() << endl;
        cout << "TH1.Entries = " << h0->GetEntries() << "; TH1.Mean: " << h0->GetMean() << "; TH1.StdDev: " << h0->GetStdDev() << endl;

        TString hOptions;
        hOptions.Form("TH1.Print X-axis = (%f,%f); nbins = %d; binwidth = %f",
                      h0->GetXaxis()->GetBinLowEdge(1),
                      h0->GetXaxis()->GetBinUpEdge(h0->GetNbinsX()),
                      h0->GetNbinsX(),
                      h0->GetXaxis()->GetBinWidth(1));

        cout << hOptions << endl;

        if(h0->InheritsFrom("TH2")) {

                TString hOptions;

                hOptions.Form("          Y-axis = (%f,%f); nbins = %d; binwidth = %f",
                              h0->GetYaxis()->GetBinLowEdge(1),
                              h0->GetYaxis()->GetBinUpEdge(h0->GetNbinsY()),
                              h0->GetNbinsY(),
                              h0->GetYaxis()->GetBinWidth(1));

                cout << hOptions << endl;
        }

        if(h0->InheritsFrom("TH3")) {

                TString hOptions;

                hOptions.Form("          Z-axis = (%f,%f); nbins = %d; binwidth = %f",
                              h0->GetZaxis()->GetBinLowEdge(1),
                              h0->GetZaxis()->GetBinUpEdge(h0->GetNbinsZ()),
                              h0->GetNbinsZ(),
                              h0->GetZaxis()->GetBinWidth(1));

                cout << hOptions << endl;
        }
}

//
// If same binning; but different ranges; method is used when you want to rebin while copy/pasting as example
TH1* UIUC::TFactory::CopyPaste(TH1 *hCopy, TH1* hPaste, Option_t* option)
{
        if(hCopy == NULL) return NULL;

        TString opt = option;
                opt.ToLower();

        int ibin,bin;
        int ix,iy,iz;
        int binx,biny,binz;
        double bx,by,bz;

        if(!opt.Contains("average") && !opt.Contains("width") && !opt.EqualTo("")) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unknown option.. option=\"%s\"", opt.Data());
        }

        if(hPaste == NULL) hPaste = CopyStruct(hCopy);
        else UIUC::TFactory::Empty(hPaste);

        //loop on all bins and refill by hand because of some issues with over/underflow
        int nbinsx = hCopy->GetNbinsX();
        int nbinsy = hCopy->GetNbinsY();
        int nbinsz = hCopy->GetNbinsZ();
        int nbinsx0 = hPaste->GetNbinsX();
        int nbinsy0 = hPaste->GetNbinsY();
        int nbinsz0 = hPaste->GetNbinsZ();

        vector< vector<double> > weight((nbinsx0+2)*(nbinsy0+2)*(nbinsz0+2), vector<double>{});
        vector< vector<double> > content((nbinsx0+2)*(nbinsy0+2)*(nbinsz0+2), vector<double>{});
        vector< vector<double> > content_error((nbinsx0+2)*(nbinsy0+2)*(nbinsz0+2), vector<double>{});
        
        for (binz=1; binz <= nbinsz; binz++) {

                bz  = hCopy->GetZaxis()->GetBinCenter(binz);
                iz  = hPaste->GetZaxis()->FindBin(bz);

                for (biny=1; biny <= nbinsy; biny++) {

                        by  = hCopy->GetYaxis()->GetBinCenter(biny);
                        iy  = hPaste->GetYaxis()->FindBin(by);

                        for (binx=1; binx <= nbinsx; binx++) {

                                bx = hCopy->GetXaxis()->GetBinCenter(binx);
                                ix  = hPaste->GetXaxis()->FindBin(bx);

                                bin = hCopy->GetBin(binx,biny,binz);
                                ibin = hPaste->GetBin(ix,iy,iz);

                                double pixel = hCopy->GetXaxis()->GetBinWidth(binx) * hCopy->GetYaxis()->GetBinWidth(biny) * hCopy->GetZaxis()->GetBinWidth(binz);
                                if(opt.Contains("average")) {

                                        weight[ibin].push_back(pixel);
                                        content[ibin].push_back(hCopy->GetBinContent(bin));
                                        content_error[ibin].push_back(hCopy->GetBinError(bin));

                                } else {

                                        if(!weight[ibin].size()) weight[ibin].push_back(0);
                                        if(!content[ibin].size()) content[ibin].push_back(0);
                                        if(!content_error[ibin].size()) content_error[ibin].push_back(0);

                                        weight[ibin][0] = 1;
                                        if(!opt.Contains("width")) pixel = 1;

                                        content[ibin][0] += pixel * hCopy->GetBinContent(bin);
                                        content_error[ibin][0] = pixel * TMath::Sqrt(content_error[ibin][0]*content_error[ibin][0] + hCopy->GetBinError(bin)*hCopy->GetBinError(bin));
                                }
                        }
                }
        }

        for (binz=1; binz <= nbinsz; binz++) {

                bz  = hCopy->GetZaxis()->GetBinCenter(binz);
                iz  = hPaste->GetZaxis()->FindBin(bz);

                for (biny=1; biny <= nbinsy; biny++) {

                        by  = hCopy->GetYaxis()->GetBinCenter(biny);
                        iy  = hPaste->GetYaxis()->FindBin(by);

                        for (binx=1; binx <= nbinsx; binx++) {

                                bx = hCopy->GetXaxis()->GetBinCenter(binx);
                                ix  = hPaste->GetXaxis()->FindBin(bx);
                                ibin = hPaste->GetBin(ix,iy,iz);

                                vector<double> stdv;
                                if(!ComputeMeanAndSV(stdv, "", content[ibin], content_error[ibin], weight[ibin])) return NULL;

                                double pixel = 1;
                                if(opt.Contains("width")) pixel = hPaste->GetXaxis()->GetBinWidth(ix)*hPaste->GetYaxis()->GetBinWidth(iy)*hPaste->GetZaxis()->GetBinWidth(iz);
                                hPaste->SetBinContent(ibin, stdv[0]/pixel);
                                hPaste->SetBinError(ibin, stdv[1]/pixel);
                        }
                }
        }

        hPaste->SetEntries(hCopy->GetEntries());
        return hPaste;
}

TH1* UIUC::TFactory::Flip(TH1* h, Option_t* option)
{
        if( h == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        TString hname = (TString) h->GetName() + ":" + opt + "-flip";
        TH1D *h0 = (TH1D*) UIUC::TFactory::CopyStruct(h, hname, h->GetTitle());

        TAxis *x1, *x2, *x3;
        if(opt == "x") {

                x3 = h->GetXaxis();
                x1 = h->GetYaxis();
                x2 = h->GetZaxis();

        } else if(opt == "y") {

                x3 = h->GetYaxis();
                x1 = h->GetXaxis();
                x2 = h->GetZaxis();

        } else if(opt == "z") {

                x3 = h->GetZaxis();
                x1 = h->GetXaxis();
                x2 = h->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\", \"z\")");
                return NULL;
        }

        int N = h->GetEntries();
        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        double c1 = 0, c2 = 0;
                        double c1_err = 0, c2_err = 0;

                        for(int x3_i = 1, x3_N = x3->GetNbins(); x3_i <= x3_N; x3_i++) {

                                if(opt == "x") {

                                        c1     = h->GetBinContent(x3_i, x1_i, x2_i);
                                        c1_err = h->GetBinError(x3_i, x1_i, x2_i);
                                        c2     = h->GetBinContent(x3->GetNbins()+1-x3_i, x1_i, x2_i);
                                        c2_err = h->GetBinError(x3->GetNbins()+1-x3_i, x1_i, x2_i);

                                        h0->SetBinContent(x3_i, x1_i, x2_i, c2);
                                        h0->SetBinError(x3_i, x1_i, x2_i, c2_err);
                                        h0->SetBinContent(x3->GetNbins()+1-x3_i, x1_i, x2_i, c1);
                                        h0->SetBinError(x3->GetNbins()+1-x3_i, x1_i, x2_i, c1_err);
                                        
                                } else if (opt == "y") {

                                        c1     = h->GetBinContent(x1_i, x3_i, x2_i);
                                        c1_err = h->GetBinError(x1_i, x3_i, x2_i);
                                        c2     = h->GetBinContent(x1_i, x3->GetNbins()+1-x3_i, x2_i);
                                        c2_err = h->GetBinError(x1_i, x3->GetNbins()+1-x3_i, x2_i);

                                        h0->SetBinContent(x1_i, x3_i, x2_i, c2);
                                        h0->SetBinError(x1_i, x3_i, x2_i, c2_err);
                                        h0->SetBinContent(x1_i, x3->GetNbins()+1-x3_i, x2_i, c1);
                                        h0->SetBinError(x1_i, x3->GetNbins()+1-x3_i, x2_i, c1_err);
                                        
                                } else if (opt == "z") {

                                        c1     = h->GetBinContent(x1_i, x2_i, x3_i);
                                        c1_err = h->GetBinError(x1_i, x2_i, x3_i);
                                        c2     = h->GetBinContent(x1_i, x2_i, x3->GetNbins()+1-x3_i);
                                        c2_err = h->GetBinError(x1_i, x2_i, x3->GetNbins()+1-x3_i);

                                        h0->SetBinContent(x1_i, x2_i, x3_i, c2);
                                        h0->SetBinError(x1_i, x2_i, x3_i, c2_err);
                                        h0->SetBinContent(x1_i, x2_i, x3->GetNbins()+1-x3_i, c1);
                                        h0->SetBinError(x1_i, x2_i, x3->GetNbins()+1-x3_i, c1_err);
                                }
                        }
                }
        }

        h0->SetEntries(N);
        
        return h0;
}

TH1*  UIUC::TFactory::NormalizeRange(TH1* h, Option_t* option)
{
        if( h == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        if(h->InheritsFrom("TH3"))
                return NormalizeRange((TH3D*) h, option);
        else if(h->InheritsFrom("TH2"))
                return NormalizeRange((TH2D*) h, option);

        return NormalizeRange((TH1D*) h);
}

TH1D* UIUC::TFactory::NormalizeRange(TH1D* h0)
{
        if( h0 == NULL) return NULL;

        TAxis *axis = h0->GetXaxis();

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TString hname = (TString) h0->GetName() + ":x-norm";

        TH1D *h = new TH1D(hname, h0->GetTitle() + TString(" [X0"+TString::Itoa(axis->GetXmin(),10)+";X1="+TString::Itoa(axis->GetXmax(),10)+"]"), h0->GetNbinsX(), 1, h0->GetNbinsX());
              h->GetXaxis()->SetTitle("x");

        return (TH1D*) UIUC::TFactory::CopyPaste((TH1*) h0, (TH1*) h);
}

TH2D* UIUC::TFactory::NormalizeRange(TH2D* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        TAxis *axis = NULL;
        if(opt == "x") axis = h0->GetXaxis();
        else if(opt == "y") axis = h0->GetYaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TString hname = (TString) h0->GetName() + ":" + opt + "-norm";

        TH2D *h = NULL;
        if(opt == "x") {

                h = new TH2D(hname, h0->GetTitle() + TString(" [X0"+TString::Itoa(axis->GetXmin(),10)+";X1="+TString::Itoa(axis->GetXmax(),10)+"]"),
                                    h0->GetNbinsX(), 1, h0->GetNbinsX(),
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax() );

        } else if(opt == "y") {

                h = new TH2D(hname, h0->GetTitle() + TString(" [Y0"+TString::Itoa(axis->GetXmin(),10)+";Y1="+TString::Itoa(axis->GetXmax(),10)+"]"),
                                    h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), 1, h0->GetNbinsY());
        }

        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());

        return (TH2D*) UIUC::TFactory::CopyPaste((TH1*) h0, (TH1*) h);
}

TH3D* UIUC::TFactory::NormalizeRange(TH3D* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        TAxis *axis = NULL;
        if(opt == "x") axis = h0->GetXaxis();
        else if(opt == "y") axis = h0->GetYaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                return NULL;
        }

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TString hname = (TString) h0->GetName() + ":" + opt + "-norm";

        TH3D *h = NULL;
        if(opt == "x")
                h = new TH3D(hname, h0->GetTitle() + TString(" [X0"+TString::Itoa(axis->GetXmin(),10)+";X1="+TString::Itoa(axis->GetXmax(),10))+"]",
                                    h0->GetNbinsX(), 1, h0->GetNbinsX(),
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                                    h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax());
        else if(opt == "y")
                h = new TH3D(hname, h0->GetTitle() + TString(" [Y0"+TString::Itoa(axis->GetXmin(),10)+";Y1="+TString::Itoa(axis->GetXmax(),10))+"]",
                                    h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), 1, h0->GetNbinsY(),
                                    h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax() );
        else if(opt == "z")
                h = new TH3D(hname, h0->GetTitle() + TString(" [Z0"+TString::Itoa(axis->GetXmin(),10)+";Z1="+TString::Itoa(axis->GetXmax(),10))+"]",
                                    h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                                    h0->GetNbinsZ(), 1, h0->GetNbinsZ());

        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());

        return (TH3D*) UIUC::TFactory::CopyPaste((TH1*) h0, (TH1*) h);
}

TH1*  UIUC::TFactory::DenormalizeRange(TH1* h, Option_t *option, double binStart, double binStop)
{
        if( h == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        if(h->InheritsFrom("TH3"))
                return DenormalizeRange((TH3D*) h, option, binStart, binStop);
        else if(h->InheritsFrom("TH2"))
                return DenormalizeRange((TH2D*) h, option, binStart, binStop);

        return DenormalizeRange((TH1D*) h, binStart, binStop);
}

TH1D* UIUC::TFactory::DenormalizeRange(TH1D* h0, double binStart, double binStop)
{
        if( h0 == NULL) return NULL;

        TString hname = (TString) h0->GetName() + ":x-denorm";
        TH1D *h = new TH1D(hname, h0->GetTitle(), h0->GetNbinsX(), binStart, binStop);

        return (TH1D*) UIUC::TFactory::SetVectorContent(h, UIUC::TFactory::GetVectorContent(h0));
}

TH2D* UIUC::TFactory::DenormalizeRange(TH2D* h0, Option_t *option, double binStart, double binStop)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        TAxis *axis = NULL;
        if(opt == "x") axis = h0->GetXaxis();
        else if(opt == "y") axis = h0->GetYaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        TString hname = (TString) h0->GetName() + ":" + opt + "-norm";

        TH2D *h = NULL;
        if(opt == "x")
                h = new TH2D(hname, h0->GetTitle() + TString(" [X0"+TString::Itoa(axis->GetXmin(),10)+";X1="+TString::Itoa(axis->GetXmax(),10)+"]"),
                                    h0->GetNbinsX(), binStart, binStop,
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax() );

        else if(opt == "y")
                h = new TH2D(hname, h0->GetTitle() + TString(" [Y0"+TString::Itoa(axis->GetXmin(),10)+";Y1="+TString::Itoa(axis->GetXmax(),10)+"]"),
                                    h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), binStart, binStop);

        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());

        return (TH2D*) UIUC::TFactory::SetVectorContent(h, UIUC::TFactory::GetVectorContent(h0));
}

TH3D* UIUC::TFactory::DenormalizeRange(TH3D* h0, Option_t *option, double binStart, double binStop)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        TAxis *axis = NULL;
        if(opt == "x") axis = h0->GetXaxis();
        else if(opt == "y") axis = h0->GetYaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                return NULL;
        }

        TString hname = (TString) h0->GetName() + ":" + opt + "-norm";

        TH3D *h = NULL;
        if(opt == "x")
                h = new TH3D(hname, h0->GetTitle() + TString(" [X0"+TString::Itoa(axis->GetXmin(),10)+";X1="+TString::Itoa(axis->GetXmax(),10))+"]",
                                    h0->GetNbinsX(), binStart, binStop,
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                                    h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax());
        else if(opt == "y")
                h = new TH3D(hname, h0->GetTitle() + TString(" [Y0"+TString::Itoa(axis->GetXmin(),10)+";Y1="+TString::Itoa(axis->GetXmax(),10))+"]",
                                    h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), binStart, binStop,
                                    h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax() );
        else if(opt == "z")
                h = new TH3D(hname, h0->GetTitle() + TString(" [Z0"+TString::Itoa(axis->GetXmin(),10)+";Z1="+TString::Itoa(axis->GetXmax(),10))+"]",
                                    h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                                    h0->GetNbinsZ(), binStart, binStop);

        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());

        return (TH3D*) UIUC::TFactory::SetVectorContent(h, UIUC::TFactory::GetVectorContent(h0));
}

std::vector<double> UIUC::TFactory::RandomVector(int N, TString pdfFormula, std::vector<double> pdfParams, double xMin, double xMax)
{
        TF1 *pdf = new TF1("random_vector["+UIUC::GetRandomStr()+"]", pdfFormula, xMin, xMax);
             pdf->SetParameters(&pdfParams[0]);

        std::vector<double> px = RandomVector(N, pdf);

        pdf->Delete();
        return px;
}

std::vector<double> UIUC::TFactory::InverseTransformSampling(int N, TF1 *pdf)
{
        std::vector<double> px(N, 0);

        UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);
        for(int i = 0; i < N; i++) {
                px[i] = pdf->GetRandom();
        }

        UIUC::HandboxMsg::DumpTTY(__METHOD_NAME__);
        return px;
}

std::vector<double> UIUC::TFactory::ContentVector(TH1 *h, int axis)
{
        if(h == NULL) return {};

        std::vector<double> v(h->GetNbinsX());
        for(int i = 1, N = v.size()+1; i < N; i++) 
        {
                switch(axis) {
                        case 0: 
                                v[i-1] = h->GetXaxis()->GetBinLowEdge(i);
                                break;
                        case 1:
                                v[i-1] = h->GetYaxis()->GetBinLowEdge(i);
                                break;
                        case 2: 
                                v[i-1] = h->GetZaxis()->GetBinLowEdge(i);
                                break;
                        default:
                                v[i-1] = h->GetBinContent(i);
                }
        }

        return v;
}
std::vector<double> UIUC::TFactory::ContentVector(TH1D* h, int axis) { return ContentVector(h, axis); }
std::vector<float>  ContentVector(TH1F* h, int axis)
{
        if(h == NULL) return {};

        std::vector<float> v(h->GetNbinsX());
        for(int i = 1, N = v.size()+1; i < N; i++) 
        {
                switch(axis) {
                        case 0: 
                                v[i-1] = h->GetXaxis()->GetBinLowEdge(i);
                                break;
                        case 1:
                                v[i-1] = h->GetYaxis()->GetBinLowEdge(i);
                                break;
                        case 2: 
                                v[i-1] = h->GetZaxis()->GetBinLowEdge(i);
                                break;
                        default:
                                v[i-1] = h->GetBinContent(i);
                }
        }

        return v;
}

std::vector<double> UIUC::TFactory::ErrorVector(TH1 *h)
{
        if(h == NULL) return {};

        std::vector<double> vErr(h->GetNbinsX());
        for(int i = 1, N = vErr.size()+1; i < N; i++)
                vErr[i-1] = h->GetBinError(i);

        return vErr;
}
std::vector<double> UIUC::TFactory::ErrorVector(TH1D* h) { return ErrorVector(h); }
std::vector<float>  ErrorVector(TH1F* h)
{
        if(h == NULL) return {};

        std::vector<float> vErr(h->GetNbinsX());
        for(int i = 1, N = vErr.size()+1; i < N; i++)
                vErr[i-1] = h->GetBinError(i);

        return vErr;
}

double UIUC::TFactory::Length(TH1* h, Option_t *option)
{
        if(h == NULL) return 0;

        TString opt = option;
        opt.ToLower();

        TAxis *axis = nullptr;
        if (opt.Contains("x") && h->InheritsFrom("TH1")) axis = h->GetXaxis();
        else if (opt.Contains("y") && h->InheritsFrom("TH2")) axis = h->GetYaxis();
        else if (opt.Contains("z") && h->InheritsFrom("TH3")) axis = h->GetZaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\")");
                return 0;
        }

        return axis->GetXmax() - axis->GetXmin();
}

TH1* UIUC::TFactory::PowerX(TH1 *_h0, double X)
{
        if(_h0 == NULL) return NULL;
        
        TH1 *h0 = (TH1*) _h0->Clone((TString) + _h0->GetName()+":x^("+TString::Itoa(X,10)+")");
        for(int i = 0, N = h0->GetNcells(); i < N; i++) {

                if(h0->IsBinOverflow(i)) continue;
                if(h0->IsBinUnderflow(i)) continue;

                double content = TMath::Power(h0->GetBinContent(i), X);
                double  sigma  = X * TMath::Power(h0->GetBinContent(i), X-1) * h0->GetBinError(i);

                h0->SetBinContent(i, IsInf(content) || IsNaN(content) || IsInf(sigma) || IsNaN(sigma) ? 0 : content);
                h0->SetBinError  (i, IsInf(content) || IsNaN(content) || IsInf(sigma) || IsNaN(sigma) ? 0 : sigma);
        }

        return h0;
}

TH1* UIUC::TFactory::Abs(TH1 *_h0)
{
        if(_h0 == NULL) return NULL;
        
        TH1 *h0 = (TH1*) _h0->Clone((TString) + _h0->GetName()+":abs");
        for(int i = 0, N = h0->GetNcells(); i < N; i++) {

                if(h0->IsBinOverflow(i)) continue;
                if(h0->IsBinUnderflow(i)) continue;

                double content = abs(h0->GetBinContent(i));

                h0->SetBinContent(i, content);
        }

        return h0;
}

TH1* UIUC::TFactory::SetRange(TH1* h, Option_t *option, double min, double max)
{
        if(h == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        if(h->InheritsFrom("TH3")) return SetRange((TH3D*) h, option, min, max);
        else if(h->InheritsFrom("TH2")) {

                if(opt.Contains("z")) h->GetZaxis()->SetRangeUser(min, max);
                else return SetRange((TH2D*) h, option, min, max);

        } else {

                if(opt.Contains("y")) h->GetYaxis()->SetRangeUser(min, max);
                else if (opt.Contains("x")) return SetRange((TH1D*) h, min, max);
                else {

                        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\", \"y\")");
                        return NULL;
                }
        }

        TAxis *axis = NULL;
        if(opt == "x") axis = h->GetXaxis();
        else if(opt == "y") axis = h->GetYaxis();
        else if(opt == "z") axis = h->GetZaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\", \"z\")");
                return NULL;
        }

        TString hname = (TString) h->GetName() + ":"+opt+"["+Form("%d_%d]", axis->FindBin(min), axis->FindBin(max));

        return (TH1*) h->Clone(hname);
}

std::vector<double> UIUC::TFactory::Range(int nBins, double xmin, double xmax)
{
        std::vector<double> x;
        
        double dx = (xmax-xmin)/nBins;
        for(int i = 0; i <= nBins; i++)
                x.push_back(xmin + i*dx);

        return x;
}

std::vector<double> UIUC::TFactory::Range(TAxis *axis)
{
        const Double_t *v = axis->GetXbins()->GetArray();
        if(v == NULL) return Range(axis->GetNbins(), axis->GetXmin(), axis->GetXmax());

        std::vector<double> x(axis->GetNbins()+1, 0);
        for(int i = 0, N = axis->GetNbins(); i <= N; i++)
                x[i] = v[i];

        return x;
}

std::vector<double> UIUC::TFactory::Range(TH1D *h)
{
        std::vector<double> x;
        for(int i = 0, N = h->GetNbinsX(); i <= N; i++)
                x.push_back(h->GetBinContent(i+1));

        return x;
}

TH1D* UIUC::TFactory::Pad(TH1D *h0, double xmin, double xmax)
{
        if(h0 == NULL) return NULL;
        if(xmin >= xmax) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong range [%d, %f; %f].. cannot set range for \"%s\"", xmin, xmax, h0->GetName());
                return NULL;
        }

        TAxis *axis = h0->GetXaxis();
        std::vector<double> x = UIUC::TFactory::Range(axis);
        UIUC::TFactory::ExtendBinRangeByStep(x, xmin, xmax, axis->GetBinWidth(1));
        if(x.size() == 0 ) return NULL;
        
        TString hname = (TString) h0->GetName() + ":x-pad";
        TH1D* h = new TH1D(hname, h0->GetTitle(), x.size()-1, &(x[0]));
        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());

        UIUC::TFactory::CopyPaste((TH1*) h0, (TH1*) h);

        return h;
}

TH1D* UIUC::TFactory::SetRange(TH1D *h0, double xmin0, double xmax0)
{
        if(h0 == NULL) return NULL;
        if(xmin0 >= xmax0) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong range [%d, %f; %f].. cannot set range for \"%s\"", xmin0, xmax0, h0->GetName());
                return NULL;
        }

        TAxis *axis = h0->GetXaxis();
        double xbinwidth0 = axis->GetBinWidth(1);

        double xmin = axis->GetBinLowEdge(axis->FindBin(xmin0));
        double xmax = axis->GetBinLowEdge(axis->FindBin(xmax0));
        
        int xbins   = (xmax-xmin)/xbinwidth0;
        if(UIUC::EpsilonEqualTo(xmin, xmax)) { // Precision reduced due to division

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Outbound range received [%.2f, %.2f].\nRange can only be set between [%.2f, %.2f] for \"%s\"", xmin0, xmax0, axis->GetXmin(), axis->GetXmax(), h0->GetTitle());
                return NULL;
        }

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TString hname = (TString) h0->GetName() + ":x["+Form("%d_%d]", axis->FindBin(xmin0), axis->FindBin(xmax0));
        
        TH1D* h = new TH1D(hname, h0->GetTitle(), xbins, xmin, xmax);
              h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
              h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
              h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());
              
        if(!UIUC::EpsilonEqualTo(xbinwidth0, h->GetXaxis()->GetBinWidth(1))) { // Precision reduced due to division

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Unexpected bin width received (=%.32f) from [%d, %f; %f]..\n                              Bin width %.32f expected from [%d, %f; %f].\n                              Range cannot be set for \"%s\"", h->GetXaxis()->GetBinWidth(1), xbins, xmin0, xmax0, xbinwidth0, axis->GetNbins(), axis->GetXmin(), axis->GetXmax(), h0->GetName());
                return NULL;
        }

        UIUC::TFactory::CopyPaste((TH1*) h0, (TH1*) h);
        return h;
}

TH2D* UIUC::TFactory::SetRange(TH2D *h0, Option_t *option, double xmin0, double xmax0)
{
        TString opt = option;
        opt.ToLower();

        if(h0 == NULL) return NULL;
        if(xmin0 >= xmax0) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Empty range.. cannot set range for \""+h0->GetName()+"\"");
                return NULL;
        }

        TAxis *axis = NULL;
        if(opt == "x") axis = h0->GetXaxis();
        else if(opt == "y") axis = h0->GetYaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        double xbinwidth0 = axis->GetBinWidth(1);
        double xmin = axis->GetBinLowEdge(axis->FindBin(xmin0));
        double xmax = axis->GetBinLowEdge (axis->FindBin(xmax0));

        int xbins = (xmax-xmin)/xbinwidth0;

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TString hname = (TString) h0->GetName() +  ":"+opt+"["+Form("%d_%d]", axis->FindBin(xmin0), axis->FindBin(xmax0));

        TH2D *h = NULL;
        if(opt == "x") {

                h = new TH2D(hname, h0->GetTitle(), xbins, xmin, xmax, h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax() );
                if(!UIUC::EpsilonEqualTo(xbinwidth0, h->GetXaxis()->GetBinWidth(1))) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Unexpected bin width received (=%.32f) [%d, %f; %f]..\nbut %.32f was expected [%d, %f; %f]. Range cannot set range for \"%s\"", h->GetXaxis()->GetBinWidth(1), xbins, xmin0, xmax0, xbinwidth0, axis->GetNbins(), axis->GetXmin(), axis->GetXmax(), h0->GetName());
                        return NULL;
                }

        } else if(opt == "y") {

                h = new TH2D(hname, h0->GetTitle(), h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(), xbins, xmin, xmax );
                if(!UIUC::EpsilonEqualTo(xbinwidth0, h->GetYaxis()->GetBinWidth(1))) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Unexpected bin width received (=%.32f) [%d, %f; %f]..\nbut %.32f was expected [%d, %f; %f]. Range cannot set range for \"%s\"", h->GetYaxis()->GetBinWidth(1), axis->GetNbins(), axis->GetXmin(), axis->GetXmax(), xbinwidth0, xbins, xmin0, xmax0, h0->GetName());
                        return NULL;
                }
        }
        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());


        if(!UIUC::EpsilonEqualTo(xbinwidth0, h->GetXaxis()->GetBinWidth(1))) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Unexpected bin width received (=%.32f) [%d, %f; %f]..\nbut %.32f was expected [%d, %f; %f]. Range cannot set range for \"%s\"", h->GetXaxis()->GetBinWidth(1), xbins, xmin0, xmax0, xbinwidth0, axis->GetNbins(), axis->GetXmin(), axis->GetXmax(), h0->GetName());
                return NULL;
        }

        UIUC::TFactory::CopyPaste((TH1*) h0, (TH1*) h);
        return h;
}

TH3D* UIUC::TFactory::SetRange(TH3D *h0, Option_t *option, double xmin0, double xmax0)
{
        TString opt = option;
        opt.ToLower();

        if(h0 == NULL) return NULL;
        if(xmin0 >= xmax0) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Empty range.. cannot set range for \""+h0->GetName()+"\"");
                return NULL;
        }

        TAxis *axis = NULL;
        if(opt == "x") axis = h0->GetXaxis();
        else if(opt == "y") axis = h0->GetYaxis();
        else if(opt == "z") axis = h0->GetZaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\", \"z\")");
                return NULL;
        }

        double xbinwidth0 = axis->GetBinWidth(1);
        double xmin = axis->GetBinLowEdge(axis->FindBin(xmin0));
        double xmax = axis->GetBinLowEdge (axis->FindBin(xmax0));

        int xbins = (xmax-xmin)/xbinwidth0;

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TString hname = (TString) h0->GetName() + ":subrange-"+Form("%d-%d", axis->FindBin(xmin0), axis->FindBin(xmax0));

        TH3D *h = NULL;
        if(opt == "x") {
                h = new TH3D(hname, h0->GetTitle(), xbins, xmin, xmax, h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax());
                if(!UIUC::EpsilonEqualTo(xbinwidth0, h->GetXaxis()->GetBinWidth(1))) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Unexpected bin width received (=%.32f) [%d, %f; %f]..\nbut %.32f was expected [%d, %f; %f]. Range cannot set range for \"%s\"", h->GetXaxis()->GetBinWidth(1), xbins, xmin0, xmax0, xbinwidth0, axis->GetNbins(), axis->GetXmin(), axis->GetXmax(), h0->GetName());
                        return NULL;
                }

        } else if(opt == "y") {
                h = new TH3D(hname, h0->GetTitle(), h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(), xbins, xmin, xmax, h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax() );
                if(!UIUC::EpsilonEqualTo(xbinwidth0, h->GetYaxis()->GetBinWidth(1))) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Unexpected bin width received (=%.32f) [%d, %f; %f]..\nbut %.32f was expected [%d, %f; %f]. Range cannot set range for \"%s\"", h->GetYaxis()->GetBinWidth(1), axis->GetNbins(), axis->GetXmin(), axis->GetXmax(), xbinwidth0, xbins, xmin0, xmax0, h0->GetName());
                        return NULL;
                }

        } else if(opt == "z") {

                h = new TH3D(hname, h0->GetTitle(), h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(), h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(), xbins, xmin, xmax );
                if(!UIUC::EpsilonEqualTo(xbinwidth0, h->GetZaxis()->GetBinWidth(1))) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Unexpected bin width received (=%.32f) [%d, %f; %f]..\nbut %.32f was expected [%d, %f; %f]. Range cannot set range for \"%s\"", h->GetZaxis()->GetBinWidth(1), axis->GetNbins(), axis->GetXmin(), axis->GetXmax(), xbinwidth0, xbins, xmin0, xmax0, h0->GetName());
                        return NULL;
                }
        }

        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());

        UIUC::TFactory::CopyPaste((TH1*) h0, (TH1*) h);

        return h;
}

TH1* UIUC::TFactory::Profile(TH1* h0, Option_t *option)
{
        TH1* h = NULL;

        if(h0->InheritsFrom("TH3")) h = UIUC::TFactory::Profile((TH3*) h0, option);
        else if(h0->InheritsFrom("TH2")) h = UIUC::TFactory::Profile((TH2*) h0, option);
        else UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) Form("Histogram \"%s\" is already TH1..", h0->GetName()));

        return h;
}

TProfile* UIUC::TFactory::Profile(TH2* h0, Option_t *option)
{
        TString opt = option;
        opt.ToLower();

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute profile over " + option + "-axis for " + h0->GetName());
        TString name = (TString) h0->GetName() + ":" + opt + "-profile";
        //TDirectory *parent = gDirectory;

        TProfile *h = NULL;
        if (opt == "x") h = h0->ProfileX();
        else if (opt == "y") h = h0->ProfileY();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        h->SetName(name);
        h->SetTitle(h0->GetTitle());
        h->SetDirectory(h0->GetDirectory());

        return h;
}

TProfile2D* UIUC::TFactory::Profile(TH3* h0, Option_t *option)
{
        TString opt = option;
        opt.ToLower();

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute profile over " + option + "-axis for " + h0->GetName());

        TString name = (TString) h0->GetName() + ":" + opt + "-profile";
        TProfile2D *hpp = h0->Project3DProfile(opt + "e");
        if(hpp == NULL) {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"xy\", \"yz\", \"zx\", \"yx\", \"zy\", \"xz\")");
                return NULL;
        }

        hpp->SetName(name);
        hpp->SetTitle(h0->GetTitle());
        hpp->SetDirectory(h0->GetDirectory());

        return hpp;
}

TList* UIUC::TFactory::Modulate(TH1 *h, Option_t *option)
{
        TString opt = option;
        opt.ToLower();

        TList *l = NULL;
        TAxis *axis = NULL;
        if(opt.Contains("x")) axis = h->GetXaxis();
        else if(opt.Contains("y")) axis = h->GetYaxis();
        else if(opt.Contains("z")) axis = h->GetZaxis();
        else {
                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Wrong first parameter found.. it must be either x,y or z.");
                return NULL;
        }

        if(axis) l = UIUC::TFactory::Modulate(h, option, 1, axis->GetNbins());
        return l;
}

TList* UIUC::TFactory::Modulate(TH1* h0, Option_t *option, int firstbin, int lastbin)
{
        TList* l = NULL;

        if(h0->InheritsFrom("TH3")) l = UIUC::TFactory::Modulate((TH3*) h0, option, firstbin, lastbin);
        else if(h0->InheritsFrom("TH2")) l = UIUC::TFactory::Modulate((TH2*) h0, option, firstbin, lastbin);
        else UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "TH1 histogram cannot be split..");

        return l;
}

int UIUC::TFactory::RemoveNaNFromGraph(TGraph *g) {

        int removed_points = 0;
        for(int i = 0, N = g->GetN(); i < N; i++) {

                double x = 0, y = 0;
                g->GetPoint(i, x, y);

                double xlow_err = 0, xhigh_err = 0;
                double ylow_err = 0, yhigh_err = 0;

                if(g->InheritsFrom("TGraphAsymmErrors")) {

                        xlow_err  = g->GetErrorXlow(i);
                        xhigh_err = g->GetErrorXhigh(i);
                        ylow_err  = g->GetErrorYlow(i);
                        yhigh_err = g->GetErrorYhigh(i);

                } else if(g->InheritsFrom("TGraphErrors")) {

                        xlow_err  = g->GetErrorX(i);
                        ylow_err  = g->GetErrorY(i);
                }

                if(UIUC::IsNaN(x) || UIUC::IsNaN(y) ||
                   UIUC::IsNaN(xlow_err) || UIUC::IsNaN(xhigh_err) ||
                   UIUC::IsNaN(ylow_err) || UIUC::IsNaN(yhigh_err)
                   ) {
                        g->RemovePoint(i--);
                        removed_points++;
                }
        }

        return removed_points;

}

TList* UIUC::TFactory::Modulate(TH2* h0, Option_t *option, int firstbin, int lastbin)
{
        TList *l = new TList();

        // Get histogram limits
        TString opt = option;
        opt.ToLower();

        TAxis *x1, *x2;
        if(opt == "x") {

                x2 = h0->GetXaxis();
                x1 = h0->GetYaxis();

        } else if(opt == "y") {

                x2 = h0->GetYaxis();
                x1 = h0->GetXaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute 1D classes over " + opt + "-axis for " + h0->GetName());

        vector<double> x1_array = {x1->GetBinLowEdge(1)};
        for(int i = 1, I = x1->GetNbins(); i <= I; i++)
                x1_array.push_back(x1->GetBinUpEdge(i));

        // Create class of histograms
        for(int i = firstbin; i <= lastbin; i++) {

                double x2_1 = x2->GetBinLowEdge(i);
                double x2_2 = x2->GetBinUpEdge(i);

                TString x2_title;
                if(!TString(x2->GetTitle()).EqualTo("")) x2_title = TVarexp(x2->GetTitle()).Tokenize(';')[0];
                else x2_title = "?";

                TString name = (TString) h0->GetName() + ":" + opt + "-mod," + Form("%f", x2_1) + "," + Form("%f", x2_2);
                TString title = Form("%.4f < %s < %.4f", x2_1, UIUC::TFactory::RemoveUnits(x2_title).Data(), x2_2);
                title += TString(";") + x1->GetTitle();
                title += TString(";") + h0->GetZaxis()->GetTitle();

                TH1D *h = new TH1D(name, name, x1_array.size()-1, &(x1_array[0]));
                h->SetTitle(title);
                h->SetDirectory(h0->GetDirectory());

                int N = 0;
                for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                        double content = 0;
                        double content_err = 0;

                        for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                                if(opt == "x") {

                                        if(h0->GetXaxis()->GetBinCenter(x2_i) < x2_1) continue;
                                        if(h0->GetXaxis()->GetBinCenter(x2_i) > x2_2) break;

                                        content     = content + h0->GetBinContent(x2_i, x1_i);
                                        content_err = TMath::Sqrt( content_err*content_err + TMath::Power(h0->GetBinError(x2_i, x1_i), 2) );

                                } else if (opt == "y") {

                                        if(h0->GetYaxis()->GetBinCenter(x2_i) < x2_1) continue;
                                        if(h0->GetYaxis()->GetBinCenter(x2_i) > x2_2) break;

                                        content     = content + h0->GetBinContent(x1_i, x2_i);
                                        content_err = TMath::Sqrt( content_err*content_err + TMath::Power(h0->GetBinError(x1_i, x2_i), 2) );
                                }

                                if( content != 0 || content_err != 0) {
                                        h->SetBinContent(x1_i, content);
                                        h->SetBinError(x1_i, content_err);

                                        N++;
                                }
                        }
                }

                h->SetEntries(N);
                l->Add(h);
        }

        return l;
}

TList* UIUC::TFactory::Modulate(TH3 *h0, Option_t *option, int firstbin, int lastbin)
{
        TList *l = new TList();

        // Get histogram limits
        TString opt = option;
        opt.ToLower();

        TAxis *x1, *x2, *x3;
        if(opt == "x") {

                x3 = h0->GetXaxis();
                x1 = h0->GetYaxis();
                x2 = h0->GetZaxis();

        } else if(opt == "y") {

                x3 = h0->GetYaxis();
                x1 = h0->GetXaxis();
                x2 = h0->GetZaxis();

        } else if(opt == "z") {

                x3 = h0->GetZaxis();
                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\", \"z\")");
                return NULL;
        }

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute 2D classes over " + opt + "-axis for " + h0->GetName());

        vector<double> x1_array = {x1->GetBinLowEdge(1)};
        for(int i = 1, I = x1->GetNbins(); i <= I; i++)
                x1_array.push_back(x1->GetBinUpEdge(i));

        vector<double> x2_array = {x2->GetBinLowEdge(1)};
        for(int i = 1, I = x2->GetNbins(); i <= I; i++)
                x2_array.push_back(x2->GetBinUpEdge(i));

        // Create class of histograms
        for(int i = firstbin; i <= lastbin; i++) {

                double x3_1 = x3->GetBinLowEdge(i);
                double x3_2 = x3->GetBinUpEdge(i);

                TString title;
                TString x3_title = TVarexp(x3->GetTitle()).Tokenize(';')[0].Data();
                if(!TString(x3->GetTitle()).EqualTo("")) title = Form("%.2f < %s < %.2f", x3_1, UIUC::TFactory::RemoveUnits(x3_title).Data(), x3_2);

                title += TString(";") + x1->GetTitle();
                title += TString(";") + x2->GetTitle();
                vector<TString> v = TVarexp(x3->GetTitle()).Tokenize(';');
                if(v.size() >= 2) title += ";" + v[1];

                TString name = (TString) h0->GetName() + ":" + opt + "-mod," + Form("%f", x3_1) + "," + Form("%f", x3_2);

                TH2D *h = new TH2D(name, name, x1_array.size()-1, &(x1_array[0]), x2_array.size()-1, &(x2_array[0]));
                h->SetTitle(title);
                h->SetDirectory(h0->GetDirectory());


                int N = 0;
                for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                        for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                                double content = 0;
                                double content_err = 0;

                                for(int x3_i = 1, x3_N = x3->GetNbins(); x3_i <= x3_N; x3_i++) {

                                        if(opt == "x") {

                                                if(h0->GetXaxis()->GetBinCenter(x3_i) < x3_1) continue;
                                                if(h0->GetXaxis()->GetBinCenter(x3_i) > x3_2) break;

                                                content     = content + h0->GetBinContent(x3_i, x1_i, x2_i);
                                                content_err = TMath::Sqrt( content_err*content_err + TMath::Power(h0->GetBinError(x3_i, x1_i, x2_i), 2) );

                                        } else if (opt == "y") {

                                                if(h0->GetYaxis()->GetBinCenter(x3_i) < x3_1) continue;
                                                if(h0->GetYaxis()->GetBinCenter(x3_i) > x3_2) break;

                                                content     = content + h0->GetBinContent(x1_i, x3_i, x2_i);
                                                content_err = TMath::Sqrt( content_err*content_err + TMath::Power(h0->GetBinError(x1_i, x3_i, x2_i), 2) );

                                        } else if (opt == "z") {

                                                if(h0->GetZaxis()->GetBinCenter(x3_i) < x3_1) continue;
                                                if(h0->GetZaxis()->GetBinCenter(x3_i) > x3_2) break;

                                                content     = content + h0->GetBinContent(x1_i, x2_i, x3_i);
                                                content_err = TMath::Sqrt( content_err*content_err + TMath::Power(h0->GetBinError(x1_i, x2_i, x3_i), 2) );
                                        }
                                }

                                h->SetBinContent(x1_i, x2_i, content);
                                h->SetBinError(x1_i, x2_i, content_err);

                                N++;
                        }
                }

                h->SetEntries(N);

                l->Add(h);
        }

        return l;
}

TH1* UIUC::TFactory::CumulativeHistogram(TH1* h, TH1 *hCumulative)
{
        if(h == NULL) return NULL;

        if(hCumulative != NULL) UIUC::TFactory::Empty(hCumulative);
        else {
                hCumulative = (TH1D*) CopyStruct(h);
                hCumulative->SetName((TString) h->GetName()+":cumulative");
        }

        double cumulative = 0;
        double cumulative_err = 0;
        for(int x1_i = 1, x1_N = h->GetNbinsX(); x1_i <= x1_N; x1_i++) {

                cumulative += TMath::Abs(h->GetBinContent(x1_i));
                cumulative_err = TMath::Sqrt(cumulative_err*cumulative_err + h->GetBinError(x1_i)*h->GetBinError(x1_i));

                hCumulative->SetBinContent(x1_i, cumulative);
                hCumulative->SetBinError(x1_i, cumulative_err);
        }

        hCumulative->GetXaxis()->SetTitle(h->GetXaxis()->GetTitle());
        hCumulative->GetYaxis()->SetTitle(h->GetYaxis()->GetTitle());
        hCumulative->GetZaxis()->SetTitle(h->GetZaxis()->GetTitle());
        hCumulative->SetEntries(h->GetEntries());
        return hCumulative;
}

TH1D* UIUC::TFactory::PullDistribution(vector<TH1*> h, int nbins)
{
        if(h.size() == 0) return NULL;
        TH1 *h0 = h[0];

        vector<bool> bCompute;
        for(int i = 0, N = h.size(); i < N; i++) {

                bCompute.push_back(UIUC::TFactory::CompareStruct(h0, h[i]));
                if(!bCompute.back()) {

                        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Not compatible structure for histogram #%d and the reference used.. skip", i);
                        continue;
                }
        }

        TH1D* hPull = new TH1D((TString) h0->GetName() + ":pull", "Pull Distribution : \"" + TString(h0->GetTitle()) +"\"", nbins, -5, 5);
        hPull->GetXaxis()->SetTitle("Z = #frac{X - #mu}{#sigma}");
        hPull->GetYaxis()->SetTitle("#Count");
        hPull->SetDirectory(h0->GetDirectory());
        hPull->Sumw2();

        for(int i = 0, N = h0->GetNcells(); i < N; i++) {

                if(h0->IsBinUnderflow(i) || h0->IsBinOverflow(i)) continue;

                vector<double> c,cerr;
                for(int j = 0, J = (int) h.size(); j < J; j++) {

                        if(!bCompute[j]) continue;
                        c.push_back(h[j]->GetBinContent(i));
                        cerr.push_back(h[j]->GetBinError(i));
                }

                vector<double> stdv;
                if(!ComputeMeanAndSV(stdv, "", c, cerr)) return NULL;

                double mean0 = stdv[0], sigma0 = stdv[2];
                for(int j = 0, J = (int) h.size(); j < J; j++) {

                        if(!bCompute[j]) continue;
                        double content = h[j]->GetBinContent(i);
                        double content_err = h[j]->GetBinError(i);
                        if(content == 0 && content_err == 0) continue;

                        double X = 0;
                        if(sigma0 != 0) {
                                
                                X = (content - mean0)/sigma0;
                                hPull->Fill(X);
                        }
                }
        }

        UIUC::TFactory::NormalizeByIntegral(hPull);
        hPull->Fit("gaus", "SQ0");
        hPull->GetFunction("gaus")->ResetBit(1<<9);

        TF1* fGaus = hPull->GetFunction("gaus");
        hPull->SetTitle(UIUC::TFactory::MultipleLines(
                                Form("%s\\#mu = %.4f and #sigma = %.4f (#chi^{2}/n.d.f = %d/%d)",
                                     hPull->GetTitle(), fGaus->GetParameter(1), fGaus->GetParameter(2), (int) fGaus->GetChisquare(), fGaus->GetNDF())
                                ));

        return hPull;
}

TH1D* UIUC::TFactory::PullDistribution(TList* hList, int nbins)
{
        vector<TH1*> vHistos;
        for(int i = 0, N = hList->GetEntries(); i < N; i++)
                vHistos.push_back((TH1*)hList->At(i));

        return PullDistribution(vHistos, nbins);
}

TH1D* UIUC::TFactory::PullDistribution(TH1* h0, int nbins)
{
        // Create histogram
        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute pull distribution for " + h0->GetName());

        TString name = (TString) h0->GetName() + ":x-pull";

        TH1D *h = new TH1D(name, name, nbins, -5, 5);
        h->SetDirectory(h0->GetDirectory());
        h->GetXaxis()->SetTitle(h0->GetYaxis()->GetTitle());
        h->Sumw2();

        TAxis *x1 = h0->GetXaxis();

        int nentries = 0;
        double mean = 0, sigma = 0;
        double sum_err = 0, sum_err2 = 0;
        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                double content = h0->GetBinContent(x1_i);
                double content_err = h0->GetBinError(x1_i);

                if(content_err != 0) {

                        mean += content/content_err;
                        sum_err  += 1/content_err;
                        sum_err2 += 1/(content_err*content_err);

                        nentries++;
                }
        }
        if(sum_err != 0) mean /= sum_err;
        if(sum_err2 != 0) sigma = 1/TMath::Sqrt(sum_err2);

        // /*cout << "Pull distribution:" << endl;
        //    cout << "- Mean: " << mean << endl;
        //    cout << "- Sigma: "  << 1/TMath::Sqrt(sum_err2) << endl;*/

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                double content = h0->GetBinContent(x1_i);
                double content_err = h0->GetBinError(x1_i);

                if( content != 0) {

                        double X = 0;
                        double stdDev = TMath::Sqrt(TMath::Abs((content_err*content_err) - sigma*sigma));
                        if(stdDev != 0) {

                                X = (content - mean)/stdDev;
                                h->Fill(X);
                        }
                }
        }

        return h;
}

TH1 *UIUC::TFactory::Invert(TH1 *h0)
{
        if(h0 == NULL) return NULL;
        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Invert distribution for " + h0->GetName());

        TH1 *h = (TH1*) h0->Clone((TString) h0->GetName() + ":inverted");
             h->SetDirectory(h0->GetDirectory());
             h->Sumw2();

        for(int iCell = 1, nCells = h->GetNcells(); iCell < nCells; iCell++) {

                double content = h0->GetBinContent(iCell);
                h->SetBinContent(iCell, -content);
        }

        return h;
}

TH1 *UIUC::TFactory::Shift(TH1 *h0, Option_t *option, double shift)
{
        if(shift == 0) return h0;

        TString opt = option;
                opt.ToLower();


        TH1 *h = (TH1*) h0->Clone((TString) h0->GetName() + ":"+opt+"-shift["+TString::Itoa(shift,10)+"]");

        TAxis *x1;
        if(opt == "x") x1 = h->GetXaxis();
        else if(opt == "y") x1 = h->GetYaxis();
        else if(opt == "z") x1 = h->GetZaxis();
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }
        
        x1->Set(x1->GetNbins(), x1->GetXmin()+shift, x1->GetXmax()+shift);

        return h;
}

TGraph *UIUC::TFactory::Shift(TGraph *g0, double shift)
{
        if(g0 == NULL) return NULL;
        if(shift == 0) return g0;

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Rolling distribution by "+shift+" units over x-axis for " + g0->GetName());
        TGraph* g = (TGraph*) g0->Clone((TString) g0->GetName() + ":x-roll["+TString::Itoa(shift,10)+"]");
        for(int i = 0, N = g->GetN(); i < N; i++)
                g->SetPointX(i+1, g->GetPointX(i+1) + shift);

        return g;
}

TH1 *UIUC::TFactory::Roll(TH1 *h0, Option_t *option, int shift)
{
        if(h0 == NULL) return NULL;
        if(shift == 0) return h0;

        if(h0->InheritsFrom("TH3")) return UIUC::TFactory::Roll((TH3*) h0, option, shift);
        if(h0->InheritsFrom("TH2")) return UIUC::TFactory::Roll((TH2*) h0, option, shift);
        
        TString opt = option;
                opt.ToLower();

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Rolling distribution by "+shift+" units over x-axis for " + h0->GetName());

        TH1* h = (TH1*) h0->Clone((TString) h0->GetName() + ":x-roll["+TString::Itoa(shift,10)+"]");
             h->SetDirectory(h0->GetDirectory());
             h->Sumw2();

        TAxis *x1 = h0->GetXaxis();
        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++)
                h->SetBinContent(UIUC::MathMod(x1_i + shift - 1, x1_N) + 1, h0->GetBinContent(x1_i));

        return h;
}

TH1 *UIUC::TFactory::Roll(TH2 *h0, Option_t *option, int shift)
{
        if(shift == 0) return h0;

        TString opt = option;
                opt.ToLower();

        TAxis *x1, *x2;
        if(opt == "x") {

                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();

        } else if(opt == "y") {

                x1 = h0->GetYaxis();
                x2 = h0->GetXaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Rolling distribution by "+shift+" units over x-axis for " + h0->GetName());

        TH1 *h = (TH1*) h0->Clone((TString) h0->GetName() + ":x-roll["+TString::Itoa(shift,10)+"]");
             h->SetDirectory(h0->GetDirectory());
             h->Sumw2();

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        double content = h0->GetBinContent(x1_i, x2_i);
                        if(opt.Contains("x")) h->SetBinContent(UIUC::MathMod(x1_i + shift - 1, x1_N) + 1, x2_i, content);
                        if(opt.Contains("y")) h->SetBinContent(x1_i, UIUC::MathMod(x2_i + shift - 1, x2_N) + 1, content);
                }
        }

        return h;
}

TH1 *UIUC::TFactory::Roll(TH3 *h0, Option_t *option, int shift)
{
        if(shift == 0) return h0;

        TString opt = option;
                opt.ToLower();

        TAxis *x1, *x2, *x3;
        if(opt == "x") {

                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();
                x3 = h0->GetZaxis();

        } else if(opt == "y") {

                x1 = h0->GetYaxis();
                x2 = h0->GetXaxis();
                x3 = h0->GetZaxis();

        } else if(opt == "z") {


                x1 = h0->GetZaxis();
                x2 = h0->GetXaxis();
                x3 = h0->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Rolling distribution by "+shift+" units over x-axis for " + h0->GetName());

        TH1 *h = (TH1*) h0->Clone((TString) h0->GetName() + ":x-roll["+TString::Itoa(shift,10)+"]");
             h->SetDirectory(h0->GetDirectory());
             h->Sumw2();

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        for(int x3_i = 1, x3_N = x3->GetNbins(); x3_i <= x3_N; x3_i++) {

                                double content = h0->GetBinContent(x1_i, x2_i, x3_i);
                                if(opt.Contains("x")) h->SetBinContent(UIUC::MathMod(x1_i + shift - 1, x1_N) + 1, x2_i, x3_i, content);
                                if(opt.Contains("y")) h->SetBinContent(x1_i, UIUC::MathMod(x2_i + shift - 1, x2_N) + 1, x3_i, content);
                                if(opt.Contains("z")) h->SetBinContent(x1_i, x2_i, UIUC::MathMod(x3_i + shift - 1, x3_N) + 1, content);
                        }
                }
        }

        return h;
}

TH1* UIUC::TFactory::FrequencyDistribution(TH1* h0, Option_t *option, int nbins, double fmin, double fmax)
{
        TH1* h = NULL;

        if(h0->InheritsFrom("TH3")) h = UIUC::TFactory::FrequencyDistribution((TH3*) h0, option, nbins, fmin, fmax);
        else if(h0->InheritsFrom("TH2")) h = UIUC::TFactory::FrequencyDistribution((TH2*) h0, option, nbins, fmin, fmax);
        else {

                //Keep option here to have same structure as other FrequencyDistribution
                TString opt = option;
                if(!opt.Contains("x")) return NULL;

                // Create histogram
                UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute frequency distribution over x-axis for " + h0->GetName());

                TString name = (TString) h0->GetName() + ":x-freq";

                TH1D *h = new TH1D(name, name, nbins, fmin, fmax);
                h->SetDirectory(h0->GetDirectory());
                h->GetXaxis()->SetTitle(h0->GetYaxis()->GetTitle());
                h->GetYaxis()->SetTitle("#count");
                h->Sumw2();

                TAxis *x1 = h0->GetXaxis();
                for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                        double content = h0->GetBinContent(x1_i);
                        if( !UIUC::EpsilonEqualTo(content,0) ) h->Fill(content);
                }
        }

        return h;
}

TH2D* UIUC::TFactory::FrequencyDistribution(TH2* h0, Option_t* option, int nbins, double fmin, double fmax)
{
        TString opt = option;
        opt.ToLower();

        TAxis *x1, *x2;
        if(opt == "x") {

                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();

        } else if(opt == "y") {

                x1 = h0->GetYaxis();
                x2 = h0->GetXaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        // Create histogram
        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute frequency distribution over " + opt + "-axis for " + h0->GetName());
        TString name = (TString) h0->GetName() + ":" + option + "-freq";

        TH2D *h = new TH2D(name, name, nbins,fmin,fmax, x2->GetNbins(),x2->GetBinLowEdge(1),x2->GetBinUpEdge(x2->GetNbins()) );
        h->SetDirectory(h0->GetDirectory());
        h->Sumw2();
        h->GetXaxis()->SetTitle(h0->GetZaxis()->GetTitle());
        h->GetYaxis()->SetTitle(x2->GetTitle());
        h->GetZaxis()->SetTitle("#count");

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        double content = 0;
                        if(opt.Contains("x")) content = h0->GetBinContent(x1_i, x2_i);
                        if(opt.Contains("y")) content = h0->GetBinContent(x2_i, x1_i);

                        if( !UIUC::EpsilonEqualTo(content,0) ) h->Fill(content, x2->GetBinCenter(x2_i));
                }
        }

        if(opt.Contains("y")) h = UIUC::TFactory::Transpose(h);
        return h;
}

TH3D* UIUC::TFactory::FrequencyDistribution(TH3* h0, Option_t* option, int nbins, double fmin, double fmax)
{
        TString opt = option;
        opt.ToLower();

        TAxis *x1, *x2, *x3;
        if(opt == "x") {

                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();
                x3 = h0->GetZaxis();

        } else if(opt == "y") {

                x1 = h0->GetYaxis();
                x2 = h0->GetXaxis();
                x3 = h0->GetZaxis();

        } else if(opt == "z") {


                x1 = h0->GetZaxis();
                x2 = h0->GetXaxis();
                x3 = h0->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        // Create histogram
        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute frequency distribution over " + opt + "-axis for " + h0->GetName());

        TString name = (TString) h0->GetName() + ":" + option + "-freq";

        TH3D *h = new TH3D(name, name, nbins,fmin,fmax, x2->GetNbins(),x2->GetBinLowEdge(1),x2->GetBinUpEdge(x2->GetNbins()), x3->GetNbins(),x3->GetBinLowEdge(1),x3->GetBinUpEdge(x3->GetNbins()) );
              h->SetDirectory(h0->GetDirectory());
              h->GetXaxis()->SetTitle(h0->GetZaxis()->GetTitle());
              h->GetYaxis()->SetTitle(x2->GetTitle());
              h->GetZaxis()->SetTitle(x3->GetTitle());
              h->Sumw2();

        // Fill frequency distribution
        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        for(int x3_i = 1, x3_N = x3->GetNbins(); x3_i <= x3_N; x3_i++) {

                                double content = 0;
                                if(opt.Contains("x")) content = h0->GetBinContent(x1_i, x2_i, x3_i);
                                if(opt.Contains("y")) content = h0->GetBinContent(x2_i, x1_i, x3_i);
                                if(opt.Contains("z")) content = h0->GetBinContent(x2_i, x3_i, x1_i);

                                if( !UIUC::EpsilonEqualTo(content,0) ) h->Fill(content, x2->GetBinCenter(x2_i), x3->GetBinCenter(x3_i));
                        }
                }
        }

        return h;
}

//static TH1* Normalize(TH3D*, TH2D*, Option_t*);
TH1* UIUC::TFactory::Normalize(TH2D* h, TH1D* h0, Option_t* option)
{
        TString opt = option;
        opt.ToLower();

        TAxis *x1 = NULL, *x2 = NULL;
        if(opt.Contains("x")) {

                x1 = h->GetXaxis();
                x2 = h->GetYaxis();

        } else if(opt.Contains("y")) {

                x1 = h->GetYaxis();
                x2 = h->GetXaxis();
        }

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {
                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        if(opt.Contains("x")) {

                                if(h0->GetBinContent(x1_i) == 0) continue;
                                h->SetBinContent(x1_i, x2_i, h->GetBinContent(x1_i,x2_i)/h0->GetBinContent(x1_i));
                                h->SetBinError(x1_i, x2_i, h->GetBinError(x1_i,x2_i)/h0->GetBinContent(x1_i));

                        } else if(opt.Contains("y")) {

                                if(h0->GetBinContent(x2_i) == 0) continue;
                                h->SetBinContent(x1_i, x2_i, h->GetBinContent(x1_i,x2_i)/h0->GetBinContent(x2_i));
                                h->SetBinError(x1_i, x2_i, h->GetBinError(x1_i,x2_i)/h0->GetBinContent(x2_i));
                        }
                }
        }

        return h;
}

TH1* UIUC::TFactory::NormalizeByBinWidth(TH1* h)
{
        if(h == NULL) return NULL;

        if(HasVariableBinWidth(h)) {

                for(int i = 1, I = h->GetNbinsX(); i <= I; i++) {
                        for(int j = 1, J = h->GetNbinsY(); j <= J; j++) {
                                for(int k = 1, K = h->GetNbinsZ(); k <= K; k++) {

                                        double pixel = h->GetXaxis()->GetBinWidth(i) *
                                                       h->GetYaxis()->GetBinWidth(j) *
                                                       h->GetZaxis()->GetBinWidth(k);

                                        int ibin = h->GetBin(i,j,k);
                                        h->SetBinContent(ibin, h->GetBinContent(ibin)/pixel);
                                        h->SetBinError(ibin, h->GetBinError(ibin)/pixel);
                                }
                        }
                }

                TAxis *xlast = NULL;
                if(h->InheritsFrom("TH2")) {
                        xlast = h->GetZaxis();
                        h->SetName((TString) h->GetName()+":z-normalize[binw]");
                } else if(h->InheritsFrom("TH1")) {
                        xlast = h->GetYaxis();
                        h->SetName((TString) h->GetName()+":y-normalize[binw]");
                }
                
                if(xlast != NULL) {

                        TString binwidthX = (h->InheritsFrom("TH1")) ? "#Deltax" : "";
                        if( !binwidthX.EqualTo("") ) {
                                if( !UIUC::TFactory::HasVariableBinWidth(h->GetXaxis()) ) binwidthX = "(" + binwidthX + Form(" = %.2f", h->GetXaxis()->GetBinWidth(1)) + ")";
                                binwidthX = " / " + binwidthX;
                        }

                        TString binwidthY = (h->InheritsFrom("TH2") || h->InheritsFrom("TH3")) ? "#Deltay" : "";
                        if( !binwidthY.EqualTo("") ) {
                                if( !UIUC::TFactory::HasVariableBinWidth(h->GetYaxis()) ) binwidthY = "(" + binwidthY + Form(" = %.2f", h->GetYaxis()->GetBinWidth(1)) + ")";
                                binwidthY = " / " + binwidthY;
                        }

                        TString binwidthZ = (h->InheritsFrom("TH3")) ? "#Deltaz" : "";
                        if( !binwidthZ.EqualTo("") ) {
                                if( !UIUC::TFactory::HasVariableBinWidth(h->GetZaxis()) ) binwidthZ = "(" + binwidthZ + Form(" = %.2f", h->GetZaxis()->GetBinWidth(1)) + ")";
                                binwidthZ = " / " + binwidthZ;
                        }

                        TString axis_title = (TString) xlast->GetTitle() + binwidthX + binwidthY + binwidthZ;
                        if(!TString(xlast->GetTitle()).EqualTo("")) xlast->SetTitle(axis_title);
                }

        } else {

                double pixel = h->GetXaxis()->GetBinWidth(1) *
                               h->GetYaxis()->GetBinWidth(1) *
                               h->GetZaxis()->GetBinWidth(1);

                h = UIUC::TFactory::Normalize(h, pixel);
                if(h->InheritsFrom("TH3")) {}
                else if(h->InheritsFrom("TH2")) {

                        TString title = h->GetZaxis()->GetTitle();
                        if(title.EqualTo("")) title = "Count";
                        title = Form("%s / %.3f", title.Data(), pixel);

                        h->SetName((TString) h->GetName()+":z-normalize[binw]");
                        h->GetZaxis()->SetTitle(title);

                } else if(h->InheritsFrom("TH1")) {

                        TString title = h->GetYaxis()->GetTitle();
                        if(title.EqualTo("")) title = "Count";
                        title = Form("%s / %.3f", title.Data(), pixel);

                        h->SetName((TString) h->GetName()+":y-normalize[binw]");
                        h->GetYaxis()->SetTitle(title);
                }
        }

        return h;

}
TH1* UIUC::TFactory::NormalizeByEntries(TH1* h)
{
        if(h == NULL) return NULL;

        double norm = (h->GetEntries() != 0) ? h->GetEntries() : 1;
        h = UIUC::TFactory::Normalize(h, norm);

        if(h->InheritsFrom("TH3")) {}
        else if(h->InheritsFrom("TH2")) {

                TString title = h->GetZaxis()->GetTitle();
                if(title.EqualTo("")) title = "Count";
                title = Form("%s / Entries", title.Data());

                h->SetName((TString) h->GetName()+":z-normalize[entries]");
                h->GetZaxis()->SetTitle(title);

        } else if(h->InheritsFrom("TH1")) {

                TString title = h->GetYaxis()->GetTitle();
                if(title.EqualTo("")) title = "Count";
                title = Form("%s / Entries", title.Data());

                h->SetName((TString) h->GetName()+":y-normalize[entries]");
                h->GetYaxis()->SetTitle(title);
        }
        return h;
}

TH1* UIUC::TFactory::NormalizeByBin(TH1* h, int i)
{
        if(h == NULL) return NULL;

        double norm = (h->GetBinContent(i) != 0) ? h->GetBinContent(i) : 1;
        h = UIUC::TFactory::Normalize(h, norm);

        h = UIUC::TFactory::Normalize(h, norm);
        if(h->InheritsFrom("TH3")) {}
        else if(h->InheritsFrom("TH2")) {

                TString title = h->GetZaxis()->GetTitle();
                if(title.EqualTo("")) title = "Count";
                title = Form("%s / %d-Bin", title.Data(), i);

                h->SetName((TString) h->GetName()+":z-normalize[bin]");
                h->GetZaxis()->SetTitle(title);

        } else if(h->InheritsFrom("TH1")) {

                TString title = h->GetYaxis()->GetTitle();
                if(title.EqualTo("")) title = "Count";
                if(!title.Contains(" / Integral")) title = Form("%s / Integral", title.Data());

                h->SetName((TString) h->GetName()+":y-normalize[bin]");
                h->GetYaxis()->SetTitle(title);
        }

        return h;
}

TH1* UIUC::TFactory::NormalizeByIntegral(TH1* h)
{
        if(h == NULL) return NULL;

        double norm = (h->Integral() != 0) ? h->Integral() : 1;
        h = UIUC::TFactory::Normalize(h, norm);

        if(h->InheritsFrom("TH3")) {}
        else if(h->InheritsFrom("TH2")) {

                TString title = h->GetZaxis()->GetTitle();
                if(title.EqualTo("")) title = "Count";
                if(!title.Contains(" / Integral")) title = Form("%s / Integral", title.Data());

                h->SetName((TString) h->GetName()+":z-normalize[integral]");
                h->GetZaxis()->SetTitle(title);

        } else if(h->InheritsFrom("TH1")) {

                TString title = h->GetYaxis()->GetTitle();
                if(title.EqualTo("")) title = "Count";
                if(!title.Contains(" / Integral")) title = Form("%s / Integral", title.Data());

                h->SetName((TString) h->GetName()+":y-normalize[integral]");
                h->GetYaxis()->SetTitle(title);
        }

        return h;
}

TH1* UIUC::TFactory::NormalizeByMax(TH1* h)
{
        if(h == NULL) return NULL;

        double norm = (h->GetMaximum() != 0) ? h->GetMaximum() : 1;
        h = UIUC::TFactory::Normalize(h, norm);

        if(h->InheritsFrom("TH3")) {}
        else if(h->InheritsFrom("TH2")) {

                TString title = h->GetZaxis()->GetTitle();
                if(title.EqualTo("")) title = "Count";
                if(!title.Contains(" / Maximum")) title = Form("%s / Maximum", title.Data());

                h->SetName((TString) h->GetName()+":z-normalize[max]");
                h->GetZaxis()->SetTitle(title);

        } else if(h->InheritsFrom("TH1")) {

                TString title = h->GetYaxis()->GetTitle();
                if(title.EqualTo("")) title = "Count";
                if(!title.Contains(" / Maximum")) title = Form("%s / Maximum", title.Data());

                h->SetName((TString) h->GetName()+":y-normalize[max]");
                h->GetYaxis()->SetTitle(title);
        }
        
        return h;
}

TH1* UIUC::TFactory::Integrate(TH1* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;
        TH1* h = NULL;

        if(h0->InheritsFrom("TH3")) h = UIUC::TFactory::Integrate((TH3*) h0, option);
        else if(h0->InheritsFrom("TH2")) h = UIUC::TFactory::Integrate((TH2*) h0, option);
        else UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) Form("Histogram \"%s\" is already TH1..", h0->GetName()));

        return h;
}

vector<double> UIUC::TFactory::GetBinWidth(TH1* h0, Option_t *option)
{
        vector<double> v;

        TString opt = option;
        opt.ToLower();

        TAxis *a0 = NULL;
        if(opt.Contains("x")) a0 = h0->GetXaxis();
        else if(opt.Contains("y")) a0 = h0->GetYaxis();
        else if(opt.Contains("z")) a0 = h0->GetZaxis();
        else {
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unknown axis.. if should be either x,y or z");
                return {};
        }

        for(int i = 1, N = a0->GetNbins(); i <= N; i++)
                v.push_back(a0->GetBinWidth(i));

        return v;
}

TH1D* UIUC::TFactory::Integrate(TH2* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        vector<double> binwidth = UIUC::TFactory::GetBinWidth(h0, opt);
        TList *l = UIUC::TFactory::Modulate(h0, opt);
        if(l == 0 || l->GetSize() == 0) return NULL;

        TAxis *x1, *x2;
        if(opt == "x") {

                x1 = h0->GetYaxis();
                x2 = h0->GetXaxis();

        } else if(opt == "y") {

                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        vector<double> x1_array = {x1->GetBinLowEdge(1)};
        for(int i = 1, I = x1->GetNbins(); i <= I; i++)
                x1_array.push_back(x1->GetBinUpEdge(i));

        TString hname = (TString) h0->GetName() + ":" + opt + "-integrate";
        TH1D *h = new TH1D(UIUC::TFileReader::DirName(hname), UIUC::TFileReader::DirName(hname), x1_array.size()-1, &(x1_array[0]));
        if(h0->GetDirectory()) h->SetDirectory(h0->GetDirectory());
        else h->SetDirectory(gDirectory);

        h->GetXaxis()->SetTitle(x1->GetTitle());
        h->GetYaxis()->SetTitle(UIUC::TFactory::AddPrefixAndSuffix("<", x2->GetTitle(), ">"));
        h->SetTitle(h0->GetTitle());
        h->SetName(hname);

        TString eMessage = (TString) "Integrate over " + option + "-axis for " + h0->GetName();
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);
        eMessage = (TString) "Output object: " + h->GetDirectory()->GetPath() + "/" + hname;
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);

        TIter Next(l);
        TH1 *hh0 = NULL;
        for(int i = 0; (hh0 = (TH1*) Next()); i++) {

                hh0->Scale(binwidth[i]);
                h->Add(hh0);

                delete hh0;
                hh0 = NULL;
        }

        if(h == NULL) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Integral 2D -> 1D failed for " + h0->GetName());
                return NULL;
        }

        return h;
}

TH2D* UIUC::TFactory::Integrate(TH3* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        vector<double> binwidth = UIUC::TFactory::GetBinWidth(h0, opt);
        TList *l = UIUC::TFactory::Modulate(h0, opt);
        if(l->GetSize() == 0) return NULL;

        TAxis *x1, *x2, *x3;
        if(opt == "x") {

                x3 = h0->GetXaxis();
                x1 = h0->GetYaxis();
                x2 = h0->GetZaxis();

        } else if(opt == "y") {

                x3 = h0->GetYaxis();
                x1 = h0->GetXaxis();
                x2 = h0->GetZaxis();

        } else if(opt == "z") {

                x3 = h0->GetZaxis();
                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }


        vector<double> x1_array = {x1->GetBinLowEdge(1)};
        for(int i = 1, I = x1->GetNbins(); i <= I; i++)
                x1_array.push_back(x1->GetBinUpEdge(i));

        vector<double> x2_array = {x2->GetBinLowEdge(1)};
        for(int i = 1, I = x2->GetNbins(); i <= I; i++)
                x2_array.push_back(x2->GetBinUpEdge(i));

        TString hname = (TString) h0->GetName() + ":" + opt + "-integrate";
        TH2D *h = new TH2D(UIUC::TFileReader::DirName(hname), UIUC::TFileReader::DirName(hname), x1_array.size()-1, &(x1_array[0]), x2_array.size()-1, &(x2_array[0]));
        if(h0->GetDirectory()) h->SetDirectory(h0->GetDirectory());
        else h->SetDirectory(gDirectory);

        h->GetXaxis()->SetTitle(x1->GetTitle());
        h->GetYaxis()->SetTitle(x2->GetTitle());
        h->GetZaxis()->SetTitle(UIUC::TFactory::AddPrefixAndSuffix("<", x3->GetTitle(), ">"));
        h->SetTitle(h0->GetTitle());
        h->SetName(hname);

        TString eMessage = (TString) "Integrate over " + option + "-axis for " + h0->GetName();
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);
        eMessage = (TString) "Output object: " + h->GetDirectory()->GetPath() + "/" + hname;
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);

        TIter Next(l);
        TH1 *hh0 = NULL;
        for(int i = 0; (hh0 = (TH1*) Next()); i++) {

                hh0->Scale(binwidth[i]);
                h->Add(hh0);

                delete hh0;
                hh0 = NULL;
        }

        if(h == NULL) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Integral 2D -> 1D failed for " + h0->GetName());
                return NULL;
        }

        return h;
}


TH1* UIUC::TFactory::Projection(TH1* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;
        TH1* h = NULL;

        if(h0->InheritsFrom("TH3")) h = UIUC::TFactory::Projection((TH3*) h0, option);
        else if(h0->InheritsFrom("TH2")) h = UIUC::TFactory::Projection((TH2*) h0, option);
        else UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) Form("Histogram \"%s\" is already TH1..", h0->GetName()));

        return h;
}

TH1D* UIUC::TFactory::Projection(TH2* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute projection over " + opt + "-axis for " + h0->GetName());
        TString name = (TString) h0->GetName() + ":" + opt + "-proj";
        TH1D *h = NULL;

        if(opt == "x") h = h0->ProjectionX("", 0, -1, "e");
        else if(opt == "y") h = h0->ProjectionY("", 0, -1, "e");
        else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        if(h == NULL) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Projection failed for " + h0->GetName());
                return NULL;
        }

        h->SetName(name);
        h->SetTitle(h0->GetTitle());
        h->SetDirectory(h0->GetDirectory());
        return h;
}

TH2D* UIUC::TFactory::Projection(TH3* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute projection over " + opt + "-axis for " + h0->GetName());
        TString name = (TString) h0->GetName() + ":" + opt + "-proj";

        TH2D *h = NULL;
        h = (TH2D*) h0->Project3D(opt + "e");

        if(h == NULL) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Projection failed for " + h0->GetName());
                return NULL;
        }

        h->SetName(name);
        h->SetTitle(h0->GetTitle());
        h->SetDirectory(h0->GetDirectory());

        return h;
}

bool UIUC::TFactory::IsEmpty(TH1* h)
{
        if(h == NULL) return 1;
        for(int i = 0, N = h->GetNcells(); i < N; i++) {

                if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                if(h->GetBinContent(i) != 0 || h->GetBinError(i) != 0) return 0;
        }
        return 1;
}

bool UIUC::TFactory::IsCompatible(TGraph* g1, TGraph* g2)
{
        if(g1 == NULL || g2 == NULL) return 0;

        if(UIUC::HandboxMsg::Error(g1->GetN() != g2->GetN(), __METHOD_NAME__,
                                   "Incompatible number of point.. between the two graphs.. \"%s\" and \"%s\"", g1->GetName(), g2->GetName())
           ) return 0;

        return 1;
}

TH1* UIUC::TFactory::Divide(TH1* h1, TH1* h2, double c1, double c2)
{
        if(!CompareStruct(h1,h2)) return NULL;

        h1->Scale(c1);
        h2->Scale(c2);

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TH1 *h = (TH1*) h1->Clone((TString) h1->GetName() + "_%_" + h2->GetName() + "_" + gRandom);

        for (int i = 1; i < h->GetNbinsX() + 1; i++) {
                for (int j = 1; j < h->GetNbinsY() + 1; j++) {
                        for (int k = 1; k < h->GetNbinsZ() + 1; k++) {

                                int ibin = h->GetBin(i,j,k);
                                int ibin1 = h1->GetBin(i,j,k);
                                int ibin2 = h2->GetBin(i,j,k);
                                if(h2->GetBinContent(ibin2) != 0 || h2->GetBinError(ibin2) != 0) {

                                        h->SetBinContent(ibin, h1->GetBinContent(ibin1) / h2->GetBinContent(ibin2));
                                        h->SetBinError(ibin, h->GetBinContent(ibin) * TMath::Sqrt(h1->GetBinError(ibin1)*h1->GetBinError(ibin1)/h1->GetBinContent(ibin1)/h1->GetBinContent(ibin1) + h2->GetBinError(ibin2)*h2->GetBinError(ibin2)/h2->GetBinContent(ibin2)/h2->GetBinContent(ibin2)));
                                }
                        }
                }
        }

        h1->Scale(1./c1);
        h2->Scale(1./c2);

        return h;
}

TGraph* UIUC::TFactory::Divide(TGraph* g1, TGraph* g2, double c1, double c2)
{
        UIUC::TFactory::Scale(g1, c1);
        UIUC::TFactory::Scale(g2, c2);

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TGraph *g = (TGraph*) g1->Clone((TString) g1->GetName() + "_%_" + g2->GetName() + "_" + gRandom);

        for (int j = 0, J = g1->GetN(); j < J; j++) {

                if(g->GetY()) g->GetY()[j] = NAN;
                if(g->GetEY()) g->GetEY()[j] = 0;
                if(g->GetEYlow()) g->GetEYlow()[j] = 0;
                if(g->GetEYhigh()) g->GetEYhigh()[j] = 0;

                for (int k = 0, K = g2->GetN(); k < K; k++) {

                        if(!UIUC::EpsilonEqualTo(g1->GetX()[j],g2->GetX()[k])) continue;
                        if(g1->GetY() == NULL) continue;
                        if(g2->GetY() == NULL) continue;
                        if(g1->GetY()[j] == 0 || g2->GetY()[k] == 0) {

                                g->GetY()[j] = 0;
                                continue;
                        }

                        g->GetY()[j] = g1->GetY()[j]/g2->GetY()[k];

                        if(g1->GetEY() != NULL && g2->GetEY() != NULL) {

                                g->GetEY()[j] = g1->GetY()[j]/g2->GetY()[k] * TMath::Sqrt(
                                        g1->GetEY()[j]*g1->GetEY()[j]/g1->GetY()[j]/g1->GetY()[j] +
                                        g2->GetEY()[j]*g2->GetEY()[j]/g1->GetY()[j]/g1->GetY()[j]
                                        );
                        }

                        if(g1->GetEYlow() != NULL && g2->GetEYlow() != NULL) {
                                g->GetEYlow()[j] = g1->GetY()[j]/g2->GetY()[k] * TMath::Sqrt(
                                        g1->GetEYlow()[j]*g1->GetEYlow()[j]/g1->GetY()[j]/g1->GetY()[j] +
                                        g2->GetEYlow()[j]*g2->GetEYlow()[j]/g2->GetY()[k]/g2->GetY()[k]
                                        );
                        }

                        if(g1->GetEYhigh() != NULL && g2->GetEYhigh() != NULL) {
                                g->GetEYhigh()[j] = g1->GetY()[j]/g2->GetY()[k] * TMath::Sqrt(
                                        g1->GetEYhigh()[j]*g1->GetEYhigh()[j]/g1->GetY()[j]/g1->GetY()[j] +
                                        g2->GetEYhigh()[j]*g2->GetEYhigh()[j]/g2->GetY()[k]/g2->GetY()[k]
                                        );
                        }

                        if(UIUC::IsNaN(g->GetY()[j])) g->RemovePoint(j);
                        if(g->GetEY() && UIUC::IsNaN(g->GetEY()[j])) g->RemovePoint(j);
                        if(g->GetEYlow() && UIUC::IsNaN(g->GetEYlow()[j])) g->RemovePoint(j);
                        if(g->GetEYhigh() && UIUC::IsNaN(g->GetEYhigh()[j])) g->RemovePoint(j);

                }
        }

        UIUC::TFactory::Scale(g1, 1./c1);
        UIUC::TFactory::Scale(g2, 1./c2);

        return g;
}

TGraph* UIUC::TFactory::Scale(TGraph* g, double c)
{
        for (int j = 0, J = g->GetN(); j < J; j++) {

                if(g->GetY() != NULL) g->GetY()[j] *= c;
                if(g->GetEY() != NULL) g->GetEY()[j] *= c;
                if(g->GetEYhigh() != NULL) g->GetEYhigh()[j] *= c;
                if(g->GetEYlow() != NULL) g->GetEYlow()[j] *= c;
        }

        return g;
}

TGraph* UIUC::TFactory::Multiply(TGraph* g1, TGraph* g2, double c1, double c2)
{
        if(!IsCompatible(g1, g2)) return NULL;
        UIUC::TFactory::Scale(g1, c1);
        UIUC::TFactory::Scale(g2, c2);

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TGraph *g = (TGraph*) g1->Clone((TString) g1->GetName() + "_*_" + g2->GetName() + "_" + gRandom);

        for (int j = 0, J = g->GetN(); j < J; j++) {

                if(g->GetY() == NULL) continue;
                if(g2->GetY() == NULL) continue;

                if(g->GetEY() != NULL && g2->GetEY() != NULL) {

                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEY()[j] = 0;
                        else g->GetEY()[j] = (g->GetY()[j]*g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEY()[j]*g->GetEY()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEY()[j]*g2->GetEY()[j]/g->GetY()[j]/g->GetY()[j]
                                        );
                }

                if(g->GetEYlow() != NULL && g2->GetEYlow() != NULL) {

                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEYlow()[j] = 0;
                        else g->GetEYlow()[j] = (g->GetY()[j]*g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEYlow()[j]*g->GetEYlow()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEYlow()[j]*g2->GetEYlow()[j]/g2->GetY()[j]/g2->GetY()[j]
                                        );
                }

                if(g->GetEYhigh() != NULL && g2->GetEYhigh() != NULL) {

                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEYhigh()[j] = 0;
                        else g->GetEYhigh()[j] = (g->GetY()[j]*g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEYhigh()[j]*g->GetEYhigh()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEYhigh()[j]*g2->GetEYhigh()[j]/g2->GetY()[j]/g2->GetY()[j]
                                        );
                }

                g->GetY()[j] *= g2->GetY()[j];
        }

        UIUC::TFactory::Scale(g1, 1/c1);
        UIUC::TFactory::Scale(g2, 1/c2);

        return g;
}

TGraph* UIUC::TFactory::Add(TGraph* g1, TGraph* g2, double c1, double c2)
{
        if(!IsCompatible(g1, g2)) return NULL;
        UIUC::TFactory::Scale(g1, c1);
        UIUC::TFactory::Scale(g2, c2);

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TGraph *g = (TGraph*) g1->Clone((TString) g1->GetName() + "+" + g2->GetName() + "_" + gRandom);

        for (int j = 0, J = g->GetN(); j < J; j++) {

                if(g->GetY() == NULL) continue;
                if(g2->GetY() == NULL) continue;

                if(g->GetEY() != NULL && g2->GetEY() != NULL) {

                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEY()[j] = 0;
                        else g->GetEY()[j] = (g->GetY()[j]+g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEY()[j]*g->GetEY()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEY()[j]*g2->GetEY()[j]/g->GetY()[j]/g->GetY()[j]
                                        );
                }

                if(g->GetEYlow() != NULL && g2->GetEYlow() != NULL) {
                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEYlow()[j] = 0;
                        else g->GetEYlow()[j] = (g->GetY()[j]+g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEYlow()[j]*g->GetEYlow()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEYlow()[j]*g2->GetEYlow()[j]/g2->GetY()[j]/g2->GetY()[j]
                                        );
                }

                if(g->GetEYhigh() != NULL && g2->GetEYhigh() != NULL) {
                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEYhigh()[j] = 0;
                        else g->GetEYhigh()[j] = (g->GetY()[j]+g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEYhigh()[j]*g->GetEYhigh()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEYhigh()[j]*g2->GetEYhigh()[j]/g2->GetY()[j]/g2->GetY()[j]
                                        );
                }

                g->GetY()[j] += g2->GetY()[j];
        }

        UIUC::TFactory::Scale(g1, 1/c1);
        UIUC::TFactory::Scale(g2, 1/c2);
        return g;
}

TGraph* UIUC::TFactory::Subtract(TGraph* g1, TGraph* g2, double c1, double c2)
{
        if(!IsCompatible(g1, g2)) return NULL;
        UIUC::TFactory::Scale(g1, c1);
        UIUC::TFactory::Scale(g2, c2);

        TString gRandom = TString::Itoa(UIUC::GetRandom(100000),10);
        TGraph *g = (TGraph*) g1->Clone((TString) g1->GetName() + "-" + g2->GetName() + "_" + gRandom);


        for (int j = 0, J = g->GetN(); j < J; j++) {

                if(g->GetY() == NULL) continue;
                if(g2->GetY() == NULL) continue;

                if(g->GetEY() != NULL && g2->GetEY() != NULL) {

                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEY()[j] = 0;
                        else g->GetEY()[j] = (g->GetY()[j]-g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEY()[j]*g->GetEY()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEY()[j]*g2->GetEY()[j]/g->GetY()[j]/g->GetY()[j]
                                        );
                }

                if(g->GetEYlow() != NULL && g2->GetEYlow() != NULL) {
                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEYlow()[j] = 0;
                        else g->GetEYlow()[j] = (g->GetY()[j]-g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEYlow()[j]*g->GetEYlow()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEYlow()[j]*g2->GetEYlow()[j]/g2->GetY()[j]/g2->GetY()[j]
                                        );
                }

                if(g->GetEYhigh() != NULL && g2->GetEYhigh() != NULL) {
                        if(g->GetY()[j] == 0 || g2->GetY()[j] == 0) g->GetEYhigh()[j] = 0;
                        else g->GetEYhigh()[j] = (g->GetY()[j]-g2->GetY()[j]) * TMath::Sqrt(
                                        g->GetEYhigh()[j]*g->GetEYhigh()[j]/g->GetY()[j]/g->GetY()[j] +
                                        g2->GetEYhigh()[j]*g2->GetEYhigh()[j]/g2->GetY()[j]/g2->GetY()[j]
                                        );
                }

                g->GetY()[j] -= g2->GetY()[j];
        }

        UIUC::TFactory::Scale(g1, 1/c1);
        UIUC::TFactory::Scale(g2, 1/c2);

        return g;
}

//
// Average method..
//
TH1* UIUC::TFactory::Average(TH1* h, Option_t *option)
{
        if(h->InheritsFrom("TH2")) return Average((TH2*) h, option);
        else if(h->InheritsFrom("TH3")) return Average((TH3*) h, option);
        else return NULL;
}

TH1D* UIUC::TFactory::Average(TH2* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        TAxis *x1, *x2;
        if(opt == "x") {

                x1 = h0->GetYaxis();
                x2 = h0->GetXaxis();

        } else if(opt == "y") {

                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        //if(UIUC::HandboxMsg::Error(HasVariableBinWidth(x2), __METHOD_NAME__,
        //                           "Variable bin width detected.. This function cannot be called.. abort")) return NULL;

        vector<double> x1_array = {x1->GetBinLowEdge(1)};
        for(int i = 1, I = x1->GetNbins(); i <= I; i++)
                x1_array.push_back(x1->GetBinUpEdge(i));

        // Type of average calculation
        TString hname = (TString) h0->GetName() + ":" + opt + "-average";
        TH1D *h = new TH1D(UIUC::TFileReader::DirName(hname), UIUC::TFileReader::DirName(hname), x1_array.size()-1, &(x1_array[0]));
        if(h0->GetDirectory()) h->SetDirectory(h0->GetDirectory());
        else h->SetDirectory(gDirectory);
        h->GetXaxis()->SetTitle(x1->GetTitle());
        h->GetYaxis()->SetTitle(UIUC::TFactory::AddPrefixAndSuffix("<", x2->GetTitle(), ">"));
        h->GetYaxis()->SetRangeUser(x2->GetBinLowEdge(1), x2->GetBinUpEdge(x2->GetNbins()));
        h->SetTitle(h0->GetTitle());
        h->SetName(hname);

        TString eMessage = (TString) "Averaging over " + option + "-axis for " + h0->GetName();
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);
        eMessage = (TString) "Output object: " + h->GetDirectory()->GetPath() + "/" + hname;
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                // Estimation of the mean value
                vector<double> c, w, w_err;
                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        c.push_back(x2->GetBinCenter(x2_i));
                        if(opt == "x") {

                                w.push_back(h0->GetBinContent(x2_i, x1_i));
                                w_err.push_back(h0->GetBinError(x2_i, x1_i));

                        } else if(opt == "y") {

                                w.push_back(h0->GetBinContent(x1_i, x2_i));
                                w_err.push_back(h0->GetBinError(x1_i, x2_i));
                        }
                }

                vector<double> stdv;
                if(!ComputeMeanAndSV(stdv, "", c, {}, w, w_err)) return NULL;
                h->SetBinContent(x1_i, stdv[0]);
                h->SetBinError(x1_i, stdv[2]);
        }

        h->SetEntries(h0->GetEntries());
        return h;
}

TH2D* UIUC::TFactory::Average(TH3* h0, Option_t *option)
{
        if(h0 == NULL) return NULL;

        TString opt = option;
        opt.ToLower();

        TAxis *x1, *x2, *x3;
        if(opt == "x") {

                x3 = h0->GetXaxis();
                x1 = h0->GetYaxis();
                x2 = h0->GetZaxis();

        } else if(opt == "y") {

                x3 = h0->GetYaxis();
                x1 = h0->GetXaxis();
                x2 = h0->GetZaxis();

        } else if(opt == "z") {

                x3 = h0->GetZaxis();
                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        // Type of average calculation
        TString hname = (TString) h0->GetName() + ":" + opt + "-average";

        //if(UIUC::HandboxMsg::Error(HasVariableBinWidth(x3), __METHOD_NAME__,
        //                           "Variable bin width detected.. This function cannot be called.. abort")) return NULL;

        vector<double> x1_array = {x1->GetBinLowEdge(1)};
        for(int i = 1, I = x1->GetNbins(); i <= I; i++)
                x1_array.push_back(x1->GetBinUpEdge(i));

        vector<double> x2_array = {x2->GetBinLowEdge(1)};
        for(int i = 1, I = x2->GetNbins(); i <= I; i++)
                x2_array.push_back(x2->GetBinUpEdge(i));

        TH2D *h = new TH2D(UIUC::TFileReader::DirName(hname), UIUC::TFileReader::DirName(hname), x1_array.size()-1, &(x1_array[0]), x2_array.size()-1, &(x2_array[0]));
        h->SetName(hname);
        if(h0->GetDirectory()) h->SetDirectory(h0->GetDirectory());
        else h->SetDirectory(gDirectory);
        h->GetXaxis()->SetTitle(x1->GetTitle());
        h->GetYaxis()->SetTitle(x2->GetTitle());
        h->GetZaxis()->SetTitle(UIUC::TFactory::AddPrefixAndSuffix("<", x3->GetTitle(), ">"));

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, (TString) "Compute average histogram over " + opt + "-axis for " + h0->GetName());
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) "==> Output object: " + TString(h->GetDirectory()->GetPath()) + "/" + hname);

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        // Estimation of the mean value
                        vector<double> c, w, w_err;
                        for(int x3_i = 1, x3_N = x3->GetNbins(); x3_i <= x3_N; x3_i++) {

                                c.push_back(x3->GetBinCenter(x3_i));
                                if(opt == "x") {
                                        w.push_back(h0->GetBinContent(x3_i, x1_i, x2_i));
                                        w_err.push_back(h0->GetBinError(x3_i, x1_i, x2_i));
                                } else if(opt == "y") {
                                        w.push_back(h0->GetBinContent(x1_i, x3_i, x2_i));
                                        w_err.push_back(h0->GetBinError(x1_i, x3_i, x2_i));
                                } else if(opt == "z") {
                                        w.push_back(h0->GetBinContent(x1_i, x2_i, x3_i));
                                        w_err.push_back(h0->GetBinError(x1_i, x2_i, x3_i));
                                }
                        }

                        vector<double> stdv;
                        ComputeMeanAndSV(stdv, "", c, {}, w, w_err);
                        h->SetBinContent(x1_i, x2_i, stdv[0]);
                        h->SetBinError(x1_i, x2_i, stdv[2]);
                }
        }

        h->SetEntries(h0->GetEntries());
        return h;
}


//
// AVERAGE BIN PER BIN !
//

TH1* UIUC::TFactory::AverageBinContent(TH1* h0, Option_t *option, Option_t* option2, Option_t* option3)
{
        if(h0->InheritsFrom("TH2")) return AverageBinContent((TH2*) h0, option, option2, option3);
        else if(h0->InheritsFrom("TH3")) return AverageBinContent((TH3*) h0, option, option2, option3);
        else return NULL;
}

TH1* UIUC::TFactory::AverageBinContent(TList* hList0,  Option_t* option2, Option_t* option3)
{
        TList *hList = UIUC::TFileReader::ReadObj(hList0);
        if(hList == NULL || hList->GetSize() == 0) return NULL;

        TString opt2 = option2;
        opt2.ToLower();
        TString opt3 = option3;
        opt3.ToLower();

        TH1 *h = NULL;
        if(hList->At(0)->InheritsFrom("TH2")) {

                TH3D *h0 = UIUC::TFactory::CreateTH3chain(GetRandomName((TString) hList->At(0)->GetName()), hList->At(0)->GetTitle(), hList);
                h = AverageBinContent(h0, "x", opt2, opt3);

                delete h0;
                h0 = NULL;

        } else if(hList->At(0)->InheritsFrom("TH1")) {

                TH2D *h0 = UIUC::TFactory::CreateTH2chain(GetRandomName((TString) hList->At(0)->GetName()), hList->At(0)->GetTitle(), hList);
                h = AverageBinContent(h0, "x", opt2, opt3);

                delete h0;
                h0 = NULL;

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Average using TH3 list is not implemented.. skip.. " + h->GetName());
                return NULL;
        }

        if(!opt2.EqualTo("")) opt2 = "-" + opt2;
        if(!opt3.EqualTo("")) opt3 = "-" + opt3;
        if(h != NULL) h->SetName(hList->At(0)->GetName() + TString(":x-averagebinc"+opt2+opt3));
        delete hList;
        hList = NULL;

        return h;
}

TH1D* UIUC::TFactory::AverageBinContent(TH2* h0, Option_t *option, Option_t* option2, Option_t* option3)
{
        TString opt = option;
        opt.ToLower();
        TString opt2 = option2;
        opt2.ToLower();
        TString opt3 = option3;
        opt3.ToLower();

        TAxis *x1, *x2;
        if(opt == "x") {

                x1 = h0->GetYaxis();
                x2 = h0->GetXaxis();

        } else if(opt == "y") {

                x2 = h0->GetYaxis();
                x1 = h0->GetXaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\")");
                return NULL;
        }

        vector<double> x1_array = {x1->GetBinLowEdge(1)};
        for(int i = 1, I = x1->GetNbins(); i <= I; i++)
                x1_array.push_back(x1->GetBinUpEdge(i));

        // Type of average calculation
        if(!opt2.EqualTo("")) opt2 = "-" + opt2;
        if(!opt3.EqualTo("")) opt3 = "-" + opt3;
        TString hname = (TString) h0->GetName() + ":" + opt + "-averagebinc" + opt2 + "" + opt3;

        TH1D *h = new TH1D(UIUC::TFileReader::DirName(hname), UIUC::TFileReader::DirName(hname), x1_array.size()-1, &(x1_array[0]) );
        if(h0->GetDirectory()) h->SetDirectory(h0->GetDirectory());
        else h->SetDirectory(gDirectory);
        h->GetXaxis()->SetTitle(x1->GetTitle());
        h->SetTitle(h0->GetTitle());
        h->SetName(hname);

        TString eMessage = (TString) "Averaging over " + option + "-axis using options ("+opt2+","+opt3+") for " + h0->GetName();
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);
        eMessage = (TString) "==> Output object: " + h->GetDirectory()->GetPath() + "/" + hname;
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                vector<double> c, c_err, w;
                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        w.push_back(x2->GetBinWidth(x2_i));
                        if(opt == "x") {

                                c.push_back(h0->GetBinContent(x2_i, x1_i));
                                c_err.push_back(h0->GetBinError(x2_i, x1_i));

                        } else if(opt == "y") {

                                c.push_back(h0->GetBinContent(x1_i, x2_i));
                                c_err.push_back(h0->GetBinError(x1_i, x2_i));
                        }
                }

                vector<double> stdv;
                if( !ComputeMeanAndSV(stdv, opt2, c, c_err, w) ) return NULL;

                h->SetBinContent(x1_i, stdv[0]);
                if(opt3.Contains("stderr")) h->SetBinError(x1_i, stdv[3]);
                else if(opt3.Contains("stddev")) h->SetBinError(x1_i, stdv[2]);
                else if(opt3.EqualTo("")) h->SetBinError(x1_i, stdv[1]);
                else UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unknown option specified \""+opt3+"\" for " + h0->GetName());
        }


        h->SetEntries(h0->GetEntries());
        return h;
}

TH2D* UIUC::TFactory::AverageBinContent(TH3* h0, Option_t *option,  Option_t *option2,  Option_t *option3)
{
        TString opt = option;
        opt.ToLower();
        TString opt2 = option2;
        opt2.ToLower();
        TString opt3 = option3;
        opt3.ToLower();

        TAxis *x1, *x2, *x3;
        if(opt == "x") {

                x3 = h0->GetXaxis();
                x1 = h0->GetYaxis();
                x2 = h0->GetZaxis();

        } else if(opt == "y") {

                x3 = h0->GetYaxis();
                x1 = h0->GetXaxis();
                x2 = h0->GetZaxis();

        } else if(opt == "z") {

                x3 = h0->GetZaxis();
                x1 = h0->GetXaxis();
                x2 = h0->GetYaxis();

        } else {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\", \"z\")");
                return NULL;
        }

        // Type of average calculation
        if(!opt2.EqualTo("")) opt2 = "-" + opt2;
        if(!opt3.EqualTo("")) opt3 = "-" + opt3;
        TString hname = (TString) h0->GetName() + ":" + opt + "-averagebinc-" + opt2 + "-" + opt3;

        vector<double> x1_array = {x1->GetBinLowEdge(1)};
        for(int i = 1, I = x1->GetNbins(); i <= I; i++)
                x1_array.push_back(x1->GetBinUpEdge(i));

        vector<double> x2_array = {x2->GetBinLowEdge(1)};
        for(int i = 1, I = x2->GetNbins(); i <= I; i++)
                x2_array.push_back(x2->GetBinUpEdge(i));

        TH2D *h = new TH2D(UIUC::TFileReader::DirName(hname), UIUC::TFileReader::DirName(hname),x1_array.size()-1, &(x1_array[0]), x2_array.size()-1, &(x2_array[0]));
        h->SetName(hname);
        if(h0->GetDirectory()) h->SetDirectory(h0->GetDirectory());
        else h->SetDirectory(gDirectory);
        h->GetXaxis()->SetTitle(x1->GetTitle());
        h->GetYaxis()->SetTitle(x2->GetTitle());

        TString eMessage = (TString) "Averaging over " + option + "-axis using options ("+opt2+","+opt3+") for " + h0->GetName();
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);
        eMessage = (TString) "==> Output object: " + h->GetDirectory()->GetPath() + "/" + hname;
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);

        for(int x1_i = 1, x1_N = x1->GetNbins(); x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1, x2_N = x2->GetNbins(); x2_i <= x2_N; x2_i++) {

                        vector<double> c, c_err,w;
                        for(int x3_i = 1, x3_N = x3->GetNbins(); x3_i <= x3_N; x3_i++) {

                                w.push_back(x3->GetBinWidth(x3_i));
                                if(opt == "x") {
                                        c.push_back(h0->GetBinContent(x3_i, x1_i, x2_i));
                                        c_err.push_back(h0->GetBinError(x3_i, x1_i, x2_i));
                                } else if (opt == "y") {
                                        c.push_back(h0->GetBinContent(x1_i, x3_i, x2_i));
                                        c_err.push_back(h0->GetBinError(x1_i, x3_i, x2_i));
                                } else if (opt == "z") {
                                        c.push_back(h0->GetBinContent(x1_i, x2_i, x3_i));
                                        c_err.push_back(h0->GetBinError(x1_i, x2_i, x3_i));
                                }
                        }

                        vector<double> stdv;
                        if( !ComputeMeanAndSV(stdv, opt2, c, c_err, w, {}) ) return NULL;

                        h->SetBinContent(x1_i, x2_i, stdv[0]);
                        if(opt3.Contains("stderr")) h->SetBinError(x1_i,  x2_i, stdv[3]);
                        else if(opt3.Contains("stddev")) h->SetBinError(x1_i,  x2_i, stdv[2]);
                        else if(opt3.EqualTo("")) h->SetBinError(x1_i,  x2_i, stdv[1]);
                        else UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unknown option specified \""+opt3+"\" for " + h0->GetName());

                }
        }

        h->SetEntries(h0->GetEntries());
        return h;
}

TGraph* UIUC::TFactory::Sort(TGraph* g0, Option_t *option)
{
        TString opt = option;
        opt.ToLower();

        vector<double> x0;
        for(int i = 0, N = g0->GetN(); i < N; i++) x0.push_back(g0->GetX()[i]);
        vector<int> index = sort_indexes(x0);

        vector<double> x, y, ey, eyl, eyh;
        for(int i = 0, N = index.size(); i < N; i++) {

                if(g0->GetX()) x.push_back(g0->GetX()[index[i]]);
                if(g0->GetY()) y.push_back(g0->GetY()[index[i]]);

                if(g0->GetEY()) ey.push_back(g0->GetEY()[index[i]]);
                if(g0->GetEYlow()) eyl.push_back(g0->GetEYlow()[index[i]]);
                if(g0->GetEYhigh()) eyh.push_back(g0->GetEYhigh()[index[i]]);
        }

        TGraph *g = NULL;
        if(g0->InheritsFrom("TGraphAsymmErrors")) g = (TGraph*) new TGraphAsymmErrors(x.size(), &x[0], &y[0], NULL, NULL, &eyl[0], &eyh[0]);
        else if(g0->InheritsFrom("TGraphErrors")) g = (TGraph*) new TGraphErrors(x.size(), &x[0], &y[0], NULL, &ey[0]);
        else g = new TGraph(x.size(), &x[0], &y[0]);

        if(g != NULL)
                g->SetName((TString) g0->GetName() + ":" + opt + "-sort");

        return g;
}

TGraph* UIUC::TFactory::Rebin(TGraph* g0, int rebin, Option_t *option)
{
        TString opt = option;
        opt.ToLower();

        bool bAverage = false;
        if(opt.Contains("average")) bAverage = true;

        if(g0 == NULL) return NULL;

        TString axis_str;
        if(rebin != 0 && rebin != 1) axis_str += "x";

        TString gname_suffix = ":rebin";
        gname_suffix += (bAverage) ? "-average" : "";
        if(!axis_str.EqualTo("")) gname_suffix += "-" + axis_str;

        if(rebin == 0 || rebin == 1) return (TGraph*) g0->Clone(g0->GetName() + gname_suffix);

        g0 = UIUC::TFactory::Sort(g0, option);

        vector<vector<double> > x(g0->GetN()/rebin),y(g0->GetN()/rebin),ey(g0->GetN()/rebin),eyl(g0->GetN()/rebin),eyh(g0->GetN()/rebin);
        for(int i = 0, N = g0->GetN(); i < N; i++) {

                if(opt.Contains("x")) {

                        if(g0->GetX()) x[i/rebin].push_back(g0->GetX()[i] / rebin);
                        if(g0->GetY()) y[i/rebin].push_back(g0->GetY()[i]);

                        if(g0->GetEY()) ey[i/rebin].push_back(g0->GetEY()[i]);
                        if(g0->GetEYlow()) eyl[i/rebin].push_back(g0->GetEYlow()[i]);
                        if(g0->GetEYhigh()) eyh[i/rebin].push_back(g0->GetEYhigh()[i]);

                } else {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Rebin along y not implemented..");
                        return NULL;
                }
        }

        vector<double> x0(g0->GetN()/rebin),y0(g0->GetN()/rebin),ey0(g0->GetN()/rebin),eyl0(g0->GetN()/rebin),eyh0(g0->GetN()/rebin);
        for(int i = 0, N = g0->GetN()/rebin; i < N; i++) {

                double weight = (bAverage) ? rebin : 1;
                x0[i] = UIUC::SumX(x[i]);
                y0[i] = TMath::Sqrt(UIUC::SumX(y[i])) / weight;
                ey0[i] = TMath::Sqrt(UIUC::SumX2(ey[i])) / weight;
                eyl0[i] = TMath::Sqrt(UIUC::SumX2(eyl[i])) / weight;
                eyh0[i] = TMath::Sqrt(UIUC::SumX2(eyh[i])) / weight;
        }

        TGraph *g = NULL;
        if(g0->InheritsFrom("TGraphAsymmErrors")) g = (TGraph*) new TGraphAsymmErrors(x0.size(), &x0[0], &y0[0], NULL, NULL, &eyl0[0], &eyh0[0]);
        else if(g0->InheritsFrom("TGraphErrors")) g = (TGraph*) new TGraphErrors(x0.size(), &x0[0], &y0[0], NULL, &ey0[0]);
        else g = new TGraph(x.size(), &x0[0], &y0[0]);

        if(g != NULL)
                g->SetName(g0->GetName() + gname_suffix);

        return g;
}

TH1* UIUC::TFactory::Rebin(TH1* h0, vector<int> vRebin, Option_t *option)
{
        TString opt = option;
        opt.ToLower();

        bool bAverage = false;
        if(opt.Contains("average")) bAverage = true;
        bool bWidth = false;
        if(opt.Contains("width")) bWidth = true;

        if(h0 == NULL) return NULL;

        TString axis_str;
        if(vRebin[0] != 0 && vRebin[0] != 1) axis_str += "x";
        if(vRebin[1] != 0 && vRebin[1] != 1) axis_str += "y";
        if(vRebin[2] != 0 && vRebin[2] != 1) axis_str += "z";

        TString hname_suffix = ":rebin";
        hname_suffix += (bAverage) ? "-averagebinc" : "";
        hname_suffix += (bWidth)   ? "-width" : "";
        if(!axis_str.EqualTo("")) hname_suffix += "-" + axis_str;

        TH1 *h = CopyStruct(h0, "", h0->GetTitle(), vRebin);
        if(h != NULL) {
                h->SetName(h0->GetName() + hname_suffix);
                h->SetDirectory(h0->GetDirectory());
        }

        UIUC::TFactory::CopyPaste(h0, h, opt);

        return h;
}

TH1* UIUC::TFactory::Sum(TList* hList0)
{
        TList *hList = UIUC::TFileReader::ReadObj(hList0);
        if(hList == NULL || hList->GetSize() == 0) return NULL;

        for(int i = 1; i < hList->GetSize(); i++) {

                TH1 *h0 = (TH1*) hList->At(0);
                TH1 *hi = (TH1*) hList->At(i);

                if(!UIUC::TFactory::CompareStruct(h0, hi) || !UIUC::TFactory::CompareStruct(hi, h0)) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) (TString) "\""+h0->GetName()+"\" not compatible with \""+hi->GetName()+"\"");
                        return NULL;
                }
        }

        if(!hList->At(0)->InheritsFrom("TH1")) {

                cerr << "Object type not supported.." << endl;
                return NULL;
        }

        TH1 *h = CopyStruct((TH1*) hList->At(0));
        if(!h->GetSumw2N()) h->Sumw2();
        if(h != NULL) {

                h->SetName(hList->At(0)->GetName());
                for(int i = 0; i < hList->GetSize(); i++) {

                        TH1 *hi = (TH1*) hList->At(i);

                        h->Add(hi);
                }
        }

        if(h != NULL) h->SetName(hList->At(0)->GetName() + TString(":sum"));

        delete hList;
        hList = NULL;

        return h;
}

int UIUC::TFactory::Load(TString rootfile_set) {

        TObjArray *rootfile_array = rootfile_set.Tokenize(" ");
        TDirectory *dir0 = gDirectory;

        int NFiles = 0;
        for (int i = 0; i < rootfile_array->GetEntries(); i++) {

                TDirectory *dir = dir0;
                TString rootfile = ((TObjString *)(rootfile_array->At(i)))->String();
                TString rootfile_objname = UIUC::TFileReader::Obj(rootfile);
                TString rootfile_url = UIUC::TFileReader::Url(rootfile);

                TFile *f = TFile::Open(rootfile_url);
                if(f != NULL) {

                        dir->mkdir(gSystem->DirName(rootfile_objname));
                        dir->cd(gSystem->DirName(rootfile_objname));

                        TH1 *h = (TH1*) f->Get(rootfile_objname);
                        if(h->InheritsFrom("TH1")) {

                                h->SetDirectory(dir);
                                this->Add(h);
                                NFiles++;
                        }
                        f->Close();
                }
        }

        delete rootfile_array;
        rootfile_array = NULL;

        cout << NFiles << " object(s) loaded" << endl;
        return NFiles;
}


bool UIUC::TFactory::FillWithError(TH1* h, double x, double weight, double weight_error)
{
        if(h == NULL) return false;

        int bin = h->FindBin(x);
        if(bin == 0 || bin == h->GetNbinsX()+1) return false;

        h->SetBinError(bin, TMath::Sqrt(h->GetBinError(bin)*h->GetBinError(bin) + weight_error*weight_error));
        h->SetBinContent(bin, h->GetBinContent(bin) + weight);
        h->SetEntries(h->GetEntries() + 1);

        return true;
}

TString UIUC::TFactory::AddSuffix(TString str, TString suffix)
{
        int index;
        if(suffix.BeginsWith("_{")) {

                TPRegexp r1("_{.*} \\[");
                TPRegexp r2("_{.*} \\(");
                if(str.Index(r1) != -1 || str.Index(r2) != -1) {

                        str.ReplaceAll("} (", ", (");
                        str.ReplaceAll("} [", ", [");
                        suffix = suffix(2,suffix.Length()-2);
                }

                TPRegexp r3("_{.*}$");
                if(str.Index(r3) != -1) {

                        str.ReplaceAll("}", ",");
                        str.ReplaceAll("}", ",");
                        suffix = suffix(2,suffix.Length()-2);
                }
        }

        if((index = str.Index(" (")) != -1) str = str(0,index) + suffix + str(index,str.Length()-index+1);
        else if((index = str.Index(" [")) != -1) str = str(0,index) + suffix + str(index,str.Length()-index+1);
        else str = str + suffix;

        return str;
}

TString UIUC::TFactory::KeepOnlyUnits(TString str)
{
        int index;

        if((index = str.Index(" (")) != -1) str = str(index+1,str.Length()-1);
        else if((index = str.Index(" [")) != -1) str = str(index+1,str.Length()-1);
        else str = "";

        return str;
}

TString UIUC::TFactory::RemoveUnits(TString str)
{
        int index;

        if((index = str.Index(" (")) != -1) str = str(0,index);
        else if((index = str.Index(" [")) != -1) str = str(0,index);
        else str = str;

        return str;
}
TString UIUC::TFactory::AddPrefixAndSuffix(TString prefix, TString str, TString suffix) {

        return AddSuffix(AddPrefix(str,prefix),suffix);
}

TString UIUC::TFactory::AddPrefix(TString str, TString prefix) {

        return prefix + str;
}

bool UIUC::TFactory::FillWithError(TH2* h, double x, double y, double weight, double weight_error)
{
        if(h == NULL) return false;

        int bin = h->FindBin(x,y);
        if(h->IsBinUnderflow(bin) || h->IsBinOverflow(bin)) return false;

        h->SetBinError(bin, TMath::Sqrt(h->GetBinError(bin)*h->GetBinError(bin) + weight_error*weight_error));
        h->SetBinContent(bin, h->GetBinContent(bin) + weight);
        h->SetEntries(h->GetEntries() + 1);

        return true;
}

bool UIUC::TFactory::FillWithError(TH3* h, double x, double y, double z, double weight, double weight_error)
{
        if(h == NULL) return false;

        int bin = h->FindBin(x,y,z);
        if(h->IsBinUnderflow(bin) || h->IsBinOverflow(bin)) return false;

        h->SetBinError(bin, TMath::Sqrt(h->GetBinError(bin)*h->GetBinError(bin) + weight_error*weight_error));
        h->SetBinContent(bin, h->GetBinContent(bin) + weight);
        h->SetEntries(h->GetEntries() + 1);

        return true;
}

/**
 * \brief Add TH1 to the factory
 *
 * \param h TH1 to store (Do NOT copy, just storing the object in a TList)
 *
 * \return true or false
 */

bool UIUC::TFactory::Add(TH1 *h) {

        if(h == NULL) {

                cerr << "Warning from <UIUC::TFactory::Add>: TH1 passed as argument is NULL" << endl;
                return false;
        }

        for(int i = 0; i < (int) this->hList->GetSize(); i++) {

                if(this->hList->At(i) == h) {

                        cerr << "Warning from <UIUC::TFactory::Add>: A TH1 with name \"" << h->GetName() << "\" already stored in the list.. skip" << endl;
                        return false;
                }
        }

        //cout << "Info from <UIUC::TFactory::Add>: " << h->GetName() << " added to the list (" << this->hList->GetSize() << " entries)" << endl;
        this->hList->Add(h);
        return true;
}

/**
 * \brief Remove a TH1 from the factory
 *
 * \param name Name of the histogram
 *
 * \return true or false
 */

bool UIUC::TFactory::Remove(TString name) {
        for(int i = 0; i < (int) this->hList->GetSize(); i++) {

                if(TString(this->hList->At(i)->GetName()).CompareTo(name) == 0) {

                        this->hList->Remove(this->hList->At(i));
                        return true;
                }
        }

        cerr << "Warning from <UIUC::TFactory::Remove>: No TH1 found with name \"" << name << "\" in the list.. skip" << endl;
        return false;
}

TH1* UIUC::TFactory::ScaleX(TH1 *h, double scaleX, Option_t *option)
{
        if(h == NULL) return NULL;

        TString opt = option;
                opt.ToLower();

        TAxis *axis = NULL;
        if(opt.Contains("x")) axis = h->GetXaxis();
        else if(opt.Contains("y")) axis = h->GetYaxis();
        else if(opt.Contains("z")) axis = h->GetZaxis();
        if(axis == NULL) {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
                return NULL;
        }

        axis->Set(axis->GetNbins(), axis->GetXmin()*scaleX, axis->GetXmax()*scaleX);
        // if(opt.Contains("x")) {

        //         h->SetBins(axis->Get, axis->GetXmin(), axis->GetXmax()*scaleX);

        // } else if(opt.Contains("y")) {

        //         h->SetBins(h->GetXaxis()->GetNbins(), h->GetXaxis()->GetXmin(), h->GetXaxis()->GetXmax(), 
        //                 axis->Get, axis->GetXmin(), axis->GetXmax()*scaleX
        //         );

        // } else if(opt.Contains("z")) {

        //         h->SetBins(h->GetXaxis()->GetNbins(), h->GetXaxis()->GetXmin(), h->GetXaxis()->GetXmax(), 
        //                 h->GetYaxis()->GetNbins(), h->GetYaxis()->GetXmin(), h->GetYaxis()->GetXmax(), 
        //                 axis->Get, axis->GetXmin(), axis->GetXmax()*scaleX
        //         );

        // } else {

        //         UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
        //         return NULL;
        // }

        return h;
}

TH1* UIUC::TFactory::ScaleY(TH1 *h, double scaleY)
{
        if(h == NULL) return NULL;
        
        h->Scale(scaleY);
        return h;
}

/**
 * \brief Normalize histogram
 *
 */
TH1* UIUC::TFactory::Normalize(TH1 *h, TString normalization_str, int type) {

        TFormula *formula = new TFormula("normalization", normalization_str);
        double normalization = formula->Eval(0);
        delete formula;

        return Normalize(h, normalization, type);
}

TH1* UIUC::TFactory::NormalizeWithError(TH1 *h, TString normalization_str, TString normalization_err_str) {

        TFormula *formula = new TFormula("normalization", normalization_str);
        double normalization = formula->Eval(0);

        TFormula *formula_err = new TFormula("normalization_err", normalization_err_str);
        double normalization_err = formula_err->Eval(0);

        delete formula;
        formula = NULL;

        delete formula_err;
        formula_err = NULL;

        return NormalizeWithError(h, normalization, normalization_err);
}

TH1* UIUC::TFactory::NormalizeWithError(TH1 *h, double normalization, double normalization_err)
{
        if( UIUC::HandboxMsg::Warning(normalization == 0, __METHOD_NAME__, "Normalization factor cannot be 0..") ) return NULL;
        if(h->InheritsFrom("TH3")) {

                for(int x1_i = 1, x1_N = h->GetNbinsX(); x1_i <= x1_N; x1_i++) {
                        for(int x2_i = 1, x2_N = h->GetNbinsY(); x2_i <= x2_N; x2_i++) {
                                for(int x3_i = 1, x3_N = h->GetNbinsZ(); x3_i <= x3_N; x3_i++) {

                                        double content = h->GetBinContent(x1_i, x2_i, x2_i);
                                        double content_err = h->GetBinError(x1_i, x2_i, x3_i);

                                        if( !UIUC::EpsilonEqualTo(content,0) ) {

                                                h->SetBinContent(x1_i, x2_i, x3_i,   content/normalization);
                                                h->SetBinError(x1_i, x2_i, x3_i, content/normalization * TMath::Sqrt(TMath::Power(content_err/content,2)+TMath::Power(normalization_err/normalization,2)) );
                                        }
                                }
                        }
                }

        } else if(h->InheritsFrom("TH2")) {

                for(int x1_i = 1, x1_N = h->GetNbinsX(); x1_i <= x1_N; x1_i++) {
                        for(int x2_i = 1, x2_N = h->GetNbinsY(); x2_i <= x2_N; x2_i++) {

                                double content = h->GetBinContent(x1_i, x2_i);
                                double content_err = h->GetBinError(x1_i, x2_i);

                                if( !UIUC::EpsilonEqualTo(content,0) ) {

                                        h->SetBinContent(x1_i, x2_i,   content/normalization);
                                        h->SetBinError(x1_i, x2_i, content/normalization * TMath::Sqrt(TMath::Power(content_err/content,2)+TMath::Power(normalization_err/normalization,2)) );
                                }
                        }
                }

        } else if(h->InheritsFrom("TH1")) {

                for(int x1_i = 1, x1_N = h->GetNbinsX(); x1_i <= x1_N; x1_i++) {

                        double content = h->GetBinContent(x1_i);
                        double content_err = h->GetBinError(x1_i);

                        if( !UIUC::EpsilonEqualTo(content,0) ) {

                                h->SetBinContent(x1_i, content/normalization);
                                h->SetBinError(x1_i, content/normalization * TMath::Sqrt(TMath::Power(content_err/content,2)+TMath::Power(normalization_err/normalization,2)) );
                        }
                }

        }

        return h;
}

bool UIUC::TFactory::SetAllBinError(TH1 *h1, double content_err)
{
        int ibin1;
        int ix1,iy1,iz1;
        int binx,biny,binz;
        double bx,by,bz;

        //loop on all bins and refill
        int nbinsx1 = h1->GetNbinsX();
        int nbinsy1 = h1->GetNbinsY();
        int nbinsz1 = h1->GetNbinsZ();

        for (binz=1; binz <= nbinsz1; binz++) {

                bz  = h1->GetZaxis()->GetBinCenter(binz);
                iz1 = h1->GetZaxis()->FindBin(bz);

                for (biny=1; biny <= nbinsy1; biny++) {

                        by  = h1->GetYaxis()->GetBinCenter(biny);
                        iy1 = h1->GetYaxis()->FindBin(by);

                        for (binx=1; binx <= nbinsx1; binx++) {

                                bx  = h1->GetXaxis()->GetBinCenter(binx);
                                ix1 = h1->GetXaxis()->FindBin(bx);

                                ibin1 = h1->GetBin(ix1,iy1,iz1);

                                h1->SetBinError(ibin1, content_err);
                        }
                }
        }

        return true;
}

bool UIUC::TFactory::SetAllBinContent(TH1 *h1, double content)
{
        int ibin1;
        int ix1,iy1,iz1;
        int binx,biny,binz;
        double bx,by,bz;

        //loop on all bins and refill
        int nbinsx1 = h1->GetNbinsX();
        int nbinsy1 = h1->GetNbinsY();
        int nbinsz1 = h1->GetNbinsZ();

        for (binz=1; binz <= nbinsz1; binz++) {

                bz  = h1->GetZaxis()->GetBinCenter(binz);
                iz1 = h1->GetZaxis()->FindBin(bz);

                for (biny=1; biny <= nbinsy1; biny++) {

                        by  = h1->GetYaxis()->GetBinCenter(biny);
                        iy1 = h1->GetYaxis()->FindBin(by);

                        for (binx=1; binx <= nbinsx1; binx++) {

                                bx  = h1->GetXaxis()->GetBinCenter(binx);
                                ix1 = h1->GetXaxis()->FindBin(bx);

                                ibin1 = h1->GetBin(ix1,iy1,iz1);
                                h1->SetBinContent(ibin1, content);
                        }
                }
        }

        return true;
}

TH1* UIUC::TFactory::DivideBinContent(TH1 *h1, TH1 *h2)
{
        int ibin1,ibin2;
        int ix1,iy1,iz1;
        int ix2,iy2,iz2;
        int binx,biny,binz;
        double bx,by,bz;

        //loop on all bins and refill
        int nbinsx1 = h1->GetNbinsX();
        int nbinsy1 = h1->GetNbinsY();
        int nbinsz1 = h1->GetNbinsZ();

        int N = h1->GetEntries();
        for (binz=1; binz <= nbinsz1; binz++) {

                bz  = h1->GetZaxis()->GetBinCenter(binz);
                iz1 = h1->GetZaxis()->FindBin(bz);
                iz2 = h2->GetZaxis()->FindBin(bz);

                for (biny=1; biny <= nbinsy1; biny++) {

                        by  = h1->GetYaxis()->GetBinCenter(biny);
                        iy1 = h1->GetYaxis()->FindBin(by);
                        iy2 = h2->GetYaxis()->FindBin(by);
                        for (binx=1; binx <= nbinsx1; binx++) {

                                bx  = h1->GetXaxis()->GetBinCenter(binx);
                                ix1 = h1->GetXaxis()->FindBin(bx);
                                ix2 = h2->GetXaxis()->FindBin(bx);

                                ibin1 = h1->GetBin(ix1,iy1,iz1);
                                ibin2 = h2->GetBin(ix2,iy2,iz2);

                                double normalization = h2->GetBinContent(ibin2);
                                if(normalization != 0) h1->SetBinContent(ibin1, h1->GetBinContent(ibin1) / normalization);
                                else h1->SetBinContent(ibin1, 0);
                        }
                }
        }

        h1->SetEntries(N);
        return h1;
}

TH1* UIUC::TFactory::DivideBinError(TH1 *h1, TH1 *h2)
{
        int ibin1,ibin2;
        int ix1,iy1,iz1;
        int ix2,iy2,iz2;
        int binx,biny,binz;
        double bx,by,bz;

        //loop on all bins and refill
        int nbinsx1 = h1->GetNbinsX();
        int nbinsy1 = h1->GetNbinsY();
        int nbinsz1 = h1->GetNbinsZ();

        int N = h1->GetEntries();
        for (binz=1; binz <= nbinsz1; binz++) {

                bz  = h1->GetZaxis()->GetBinCenter(binz);
                iz1 = h1->GetZaxis()->FindBin(bz);
                iz2 = h2->GetZaxis()->FindBin(bz);

                for (biny=1; biny <= nbinsy1; biny++) {

                        by  = h1->GetYaxis()->GetBinCenter(biny);
                        iy1 = h1->GetYaxis()->FindBin(by);
                        iy2 = h2->GetYaxis()->FindBin(by);

                        for (binx=1; binx <= nbinsx1; binx++) {

                                bx  = h1->GetXaxis()->GetBinCenter(binx);
                                ix1 = h1->GetXaxis()->FindBin(bx);
                                ix2 = h2->GetXaxis()->FindBin(bx);

                                ibin1 = h1->GetBin(ix1,iy1,iz1);
                                ibin2 = h2->GetBin(ix2,iy2,iz2);

                                double normalization_err = h2->GetBinError(ibin2);
                                if(normalization_err != 0) h1->SetBinError(ibin1, h1->GetBinError(ibin1) / normalization_err);
                                else h1->SetBinError(ibin1, 0);
                        }
                }
        }

        h1->SetEntries(N);
        return h1;
}


TH1* UIUC::TFactory::DivideWithoutErrorPropagation(TH1 *h1, TH1 *h2)
{
        int ibin1,ibin2;
        int ix1,iy1,iz1;
        int ix2,iy2,iz2;
        int binx,biny,binz;
        double bx,by,bz;

        //loop on all bins and refill
        int nbinsx1 = h1->GetNbinsX();
        int nbinsy1 = h1->GetNbinsY();
        int nbinsz1 = h1->GetNbinsZ();

        int N = h1->GetEntries();
        for (binz=1; binz <= nbinsz1; binz++) {

                bz  = h1->GetZaxis()->GetBinCenter(binz);
                iz1 = h1->GetZaxis()->FindBin(bz);
                iz2 = h2->GetZaxis()->FindBin(bz);

                for (biny=1; biny <= nbinsy1; biny++) {

                        by  = h1->GetYaxis()->GetBinCenter(biny);
                        iy1 = h1->GetYaxis()->FindBin(by);
                        iy2 = h2->GetYaxis()->FindBin(by);

                        for (binx=1; binx <= nbinsx1; binx++) {

                                bx  = h1->GetXaxis()->GetBinCenter(binx);
                                ix1 = h1->GetXaxis()->FindBin(bx);
                                ix2 = h2->GetXaxis()->FindBin(bx);

                                ibin1 = h1->GetBin(ix1,iy1,iz1);
                                ibin2 = h2->GetBin(ix2,iy2,iz2);

                                double normalization = h2->GetBinContent(ibin2);
                                if(normalization != 0) {

                                        h1->SetBinError(ibin1, h1->GetBinError(ibin1) / normalization);
                                        h1->SetBinContent(ibin1, h1->GetBinContent(ibin1) / normalization);

                                } else {

                                        h1->SetBinError(ibin1, 0);
                                        h1->SetBinContent(ibin1, 0);
                                }
                        }
                }
        }

        h1->SetEntries(N);
        return h1;
}

TH1* UIUC::TFactory::MultiplyWithoutErrorPropagation(TH1 *h1, TH1 *h2)
{
        if(h1 == NULL || h2 == NULL) return NULL;
        if(h1->GetEntries() != h2->GetEntries()) return NULL;

        int N = h1->GetEntries();

        for (int bin = 0; bin <= h1->GetNcells(); ++bin) {

                if(h1->IsBinOverflow(bin)) continue;
                if(h1->IsBinUnderflow(bin)) continue;

                double N = h2->GetBinContent(bin);
                if(N != 0) {

                        h1->SetBinContent(bin, N * h1->GetBinContent(bin));
                        h1->SetBinError  (bin, N * h1->GetBinError(bin));
                }
        }

        h1->SetEntries(N);
        return h1;
}

TH1* UIUC::TFactory::Normalize(TH1 *h, double normalization, int type) {

        if(h == NULL) return NULL;
        if(normalization == 0) return NULL;

        double content_normalization, content_err_normalization;
        switch(type) {

        case UIUC::TFactory::kRepetitionAxis: // Used when doing average
                content_normalization = normalization;
                content_err_normalization = TMath::Sqrt(normalization);
                break;

        case UIUC::TFactory::kSimpleAxis:
        default: // Used when normalizing by a constant

                content_normalization = normalization;
                content_err_normalization = normalization;
                break;
        }


        int N = h->GetEntries();
        if(h->InheritsFrom("TH3")) {

                for(int x1_i = 1, x1_N = h->GetNbinsX(); x1_i <= x1_N; x1_i++) {

                        for(int x2_i = 1, x2_N = h->GetNbinsY(); x2_i <= x2_N; x2_i++) {

                                for(int x3_i = 1, x3_N = h->GetNbinsZ(); x3_i <= x3_N; x3_i++) {

                                        double content     = h->GetBinContent(x1_i, x2_i, x2_i);
                                        double content_err = h->GetBinError(x1_i, x2_i, x3_i);

                                        if(content_normalization     != 0) h->SetBinContent(x1_i, x2_i, x3_i, content / content_normalization);
                                        if(content_err_normalization != 0) h->SetBinError(x1_i, x2_i, x3_i, content_err / content_err_normalization);
                                }
                        }
                }

        } else if(h->InheritsFrom("TH2")) {

                for(int x1_i = 1, x1_N = h->GetNbinsX(); x1_i <= x1_N; x1_i++) {
                        for(int x2_i = 1, x2_N = h->GetNbinsY(); x2_i <= x2_N; x2_i++) {

                                double content     = h->GetBinContent(x1_i, x2_i);
                                double content_err = h->GetBinError(x1_i, x2_i);

                                if(content_normalization     != 0) h->SetBinContent(x1_i, x2_i, content / content_normalization);
                                if(content_err_normalization != 0) h->SetBinError(x1_i, x2_i, content_err / content_err_normalization);
                        }
                }

        } else if(h->InheritsFrom("TH1")) {

                for(int x1_i = 1, x1_N = h->GetNbinsX(); x1_i <= x1_N; x1_i++) {

                        double content     = h->GetBinContent(x1_i);
                        double content_err = h->GetBinError(x1_i);

                        if(content_normalization     != 0) h->SetBinContent(x1_i, content / content_normalization);
                        if(content_err_normalization != 0) h->SetBinError(x1_i, content_err / content_err_normalization);
                }

        }

        h->SetEntries(N);
        return h;
}


/**
 * \brief Normalize histogram
 *
 */

TH1* UIUC::TFactory::Offset(TH1 *h, int x1_offset, int x2_offset, int x3_offset, bool kModulo) {

        if(kModulo) {

                if(x1_offset == 0 && x2_offset != 0 && x3_offset != 0) kModulo = UIUC::TFactory::kNoModulo;
                if(x1_offset != 0 && x2_offset == 0 && x3_offset != 0) kModulo = UIUC::TFactory::kNoModulo;
                if(x1_offset == 0 && x2_offset != 0 && x3_offset == 0) kModulo = UIUC::TFactory::kNoModulo;
                if(x1_offset != 0 && x2_offset != 0 && x3_offset != 0) kModulo = UIUC::TFactory::kNoModulo;

                if(kModulo == UIUC::TFactory::kNoModulo) {

                        cerr << "Warning from <UIUC::TFactory::Offset>: Modulo option can only be used when single axis offset.." << endl;
                        return NULL;
                }
        }

        // Clone histogram
        TH1 *hClone = (TH1*) h->Clone();
        hClone->SetName((TString) h->GetName() + "_clone");
        hClone->SetTitle(h->GetTitle());

        TString x1_sign = (x1_offset > 0) ? "+" : "-";
        TString x2_sign = (x2_offset > 0) ? "+" : "-";
        TString x3_sign = (x3_offset > 0) ? "+" : "-";

        TString hOptions;
        if(x1_offset != 0) hOptions += ":x" + x1_sign + TString::Itoa(x1_offset,10);
        if(x2_offset != 0) hOptions += ":y" + x2_sign + TString::Itoa(x2_offset,10);
        if(x3_offset != 0) hOptions += ":z" + x3_sign + TString::Itoa(x3_offset,10);
        h->SetName(h->GetName() + hOptions);

        UIUC::TFactory::Empty(h);
        if(kModulo == UIUC::TFactory::kNoModulo) {

                double x1_min = h->GetXaxis()->GetXmin();
                double x1_max = h->GetXaxis()->GetXmax();
                if(x1_offset>0) x1_max += x1_offset * h->GetXaxis()->GetBinWidth(1);
                else x1_min += x1_offset * h->GetXaxis()->GetBinWidth(1);

                double x2_min = h->GetYaxis()->GetXmin();
                double x2_max = h->GetYaxis()->GetXmax();
                if(x2_offset>0) x2_max += x2_offset * h->GetYaxis()->GetBinWidth(1);
                else x2_min += x2_offset * h->GetYaxis()->GetBinWidth(1);

                double x3_min = h->GetZaxis()->GetXmin();
                double x3_max = h->GetZaxis()->GetXmax();
                if(x3_offset>0) x3_max += x3_offset * h->GetZaxis()->GetBinWidth(1);
                else x3_min += x3_offset * h->GetZaxis()->GetBinWidth(1);

                if(h->InheritsFrom("TH3")) {

                        h = (TH3D*) UIUC::TFactory::SetRange((TH3D*) h, "x", x1_min, x1_max);
                        h = (TH3D*) UIUC::TFactory::SetRange((TH3D*) h, "y", x2_min, x2_max);
                        h = (TH3D*) UIUC::TFactory::SetRange((TH3D*) h, "z", x3_min, x3_max);

                } else if(h->InheritsFrom("TH2")) {

                        h = (TH2D*) UIUC::TFactory::SetRange((TH2D*) h, "x", x1_min, x1_max);
                        h = (TH2D*) UIUC::TFactory::SetRange((TH2D*) h, "y", x2_min, x2_max);

                } else if(h->InheritsFrom("TH1")) {

                        h = (TH1D*) UIUC::TFactory::SetRange((TH1D*) h, x1_min, x1_max);
                }
        }

        int x1_N = hClone->GetNbinsX();
        int x2_N = hClone->GetNbinsY();
        int x3_N = hClone->GetNbinsZ();

        for(int x1_i = 1; x1_i <= x1_N; x1_i++) {

                for(int x2_i = 1; x2_i <= x2_N; x2_i++) {

                        for(int x3_i = 1; x3_i <= x3_N; x3_i++) {

                                double content = hClone->GetBinContent(x1_i, x2_i, x2_i);
                                double content_err = hClone->GetBinError(x1_i, x2_i, x3_i);
                                double bx,by,bz;
                                int ix,iy,iz;

                                if(kModulo == UIUC::TFactory::kNoModulo) {

                                        bx  = hClone->GetXaxis()->GetBinCenter(x1_i);
                                        by  = hClone->GetYaxis()->GetBinCenter(x2_i);
                                        bz  = hClone->GetZaxis()->GetBinCenter(x3_i);

                                        ix = h->GetXaxis()->FindBin(bx)+x1_offset;
                                        iy = h->GetYaxis()->FindBin(by)+x2_offset;
                                        iz = h->GetZaxis()->FindBin(bz)+x3_offset;

                                } else {
                                        ix = UIUC::MathMod(x1_i + x1_offset, x1_N)+1;
                                        iy = UIUC::MathMod(x2_i + x2_offset, x2_N)+1;
                                        iz = UIUC::MathMod(x3_i + x3_offset, x3_N)+1;
                                }

                                if( !UIUC::EpsilonEqualTo(content,0) ) h->SetBinContent(ix, iy, iz, content);
                                if(content_err != 0) h->SetBinError(ix, iy, iz, content_err);
                        }
                }
        }

        hClone->Delete();
        return h;
}



/**
 * \brief Write all stored histograms (open before a TFile if it's not already done. e.g. PHAST already open)
 *
 */

void UIUC::TFactory::WriteTH1() {

        for(int i = 0; i < (int) this->hList->GetSize(); i++) {

                this->hList->At(i)->Write();
        }
}

/*
 * \brief Get the stored 1D-histogram with a specific name
 * \param name Name of the histogram
 *
 * \return TH1D Histogram found (NULL if not)
 *
 */

TH1* UIUC::TFactory::Get(TString name) {

        for(int i = 0; i < (int) this->hList->GetSize(); i++) {

                if(TString(this->hList->At(i)->GetName()).CompareTo(name) == 0) return ((TH1*) this->hList->At(i));
        }

        cout << "Warning from <UIUC::TFactory::Get>: " << name << " not found.." << endl;
        return NULL;
}

TList* UIUC::TFactory::GetList() {
        return this->hList;
}

void UIUC::TFactory::Print(Option_t *option) const {

        for(int i = 0; i < (int) this->hList->GetSize(); i++) {

                TH1 *hi = (TH1*) this->hList->At(i);
                cout << "OBJ(" << option << "): " << hi->IsA()->GetName() << "\t" << hi->GetName() << "\t" << hi->GetTitle() << endl;
        }
}

TH1* UIUC::TFactory::SetOrigin(TH1 *h, double x0, double y0, double z0)
{
        if(h == NULL) return NULL;

        h = (TH1*) h->Clone();

        if (!UIUC::IsNaN(x0)) {

                double xMin = h->GetXaxis()->GetXmin();
                double xMax = h->GetXaxis()->GetXmax();
                
                if(x0 > xMin && x0 < xMax) {

                        double xRangeMin = h->GetXaxis()->GetBinLowEdge(h->GetXaxis()->GetFirst());
                        double xRangeMax = h->GetXaxis()->GetBinUpEdge(h->GetXaxis()->GetLast());

                        h->GetXaxis()->Set(h->GetNbinsX(), xMin-x0, xMax-x0);
                        h->SetName((TString) h->GetName()+":x0["+Form("%.4f", x0)+"]");
                        h->GetXaxis()->SetRangeUser(xRangeMin, xRangeMax); // Reset displaied range to user value 
                }
        }

        if(!UIUC::IsNaN(y0) && h->InheritsFrom("TH2")) {

                double yMin = h->GetYaxis()->GetXmin();
                double yMax = h->GetYaxis()->GetXmax();
                if(y0 > yMin && y0 < yMax) {

                        double yRangeMin = h->GetYaxis()->GetBinLowEdge(h->GetYaxis()->GetFirst());
                        double yRangeMax = h->GetYaxis()->GetBinUpEdge(h->GetYaxis()->GetLast());

                        h->GetYaxis()->Set(h->GetNbinsY(), yMin-y0, yMax-y0);
                        h->SetName((TString) h->GetName()+":y0["+Form("%.4f", y0)+"]");
                        h->GetYaxis()->SetRangeUser(yRangeMin, yRangeMax); // Reset displaied range to user value 
                }
        }
        
        if(!UIUC::IsNaN(z0) && h->InheritsFrom("TH3")) {

                double zMin = h->GetZaxis()->GetXmin();
                double zMax = h->GetZaxis()->GetXmax();
                if(z0 > zMin && z0 < zMax) {

                        double zRangeMin = h->GetZaxis()->GetBinLowEdge(h->GetZaxis()->GetFirst());
                        double zRangeMax = h->GetZaxis()->GetBinUpEdge(h->GetZaxis()->GetLast());

                        h->GetZaxis()->Set(h->GetNbinsZ(), zMin-z0, zMax-z0);
                        h->SetName((TString) h->GetName()+":z0["+Form("%.4f", z0)+"]");
                        h->GetZaxis()->SetRangeUser(zRangeMin, zRangeMax); // Reset displaied range to user value 
                }
        }

        TList *l = h->GetListOfFunctions();
        for(int i = 0; i < (int) l->GetSize(); i++) {
        
                TObject *obj = l->At(i);
                if(obj->InheritsFrom("TPolyMarker")) {
                        
                        TPolyMarker *pm = (TPolyMarker *) obj->Clone();

                        Double_t *x = pm->GetX();
                        Double_t *y = pm->GetY();
                        for(int i = 0, N = pm->GetN(); i < N; i++)
                                pm->SetPoint(i, x[i] - (UIUC::IsNaN(x0) ? 0 : x0), y[i] - (UIUC::IsNaN(y0) ? 0 : y0));

                        h->GetListOfFunctions()->AddBefore(obj, pm);
                        h->GetListOfFunctions()->Remove(obj);
                }
                
        }
        
        return h;
}
                
TList * UIUC::TFactory::Search(TRegexp regex, int repeat) {

        TList *l = new TList;
        for(int i = 0; i < (int) this->hList->GetSize(); i++) {

                TH1 *hi = (TH1 *) this->hList->At(i);

                TString name = hi->GetName();
                if( name.Contains(regex) ) l->Add(hi);
        }

        // Repeat this elem:entary pattern..
        int lsize = l->GetSize();
        for(int i = 0; i < repeat; i++) {

                TIter Next(l);
                for(int j = 0; j < lsize; j++) l->Add(Next());

        }

        return l;
}

TH1D* UIUC::TFactory::CreateTH1chain(TString name, TString description, TRegexp regex, int repeat) {

        // Look for TH1 matching with the pattern provided
        TList *l = this->Search(regex, repeat);
        if( !l->GetSize() ) {

                cout << "[Warning] Object list empty.. No match with the pattern specified for " << name << endl;
                return NULL;
        }

        TH1D* h = UIUC::TFactory::CreateTH1chain(name, description, l);

        delete l;
        l = NULL;

        return h;
}

TH1D* UIUC::TFactory::CreateTH1chain(TString name, TString description, TList* l0)
{
        TList *l = UIUC::TFileReader::ReadObj(l0);
        if(l == NULL || l->GetSize() == 0) return NULL;

        // Create a 2D-histogram from the list
        TH1D *h = NULL;
        TH1 *h0 = (TH1 *) l->At(0);
        if(h0->InheritsFrom("TH2") || h0->InheritsFrom("TH3")) {

                cerr << "Too low dimension.. cannot chain a 2D/3D as a 1D histogram.." << endl;
                return NULL;
        }

        if(UIUC::HandboxMsg::Error(HasVariableBinWidth(h0), __METHOD_NAME__,
                                   "Variable bin width detected.. This function cannot be called.. abort")) return NULL;

        int nbins = h0->GetNbinsX();
        double xmin = h0->GetXaxis()->GetXmin();
        double xmax = h0->GetXaxis()->GetXmax();

        for(int i = 1, I = l->GetSize(); i < I; i++) {

                TH1 *hi = (TH1 *) l->At(i);
                if(!UIUC::TFactory::CompareBinWidth(h0, hi)) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) (TString) "\""+h0->GetName()+"\" not compatible with \""+hi->GetName()+"\"");
                        return NULL;
                }

                xmax += (hi->GetXaxis()->GetXmax()-hi->GetXaxis()->GetXmin());
                nbins += hi->GetNbinsX();

                TString eMessage = (TString) "Processing " + hi->GetName() + "(" + TString::Itoa(i,10) + "/" + TString::Itoa(I,10) + ")";
                UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);
        }

        h = new TH1D(name, description, nbins,xmin,xmax);
        h->SetDirectory(h0->GetDirectory());
        h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());

        int i0 = 0;
        for(int i = 0, I = l->GetSize(); i < I; i++) {

                TH1 *hi = (TH1 *) l->At(i);
                for(int iBin = 1; iBin < hi->GetNbinsX() + 1; iBin++) {

                        h->SetBinError(i0+iBin, hi->GetBinError(iBin));
                        h->SetBinContent(i0+iBin, hi->GetBinContent(iBin));
                }

                i0 += hi->GetNbinsX();
                h->SetEntries(hi->GetEntries()+h->GetEntries());
        }

        delete l;
        l = NULL;

        return h;
}

TH2D* UIUC::TFactory::CreateTH2chain(TString name, TString description, TRegexp regex, int repeat) {

        // Look for TH1 matching with the pattern provided
        TList *l = this->Search(regex, repeat);
        if( !l->GetSize() ) {

                cout << "[Warning] Object list empty.. No match with the pattern specified for " << name << endl;
                return NULL;
        }

        TH2D* h = UIUC::TFactory::CreateTH2chain(name, description, l);

        delete l;
        l = NULL;

        return h;
}


TH2D* UIUC::TFactory::CreateTH2chain(TString name, TString description, TList* l0, vector<int> vRun)
{
        TList *l = UIUC::TFileReader::ReadObj(l0);
        if(l == NULL || l->GetSize() == 0) return NULL;

        TH1 *h0 = (TH1 *) l->At(0);

        if(h0->InheritsFrom("TH3")) {

                cerr << "Too low dimension.. cannot chain a 3D as a 2D histogram.." << endl;
                return NULL;
        }

        TH1 *hStruct = CopyStruct(h0);
        TH2D *h = NULL;

        TString className = h0->IsA()->GetName();
        int N = 0;

        for(int i = 0, I = l->GetSize(); i < I; i++) {

                TH1 *h = (TH1*) l->At(i);
                N += h->GetEntries();
        }

        if(UIUC::HandboxMsg::Error(HasVariableBinWidth(h0), __METHOD_NAME__,
                                   "Variable bin width detected.. This function cannot be called.. abort")) return NULL;

        if(className.Contains("TH2") || className.Contains("TH2D") ) {

                // Create a 2D-histogram from the list
                int xBin = h0->GetNbinsX();
                int xmin = h0->GetXaxis()->GetXmin();
                int xmax = h0->GetXaxis()->GetXmax();

                double h0BinWidth = (h0->GetYaxis()->GetXmax() - h0->GetYaxis()->GetXmin()) / h0->GetNbinsY();
                double yMin = h0->GetYaxis()->GetXmin();
                double yMax = h0->GetYaxis()->GetXmax();

                // Check if 2D-histogram and look for limits
                for(int i = 1, I = l->GetSize(); i < I; i++) {

                        TH2 *hi = (TH2 *) l->At(i);
                        if(!UIUC::TFactory::CompareBinWidth(h0, hi)) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) (TString) "\""+h0->GetName()+"\" not compatible with \""+hi->GetName()+"\"");
                                return NULL;
                        }

                        TString eMessage = (TString) "Processing " + hi->GetName() + "(" + TString::Itoa(i,10) + "/" + TString::Itoa(I,10) + ")";
                        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);

                        xBin += hi->GetNbinsX();
                        xmax += hi->GetXaxis()->GetXmax()-hi->GetXaxis()->GetXmin();

                        if(yMin > hi->GetYaxis()->GetXmin()) yMin = hi->GetYaxis()->GetXmin();
                        if(yMax < hi->GetYaxis()->GetXmax()) yMax = hi->GetYaxis()->GetXmax();
                }

                int yBin = (yMax-yMin)/h0BinWidth;

                // Creation of the histogram
                h = new TH2D(name, description, xBin,xmin,xmax, yBin,yMin,yMax);
                h->SetDirectory(h0->GetDirectory());
                h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
                h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());

                int i0 = 0;
                for(int k = 0; k < (int) l->GetSize(); k++) {

                        TH2D *hi = (TH2D *) l->At(k);
                        for(int i = 1; i < hi->GetNbinsX()+1; i++) {

                                for(int j = 1; j < hi->GetNbinsY()+1; j++) {

                                        h->SetBinError(i0+i, j, hi->GetBinError(i, j));
                                        h->SetBinContent(i0+i, j, hi->GetBinContent(i, j));
                                }
                        }

                        i0 += hi->GetNbinsX();
                        h->SetEntries(hi->GetEntries()+h->GetEntries());
                }

        } else if(className.Contains("TH1") || className.Contains("TH1D") ) {

                // RESCALE ACCORDING TO RUNNUMBER IF vRun is valid...
                int xBin, xmin, xmax;
                if ((int) vRun.size() == (int) l->GetSize()) {

                        int first_run = vRun[distance(vRun.begin(), min_element(vRun.begin(), vRun.end()))];
                        int last_run  = vRun[distance(vRun.begin(), max_element(vRun.begin(), vRun.end()))];

                        xBin = last_run-first_run+1;
                        xmin = first_run;
                        xmax = last_run+1;

                        TList *newl = new TList;
                        for(int x = xmin; x < xmax; x++) {

                                bool found = false;
                                for(int i = 0, N = vRun.size(); i < N; i++) {

                                        if(vRun[i] == x) {

                                                vRun[i] = -1;
                                                for(int j = i; j < N; j++) {

                                                        if(vRun[j] == x) {

                                                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Run ID found 2 times... something wrong... abort");
                                                                return NULL;
                                                        }
                                                }

                                                newl->Add(l->At(i));
                                                found = true;
                                        }
                                }

                                if(!found) newl->Add(hStruct);
                        }

                        delete l;
                        l = newl;

                } else {

                        vRun.clear();
                        for(int i = 1, N = l->GetSize()+1; i <= N; i++) vRun.push_back(i);

                        xBin = l->GetSize();
                        xmin = 1;
                        xmax = xBin+1;
                }

                // Create a 1D-histogram from the list
                double h0BinWidth = (h0->GetXaxis()->GetXmax() - h0->GetXaxis()->GetXmin()) / h0->GetNbinsX();
                double yMin = h0->GetXaxis()->GetXmin();
                double yMax = h0->GetXaxis()->GetXmax();

                // Check if 1D-histogram and look for limits
                for(int i = 0, I = l->GetSize(); i < I; i++) {

                        TH1 *hi = (TH1 *) l->At(i);

                        TString eMessage = (TString) "Processing " + hi->GetName() + "(" + TString::Itoa(i+1,10) + "/" + TString::Itoa(I,10) + ")";
                        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);

                        className = hi->IsA()->GetName();
                        if( !className.Contains("TH1") && !className.Contains("TH1D") ) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Not all histograms are 1D-histograms.. e.g. " + hi->GetName() + " is " + hi->IsA()->GetName());
                                return NULL;
                        }

                        if( !UIUC::EpsilonEqualTo(hi->GetXaxis()->GetBinWidth(1), h0->GetXaxis()->GetBinWidth(1))) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Try to merge histogram with different bin width.." + hi->GetName() );
                                return NULL;
                        }

                        if(yMin > hi->GetXaxis()->GetXmin()) yMin = hi->GetXaxis()->GetXmin();
                        if(yMax < hi->GetXaxis()->GetXmax()) yMax = hi->GetXaxis()->GetXmax();
                }
                int yBin = (yMax-yMin)/h0BinWidth;

                // Creation of the histogram
                h = new TH2D(name, description, xBin,xmin,xmax, yBin,yMin,yMax);
                h->GetXaxis()->SetNoExponent();
                h->SetDirectory(h0->GetDirectory());
                h->GetYaxis()->SetTitle(h0->GetXaxis()->GetTitle());

                for(int i = 1; i <= (int) l->GetSize(); i++) {

                        TH1D *hi = (TH1D *) l->At(i-1);
                        int j0 = (hi->GetXaxis()->GetXmin() - yMin) / h0BinWidth;

                        for(int j = 1; j < hi->GetNbinsX()+1; j++) {

                                h->SetBinError(i, j0+j, hi->GetBinError(j));
                                h->SetBinContent(i, j0+j, hi->GetBinContent(j));
                        }

                        h->SetEntries(hi->GetEntries()+h->GetEntries());
                }

        } else {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Object not valid.. It must be TH1 or TH2");
                return NULL;
        }

        delete l;
        l = NULL;

        delete hStruct;
        hStruct = NULL;

        h->SetEntries(N);
        return h;
}

TH3D* UIUC::TFactory::CreateTH3chain(TString name, TString description, TRegexp regex, int repeat)
{
        // Look for TH1 matching with the pattern provided
        TList *l = this->Search(regex, repeat);
        if( !l->GetSize() ) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Object list empty.. No match with the pattern specified for " + name);
                return NULL;
        }

        TH3D* h0 = UIUC::TFactory::CreateTH3chain(name, description, l);

        delete l;
        l = NULL;

        return h0;
}

bool UIUC::TFactory::CompareData(TH1* h1, TH1* h2, double epsilon, bool overflow) {

        if(!UIUC::TFactory::CompareStruct(h1, h2)) return false;

        for(int i = 0, N = h1->GetNcells(); i < N; i++) {

                if(!overflow && h1->IsBinOverflow (i)) continue;
                if(!overflow && h1->IsBinUnderflow(i)) continue;

                if(!UIUC::EpsilonEqualTo(h1->GetBinError  (i), h2->GetBinError  (i), epsilon)) return 0;
                if(!UIUC::EpsilonEqualTo(h1->GetBinContent(i), h2->GetBinContent(i), epsilon)) return 0;
        }

        return 1;
}

bool UIUC::TFactory::CompareData(TGraph* g1, TGraph* g2, double epsilon) {

        if(!UIUC::TFactory::CompareStruct(g1, g2)) return false;

        for(int i = 0, N = g1->GetN(); i < N; i++) {

                if(!UIUC::EpsilonEqualTo(g1->GetX()[i], g2->GetX()[i], epsilon)) return 0;
                if(g1->GetEXlow() && g2->GetEXlow() && !UIUC::EpsilonEqualTo(g1->GetEXlow()[i], g2->GetEXlow()[i])) return 0;
                if(g1->GetEXhigh() && g2->GetEXhigh() && !UIUC::EpsilonEqualTo(g1->GetEXhigh()[i], g2->GetEXhigh()[i])) return 0;
                if(g1->GetEX() && g2->GetEX() && !UIUC::EpsilonEqualTo(g1->GetEX()[i], g2->GetEX()[i])) return 0;
        }

        return 1;
}

bool UIUC::TFactory::CompareStruct(TH1* h1, TH1* h2) {

        if(h1 == NULL || h2 == NULL) return 0;

        if( !h1->InheritsFrom(h2->IsA()) ) return 0;

        if( !UIUC::TFactory::CompareBinWidth(h1, h2) ) return 0;

        if(h1->GetNbinsX() != h2->GetNbinsX() ) return 0;
        if(h1->GetNbinsY() != h2->GetNbinsY() ) return 0;
        if(h1->GetNbinsZ() != h2->GetNbinsZ() ) return 0;

        return 1;
}

bool UIUC::TFactory::CompareStruct(TGraph* g1, TGraph* g2) {

        if(g1 == NULL || g2 == NULL) return 0;
        if(g1->GetN() != g2->GetN() ) return 0;
        if( (g1->GetEXlow() && g2->GetEXlow() == NULL) || (g1->GetEXlow() == NULL && g2->GetEXlow()) ) return 0;
        if( (g1->GetEXhigh() && g2->GetEXhigh() == NULL) || (g1->GetEXhigh() == NULL && g2->GetEXhigh()) ) return 0;
        if( (g1->GetEX() && g2->GetEX() == NULL) || (g1->GetEX() == NULL && g2->GetEX()) ) return 0;

        return 1;
}

bool UIUC::TFactory::CompareBinWidth(TH1* h1, TH1* h2) {

        if( !UIUC::EpsilonEqualTo(h1->GetXaxis()->GetBinWidth(1), h2->GetXaxis()->GetBinWidth(1))) return 0;
        if( !UIUC::EpsilonEqualTo(h1->GetYaxis()->GetBinWidth(1), h2->GetYaxis()->GetBinWidth(1))) return 0;
        if( !UIUC::EpsilonEqualTo(h1->GetZaxis()->GetBinWidth(1), h2->GetZaxis()->GetBinWidth(1))) return 0;

        return 1;
}

TH3D* UIUC::TFactory::CreateTH3chain(TString name, TString description, TList* l0, vector<int> vRun)
{
        TList *l = UIUC::TFileReader::ReadObj(l0);
        if(l == NULL || l->GetSize() == 0) return NULL;

        TH1 *h0 = (TH1 *) l->At(0);
        TH1 *hStruct = CopyStruct(h0);
        TH3D *h = NULL;

        if(UIUC::HandboxMsg::Error(HasVariableBinWidth(h0), __METHOD_NAME__,
                                   "Variable bin width detected.. This function cannot be called.. abort")) return NULL;

        int N = 0;
        for(int m = 0; m < (int) l->GetSize(); m++) {

                N += ((TH1*) l->At(m))->GetEntries();
        }

        TString className = h0->IsA()->GetName();
        if(className.Contains("TH3") || className.Contains("TH3D") ) {

                // Create a 2D-histogram from the list
                int xBin = h0->GetNbinsX();
                double xmin = h0->GetXaxis()->GetXmin();
                double xmax = h0->GetXaxis()->GetXmax();

                double yBinWidth = (h0->GetYaxis()->GetXmax() - h0->GetYaxis()->GetXmin()) / h0->GetNbinsY();
                double yMin = h0->GetYaxis()->GetXmin();
                double yMax = h0->GetYaxis()->GetXmax();

                double zBinWidth = (h0->GetZaxis()->GetXmax() - h0->GetZaxis()->GetXmin()) / h0->GetNbinsZ();
                double zMin = h0->GetZaxis()->GetXmin();
                double zMax = h0->GetZaxis()->GetXmax();

                // Check if 2D-histogram and look for limits
                for(int i = 0, I = l->GetSize(); i < I; i++) {

                        TH3 *hi = (TH3 *) l->At(i);
                        if(!UIUC::TFactory::CompareBinWidth(h0, hi)) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) (TString) "\""+h0->GetName()+"\" not compatible with \""+hi->GetName()+"\"");
                                return NULL;
                        }

                        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) Form("Processing %s (%d/%d)", hi->GetName(), i, I));

                        xBin += hi->GetNbinsX();
                        xmax += hi->GetXaxis()->GetXmax()-hi->GetXaxis()->GetXmin();

                        if(yMin > hi->GetYaxis()->GetXmin()) yMin = hi->GetYaxis()->GetXmin();
                        if(yMax < hi->GetYaxis()->GetXmax()) yMax = hi->GetYaxis()->GetXmax();

                        if(zMin > hi->GetZaxis()->GetXmin()) zMin = hi->GetZaxis()->GetXmin();
                        if(zMax < hi->GetZaxis()->GetXmax()) zMax = hi->GetZaxis()->GetXmax();
                }

                int yBin = (yMax-yMin)/yBinWidth;
                int zBin = (zMax-zMin)/zBinWidth;

                // Creation of the histogram

                h = new TH3D(name, description, xBin,xmin,xmax, yBin,yMin,yMax, zBin,zMin,zMax);
                h->GetXaxis()->SetNoExponent();
                h->SetDirectory(h0->GetDirectory());
                h->GetXaxis()->SetTitle(h0->GetXaxis()->GetTitle());
                h->GetYaxis()->SetTitle(h0->GetYaxis()->GetTitle());
                h->GetZaxis()->SetTitle(h0->GetZaxis()->GetTitle());

                int i0 = 0;
                for(int m = 0; m < (int) l->GetSize(); m++) {

                        TH3D *hi = (TH3D *) l->At(m);
                        for(int i = 1; i < hi->GetNbinsX()+1; i++) {

                                for(int j = 1; j < hi->GetNbinsY()+1; j++) {

                                        for(int k = 1; k < hi->GetNbinsZ()+1; k++) {

                                                h->SetBinError(i0+i, j, k, hi->GetBinError(i, j, k) );
                                                h->SetBinContent(i0+i, j, k, hi->GetBinContent(i, j, k) );
                                        }
                                }
                        }

                        h->SetEntries(hi->GetEntries()+h->GetEntries());
                        i0 += hi->GetNbinsX();
                }

        } else if(className.Contains("TH2") || className.Contains("TH2D") ) {

                // RESCALE ACCORDING TO RUNNUMBER IF vRun is valid...
                int xBin, xmin, xmax;
                if ((int) vRun.size() == (int) l->GetSize()) {

                        int first_run = vRun[distance(vRun.begin(), min_element(vRun.begin(), vRun.end()))];
                        int last_run  = vRun[distance(vRun.begin(), max_element(vRun.begin(), vRun.end()))];

                        xBin = last_run-first_run+1;
                        xmin = first_run;
                        xmax = last_run+1;

                        TList *newl = new TList;
                        for(int x = xmin; x < xmax; x++) {

                                bool found = false;
                                for(int i = 0, N = vRun.size(); i < N; i++) {

                                        if(vRun[i] == x) {

                                                vRun[i] = -1;
                                                for(int j = i; j < N; j++) {

                                                        if(vRun[j] == x) {

                                                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Run ID found 2 times... something wrong... abort");
                                                                return NULL;
                                                        }
                                                }
                                                newl->Add(l->At(i));
                                                found = true;
                                        }
                                }

                                if(!found) newl->Add(hStruct);
                        }

                        delete l;
                        l = newl;

                } else {

                        vRun.clear();
                        for(int i = 1, N = l->GetSize()+1; i <= N; i++) vRun.push_back(i);

                        xBin = l->GetSize();
                        xmin = 1;
                        xmax = xBin+1;
                }

                // Create a 2D-histogram from the list
                double yBinWidth = (h0->GetXaxis()->GetXmax() - h0->GetXaxis()->GetXmin()) / h0->GetNbinsX();
                double yMin = h0->GetXaxis()->GetXmin();
                double yMax = h0->GetXaxis()->GetXmax();

                double zBinWidth = (h0->GetYaxis()->GetXmax() - h0->GetYaxis()->GetXmin()) / h0->GetNbinsY();
                double zMin = h0->GetYaxis()->GetXmin();
                double zMax = h0->GetYaxis()->GetXmax();

                // Check if 2D-histogram and look for limits
                for(int i = 0, I = l->GetSize(); i < I; i++) {

                        TH2D *hi = (TH2D *) l->At(i);

                        TString eMessage = (TString) "Processing " + hi->GetName() + " (" + TString::Itoa(i,10) + "/" + TString::Itoa(I,10) + ")";
                        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) eMessage);

                        className = hi->IsA()->GetName();
                        if( !className.Contains("TH2") && !className.Contains("TH2D") ) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Not all histograms are 2D-histograms.. e.g. " + hi->GetName() + " is " + hi->IsA()->GetName());
                                return NULL;
                        }

                        if( !UIUC::EpsilonEqualTo(hi->GetXaxis()->GetBinWidth(1), h0->GetXaxis()->GetBinWidth(1))) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Try to merge histogram with different bin width along X.." + hi->GetName() );
                                return NULL;
                        }

                        if( !UIUC::EpsilonEqualTo(hi->GetYaxis()->GetBinWidth(1), h0->GetYaxis()->GetBinWidth(1))) {

                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Try to merge histogram with different bin width along Y.." + hi->GetName() );
                                return NULL;
                        }

                        if(yMin > hi->GetXaxis()->GetXmin()) yMin = hi->GetXaxis()->GetXmin();
                        if(yMax < hi->GetXaxis()->GetXmax()) yMax = hi->GetXaxis()->GetXmax();

                        if(zMin > hi->GetYaxis()->GetXmin()) zMin = hi->GetYaxis()->GetXmin();
                        if(zMax < hi->GetYaxis()->GetXmax()) zMax = hi->GetYaxis()->GetXmax();
                }

                int yBin = (yMax-yMin)/yBinWidth;
                int zBin = (zMax-zMin)/zBinWidth;

                // Creation of the histogram
                h = new TH3D(name, description, xBin,xmin,xmax, yBin,yMin,yMax, zBin,zMin,zMax);
                h->SetDirectory(h0->GetDirectory());
                h->GetYaxis()->SetTitle(h0->GetXaxis()->GetTitle());
                h->GetZaxis()->SetTitle(h0->GetYaxis()->GetTitle());

                for(int i = 1; i <= xBin; i++) {

                        TH2D *hi = (TH2D *) l->At(i-1);
                        for(int j = 1; j < hi->GetNbinsX()+1; j++) {

                                for(int k = 1; k < hi->GetNbinsY()+1; k++) {

                                        h->SetBinError(i, j, k, hi->GetBinError(j, k));
                                        h->SetBinContent(i, j, k, hi->GetBinContent(j, k));
                                }
                        }

                        h->SetEntries(hi->GetEntries()+h->GetEntries());
                }

        } else {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) (TString) "Object not valid.. It must be TH2 or TH3");
                return NULL;
        }

        delete l;
        l = NULL;

        delete hStruct;
        hStruct = NULL;


        h->SetEntries(N);
        return h;
}

void UIUC::TFactory::PrintASCII(TH1 *h0, Notation display) {

        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) "Printing extended required for \""+TString(h0->GetName())+"\" (TH1*)");

        TString className = h0->IsA()->GetName();
        int length = 0;
        for (int i = 1; i < h0->GetNbinsX() + 1; i++) {
                for (int j = 1; j < h0->GetNbinsY() + 1; j++) {
                        for (int k = 1; k < h0->GetNbinsZ() + 1; k++) {

                                length = TMath::Max(length, TString(h0->GetXaxis()->GetBinLabel(i)).Length());
                        }
                }
        }
        if(length > 50) length = 50;

        for (int i = 1; i < h0->GetNbinsX() + 1; i++) {
                for (int j = 1; j < h0->GetNbinsY() + 1; j++) {
                        for (int k = 1; k < h0->GetNbinsZ() + 1; k++) {

                                int ibin = h0->GetBin(i,j,k);
                                if(h0->GetBinContent(ibin) != 0 || h0->GetBinError(ibin) != 0) {

                                        cout << left;
                                        if(TString(h0->GetXaxis()->GetBinLabel(ibin)).EqualTo("")) cout << "bin#" << i << "\t";
                                        else cout << setw(length+10) << h0->GetXaxis()->GetBinLabel(i) << "\t";

                                        double dd = h0->GetBinContent(ibin);
                                        double dd_err = h0->GetBinError(ibin);


                                        switch(display) {
                                        case E_SCIENTIFIC:
                                                cout << Form("%e", dd);
                                                if(dd_err != 0) cout << " +/- " << Form("%e", dd_err);
                                                break;

                                        case E_INT:
                                                if(UIUC::EpsilonEqualTo((int) dd, round(dd))) cout << Form("%d", (int) dd);
                                                else cout << "NaN";

                                                if(dd_err != 0) {

                                                        if(UIUC::EpsilonEqualTo((int) dd_err, round(dd_err))) cout << " +/- " << Form("%d", (int) dd_err);
                                                        else cout << " +/- NaN";
                                                }

                                                break;
                                        case E_ROUNDED:
                                                cout << Form("%.2f", dd);
                                                if(dd_err != 0) cout << " +/- " << Form("%.2f", dd_err);
                                                break;
                                        default:
                                        case E_FLOAT:
                                                cout << Form("%f", dd);
                                                if(dd_err != 0) cout << " +/- " << Form("%f", dd_err);
                                        }

					cout << " (";
                                        if(h0->GetBinContent(1) != 0) cout << "1st-bin:"<< Form("%.2f%%", 100*dd/h0->GetBinContent(1)) << "; ";
					if(dd != 0) cout << "err:" << Form("%.2f%%", 100*dd_err/dd);
                                        cout << ")" << endl;
                                }
                        }
                }
        }
        cout << endl;
}


void UIUC::TFactory::PrintLatex(TH1 *h0, Notation display) {

        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, (TString) "Printing extended required for \""+TString(h0->GetName())+"\" (TH1*)");

        TString className = h0->IsA()->GetName();
        int length = 0;
        for (int i = 1; i < h0->GetNbinsX() + 1; i++) {
                for (int j = 1; j < h0->GetNbinsY() + 1; j++) {
                        for (int k = 1; k < h0->GetNbinsZ() + 1; k++) {

                                length = TMath::Max(length, TString(h0->GetXaxis()->GetBinLabel(i)).Length());
                        }
                }
        }
        if(length > 50) length = 50;

        for (int i = 1; i < h0->GetNbinsX() + 1; i++) {
                for (int j = 1; j < h0->GetNbinsY() + 1; j++) {
                        for (int k = 1; k < h0->GetNbinsZ() + 1; k++) {

                                int ibin = h0->GetBin(i,j,k);
                                if(h0->GetBinContent(ibin) != 0 || h0->GetBinError(ibin) != 0) {

                                        cout << left;
                                        if(TString(h0->GetXaxis()->GetBinLabel(ibin)).EqualTo("")) cout << "bin#" << i << "\t";
                                        else cout << setw(length+10) << h0->GetXaxis()->GetBinLabel(i) << "\t";

                                        double dd = h0->GetBinContent(ibin);
                                        double dd_err = h0->GetBinError(ibin);


                                        switch(display) {
                                        case E_SCIENTIFIC:
                                                cout << Form("%e", dd);
                                                if(dd_err != 0) cout << " +/- " << Form("%e", dd_err);
                                                break;

                                        case E_INT:
                                                if(UIUC::EpsilonEqualTo((int) dd, round(dd))) cout << Form("%d", (int) dd);
                                                else cout << "NaN";

                                                if(dd_err != 0) {

                                                        if(UIUC::EpsilonEqualTo((int) dd_err, round(dd_err))) cout << " +/- " << Form("%d", (int) dd_err);
                                                        else cout << " +/- NaN";
                                                }

                                                break;
                                        case E_ROUNDED:
                                                cout << Form("%.2f", dd);
                                                if(dd_err != 0) cout << " +/- " << Form("%.2f", dd_err);
                                                break;
                                        default:
                                        case E_FLOAT:
                                                cout << Form("%f", dd);
                                                if(dd_err != 0) cout << " +/- " << Form("%f", dd_err);
                                        }

                                        cout << " (";
                                        if(h0->GetBinContent(1) != 0) cout << "1st-bin:"<< Form("%.2f%%", 100*dd/h0->GetBinContent(1)) << "; ";
                                        if(dd != 0) cout << "err:" << Form("%.2f%%", 100*dd_err/dd);
                                        cout << ")" << endl;
                                }
                        }
                }
        }
        cout << endl;
}


bool UIUC::TFactory::SaveIntoASCII(TString ascii_str, std::ios_base::openmode mode, TTree *t, TString varname, TString selection0) {

        // Replace all flags by double..
        varname.ReplaceAll("/D", "");
        varname.ReplaceAll("/I", "");
        varname.ReplaceAll("/O", "");
        varname.ReplaceAll("/F", "");

        UIUC::HandboxMsg::PrintDebug(1, __METHOD_NAME__, "Writing \"%s\" into %s ongoing..", t->GetName(), ascii_str.Data());
        ofstream fASCII(ascii_str.Data(), mode);
        if( UIUC::HandboxMsg::Error(!fASCII.is_open(), __METHOD_NAME__, "Failed to open \"" + ascii_str + "\"..")) return 1;

        vector< vector<double> > selection = TVarexp(varname).GetSelection(t, selection0);

        stringstream ss;
        TString varname0 = varname;
        varname0.ReplaceAll(":", "/D:");
        varname0 += "/D";
        ss << "# " << varname0 << endl;

        int nvars = selection.size();
        int nentries = selection[0].size();
        for(int k = 0; k < nentries; k++) {

                for(int j = 0; j < nvars; j++) ss << left << setw(20) << selection[j][k];
                ss << endl;
        }

        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Writing %d entries and %d variables into \"%s\"..", nentries, nvars, ascii_str.Data());

        fASCII << ss.str();

        fASCII.close();
        return true;
}

bool UIUC::TFactory::SaveIntoASCII(TString ascii_str, std::ios_base::openmode mode, TGraph *g) {

        UIUC::HandboxMsg::PrintDebug(1, __METHOD_NAME__, "Writing \"%s\" into %s ongoing..", g->GetName(),ascii_str.Data());
        ofstream fOutput(ascii_str.Data(), mode);

        fOutput << "# TGraph.N = " << g->GetN() << endl;
        fOutput << "# TGraph.Name = " << g->GetName() << endl;
        fOutput << "# TGraph.Title = " << g->GetTitle() << endl;

        int N = g->GetN();
        vector<double> x, y, ey, eyl, eyh;
        for(int i = 0; i < N; i++) {
                if(g->GetX() != NULL) x.push_back(g->GetX()[i]);
                if(g->GetY() != NULL) y.push_back(g->GetY()[i]);
                if(g->GetEY() != NULL) ey.push_back(g->GetEY()[i]);
                if(g->GetEYlow() != NULL) eyl.push_back(g->GetEYlow()[i]);
                if(g->GetEYhigh() != NULL) eyh.push_back(g->GetEYhigh()[i]);
        }

        TString className = g->IsA()->GetName();
        if( className.Contains("TGraphAsymmErrors") ) {

                fOutput << "# Object: TGraphAsymmErrors" << endl;
                for(int i = 0, N = g->GetN(); i < N; i++)
                        fOutput << x[i] << " " << y[i] << " + " << eyh[i] << " - " << eyl[i] << endl;

        } else if( className.Contains("TGraphErrors") ) {

                fOutput << "# Object: TGraphErrors" << endl;
                for(int i = 0, N = g->GetN(); i < N; i++)
                        fOutput << x[i] << " " << y[i] << " +/- " << ey[i] << endl;

        } else if( className.Contains("TGraph") ) {

                fOutput << "# Object: TGraph" << endl;
                for(int i = 0, N = g->GetN(); i < N; i++)
                        fOutput << x[i] << " " << y[i] << endl;

        } else UIUC::HandboxMsg::PrintError(__METHOD_NAME__, " Unknown object. Is it TGraph?");

        fOutput.close();
        return true;
}

bool UIUC::TFactory::SaveIntoASCII(TString ascii_str,std::ios_base::openmode mode, TH1 *h) {

        ofstream fASCII(ascii_str, mode);
        if( !fASCII.is_open() ) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "ASCII file " + ascii_str + " not found..");
                return -1;
        }

        fASCII << "# TH1 Type: " << h->IsA()->GetName() << endl;
        fASCII << "# TH1 Ptr: " << h << "; TH1.Name: " << h->GetName() << "; TH1.Title: " << h->GetTitle() << endl;
        fASCII << "# TH1.Entries = " << h->GetEntries() << "; TH1.Mean: " << h->GetMean() << "; TH1.StdDev: " << h->GetStdDev() << endl;

        TString hOptions;
        hOptions.Form("# X-axis = (%f,%f); nbins = %d; binwidth = %f",
                      h->GetXaxis()->GetBinLowEdge(1),
                      h->GetXaxis()->GetBinUpEdge(h->GetNbinsX()),
                      h->GetNbinsX(),
                      h->GetXaxis()->GetBinWidth(1));

        fASCII << hOptions << endl;

        if(h->InheritsFrom("TH2")) {

                TString hOptions;

                hOptions.Form("# Y-axis = (%f,%f); nbins = %d; binwidth = %f",
                              h->GetYaxis()->GetBinLowEdge(1),
                              h->GetYaxis()->GetBinUpEdge(h->GetNbinsY()),
                              h->GetNbinsY(),
                              h->GetYaxis()->GetBinWidth(1));

                fASCII << hOptions << endl;
        }

        if(h->InheritsFrom("TH3")) {

                TString hOptions;

                hOptions.Form("# Z-axis = (%f,%f); nbins = %d; binwidth = %f",
                              h->GetZaxis()->GetBinLowEdge(1),
                              h->GetZaxis()->GetBinUpEdge(h->GetNbinsZ()),
                              h->GetNbinsZ(),
                              h->GetZaxis()->GetBinWidth(1));

                fASCII << hOptions << endl;
        }


        fASCII << left;
        if(h->InheritsFrom("TH1")) fASCII << "# X0 X1";
        else return 0;
        if(h->InheritsFrom("TH3")) fASCII << setw(10) <<" Y0 Y1";
        if(h->InheritsFrom("TH2")) fASCII << setw(10) <<" Z0 Z1";

        fASCII << setw(10) << " BinContent " << setw(10) << " BinError " << endl;

        int ibin;
        int ix,iy,iz;
        int binx,biny,binz;
        double bx,by,bz;
        double bx0,by0,bz0;
        double bx1,by1,bz1;

        //loop on all bins and refill
        int nbinsx = h->GetNbinsX();
        int nbinsy = h->GetNbinsY();
        int nbinsz = h->GetNbinsZ();

        for (binz=1; binz <= nbinsz; binz++) {

                bz0 = h->GetZaxis()->GetBinLowEdge(binz);
                bz1 = h->GetZaxis()->GetBinUpEdge(binz);
                bz = h->GetZaxis()->GetBinCenter(binz);
                iz = h->GetZaxis()->FindBin(bz);

                for (biny=1; biny <= nbinsy; biny++) {

                        by0 = h->GetYaxis()->GetBinLowEdge(biny);
                        by1 = h->GetYaxis()->GetBinUpEdge(biny);
                        by = h->GetYaxis()->GetBinCenter(biny);
                        iy = h->GetYaxis()->FindBin(by);

                        for (binx=1; binx <= nbinsx; binx++) {

                                bx0 = h->GetXaxis()->GetBinLowEdge(binx);
                                bx1 = h->GetXaxis()->GetBinUpEdge(binx);
                                bx = h->GetXaxis()->GetBinCenter(binx);
                                ix = h->GetXaxis()->FindBin(bx);

                                ibin = h->GetBin(ix,iy,iz);

                                if(h->InheritsFrom("TH1")) fASCII << setw(10) << bx0 << " " << bx1 << " ";
                                if(h->InheritsFrom("TH2")) fASCII << setw(10) << by0 << " " << by1 << " ";
                                if(h->InheritsFrom("TH3")) fASCII << setw(10) << bz0 << " " << bz1 << " ";
                                fASCII << setw(10) << h->GetBinContent(ibin) << " " << h->GetBinError(ibin);
                                fASCII << endl;
                        }
                }
        }

        fASCII.close();
        return true;
}

bool UIUC::TFactory::SaveIntoASCII(TString ascii_str,std::ios_base::openmode mode, TMatrixD *m, TString desc) {

        ofstream fASCII(ascii_str, mode);
        if( !fASCII.is_open() ) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "ASCII file " + ascii_str + " not found..");
                return -1;
        }

        fASCII << "# TMatrixD.Size: (rows,cols) = " << m->GetNrows() << "x" << m->GetNcols() << endl;
        if(!desc.EqualTo("")) fASCII << "# TMatrixD.Desc: " << desc << endl;

        for(int i = 0, I = m->GetNrows(); i < I; i++) {

                for(int j = 0, J = m->GetNrows(); j < J; j++)
                        fASCII << (*m)[i][j] << " ";

                fASCII << endl;
        }

        fASCII.close();
        return true;
}

TGraph* UIUC::TFactory::LoadGraphFromASCII(TString asciifilename)
{
        TMatrixD *m = LoadMatrixFromASCII(asciifilename);
        if(m == NULL) return NULL;
        if(!m->GetNrows()) return NULL;

        UIUC::HandboxMsg::PrintDebug(0,__METHOD_NAME__, m);

        vector<double> x,y,ey,eyl,eyh;
        switch(m->GetNcols()) {

        case 2:   // TGraph(X,Y)
                for(int i = 0, I = m->GetNrows(); i < I; i++) {

                        x.push_back((*m)[i][0]);
                        y.push_back((*m)[i][1]);
                }
                break;

        case 3:
                for(int i = 0, I = m->GetNrows(); i < I; i++) {

                        x.push_back((*m)[i][0]);
                        y.push_back((*m)[i][1]);
                        ey.push_back((*m)[i][2]);
                }
                break;

        case 4:
                for(int i = 0, I = m->GetNrows(); i < I; i++) {

                        x.push_back((*m)[i][0]);
                        y.push_back((*m)[i][1]);
                        eyl.push_back((*m)[i][2]);
                        eyh.push_back((*m)[i][3]);
                }
                break;

        default:
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected format (N=%d) in \"%s\".. Cannot convert to TGraph", m->GetNcols(), asciifilename.Data());
                return NULL;
        }

        TGraph *g = NULL;
        if(eyl.size()) g = (TGraph*) new TGraphAsymmErrors(x.size(), &x[0], &y[0], NULL, NULL, &eyl[0], &eyh[0]);
        else if(ey.size()) g = (TGraph*) new TGraphErrors(x.size(), &x[0], &y[0], NULL, &ey[0]);
        else g = new TGraph(x.size(), &x[0], &y[0]);

        g->SetName(UIUC::TFactory::GetRandomName("g"));
        return g;
}

bool UIUC::TFactory::ExtendBinRangeByStep(vector<double> &x, double x0, double x1, double dx)
{
        if(UIUC::HandboxMsg::Error(x0 > x1, __METHOD_NAME__, "Wrong range specified.. X0(=%.2f) > X1(=%.2f)", x0, x1)) return false;  

        while(dx != 0 && x.size() && x[0] < x0) x.erase(x.begin());
        while(dx != 0 && x.size() && x[0] > x0) x.insert(x.begin(), x[0] - TMath::Abs(dx));
        while(dx != 0 && x.size() && x[x.size()-1] > x1) x.erase(x.end() - 1);
        while(dx != 0 && x.size() && x[x.size()-1] < x1) x.insert(x.end(), x[x.size()-1] + TMath::Abs(dx));

        return true;
}

bool UIUC::TFactory::ExtendBinRange(vector<double> &x, double x0, double x1, double precision)
{
        if(UIUC::HandboxMsg::Error(x0 > x1, __METHOD_NAME__, "Wrong range specified.. X0(=%.2f) > X1(=%.2f)", x0, x1)) return false;     
        x0 = UIUC::Round(x0,precision); // Make sure the double is rounded to the correct precision before any computation
        x1 = UIUC::Round(x1,precision);

        unsigned int ik1 = find_epsilon(x.begin(),x.end(), x0) - x.begin();
        unsigned int ik2 = find_epsilon(x.begin(),x.end(), x1) - x.begin();

        UIUC::HandboxMsg::PrintDebug(30,__METHOD_NAME__, x);
        UIUC::HandboxMsg::PrintDebug(30,__METHOD_NAME__, "X0(ik1=%d) = %f", ik1, x0);
        UIUC::HandboxMsg::PrintDebug(30,__METHOD_NAME__, "X1(ik2=%d) = %f", ik2, x1);

        if(!x.size()) {

                x.push_back(x0);
                x.push_back(x1);

        } else if (ik1 != x.size() && ik2 != x.size()) {

                if(ik1 != ik2 && ik1+1 != ik2) {

                        UIUC::HandboxMsg::PrintError(
                                __METHOD_NAME__, "[X0(ik1=%d) = %.16f], [X1(ik2=%d) = %.16f] are overlapping with other bins..",
                                ik1, x0, ik2, x1);
                        return 0;
                }

        } else if(ik1 != x.size() && ik2 == x.size()) {

                if(ik1 == x.size()-1) x.insert(x.end(), x1);
                else {
                        UIUC::HandboxMsg::PrintError(
                                __METHOD_NAME__, "[X1(ik2=%d) = %.16f] must be a vector edge, cannot add [X0(ik1=%d) = %.16f]",
                                ik2, x1, ik1, x0);
                        return 0;
                }

        } else if(ik1 == x.size() && ik2 != x.size()) {

                if(ik2 == 0) x.insert(x.begin(), x0);
                else {
                        UIUC::HandboxMsg::PrintError(
                                __METHOD_NAME__, "[X1(ik2=%d) = %.16f] must be a vector edge, cannot add [X0(ik1=%d) = %.16f]",
                                ik2, x1, ik1, x0);
                        return 0;
                }
        
        } else if(ik1 == x.size() && ik2 == x.size()) {

                if(x1 < x[0]) {

                        x.insert(x.begin(), x1);
                        x.insert(x.begin(), x0);

                } else if(x0 > x[x.size()-1]) {

                        x.insert(x.end(), x0);
                        x.insert(x.end(), x1);

                } else {

                        UIUC::HandboxMsg::PrintError(
                                __METHOD_NAME__, "[X1(ik2=%d) = %.16f] must be a vector edge, cannot add [X0(ik1=%d) = %.16f]",
                                ik2, x1, ik1, x0);

                        return 0;
                }
        }
        
        UIUC::HandboxMsg::PrintDebug(30,__METHOD_NAME__, x);

        return true;
}

TTree* UIUC::TFactory::LoadTreeFromASCII(TString asciifilename, TString desc, char delimiter)
{
        TTree *t = NULL;

        vector<TString> vi = UIUC::TFileReader::RecursiveExpandTextFile(asciifilename);
        for(int i = 0, N = vi.size(); i < N; i++) {

                UIUC::HandboxMsg::PrintProgressBar("Reading tree.. \""+vi[i]+"\" ", i+1, N, 1);
                if(!UIUC::TFileReader::IsTextFile(vi[i])) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "%s is not an ASCII file.. abort", vi[i].Data());
                        return NULL;
                }

                if(desc.EqualTo("")) desc = UIUC::TFileReader::FetchTreeDescriptor(vi[i]);
                if(UIUC::HandboxMsg::Error(desc.EqualTo(""), __METHOD_NAME__, "Tree descriptor neither specified nor found in \"%s\"", asciifilename.Data())) return NULL;

                if(t != NULL) t->ReadFile(vi[i]);
                else {

                        t = new TTree(UIUC::TFactory::GetRandomName("t"), desc);
                        t->ReadFile(vi[i], desc, delimiter);
                }
        }

        return t;
}

TH1* UIUC::TFactory::LoadHistFromASCII(TString asciifilename) {

        TMatrixD *m = LoadMatrixFromASCII(asciifilename);
        if(m == NULL) return NULL;
        if(!m->GetNrows()) return NULL; 

        vector<double> x,y,z,c,cerr;
        for(int i = 0, I = m->GetNrows(); i < I; i++) {

                // Special case: Remove last entry. in the case (Ncols == 2)
                if(m->GetNcols() == 2 && i >= I-1) break; 

                // (X0,X1);(Y0,Y1);(Z0,Z1) are the lower/upper edges of the histogram
                switch(m->GetNcols()) {

                        case 7: [[fallthrough]];
                        case 8:   // TH3D (X0,X1,Y0,Y1,Z0,Z1) + (Content+Error)
                                if(!ExtendBinRange(z, (*m)[i][4], (*m)[i][5])) {
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Error happened at line #%d", i+1);
                                        return NULL;
                                }
                                [[fallthrough]];

                        case 5: [[fallthrough]];
                        case 6: // TH2D (X0,X1,Y0,Y1) + (Content+Error)
                                if(!ExtendBinRange(y, (*m)[i][2], (*m)[i][3])) {
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Error happened at line #%d", i+1);
                                        return NULL;
                                }
                                [[fallthrough]];

                        case 3: [[fallthrough]]; // TH1D (X0,X1) + (Content+No error)
                        case 4: // TH1D (X0,X1) + (Content+Error)
                                if(!ExtendBinRange(x, (*m)[i][0], (*m)[i][1])) {
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Error happened at line #%d", i+1);
                                        return NULL;
                                }
                                break;

                        case 2: // TH1D (X0,X1) + (Content+No error)

                                if(!ExtendBinRange(x, (*m)[i][0], (*m)[i+1][0])) {
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Error happened at line #%d", i+1);
                                        return NULL;
                                }
                                break;

                        default:
                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected format (N=%d columns found) for \"%s\".. Cannot convert to TH1", m->GetNcols(), asciifilename.Data());
                                return NULL;
                }
        }

        TH1 *h = NULL;
        TString name = UIUC::TFactory::GetRandomName("h");
        if(x.size() && y.size() && z.size()) {

                h = new TH3D(name, asciifilename, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                for(int i = 0, I = m->GetNrows(); i < I; i++) {

                        double x = (*m)[i][0] + ((*m)[i][1]-(*m)[i][0])/2;
                        double y = (*m)[i][2] + ((*m)[i][3]-(*m)[i][2])/2;
                        double z = (*m)[i][4] + ((*m)[i][5]-(*m)[i][4])/2;
                        double c = (*m)[i][6];
                        double cerr = (m->GetNcols() > 7) ? (*m)[i][7] : 0;

                        int ibin = h->FindBin(x, y, z);
                        h->SetBinContent(ibin, c);
                        h->SetBinError(ibin, cerr);
                }

        } else if(x.size() && y.size()) {

                h = new TH2D(name, asciifilename, x.size()-1, &x[0], y.size()-1, &y[0]);
                for(int i = 0, I = m->GetNrows(); i < I; i++) {

                        double x = (*m)[i][0] + ((*m)[i][1]-(*m)[i][0])/2;
                        double y = (*m)[i][2] + ((*m)[i][3]-(*m)[i][2])/2;
                        double c = (*m)[i][4];
                        double cerr = (m->GetNcols() > 5) ? (*m)[i][5] : 0;

                        int ibin = h->FindBin(x,y);
                        h->SetBinContent(ibin, c);
                        h->SetBinError(ibin, cerr);
                }

        } else if(x.size()) {

                h = new TH1D(name, asciifilename, x.size()-1, &x[0]);
                for(int i = 0, I = m->GetNrows(); i < I; i++) {

                        if(m->GetNcols() < 3 && i == I-1) continue;
                        double x = m->GetNcols() < 3 ? (*m)[i][0] + ((*m)[i+1][0]-(*m)[i][0])/2 : (*m)[i][0] + ((*m)[i][1]-(*m)[i][0])/2;
                        double c = m->GetNcols() < 3 ? (*m)[i][1]                               : (*m)[i][2];
                        double cerr = (m->GetNcols() > 3) ? (*m)[i][3] : 0;

                        int ibin = h->FindBin(x);
                        h->SetBinContent(ibin, c);
                        h->SetBinError(ibin, cerr);
                }
        }

        return h;
}

TMatrixD* UIUC::TFactory::LoadMatrixFromASCII(TString asciifilename) {

        if(!UIUC::TFileReader::IsTextFile(asciifilename)) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "%s is not an ASCII file.. abort", asciifilename.Data());
                return NULL;
        }

        ifstream fASCII(asciifilename);
        if( !fASCII.is_open() ) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "ASCII file %s not found..", asciifilename.Data());
                return NULL;
        }

        int ncols = 0, nrows = 0;
        vector<vector<double>> v0;
        for(string line; getline(fASCII, line); ) {

                line = trim(line);
                if(TString(line).EqualTo("")) continue;
                
                std::vector<TString> entries = UIUC::Explode(" ", line);
                if(entries.size() == 0 ) continue;

                if( ((char) entries[0][0]) == '#' ) continue;

                v0.push_back({});
        
                int icols = 0;
                for(int i = 0, N = entries.size(); i < N; i++) {

                        if(entries[i].EqualTo("")) continue;

                        double d = entries[i].Atof();
                        if(UIUC::IsNaN(d)) d = UIUC::InterpretFormula(entries[i]);

                        v0.back().push_back(d);
                        icols++;
                }

                ncols = TMath::Max(ncols, icols);
                nrows++;
        }

        fASCII.close();
        if(!nrows || !ncols) return NULL;

        TMatrixD *m = new TMatrixD(nrows, ncols);
        for(int i = 0, I = v0.size(); i < I; i++)
                for(int j = 0, J = v0[i].size(); j < J; j++)
                        (*m)[i][j] = v0[i][j];

        return m;
}


void UIUC::TFactory::DrawPeak(TH1 *h, Option_t *option, vector<pair<double, double>> pos, Color_t color) 
{
    if (h == 0) return;

    TPolyMarker *pm = new TPolyMarker(pos.size());
    for(int i = 0, N = pos.size(); i < N; i++) {
    
        pm->SetPoint(i, pos[i].first, pos[i].second);
        pm->SetMarkerStyle(23);
        pm->SetMarkerColor(color);
        pm->SetMarkerSize(1.3);
    }

    h->GetListOfFunctions()->Add(pm);
    h->Draw(option);
}

vector<pair<double, double>> UIUC::TFactory::PeakScan(TH1 *h, Option_t *option, int nPeaks, double sigma, double threshold)
{
    double integral = (h->Integral() != 0) ? h->Integral() : 1;
    h = UIUC::TFactory::ScaleY(h, 1./integral);

    TSpectrum *spectrum = new TSpectrum(nPeaks);
    Int_t nCandidatePeaks = spectrum->Search(h, sigma, "goff", threshold);

    TString opt = option;
            opt.ToLower();

    Double_t *xPeaks;
             xPeaks = spectrum->GetPositionX();

    double tGW = NAN;
    int nPeakFound = 0;

    vector<pair<double, double>> posRejected;
    vector<pair<double, double>> posAccepted;
    for (int p = 0; p < nCandidatePeaks; p++) {

        Double_t xP = xPeaks[p];

        Int_t bin   = h->GetXaxis()->FindBin(xP);
        Double_t yP = h->GetBinContent(bin);

        if(TMath::Abs(yP) < threshold) {
        
            posRejected.push_back(make_pair(xP, yP * integral));
            continue;
        }

        posAccepted.push_back(make_pair(xP, yP * integral));
        nPeakFound++;
    }

    h = UIUC::TFactory::ScaleY(h, integral);
    if(!opt.Contains("goff")) {

        if(posRejected.size() > 0) UIUC::TFactory::DrawPeak(h, opt, posRejected, kRed);
        if(posAccepted.size() > 0) UIUC::TFactory::DrawPeak(h, opt, posAccepted, kGreen);
    }
    
    UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Peak candidate(s): %d found", nPeakFound);
    for(int i = 0, N = posAccepted.size(); i < N; i++)
        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "- Timing position #%d: t = %.5f", i, posAccepted[i].first);

    return posAccepted;
}

std::vector<double> UIUC::TFactory::LoadVectorFromASCII(TString asciifilename)
{
        TMatrixD *m = UIUC::TFactory::LoadMatrixFromASCII(asciifilename);
        std::vector<double> v;

        switch(m->GetNcols()) {

                case 1:
                        for(int i = 0, I = m->GetNrows(); i < I; i++)
                                v.push_back((*m)[i][0]);

                        break;

                default:
                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected format (N=%d columns found) for \"%s\".. Cannot convert to std::vector<std::complex>", m->GetNcols(), asciifilename.Data());
        }

        return v;
}

std::vector<std::complex<double>> UIUC::TFactory::LoadComplexVectorFromASCII(TString asciifilename)
{
        TMatrixD *m = UIUC::TFactory::LoadMatrixFromASCII(asciifilename);
        std::vector<std::complex<double>> v;

        switch(m->GetNcols()) {

                case 1:
                        for(int i = 0, I = m->GetNrows(); i < I; i++)
                                v.push_back(std::complex<double>((*m)[i][0], 0));

                        break;
                case 2:
                        for(int i = 0, I = m->GetNrows(); i < I; i++)
                                v.push_back(std::complex<double>((*m)[i][0], (*m)[i][1]));
                        break;
                default:
                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Unexpected format (N=%d columns found) for \"%s\".. Cannot convert to std::vector<double>", m->GetNcols(), asciifilename.Data());
        }

        return v;
}

std::vector<std::vector<TString>> UIUC::TFactory::LoadStringFromASCII(TString asciifilename)
{
        std::ifstream inputFile(asciifilename);
        if (!inputFile.is_open()) {
                std::cerr << "Error: Unable to open the input file." << std::endl;
                return {};
        }

        std::vector<std::vector<TString>> stringVector;
        for(string line; getline(inputFile, line); ) {

                std::vector<TString> str = UIUC::Explode(" ", TString(line.c_str()));
                UIUC::RemoveEmpty(str);
                
                stringVector.push_back(str);
        }

        inputFile.close();

        return stringVector;
}

bool UIUC::TFactory::SaveIntoASCII(TString ascii_str, std::ios_base::openmode mode, const std::vector<std::vector<TString>> &v)
{
        ofstream fASCII(ascii_str, mode);
        if( !fASCII.is_open() ) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "ASCII file " + ascii_str + " not found..");
                return false;
        }

        fASCII << "# Lines = " << v.size() << endl;
        for(int i = 0, I = v.size(); i < I; i++) {

                for(int j = 0, J = v.size(); j < J; j++)
                        fASCII << Form("%s", UIUC::Implode("\t", v[i]).Data());

                fASCII << endl;
        }

        fASCII.close();
        return true;
}

bool UIUC::TFactory::SaveIntoASCII(TString ascii_str, std::ios_base::openmode mode, const std::vector<double> &v, int precision)
{
        std::vector<std::vector<double>> vv(v.size());
        for(int i = 0, I = v.size(); i < I; i++) {
                vv[i] = std::vector<double>({v});
        }

        return UIUC::TFactory::SaveIntoASCII(ascii_str, mode, vv, precision);
}

bool UIUC::TFactory::SaveIntoASCII(TString ascii_str, std::ios_base::openmode mode, const std::vector<std::vector<double>> &v, int precision)
{
        ofstream fASCII(ascii_str, mode);
        if( !fASCII.is_open() ) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "ASCII file " + ascii_str + " not found..");
                return false;
        }

        fASCII << "# vector.size = " << v.size() << endl;
        for(int i = 0, I = v.size(); i < I; i++) {

                for(int j = 0, J = v[i].size(); j < J; j++)
                        fASCII << Form("%."+TString::Itoa(precision,10)+"e", v[i][j]);

                fASCII << endl;
        }

        fASCII.close();
        return true;
}

bool UIUC::TFactory::SaveIntoASCII(TString ascii_str,std::ios_base::openmode mode, const std::vector<std::complex<double>> &v, int precision)
{
        ofstream fASCII(ascii_str, mode);
        if( !fASCII.is_open() ) {

                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "ASCII file " + ascii_str + " not found..");
                return false;
        }

        fASCII << "# complex.size = " << v.size() << endl;
        for(int i = 0, I = v.size(); i < I; i++) {

                fASCII << Form("%."+TString::Itoa(precision,10)+"e %."+TString::Itoa(precision,10)+"e", v[i].real(), v[i].imag());
                fASCII << endl;
        }

        fASCII.close();
        return true;
}