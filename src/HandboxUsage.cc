/**
 **********************************************
 *
 * \file HandboxUsage.cc
 * \brief Source code of the Handbox class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <UIUC/HandboxUsage.h>

using namespace std;
#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

bool UIUC::HandboxUsage::bCallGraphics = true;
bool UIUC::HandboxUsage::bCallHistos = true;
bool UIUC::HandboxUsage::bCallTrees = true;

TApplication *UIUC::HandboxUsage::fApp = NULL;
TBrowser *UIUC::HandboxUsage::fBrowser = NULL;

bool UIUC::HandboxUsage::bGraphicsLoop = true;
int  UIUC::HandboxUsage::iApplyGraphics = 0;
bool UIUC::HandboxUsage::bOpenGraphics = false;

const bool UIUC::HandboxUsage::kReverseLogic = true;
const bool UIUC::HandboxUsage::kMandatory = true;
const bool UIUC::HandboxUsage::kOptional  = !UIUC::HandboxUsage::kMandatory;
const bool UIUC::HandboxUsage::kUseDefault = true;
const bool UIUC::HandboxUsage::kNotUseDefault  = !UIUC::HandboxUsage::kUseDefault;

void UIUC::HandboxUsage::EnableGraphicsHandler(){

    UIUC::HandboxUsage::bGraphicsLoop = true;

    UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Graphics catcher is enabled..");

    UIUC::HandboxMsg::SetSingleCarriageReturn();
    UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Please use Ctrl+C to close canvas(es)..\n");
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = UIUC::HandboxUsage::HandlerGRAPHICS;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
}

void UIUC::HandboxUsage::DisableSignalHandler() {

    UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Signal catcher is disabled..");
    signal(SIGINT, SIG_DFL);
}

void UIUC::HandboxUsage::HandlerGRAPHICS(int s){

    cerr << endl;
    cerr << "** Caught signal " << s << endl;
    cerr << "** You pressed CTRL-C => Trying to exit graphics mode.." << endl;
    cerr << "** Resume normal execution.." << endl;

    UIUC::HandboxUsage::DisableSignalHandler();
    UIUC::HandboxUsage::bGraphicsLoop = false;

    UIUC::HandboxMsg::EmptyTrash();
}

void UIUC::HandboxUsage::OpenGraphics(Option_t *option) {

    if(bOpenGraphics) return;     // Already opened

    TString opt = option;
    if(opt.EqualTo("")) opt = gSystem->Getenv("ESCALADE_OPT");

    // If draw option is set.. start a session in advance..
    if((opt.Contains("b") || opt.Contains("g"))) {

        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Graphics interface opened.. close windows to exit..");
        if(!UIUC::HandboxUsage::fApp) UIUC::HandboxUsage::fApp = new TApplication("root", NULL, NULL);
    }

    if(opt.Contains("b")) {

        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "TBrowser interface opened.. close windows to exit..");
        if(UIUC::HandboxUsage::fBrowser == NULL) UIUC::HandboxUsage::fBrowser = new UIUC::HandboxBrowser();
    }

    if(!opt.Contains("b") && !opt.Contains("g")) {

        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No graphics interface called.. (Use b-option for tbrowser / g-option for graphics)");
    }

    UIUC::HandboxUsage::bOpenGraphics = true;
}

bool UIUC::HandboxUsage::HasGraphics(Option_t *option) {

    TString opt = option;
    if(opt.EqualTo("")) opt = gSystem->Getenv("ESCALADE_OPT");

    return (UIUC::HandboxUsage::bOpenGraphics || opt.Contains("g") || opt.Contains("b")) && !gROOT->IsBatch();
}

void UIUC::HandboxUsage::WaitForGraphicsToBeClosed(Option_t *option) {

    TString opt = option;
    if(opt.EqualTo("")) opt = gSystem->Getenv("ESCALADE_OPT");

    UIUC::HandboxUsage::EnableGraphicsHandler();
    if ( gROOT->GetListOfCanvases()->GetSize() ) {

        TString eMessage = "Canvas found, but no graphics option enabled.. skip";
        if(UIUC::HandboxMsg::Warning(!HasGraphics(opt), __METHOD_NAME__, eMessage)) {

            UIUC::HandboxUsage::bGraphicsLoop = false;
            return;
        }

        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "List of canvases found:");
        for(int i = 0; i < gROOT->GetListOfCanvases()->GetSize(); i++) {

            TCanvas *c = (TCanvas*) gROOT->GetListOfCanvases()->At(i);
            c->cd();             //Needed to set back gPad to the main canvas, not subpad

            UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "* Name:\"" + TString(c->GetName()) + "\"; (Title: \""+TString(c->GetTitle())+"\")");
            c->Modified();
            c->Update();
        }

        while(UIUC::HandboxUsage::bGraphicsLoop)
            gSystem->ProcessEvents();
    }

    UIUC::HandboxUsage::CloseGraphics();
    UIUC::HandboxUsage::DisableSignalHandler();
}

void UIUC::HandboxUsage::CloseGraphics()
{
    if(fBrowser) {

        delete fBrowser;
        fBrowser = NULL;
    }

    UIUC::HandboxUsage::bOpenGraphics = false;
}

vector<TObject*> UIUC::HandboxUsage::ApplyGraphicsOptions(TVirtualPad *vpad, TString *option0, vector<TObject*> vObject)
{
    bCallGraphics = true;
    TString option = (option0 == NULL) ? "" : *option0;

    // Pad information
    if(vpad != NULL) {

        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, (TString) "Applying handbox graphics options on \""+vpad->GetName()+"\"");

        if(!TString(gSystem->Getenv("ESCALADE_CANVAS_TITLE")).EqualTo("")) {

            TString title0 = gSystem->Getenv("ESCALADE_CANVAS_TITLE");
            if(title0.BeginsWith(";")) title0 = " " + title0;

            TObjArray *array = title0.Tokenize(";");
            int w = (array->GetEntries() > 1) ? ((TObjString *)(array->At(1)))->String().Atoi() : vpad->GetWw();
            int h = (array->GetEntries() > 2) ? ((TObjString *)(array->At(2)))->String().Atoi() : vpad->GetWh();

            TString title = (array->GetEntries() > 0) ? (TString) ((TObjString *)(array->At(0)))->String() : (TString) vpad->GetTitle();
            if(!title.EqualTo("")) ((TCanvas*) vpad)->SetTitle(title);
            if(vpad->InheritsFrom("TCanvas"))
                ((TCanvas*) vpad)->SetWindowSize(w, h);

            delete array;
            array = NULL;
        }

        if(gSystem->Getenv("ESCALADE_PALETTE")) gStyle->SetPalette(TString(gSystem->Getenv("ESCALADE_PALETTE")).Atoi());
        if(gSystem->Getenv("ESCALADE_GRIDX")) vpad->SetGridx();
        if(gSystem->Getenv("ESCALADE_GRIDY")) vpad->SetGridy();

        if(gSystem->Getenv("ESCALADE_LOGX")) vpad->SetLogx();
        if(gSystem->Getenv("ESCALADE_LOGY")) {

            pair<double,double> margin = {0.1, 0.1};
            UIUC::TFactory::SetAutomaticRangeUser(vObject, margin, 1);
            vpad->SetLogy();
        }

        if(gSystem->Getenv("ESCALADE_LOGZ")) vpad->SetLogz();
        if(gSystem->Getenv("ESCALADE_NOSTATBOX")) gStyle->SetOptStat(0);
    }

    if(!vObject.size()) return {};

    UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Adding special title to the vector list to be drawn..");
    if(UIUC::TFactory::FindFirstHistogram(vObject) == NULL) return {};

    TH1 *firstObj = UIUC::TFactory::FindFirstHistogram(vObject);
    TH1 *obj0 = (TH1*) firstObj->Clone("HandboxObj0");
    ((TH1*) obj0)->SetDirectory(0);

    // Change name of obj0
    TString statbox_title;
    statbox_title = (TString) "Statbox";
    ((TH1*) obj0)->SetName(statbox_title);
   
    // Change title of obj0
    if(!TString(gSystem->Getenv("ESCALADE_TITLE")).EqualTo("")) {

        vector<TString> vTitle0 = TVarexp(gSystem->Getenv("ESCALADE_TITLE")).Tokenize(';');
        vector<TString> vTitle;
        for(int i = 0; i < (int) vTitle0.size(); i++) {

            if(UIUC::MathMod(i,4) == 0) {

                vTitle.push_back(vTitle0[i]);
                if(vTitle.back().EqualTo("")) vTitle.back() = " ";
                continue;
            }

            vTitle.back() += ";" + vTitle0[i];
        }

        TString title = vTitle[iApplyGraphics%vTitle.size()];
        obj0->SetTitle(UIUC::TFactory::MultipleLines(title));
    }

    TAxis *xaxis = ((TH1*)obj0)->GetXaxis();

    if(gSystem->Getenv("ESCALADE_XTITLE") && xaxis != NULL) 
        xaxis->SetTitle(gSystem->Getenv("ESCALADE_XTITLE"));
    TAxis *yaxis = ((TH1*)obj0)->GetYaxis();
    if(gSystem->Getenv("ESCALADE_YTITLE") && yaxis != NULL)
        yaxis->SetTitle(gSystem->Getenv("ESCALADE_YTITLE"));

    TAxis *zaxis = ((TH1*)obj0)->GetZaxis();
    if(gSystem->Getenv("ESCALADE_ZTITLE") && zaxis != NULL)
        zaxis->SetTitle(gSystem->Getenv("ESCALADE_ZTITLE"));

    TString timeFormat = "";

    // X axis daytime format
    if(gSystem->Getenv("ESCALADE_DAYX") && xaxis != NULL)
        timeFormat = "%Y-%m-%d";
    if(gSystem->Getenv("ESCALADE_TIMEX") && xaxis != NULL)
        timeFormat += " %H:%M:%S";

    if(!timeFormat.EqualTo(""))
    {
        xaxis->SetTimeDisplay(true);
        xaxis->SetNdivisions(gSystem->Getenv("ESCALADE_DAYX") && gSystem->Getenv("ESCALADE_TIMEX") ? 503 : 505);
        xaxis->SetTimeFormat(timeFormat.Strip(TString::kBoth).Data() + TString(" UTC"));
        xaxis->SetTimeOffset(0,"GMT");
    }

    // Z axis daytime format
    timeFormat = "";
    if(gSystem->Getenv("ESCALADE_DAYY") && yaxis != NULL)
        timeFormat = "%Y-%m-%d";
    if(gSystem->Getenv("ESCALADE_TIMEY") && yaxis != NULL)
        timeFormat += " %H:%M:%S";
    
    if(!timeFormat.EqualTo(""))
    {
        yaxis->SetTimeDisplay(true);
        xaxis->SetNdivisions(gSystem->Getenv("ESCALADE_DAYY") && gSystem->Getenv("ESCALADE_TIMEY") ? 503 : 505);
        yaxis->SetTimeFormat(timeFormat.Strip(TString::kBoth).Data() + TString(" UTC"));
        yaxis->SetTimeOffset(0,"GMT");
    }

    // Z axis daytime format
    timeFormat = "";
    if(gSystem->Getenv("ESCALADE_DAYZ") && zaxis != NULL)
        timeFormat = "%Y-%m-%d";
    if(gSystem->Getenv("ESCALADE_TIMEZ") && zaxis != NULL)
        timeFormat += " %H:%M:%S";

    if(!timeFormat.EqualTo(""))
    {
        zaxis->SetTimeDisplay(true);
        xaxis->SetNdivisions(gSystem->Getenv("ESCALADE_DAYZ") && gSystem->Getenv("ESCALADE_TIMEZ") ? 503 : 505);
        zaxis->SetTimeFormat(timeFormat.Strip(TString::kBoth).Data());
        zaxis->SetTimeOffset(0,"GMT");
    }

    // Axis Divisions
    if(gSystem->Getenv("ESCALADE_DIVISIONX") && xaxis) 
        xaxis->SetNdivisions(TString(gSystem->Getenv("ESCALADE_DIVISIONX")).Atoi());
    if(gSystem->Getenv("ESCALADE_DIVISIONY") && yaxis) 
        yaxis->SetNdivisions(TString(gSystem->Getenv("ESCALADE_DIVISIONY")).Atoi());
    if(gSystem->Getenv("ESCALADE_DIVISIONZ") && zaxis) 
        zaxis->SetNdivisions(TString(gSystem->Getenv("ESCALADE_DIVISIONZ")).Atoi());

    // Axis title offset
    if(xaxis != NULL) xaxis->SetTitleOffset(1.40);
    if(yaxis != NULL) yaxis->SetTitleOffset(1.25);
    
    std::vector<TObject*>::iterator it;
    while( (it = find(vObject.begin(), vObject.end(), nullptr)) != vObject.end())
        vObject.erase(it);
        
    // THIS SHOULDN'T BE applied HERE..
    // Range should be selected by user.. (e.g. ratio plot + main plot.. axis might be different)
    //  UIUC::TFactory::SetAutomaticRangeUser(vObject);
    //  obj0 = (TH1*) UIUC::TFactory::SetAutomaticRange(obj0, vObject);

    double xmin = TString(gSystem->Getenv("ESCALADE_XMIN")).Atof();
    double xmax = TString(gSystem->Getenv("ESCALADE_XMAX")).Atof();
    double ymin = TString(gSystem->Getenv("ESCALADE_YMIN")).Atof();
    double ymax = TString(gSystem->Getenv("ESCALADE_YMAX")).Atof();
    double zmin = TString(gSystem->Getenv("ESCALADE_ZMIN")).Atof();
    double zmax = TString(gSystem->Getenv("ESCALADE_ZMAX")).Atof();

    // Rearrange the outgoing vector
    vObject.insert(vObject.begin(), obj0);
    //  UIUC::TFactory::Empty(obj0);

    for(int i = 0; i < vObject.size(); i++) {

        if(vObject[i] == NULL) continue;

        if( ((TH1*)vObject[i])->InheritsFrom("TH1") ) {

            if(gSystem->Getenv("ESCALADE_XMIN") && gSystem->Getenv("ESCALADE_XMAX") && ((TH1*)vObject[i])->GetXaxis()) ((TH1*)vObject[i])->GetXaxis()->SetRangeUser(xmin, xmax);
            if(gSystem->Getenv("ESCALADE_YMIN") && gSystem->Getenv("ESCALADE_YMAX") && ((TH1*)vObject[i])->GetYaxis()) ((TH1*)vObject[i])->GetYaxis()->SetRangeUser(ymin, ymax);
            if(gSystem->Getenv("ESCALADE_ZMIN") && gSystem->Getenv("ESCALADE_ZMAX") && ((TH1*)vObject[i])->GetZaxis()) ((TH1*)vObject[i])->GetZaxis()->SetRangeUser(zmin, zmax);

        } else if( ((TH1*)vObject[i])->InheritsFrom("TGraph") ) {

            if(gSystem->Getenv("ESCALADE_XMIN") && gSystem->Getenv("ESCALADE_XMAX") && ((TGraph*)vObject[i])->GetXaxis()) ((TGraph*)vObject[i])->GetXaxis()->SetRangeUser(xmin, xmax);
            if(gSystem->Getenv("ESCALADE_YMIN") && gSystem->Getenv("ESCALADE_YMAX") && ((TGraph*)vObject[i])->GetYaxis()) ((TGraph*)vObject[i])->GetYaxis()->SetRangeUser(ymin, ymax);
        }
    }

    // Hide the reference..
    obj0->SetLineWidth(0);
    obj0->SetLineColor(0);
    obj0->SetMarkerColor(0);
    obj0->SetFillColor(0);

    //  delete vObject[0];
    //vObject[0] = obj0;

    if(((TH1*) obj0)->GetFillStyle()==1001) option.ReplaceAll("BAR", "");
    if(gSystem->Getenv("ESCALADE_NOERROR")) {

        for(int i = 0, N = vObject.size(); i < N; i++) {

            if(vObject[i] == NULL) continue;
            ((TH1*)vObject[i])->SetFillStyle(1001);
            ((TH1*)vObject[i])->SetFillColor(0);
        }

        option += "HIST ";
    }

    if(option0 != NULL) *option0 = option;

    iApplyGraphics++;
    return vObject;
}

void UIUC::HandboxUsage::ApplyHistosOptions(TH1 *h)
{
    bCallHistos = true;
    if(h == NULL) return;

    UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, (TString) "Applying handbox histos options on \""+h->GetName()+"\"");

    if(gSystem->Getenv("ESCALADE_HNORM0")) h = UIUC::TFactory::NormalizeByBin(h, TString(gSystem->Getenv("ESCALADE_HNORM0")).Atoi());
    else if (gSystem->Getenv("ESCALADE_HNORM1")) h = UIUC::TFactory::NormalizeByMax(h);
    else if (gSystem->Getenv("ESCALADE_HNORM2")) h = UIUC::TFactory::NormalizeByEntries(h);
    else if (gSystem->Getenv("ESCALADE_HNORM3")) h = UIUC::TFactory::NormalizeByIntegral(h);
    if(gSystem->Getenv("ESCALADE_HBINWIDTH")) h = UIUC::TFactory::NormalizeByBinWidth(h);

    if(gSystem->Getenv("ESCALADE_HSCALE")) UIUC::TFactory::Normalize(h, "1./("+TString(gSystem->Getenv("ESCALADE_HSCALE"))+")");

    if(gSystem->Getenv("ESCALADE_HRELATIVEUNCERTAINTY")) UIUC::TFactory::DivideWithoutErrorPropagation(h, h);
    if(gSystem->Getenv("ESCALADE_HONLYCONTENT")) UIUC::TFactory::RemoveError(h);
    if(gSystem->Getenv("ESCALADE_HONLYERROR")) UIUC::TFactory::RemoveContent(h);
}

void UIUC::HandboxUsage::ApplyTreesOptions(TTree *t)
{
    bCallTrees = true;
    if(t == NULL) return;

    if(gSystem->Getenv("ESCALADE_TREEENTRIES")) {

           int nentries = TString(gSystem->Getenv("ESCALADE_TREEENTRIES")).Atoi();
           if(nentries < t->GetEntries()) t->SetEntries(nentries);
    }
}

void UIUC::HandboxUsage::Init(int *newArgc, char **newArgv, Option_t *newOption) {

    if(newArgc == 0) return;
    if(UIUC::HandboxMsg::Error(this->argc != 0, __METHOD_NAME__, "UIUC::HandboxUsage already initialized..")) return;
  
    this->option = newOption;
    TString opt = this->option;
            opt.ToLower();

    this->kPrintUsageOnDestroy = true;

    this->iarg = 0;
    this->kUsage = 1;
    this->bArgOptionLock = false;

    this->argc = *newArgc;
    for(int i = 0; i < this->argc; i++) {

        TString argv_str = newArgv[i];
        argv_str.ReplaceAll('\n', ' ');
        argv.push_back(argv_str);
    }

    this->AddTitle("Default handbox options");

    // Debug must be before redirecting..
    int iDebug = (int) Verbosity::VERBOSITY_NORMAL;

    bool debug = false;
    this->AddOption("-v", "", &debug);
    if(debug) iDebug = (int) Verbosity::VERBOSITY_VERBOSE;
    this->AddOption("-vv", "", &debug);
    if(debug) iDebug = (int) Verbosity::VERBOSITY_VERY_VERBOSE;
    this->AddOption("-vvv", "", &debug);
    if(debug) iDebug = (int) Verbosity::VERBOSITY_DEBUG;
    this->AddOption("--debug", "", &debug);
    if(debug) iDebug = (int) Verbosity::VERBOSITY_DEBUG;

    UIUC::HandboxMsg::kDebug = TMath::Max(UIUC::HandboxMsg::kDebug, iDebug);
    UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);

    bool bQuiet = false;
    this->AddOption("--silent|--quiet|-q", "", &bQuiet);
    if(bQuiet) UIUC::HandboxMsg::kQuiet = true;

    bool bSuperQuiet = false;
    this->AddOption("--very-silent|--very-quiet|-qq", "", &bSuperQuiet);
    if(bSuperQuiet) UIUC::HandboxMsg::kSuperQuiet = true;

    this->AddOption("--help|-h", "", &this->kUsage, kReverseLogic);

    TString HandboxOptionStr = "";
    this->AddOption("--opt|--option", "(Custom handbox option)", &HandboxOptionStr);
    if(!HandboxOptionStr.EqualTo("")) gSystem->Setenv("ESCALADE_OPT", TString(gSystem->Getenv("ESCALADE_OPT")) + HandboxOptionStr);

    bool bNoUpdate = false;
    this->AddOption("--no-update", "(Do not update if a ROOT object is found in the outgoing file)", &bNoUpdate);
    if(bNoUpdate) gSystem->Setenv("ESCALADE_NOUPDATE", "1");

    bool bSumw2;
    this->AddOption("--no-sumw2", "(Sumw2 is set by default and applied to all histos.. disable only after rtfm)", &bSumw2, UIUC::HandboxUsage::kReverseLogic);
    if(bSumw2) gSystem->Setenv("ESCALADE_HSUMW2", "1");
    TH1::SetDefaultSumw2(bSumw2);

    // Basics options...
    if(opt.Contains("basics")) {

        opt.ReplaceAll("basics", "");

        this->AddTitle("Basic handbox options");

        TString HandboxInputPattern;
        this->AddOption("--input", "input.root (incl. patterns; e.g. input-{}-{}.root)", &HandboxInputPattern);
        if(!HandboxInputPattern.EqualTo("")) gSystem->Setenv("ESCALADE_HINPUT", HandboxInputPattern);

        TString HandboxOutputPattern;
        this->AddOption("--output", "output.root (incl. patterns; e.g. output-{}-{}.root, or output-{0}-{1}.root)", &HandboxOutputPattern);
        if(!HandboxOutputPattern.EqualTo("")) gSystem->Setenv("ESCALADE_HOUTPUT", HandboxOutputPattern);

        TString HandboxOutputFinalizePattern;
        this->AddOption("--output-finalize", "output-finalize.root", &HandboxOutputFinalizePattern);
        if(!HandboxOutputFinalizePattern.EqualTo("")) gSystem->Setenv("ESCALADE_HOUTPUT_FINALIZE", HandboxOutputFinalizePattern);

        TString HandboxOutputTitle;
        this->AddOption("--output-title", "Output description to apply", &HandboxOutputTitle);
        if(!HandboxOutputTitle.EqualTo("")) gSystem->Setenv("ESCALADE_HOUTPUT_TITLE", HandboxOutputTitle);

        bool bDumpFile = false;
        this->AddOption("--output-dump", "(Dump all remaining files in the memory into a file)", &bDumpFile);
        if(bDumpFile) gSystem->Setenv("ESCALADE_OPT", TString(gSystem->Getenv("ESCALADE_OPT"))+"d");

        bool bPrint = false;
        this->AddOption("--print0", "(Print info about all objects in handbox's memory)", &bPrint);
        if(bPrint) gSystem->Setenv("ESCALADE_OPT", TString(gSystem->Getenv("ESCALADE_OPT"))+"p");

        int seed = 0;
        this->AddOption("--seed", "Random seed", &seed);
        if(seed) {
            gSystem->Setenv("ESCALADE_SEED", TString::Itoa(seed,10));
            UIUC::SetRandomSeed(seed);
        }

        TString PdfNameStr;
        this->AddOption("--pdf", "Change outgoing PDF name", &PdfNameStr);
        if(!PdfNameStr.EqualTo("")) {
            
            gSystem->Setenv("ESCALADE_PDFNAME", PdfNameStr);
            gSystem->Setenv("ESCALADE_OPT", TString(gSystem->Getenv("ESCALADE_OPT"))+"c");
        }

        int nThreads = -1;
        this->AddOption("--threads|-j", "Number of concurrent threads", &nThreads);
        if(nThreads > 0) gSystem->Setenv("ESCALADE_NTHREADS", TString::Itoa(nThreads,10));

        int iEntries = -1;
        this->AddOption("--read-entries|-re", "(Limit the number of entry to read with handbox)", &iEntries);
        if(iEntries > 0) gSystem->Setenv("ESCALADE_READENTRIES", TString::Itoa(iEntries,10));

        int iSkipEntries = -1;
        this->AddOption("--skip-entries|-se", "(Skip entry in handbox)", &iSkipEntries);
        if(iSkipEntries > 0) gSystem->Setenv("ESCALADE_SKIPENTRIES", TString::Itoa(iSkipEntries,10));

        // TO BE REPLACED WITH BADLIST
        // Apply generic event list based on identifiers
        // TString BadListStr;
        // this->AddOption("--badlist", "<badlist.txt> (This still require to call the method or apply it to your tree/histogram)", &BadListStr);
        // if(!BadListStr.EqualTo("")) {

        //     BadListStr.ReplaceAll('\n', ' ');
        //     if(BadListStr.Contains("{}")) {

        //         UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "You cannot use \"{}\" using badlist parameter..");
        //         this->kUsage = false;

        //     } else {

        //         vector<TString> vBadListStr = UIUC::TFileReader::ExpandWildcardUrl(BadListStr);
        //         if((int) vBadListStr.size() == 0) {

        //             UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "No badlist file found after expanding wildcard..");
        //             this->kUsage = false;

        //         } else {

        //             BadListStr = vBadListStr[0];
        //             for(int i = 1; i < (int) vBadListStr.size(); i++) BadListStr += " " + vBadListStr[i];

        //             gSystem->Setenv("ESCALADE_BADID", BadListStr);
        //             if( !UIUC::TFileReader::LoadBadList(gSystem->Getenv("ESCALADE_BADID"))) this->kUsage = false;
        //             UIUC::TFileReader::bUseBadList = false;
        //         }
        //     }
        // }

        // TString GoodSpillStr;
        // this->AddOption("--goodspill", "<goodspill.txt> (This still require to call the method or apply it to your tree/histogram)", &GoodSpillStr);
        // if(!GoodSpillStr.EqualTo("")) {

        //     GoodSpillStr.ReplaceAll('\n', ' ');
        //     if(GoodSpillStr.Contains("{}")) {

        //         UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "You cannot use \"{}\" using GoodSpill parameter..");
        //         this->kUsage = false;

        //     } else {

        //         vector<TString> vGoodSpillStr = UIUC::TFileReader::ExpandWildcardUrl(GoodSpillStr);
        //         if((int) vGoodSpillStr.size() == 0) {

        //             UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "No GoodSpill file found after expanding wildcard..");
        //             this->kUsage = false;

        //         } else {

        //             GoodSpillStr = vGoodSpillStr[0];
        //             for(int i = 1; i < (int) vGoodSpillStr.size(); i++) GoodSpillStr += " " + vGoodSpillStr[i];

        //             gSystem->Setenv("ESCALADE_GOODSPILL", GoodSpillStr);
        //             if( !UIUC::TFileReader::LoadGoodSpill(gSystem->Getenv("ESCALADE_GOODSPILL"))) this->kUsage = false;
        //             UIUC::TFileReader::bUseGoodSpill = false;
        //         }
        //     }
        // }
    }

    // Graphics options...
    if(opt.Contains("graphics")) {

        opt.ReplaceAll("graphics", "");
        this->AddTitle("Graphical handbox options");
        bCallGraphics = false;

        bool bGraphics = false;
        this->AddOption("--graphics", "(Draw canvas)", &bGraphics);
        if(bGraphics) {

            gSystem->Setenv("ESCALADE_OPT", TString(gSystem->Getenv("ESCALADE_OPT"))+"g");
            UIUC::HandboxUsage::OpenGraphics("g");
        }

        TString HandboxStyleStr = "";
        double fsize = NAN,tsize,lsize,asize, ssize;
        this->AddOption("--font-size|--fsize", "Change size of all fonts", &fsize);
        if(fsize != NAN) {
            tsize = fsize;
            lsize = fsize;
            asize = fsize;
            ssize = fsize;
        }

        if(!UIUC::IsNaN(tsize)) gSystem->Setenv("ESCALADE_TSIZE", Form("%f", tsize));
        if(!UIUC::IsNaN(lsize)) gSystem->Setenv("ESCALADE_LSIZE", Form("%f", lsize));
        if(!UIUC::IsNaN(asize)) gSystem->Setenv("ESCALADE_ASIZE", Form("%f", asize));
        if(!UIUC::IsNaN(ssize)) gSystem->Setenv("ESCALADE_SSIZE", Form("%f", ssize));

        this->AddOption("--title-size|--tsize", "Change size of the font", &tsize);
        this->AddOption("--legend-size|--lsize", "Change size of the font", &lsize);
        this->AddOption("--axis-size|--asize", "Change size of the font", &asize);
        this->AddOption("--statbox-size|--ssize", "Change size of the font", &ssize);

        double foffset = NAN,toffset,loffset,aoffset, soffset;
        this->AddOption("--font-offset|--foffset", "Change offset of the font", &foffset);
        if(foffset != NAN) {
            toffset = foffset;
            loffset = foffset;
            aoffset = foffset;
            soffset = foffset;
        }
        if(!UIUC::IsNaN(toffset)) gSystem->Setenv("ESCALADE_TOFFSET", Form("%f", toffset));
        if(!UIUC::IsNaN(loffset)) gSystem->Setenv("ESCALADE_LOFFSET", Form("%f", loffset));
        if(!UIUC::IsNaN(aoffset)) gSystem->Setenv("ESCALADE_AOFFSET", Form("%f", aoffset));
        if(!UIUC::IsNaN(soffset)) gSystem->Setenv("ESCALADE_SOFFSET", Form("%f", soffset));

        this->AddOption("--title-offset|--toffset", "Change offset of the font", &toffset);
        this->AddOption("--legend-offset|--loffset", "Change offset of the font", &loffset);
        this->AddOption("--axis-offset|--aoffset", "Change offset of the font", &aoffset);
        this->AddOption("--statbox-offset|--soffset", "Change offset of the font", &soffset);

        this->AddOption("--style|--custom-style", "<style name> (Load a custom precompiled style)", &HandboxStyleStr);
        if(!HandboxStyleStr.EqualTo("")) {

            gSystem->Setenv("ESCALADE_OPT", TString(gSystem->Getenv("ESCALADE_OPT"))+"h");
            gSystem->Setenv("ESCALADE_STYLE", HandboxStyleStr.Data());
            UIUC::HandboxStyle::Instance().UseStyle(HandboxStyleStr);
        }

        int imax = 0;
        this->AddOption("--maxdigit-axis|--maxis", "Change max number of digits", &imax);
        if(imax) TGaxis::SetMaxDigits(imax);

        TString HandboxCanvasTitleStr = "";
        this->AddOption("--canvas|--canvas-title", "title;width;height", &HandboxCanvasTitleStr);
        if(!HandboxCanvasTitleStr.EqualTo("")) gSystem->Setenv("ESCALADE_CANVAS_TITLE", HandboxCanvasTitleStr);

        TString HandboxPaletteStr = "";
        this->AddOption("--pal|--palette", "", &HandboxPaletteStr);
        if(!HandboxPaletteStr.EqualTo("")) gSystem->Setenv("ESCALADE_PALETTE", HandboxPaletteStr);

        TString HandboxTitleStr = "";
        this->AddOption("--title", "", &HandboxTitleStr);
        if(!HandboxTitleStr.EqualTo("")) gSystem->Setenv("ESCALADE_TITLE", HandboxTitleStr);

        bool bNoStatBox= false;
        this->AddOption("--no-statbox", "", &bNoStatBox);
        if(bNoStatBox) gSystem->Setenv("ESCALADE_NOSTATBOX", "1");

        bool bNoLegend= false;
        this->AddOption("--no-legend", "", &bNoLegend);
        if(bNoLegend) gSystem->Setenv("ESCALADE_NOLEGEND", "1");
        UIUC::HandboxStyle::Instance().ShowLegend();

        bool bAutoScale= false;
        this->AddOption("--autoscale", "Autoscale axis defined in custom style", &bAutoScale);
        if(bAutoScale) gSystem->Setenv("ESCALADE_AUTOSCALE", "1");

        vector<double> xrange(2, NAN);
        this->AddOption("--x-axis|-x|--x", "", xrange);
        if(!UIUC::IsNaN(xrange[0])) gSystem->Setenv("ESCALADE_XMIN", Form("%.30f", xrange[0]));
        if(!UIUC::IsNaN(xrange[1])) gSystem->Setenv("ESCALADE_XMAX", Form("%.30f", xrange[1]));

        vector<double> yrange(2, NAN);
        this->AddOption("--y-axis|-y|--y", "", yrange);
        if(!UIUC::IsNaN(yrange[0])) gSystem->Setenv("ESCALADE_YMIN", Form("%.30f", yrange[0]));
        if(!UIUC::IsNaN(yrange[1])) gSystem->Setenv("ESCALADE_YMAX", Form("%.30f", yrange[1]));

        vector<double> zrange(2, NAN);
        this->AddOption("--z-axis|-z|--z", "", zrange);
        if(!UIUC::IsNaN(zrange[0])) gSystem->Setenv("ESCALADE_ZMIN", Form("%.30f", zrange[0]));
        if(!UIUC::IsNaN(zrange[1])) gSystem->Setenv("ESCALADE_ZMAX", Form("%.30f", zrange[1]));

        TString xtitle;
        this->AddOption("--x-title|--titlex", "", &xtitle);
        if(!xtitle.EqualTo("")) gSystem->Setenv("ESCALADE_XTITLE", xtitle);

        TString ytitle;
        this->AddOption("--y-title|--titley", "", &ytitle);
        if(!ytitle.EqualTo("")) gSystem->Setenv("ESCALADE_YTITLE", ytitle);

        TString ztitle;
        this->AddOption("--z-title|--titlez", "", &ztitle);
        if(!ztitle.EqualTo("")) gSystem->Setenv("ESCALADE_ZTITLE", ztitle);

        TString ScaleStr = "";
        this->AddOption("--scale ", "1e33", &ScaleStr);
        if(!ScaleStr.EqualTo(" ")) gSystem->Setenv("ESCALADE_SCALE", ScaleStr);

        bool bDayx = false;
        this->AddOption("--x-day|--dayx", "", &bDayx);
        if(bDayx) gSystem->Setenv("ESCALADE_DAYX", "1");

        bool bDayy = false;
        this->AddOption("--y-day|--dayy", "", &bDayy);
        if(bDayy) gSystem->Setenv("ESCALADE_DAYY", "1");

        bool bDayz = false;
        this->AddOption("--z-day|--dayz", "", &bDayz);
        if(bDayz) gSystem->Setenv("ESCALADE_DAYZ", "1");

        bool bTimex = false;
        this->AddOption("--x-time|--timex", "", &bTimex);
        if(bTimex) gSystem->Setenv("ESCALADE_TIMEX", "1");

        bool bTimey = false;
        this->AddOption("--y-time|--timey", "", &bTimey);
        if(bTimey) gSystem->Setenv("ESCALADE_TIMEY", "1");

        bool bTimez = false;
        this->AddOption("--z-time|--timez", "", &bTimez);
        if(bTimez) gSystem->Setenv("ESCALADE_TIMEZ", "1");

        bool bLogx= false;
        this->AddOption("--x-log|--logx", "", &bLogx);
        if(bLogx) gSystem->Setenv("ESCALADE_LOGX", "1");

        bool bLogy= false;
        this->AddOption("--y-log|--logy", "", &bLogy);
        if(bLogy) gSystem->Setenv("ESCALADE_LOGY", "1");

        bool bLogz= false;
        this->AddOption("--z-log|--logz", "", &bLogz);
        if(bLogz) gSystem->Setenv("ESCALADE_LOGZ", "1");

        bool bGridx= false;
        this->AddOption("--x-grid|--gridx", "", &bGridx);
        if(bGridx) gSystem->Setenv("ESCALADE_GRIDX", "1");

        bool bGridy= false;
        this->AddOption("--y-grid|--gridy", "", &bGridy);
        if(bGridy) gSystem->Setenv("ESCALADE_GRIDY", "1");

        double division_x = NAN;
        this->AddOption("--division-x|--divx", "", &division_x);
        if(!UIUC::IsNaN(division_x)) gSystem->Setenv("ESCALADE_DIVISIONX",TString::Itoa((int) division_x,10));

        double division_y = NAN;
        this->AddOption("--division-y|--divy", "", &division_y);
        if(!UIUC::IsNaN(division_y)) gSystem->Setenv("ESCALADE_DIVISIONY",TString::Itoa((int) division_y,10));

        double division_z = NAN;
        this->AddOption("--division-z|--divz", "", &division_z);
        if(!UIUC::IsNaN(division_z)) gSystem->Setenv("ESCALADE_DIVISIONZ",TString::Itoa((int) division_z,10));

        bool bNoError = false;
        this->AddOption("--no-errorbar", "", &bNoError);
        if(bNoError) gSystem->Setenv("ESCALADE_NOERROR", "1");

        bool bPrelimMark = false;
        this->AddOption("--preliminary|--prelim", "", &bPrelimMark);
        if(bPrelimMark) gSystem->Setenv("ESCALADE_PRELIMINARY", "1");

        TString sTagMark;
        this->AddOption("--tag", "", &sTagMark);
        if(!sTagMark.EqualTo("")) gSystem->Setenv("ESCALADE_TAG", sTagMark);
    }

    // Command related to histograms processing..
    if(opt.Contains("histos")) {

        opt.ReplaceAll("histos", "");
        this->AddTitle("Histogram handbox options");
        bCallHistos = false;

        double iNorm0 = -1;
        bool bNorm1, bNorm2, bNorm3;
        this->AddOption("--norm0", "<i> (use i-th bin normalization on histograms)", &iNorm0);
        if(iNorm0 != -1) gSystem->Setenv("ESCALADE_HNORM0", TString::Itoa(iNorm0,10));
        this->AddOption("--norm1", "(use max normalization on histograms)", &bNorm1);
        if(bNorm1) gSystem->Setenv("ESCALADE_HNORM1", "1");
        this->AddOption("--norm2", "(use nentries normalization on histograms)", &bNorm2);
        if(bNorm2) gSystem->Setenv("ESCALADE_HNORM2", "1");
        this->AddOption("--norm3", "(use integrale normalization on histograms)", &bNorm3);
        if(bNorm3) gSystem->Setenv("ESCALADE_HNORM3", "1");

        bool bDivideByBin = false;
        this->AddOption("--norm4|--bin-width", "(Divide the histogram by bin width)", &bDivideByBin);
        if(bDivideByBin) gSystem->Setenv("ESCALADE_HBINWIDTH", "1");

        bool bOnlyContent;
        this->AddOption("--only-content", "Remove error bar on the final outgoing histogram", &bOnlyContent);
        if(bOnlyContent) gSystem->Setenv("ESCALADE_HONLYCONTENT", "1");

        bool bOnlyError;
        this->AddOption("--only-error", "Remove content bar on the final outgoing histogram", &bOnlyError);
        if(bOnlyError) gSystem->Setenv("ESCALADE_HONLYERROR", "1");

        bool bOnlyRelativeUncertainty;
        this->AddOption("--only-error-relative|--only-relative", "Compute the relative uncertainty", &bOnlyRelativeUncertainty);
        if(bOnlyRelativeUncertainty) gSystem->Setenv("ESCALADE_HRELATIVEUNCERTAINTY", "1");
    }

    // Command related to histograms processing..
    if(opt.Contains("trees")) {

        opt.ReplaceAll("trees", "");
        this->AddTitle("Tree handbox options");
        bCallTrees = false;

        TString selectionStr;
        this->AddOption("--selection", "x<y", &selectionStr);
        if(!selectionStr.EqualTo("")) {

            selectionStr.ReplaceAll('\n', ' ');
            gSystem->Setenv("ESCALADE_TREESELECTION", selectionStr); // Selection is stored to be used later in trees...
            // UIUC::TFileReader::bUseEventList = false;
        }

        int iEntries = -1;
        this->AddOption("--nentries", "(Limit the number of entries to read)", &iEntries);
        if(iEntries > 0) gSystem->Setenv("ESCALADE_TREEENTRIES", TString::Itoa(iEntries,10));
    }

    TString opt2 = opt.ReplaceAll(" ", "");
    if(UIUC::HandboxMsg::Error(!opt2.EqualTo(""), __METHOD_NAME__, "Invalid handbox usage option provided.. \""+opt+"\" cannot be used.. please check !")) this->kUsage = false;
    this->AddTitle("User-defined options and arguments");

    // Put errors in the buffer..
    TString err;
    UIUC::HandboxMsg::GetTTY(__METHOD_NAME__, NULL, &err);
    if(!err.EqualTo("")) {
        bufferTTY[1].push_back(err);
    }
}

void UIUC::HandboxUsage::AddTitle(TString title)
{
    this->vTitle.push_back(title);
    this->vTitlePos.push_back(this->vArgName.size());
}

void UIUC::HandboxUsage::AddMessage(TString str)
{
    usage_msg += '\n' + str + '\n';
}

bool UIUC::HandboxUsage::AskHelp(int iarg)
{
    if(iarg > 0 && iarg < this->argc) {

         if(this->argv[iarg].EqualTo("--help") || this->argv[iarg].EqualTo("-h")) {

            this->kUsage = false;
            return true;
        }
    }

    for(int i = 1; i < this->argc; i++) {

        if(this->argv[i].EqualTo("--help") || this->argv[i].EqualTo("-h")) {

            this->kUsage = false;
            return true;
        }
    }

    return false;
}

bool UIUC::HandboxUsage::Usage(bool bForceUsage)
{
    if(UIUC::HandboxMsg::Error(this->argc == 0, __METHOD_NAME__, "UIUC::HandboxUsage not initialized..")) return 0;
    for(int i = 0; i < this->argc; i++) {

        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, TString("arg[i] = ") + this->argv[i]);
    }

    this->Print();
    cout << UIUC::HandboxMsg::kBlue << usage_msg << UIUC::HandboxMsg::kNoColor;
    cout << endl;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    if(!this->kUsage || bForceUsage) {

        // Print usage line
        cout << UIUC::HandboxMsg::kGreen << "Usage: " << UIUC::HandboxMsg::kNoColor << this->argv[0] << " ";

        bool bFirstMandatoryArg = true;
        for(int i = 0; i < this->vArgDefaultValue.size(); i++) {

            // Print title lines...
            unsigned int title_index = find(this->vTitlePos.begin(), this->vTitlePos.end(), i) - this->vTitlePos.begin();
            if( title_index != this->vTitlePos.size() ) {

                cout << endl << '\t' << UIUC::HandboxMsg::kPurple << "" << this->vTitle[title_index] << ":" << UIUC::HandboxMsg::kNoColor;
            }

            // Argument and options

            if(!this->vArgName[i].EqualTo("")) cout << endl << '\t';
            else {

                if(bFirstMandatoryArg) cout << endl << '\t';
                bFirstMandatoryArg = false;
            }
            cout << this->vArgColor[i];

            if(!this->vArgMandatory[i]) cout << "[";
            else cout << "<";

            if(!this->vArgName[i].EqualTo("")) {

                cout << this->vArgName[i];
                if(!this->vArgDefaultValue[i].EqualTo("")) cout << " \"";
            }

            cout << this->vArgDefaultValue[i];
            if(!this->vArgName[i].EqualTo("") && !this->vArgDefaultValue[i].EqualTo("")) cout << "\"";

            if(!this->vArgMandatory[i]) cout << "]";
            else cout << ">";

            cout << UIUC::HandboxMsg::kNoColor << " ";
        }

        if(!this->vArgDefaultValue.size()) cout << endl;
	cout << endl;

    } else {

        if((int) vArgNotUsed.size() != 0) {

            TString eMessage = this->argv[0] + ": too many parameters\n";
            eMessage += "Try '" + this->argv[0] + " --help' for more information";

            cerr << eMessage << endl;

            bCallTrees = true;
            bCallHistos = true;
            bCallGraphics = true;
            exit(1);
        }
    }

    for(int i = 0; i < (int) this->bufferTTY[0].size(); i++) {

        if(!this->bufferTTY[0][i].EqualTo("")) cout << UIUC::HandboxMsg::Trim(this->bufferTTY[0][i]) << endl;
        if(!this->bufferTTY[1][i].EqualTo("")) cerr << UIUC::HandboxMsg::Trim(this->bufferTTY[1][i]) << endl;
    }

    if(!WrongUsageStr.EqualTo(""))
        cerr << "Error: " << WrongUsageStr;

    return this->kUsage;
}

vector<int> UIUC::HandboxUsage::GetArgNotUsed() {

    vector <int> vNotUsedIndexes;

    for(int i = 1; i < this->argc; i++) {

        if( find(this->vArgAlreadyUsed.begin(), this->vArgAlreadyUsed.end(), i) == this->vArgAlreadyUsed.end() ) {

            vNotUsedIndexes.push_back(i);
        }
    }

    return vNotUsedIndexes;
}

int UIUC::HandboxUsage::GetArgIndex(TString argname) {

    int iarg = -1;

    vector<TString> argLC;
    for(int j = 0, J = this->argv.size(); j < J; j++) {
        argLC.push_back(this->argv[j]);
        argLC[j].ToLower();
    }

    // Find the option !
    TObjArray *argname_array = argname.Tokenize("|");
    for (int i = 0; i < argname_array->GetEntries(); i++) {

        TString optname = UIUC::HandboxMsg::Trim(((TObjString *)(argname_array->At(i)))->String());
        TString optNameLC = optname;
                optNameLC.ToLower();

        int index = find(argLC.begin(), argLC.end(), optNameLC) - argLC.begin();
        if( index < (int) this->argv.size() ) {

            // Double option found in the user command
            if(iarg != -1) {

                TString eMessage = "Option \""+optname+"\" already called..";
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

                this->kUsage = false;
                return -1;
            }
            iarg = index;
        }
    }

    delete argname_array;
    argname_array = NULL;

    // Check if a parameter has been used 2 types = wrong command written by the user
    if( find(this->vArgAlreadyUsed.begin(), this->vArgAlreadyUsed.end(), iarg) != this->vArgAlreadyUsed.end() ) {

        this->kUsage = false;
        return -1;
    }

    return iarg;
}

bool UIUC::HandboxUsage::CheckArg(int iarg) {

    TString str = "-?";
    if(iarg >= 0 && iarg < this->argc) str = this->argv[iarg];
    else {

        this->kUsage = false;
        return false;
    }

    if((str.BeginsWith("-") && !str.Contains(" ") && find(this->vArgAlreadyUsed.begin(), this->vArgAlreadyUsed.end(), iarg) != this->vArgAlreadyUsed.end() ) /*|| str.EqualTo("")*/) {

        if( str.EqualTo("--help") || str.EqualTo("-h") ) {

            this->kUsage = false;
            return false;
        }

        UIUC::HandboxMsg::DumpTTY(__METHOD_NAME__);

        TString eMessage;
        if(iarg < 0) eMessage = this->argv[0] + ": unknown parameter\n";
        else if(iarg >= this->argc) eMessage = this->argv[0] + ": missing parameter\n";
        else if(str.EqualTo("")) eMessage = this->argv[0] + ": empty string parameter found..; program not expected to be used like this.\n";
        else eMessage = this->argv[0] + ": unexpected predicate `" + str + "`; program not expected to be used like this. (one arg start with a dash ?)\n";
        eMessage += "Try '" + this->argv[0] + " --help' for more information";

        cerr << eMessage << endl;

        exit(1);
    }

    return true;
}

TString UIUC::HandboxUsage::GetCurrentTime(TString fmt)
{
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::ostringstream oss;
    oss << std::put_time(&tm, fmt.Data());

    return TString(oss.str().c_str());
}

//
// Add option for BOOLEAN
//
bool UIUC::HandboxUsage::AddOption(TString name, TString description, bool *value, bool reverse_logic)
{

    TString eMessage = "Option lock is enabled.. You have to define all options first. Move all argument definitions..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    bool b = false;
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        b = true;
    }

    *value = (reverse_logic == UIUC::HandboxUsage::kReverseLogic) ? !b : b;
    return true;
}


//
// Add option for INT
//
bool UIUC::HandboxUsage::AddOption(TString name, TString description, int *value, double valueByDefault)
{
    double dValue = *value;

    bool b = AddOption(name, description, &dValue, valueByDefault);             // Use the double-like methods

    *value = (int) dValue;
    return b;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, int *value, TString valueByDefault)
{
    return AddOption(name, description, value, UIUC::InterpretFormula(valueByDefault));
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<int> &value, TString valueByDefault_str, char delimiter, int expected_size)
{
    vector<int> valueByDefault;

    TObjArray *array = valueByDefault_str.Tokenize(delimiter);
    for (int i = 0; i < array->GetEntries(); i++) {

        valueByDefault.push_back( UIUC::InterpretFormula(((TObjString *)(array->At(i)))->String()) );
    }
    delete array;
    array = NULL;

    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter, expected_size);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<int> &value, vector<int> valueByDefault, char delimiter, int expected_size)
{
    vector<double> dValue(value.begin(), value.end());
    vector<double> dValueByDefault(valueByDefault.begin(), valueByDefault.end());
    for(int i = 0, N = value.size(); i < N; i++) dValue[i] = value[i];

    bool b = AddOption(name, description, dValue, dValueByDefault, delimiter, expected_size);

    value.resize(dValue.size());
    for(int i = 0, N = dValue.size(); i < N; i++) value[i] = (int) dValue[i];

    return b;
}

//
// Add option for Float
//
bool UIUC::HandboxUsage::AddOption(TString name, TString description, float *value, double valueByDefault)
{
    double dValue = *value;

    bool b = AddOption(name, description, &dValue, valueByDefault);             // Use the double-like methods
    *value = (float) dValue;

    return b;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, float *value, TString valueByDefault)
{
    return AddOption(name, description, value, UIUC::InterpretFormula(valueByDefault));
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<float> &value, TString valueByDefault_str, char delimiter, int expected_size)
{
    vector<float> valueByDefault;

    TObjArray *array = valueByDefault_str.Tokenize(delimiter);
    for (int i = 0; i < array->GetEntries(); i++) {

        TString str = ((TObjString *)(array->At(i)))->String();
        valueByDefault.push_back( UIUC::InterpretFormula(str) );
    }
    delete array;
    array = NULL;

    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter, expected_size);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<float> &value, vector<float> valueByDefault, char delimiter, int expected_size)
{
    vector<double> dValue(value.begin(), value.end());
    vector<double> dValueByDefault(valueByDefault.begin(), valueByDefault.end());
    for(int i = 0, N = value.size(); i < N; i++) dValue[i] = value[i];

    bool b = AddOption(name, description, dValue, dValueByDefault, delimiter, expected_size);

    value.resize(dValue.size());
    for(int i = 0, N = dValue.size(); i < N; i++) value[i] = (float) dValue[i];

    return b;
}





//
// AddOption for Double
//
bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<double> &value, TString valueByDefault_str, char delimiter, int expected_size)
{
    vector<double> valueByDefault;

    TObjArray *array = valueByDefault_str.Tokenize(delimiter);
    for (int i = 0; i < array->GetEntries(); i++) {

        TString str = ((TObjString *)(array->At(i)))->String();
        valueByDefault.push_back( UIUC::InterpretFormula(str) );
    }
    delete array;
    array = NULL;

    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter, expected_size);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<double> &value, vector<double> valueByDefault, char delimiter, int expected_size)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    if(AskHelp()) return false;

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str0 = this->argv[arg_index+1];

        TObjArray *array = str0.Tokenize(delimiter);
        if((unsigned int) array->GetEntries() > value.size()) value.resize(array->GetEntries());
        for (int i = 0; i < array->GetEntries(); i++) {

            TString str = ((TObjString *)(array->At(i)))->String();
            value[i] = UIUC::InterpretFormula(str);
        }

        delete array;
        array = NULL;

    } else {

        // Set the default value if found
        if(valueByDefault.size() > value.size()) value.resize(valueByDefault.size());
        for(int i = 0, N = valueByDefault.size(); i < N; i++) {

            if( !UIUC::IsNaN(valueByDefault[i]) ) value[i] = valueByDefault[i];
        }
    }

    if(expected_size != -1 && value.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(value.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    return true;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, double *value, TString valueByDefault)
{
    return AddOption(name, description, value, UIUC::InterpretFormula(valueByDefault));
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, double *value, double valueByDefault)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }


    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    // Set the default value if found
    if( !UIUC::IsInf(valueByDefault) ) {

        *value = valueByDefault;
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\" default value found %f", name.Data(), valueByDefault));
    }

    // Set the proper value if found
    //if(AskHelp()) return false;

    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        *value = UIUC::InterpretFormula(str);
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\" found and set to \"%s\"", name.Data(), str.Data()));
        return true;
    }

    UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\" set to \"%f\"", name.Data(), *value));
    return false;
}

//
// AddOption for bool
bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<bool> &value, vector<bool> vReverseLogic, char delimiter, int expected_size)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    //if(AskHelp()) return false;

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        TObjArray *array = str.Tokenize(delimiter);
        if((unsigned int) array->GetEntries() > value.size()) value.resize(array->GetEntries());
        for (int i = 0; i < array->GetEntries(); i++) {

            const char *s = ((TObjString *)(array->At(i)))->String().Data();

            bool b;
            istringstream(s) >> std::boolalpha >> b;

            value[i] = b;
        }

        delete array;
        array = NULL;

    } else {

        // Set the default value if found
        if(vReverseLogic.size() > value.size()) value.resize(vReverseLogic.size());
        for(int i = 0, N = vReverseLogic.size(); i < N; i++) {

            if( !vReverseLogic[i] ) {

                value[i] = true;
                UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\"[%d] reverse logic value found.", name.Data(), i));
            }
        }
    }

    if(expected_size != -1 && value.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"%s\"";
        eMessage += ", " + TString::Itoa(value.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    return true;
}


//
// AddOption for TString
//
bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<TString> &value, TString valueByDefault_str, char delimiter, int expected_size)
{
    vector<TString> valueByDefault;

    TObjArray *array = valueByDefault_str.Tokenize(delimiter);
    for (int i = 0; i < array->GetEntries(); i++) {

        valueByDefault.push_back( ((TObjString *)(array->At(i)))->String() );
    }
    delete array;
    array = NULL;

    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter, expected_size);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<TString> &value, vector<TString> valueByDefault, char delimiter, int expected_size)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }


    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    //if(AskHelp()) return false;

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        TObjArray *array = str.Tokenize(delimiter);
        if((unsigned int) array->GetEntries() > value.size()) value.resize(array->GetEntries());
        for (int i = 0; i < array->GetEntries(); i++) {

            value[i] = ((TObjString *)(array->At(i)))->String();
        }

        delete array;
        array = NULL;

    } else {

        // Set the default value if found
        if(valueByDefault.size() > value.size()) value.resize(valueByDefault.size());
        for(int i = 0, N = valueByDefault.size(); i < N; i++) {

            if( !valueByDefault[i].EqualTo("") ) {

                value[i] = valueByDefault[i];
                UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\"[%d] default value found %s", name.Data(), i, valueByDefault[i].Data()));
            }
        }
    }

    if(expected_size != -1 && value.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"%s\"";
        eMessage += ", " + TString::Itoa(value.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    return true;
}


//
// AddOption for TString
//
bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<TFileReader> &value, TString valueByDefault_str, TClass *cl, char delimiter, int expected_size)
{
    vector<TString> valueByDefault = UIUC::Explode(delimiter, valueByDefault_str);
    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, cl, delimiter, expected_size);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<TFileReader> &value, TClass *cl, TString valueByDefault_str, char delimiter, int expected_size)
{
    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault_str, cl, delimiter, expected_size);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<TFileReader> &value, TClass *cl, vector<TString> valueByDefault, char delimiter, int expected_size)
{
    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, cl, delimiter, expected_size);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, vector<TFileReader> &value, vector<TString> valueByDefault, TClass *cl, char delimiter, int expected_size)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }


    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    //if(AskHelp()) return false;

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        TObjArray *array = str.Tokenize(delimiter);
        if((unsigned int) array->GetEntries() > value.size()) value.resize(array->GetEntries());
        for (int i = 0; i < array->GetEntries(); i++) {

            value[i].SetClassName(cl);
            value[i].Init( ((TObjString *) (array->At(i)))->String() );
        }

        delete array;
        array = NULL;

    } else {

        // Set the default value if found
        if(valueByDefault.size() > value.size()) value.resize(valueByDefault.size());
        for(int i = 0, N = valueByDefault.size(); i < N; i++) {

            if( !valueByDefault[i].EqualTo("") ) {

                value[i].SetClassName(cl);
                value[i].Init( valueByDefault[i] );
                UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\"[%d] default value found %s", name.Data(), i, valueByDefault[i].Data()));
            }
        }
    }

    if(expected_size != -1 && value.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"%s\"";
        eMessage += ", " + TString::Itoa(value.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    return true;
}

bool UIUC::HandboxUsage::IsArgAlreadyDefined(TString argname) {

    vector<TString> argname0_array;
    for(int i = 0, N = this->vArgName.size(); i < N; i++) {

        vector<TString> array = TVarexp(this->vArgName[i]).Tokenize('|');
        for(int j = 0, J = array.size(); j < J; j++) array[j].ToLower();
        
        argname0_array.insert(argname0_array.begin(), array.begin(), array.end());
    }

    TObjArray *argname_array = argname.Tokenize("|");
    for (int i = 0; i < argname_array->GetEntries(); i++) {

        TString optname = UIUC::HandboxMsg::Trim(((TObjString *)(argname_array->At(i)))->String());
        TString optNameLC = optname;
                optNameLC.ToLower();

        if( find(argname0_array.begin(), argname0_array.end(), optNameLC) != argname0_array.end()) {

            TString eMessage = "Option \""+optname+"\" already added.. You cannot have a duplicated option..";
            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);
            this->kUsage = false;
            return 1;
        }

    }

    return 0;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, TVector2 *value, TString valueByDefault_str, char delimiter)
{
    int expected_size = 2;
    TVector2 valueByDefault;

    TObjArray *array = valueByDefault_str.Tokenize(delimiter);
    if(array->GetEntries() > (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(array->GetEntries(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    valueByDefault.SetX( array->GetEntries() > 0 ? UIUC::InterpretFormula(((TObjString *)(array->At(0)))->String()) : 0);
    valueByDefault.SetY( array->GetEntries() > 1 ? UIUC::InterpretFormula(((TObjString *)(array->At(1)))->String()) : 0);

    delete array;
    array = NULL;

    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, TVector2 *value, std::vector<double> valueByDefault_vector, char delimiter)
{
    int expected_size = 2;
    TVector2 valueByDefault;

    if(valueByDefault_vector.size() > (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(valueByDefault_vector.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    valueByDefault.SetX( valueByDefault_vector.size() > 0 ? valueByDefault_vector[0] : 0);
    valueByDefault.SetY( valueByDefault_vector.size() > 1 ? valueByDefault_vector[1] : 0);

    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, TVector2 *value, TVector2 valueByDefault, char delimiter)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    if(AskHelp()) return false;

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str0 = this->argv[arg_index+1];

        TObjArray *array = str0.Tokenize(delimiter);
        value->SetX( array->GetEntries() > 0 ? UIUC::InterpretFormula(((TObjString *)(array->At(0)))->String()) : valueByDefault.X());
        value->SetY( array->GetEntries() > 1 ? UIUC::InterpretFormula(((TObjString *)(array->At(1)))->String()) : valueByDefault.Y());

        delete array;
        array = NULL;

    } else {

        value->SetX( valueByDefault.X() );
        value->SetY( valueByDefault.Y() );
    }

    return true;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, TVector3 *value, TString valueByDefault_str, char delimiter)
{
    int expected_size = 3;
    TVector3 valueByDefault;

    TObjArray *array = valueByDefault_str.Tokenize(delimiter);
    if(array->GetEntries() > (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(array->GetEntries(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    valueByDefault.SetX( array->GetEntries() > 0 ? UIUC::InterpretFormula(((TObjString *)(array->At(0)))->String()) : 0);
    valueByDefault.SetY( array->GetEntries() > 1 ? UIUC::InterpretFormula(((TObjString *)(array->At(1)))->String()) : 0);
    valueByDefault.SetZ( array->GetEntries() > 2 ? UIUC::InterpretFormula(((TObjString *)(array->At(2)))->String()) : 0);

    delete array;
    array = NULL;

    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, TVector3 *value, std::vector<double> valueByDefault_vector, char delimiter)
{
    int expected_size = 3;
    TVector3 valueByDefault;

    if(valueByDefault_vector.size() > (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(valueByDefault_vector.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    valueByDefault.SetX( valueByDefault_vector.size() > 0 ? valueByDefault_vector[0] : 0);
    valueByDefault.SetY( valueByDefault_vector.size() > 1 ? valueByDefault_vector[1] : 0);
    valueByDefault.SetZ( valueByDefault_vector.size() > 2 ? valueByDefault_vector[2] : 0);

    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter);
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, TVector3 *value, TVector3 valueByDefault, char delimiter)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    if(AskHelp()) return false;

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str0 = this->argv[arg_index+1];

        TObjArray *array = str0.Tokenize(delimiter);
        value->SetX( array->GetEntries() > 0 ? UIUC::InterpretFormula(((TObjString *)(array->At(0)))->String()) : valueByDefault.X());
        value->SetY( array->GetEntries() > 1 ? UIUC::InterpretFormula(((TObjString *)(array->At(1)))->String()) : valueByDefault.Y());
        value->SetZ( array->GetEntries() > 2 ? UIUC::InterpretFormula(((TObjString *)(array->At(2)))->String()) : valueByDefault.Z());

        delete array;
        array = NULL;

    } else {

        value->SetX( valueByDefault.X() );
        value->SetY( valueByDefault.Y() );
        value->SetZ( valueByDefault.Z() );
    }

    return true;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, TString *value, TString valueByDefault)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after the last UIUC::HandboxUsage::AddOption in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }


    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    //if(AskHelp()) return false;

    if( !valueByDefault.EqualTo("") ) {

        *value = valueByDefault;
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\" default value found %s", name.Data(), valueByDefault.Data()));
    }

    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        *value = str;
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\" found and set to \"%s\"", name.Data(), str.Data()));

        return true;
    }

    UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, Form("Arg \"%s\" set to \"%s\"", name.Data(), value->Data()));
    return false;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, char *value, char valueByDefault)
{
    TString value_str;
    TString valueByDefault_str = (TString) valueByDefault;

    bool b = UIUC::HandboxUsage::AddOption(name, description, &value_str, valueByDefault_str);
    *value = value_str[0];

    return b;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, TFileReader *value, TString valueByDefault)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "TFileReader ptr passed is a NULL pointer..";
    if( UIUC::HandboxMsg::Error(value == NULL, __METHOD_NAME__, eMessage)) {
        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    //if(AskHelp()) return false;

    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        if(UIUC::TFileReader::Obj(str).EqualTo("/")) {

            TString objname = UIUC::TFileReader::Obj(valueByDefault);
            if(!objname.EqualTo("/")) {

                str += ":" + objname;
            }
        }

        (*value).Init(str);
        return true;
    }

    //if( !valueByDefault.EqualTo("") ) (*value).Init(valueByDefault);
    return true;
}

bool UIUC::HandboxUsage::AddArgument(std::map<TString, double>* value, TString valueByDefault, char delimiter, char delimiterKey, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
    for(int i = 0, N = vv.size(); i < N; i++) {

        vector<TString> keyvalue = UIUC::TFileReader::SplitString(vv[i], delimiterKey);
        if(keyvalue.size() != 2) {
            
            TString eMessage  = Form("\"%s\" is an invalid value and cannot be formatted using \"%c\" delimiter for argument #%d", keyvalue[0].Data(), delimiterKey, vArgNotUsed[0]-1);
            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

            this->kUsage = false;
            return false;
        }
    
        value->insert({keyvalue[0], UIUC::InterpretFormula(keyvalue[1])});
    }


    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);
    
    return true;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, std::map<TString, double> *value, std::map<TString, double>valueByDefault, char delimiter, char delimiterKey)
{
    TString eMessage = "Option lock is enabled.. You have to define all --options first. Move all argument definitions after in your code..";
    if( UIUC::HandboxMsg::Error(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }
    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( UIUC::HandboxMsg::Error(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from UIUC::HandboxUsage..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgDefaultValue.push_back(description);
    this->vArgMandatory.push_back(UIUC::HandboxUsage::kOptional);
    this->vArgColor.push_back(UIUC::HandboxMsg::kNoColor);

    if(AskHelp()) return false;

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];
        vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
        for(int i = 0, N = vv.size(); i < N; i++) {

            vector<TString> keyvalue = UIUC::TFileReader::SplitString(vv[i], delimiterKey);
            if(keyvalue.size() != 2) {
                
                TString eMessage  = Form("\"%s\" is an invalid value and cannot be formatted using \"%c\" delimiter for option \"%s\"", keyvalue[0].Data(), delimiterKey, name.Data());
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

                this->kUsage = false;
                return false;
            }

            value->insert({keyvalue[0], UIUC::InterpretFormula(keyvalue[1])});
        }

    } else {

        map<TString, double>::iterator it;
        for (it = valueByDefault.begin(); it != valueByDefault.end(); it++)
            value->insert({it->first, it->second});
    }

    return true;
}

bool UIUC::HandboxUsage::AddOption(TString name, TString description, std::map<TString, double> *value, TString valueByDefault_str, char delimiter, char delimiterKey)
{
    std::map<TString, double> valueByDefault;

    vector<TString> vv = UIUC::TFileReader::SplitString(valueByDefault_str, delimiter);
    for(int i = 0, N = vv.size(); i < N; i++) {

        vector<TString> keyvalue = UIUC::TFileReader::SplitString(vv[i], delimiterKey);
        if(keyvalue.size() != 2) {
            
            TString eMessage  = Form("\"%s\" is an invalid value and cannot be formatted using \"%c\" delimiter for option \"%s\"", keyvalue[0].Data(), delimiterKey, name.Data());
            UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

            this->kUsage = false;
            return false;
        }

        valueByDefault.insert({keyvalue[0], UIUC::InterpretFormula(keyvalue[1])});
    }
    
    return UIUC::HandboxUsage::AddOption(name, description, value, valueByDefault, delimiter);
}

bool UIUC::HandboxUsage::AddArgument(int *value, TString valueByDefault, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);

    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if(!this->CheckArg(vArgNotUsed[0])) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    *value = str.Atoi();
    return true;
}

bool UIUC::HandboxUsage::AddArgument(vector<int>& v, TString valueByDefault, char delimiter, int expected_size, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();

    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if(!this->CheckArg(vArgNotUsed[0])) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    v.resize(vv.size());
    for(int j = 0; j < vv.size(); j++) v[j] = TString(vv[j]).Atoi();

    return true;
}

bool UIUC::HandboxUsage::AddArgument(vector<bool>& v, TString valueByDefault, char delimiter, int expected_size, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }


    v.resize(vv.size());
    for(int j = 0; j < vv.size(); j++) {

        str = TString(vv[j]);
        str.ToLower();
                str = str.Strip(TString::kBoth);

        v[j] = (!str.EqualTo("0")) && (!str.EqualTo("no")) && (!str.EqualTo(""));
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    return true;
}

bool UIUC::HandboxUsage::AddArgument(vector<double>& v, TString valueByDefault, char delimiter, int expected_size, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    v.resize(vv.size());
    for(int j = 0; j < vv.size(); j++) v[j] = UIUC::InterpretFormula(vv[j]);
    return true;
}


bool UIUC::HandboxUsage::AddArgument(vector<float>& v, TString valueByDefault, char delimiter, int expected_size, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    v.resize(vv.size());
    for(int j = 0; j < vv.size(); j++) v[j] = UIUC::InterpretFormula(vv[j]);
    return true;
}

bool UIUC::HandboxUsage::AddArgument(vector<TString>& v, TString valueByDefault, char delimiter, int expected_size, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    v.resize(vv.size());
    for(int j = 0; j < vv.size(); j++) v[j] = vv[j];

    return true;
}

bool UIUC::HandboxUsage::AddArgument(vector<TFileReader>& v, TString valueByDefault, TClass *cl, char delimiter, int expected_size, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);

    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    v.resize(vv.size());
    for(int j = 0; j < vv.size(); j++) {

        v[j].SetClassName(cl);
        if(UIUC::TFileReader::Obj(vv[j]).EqualTo("/")) {

            TString objname = UIUC::TFileReader::Obj(valueByDefault);
            TString eMessage = "Argument #" + TString::Itoa(vArgNotUsed[0], 10) + " has no objname.. Default value (=\""+objname+"\") will be used";
            UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);

            vv[j] += ":" + objname;
        }

        v[j].Init(vv[j]);
    }

    return true;
}

bool UIUC::HandboxUsage::AddArgument(bool *value, TString valueByDefault, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    str.ToLower();
            str = str.Strip(TString::kBoth);

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    *value = (!str.EqualTo("0")) && (!str.EqualTo("no")) && (!str.EqualTo(""));
    return true;
}

bool UIUC::HandboxUsage::AddArgument(double *value, TString valueByDefault, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
            str = str.Strip(TString::kBoth);
    
    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    *value = UIUC::InterpretFormula(str);
    return true;
}

bool UIUC::HandboxUsage::AddArgument(TVector2 *v, TString valueByDefault, char delimiter, bool kMandatory)
{
    int expected_size = 2;
    
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    v->SetX(UIUC::InterpretFormula(vv[0]));
    v->SetY(UIUC::InterpretFormula(vv[1]));
    
    return true;
}

bool UIUC::HandboxUsage::AddArgument(TVector3*v, TString valueByDefault, char delimiter, bool kMandatory)
{
    int expected_size = 3;
    
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    this->bArgOptionLock = true;
    //if(AskHelp()) return false;

    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    vector<TString> vv = UIUC::TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    v->SetX(UIUC::InterpretFormula(vv[0]));
    v->SetY(UIUC::InterpretFormula(vv[1]));
    v->SetZ(UIUC::InterpretFormula(vv[2]));
    
    return true;
}

bool UIUC::HandboxUsage::AddArgument(TString *value, TString valueByDefault, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);
    //if(AskHelp()) return false;

    this->bArgOptionLock = true;
    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
            str = str.Strip(TString::kBoth);

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    *value = str;
    return true;
}

bool UIUC::HandboxUsage::AddArgument(TFileReader *value, TString valueByDefault, bool kMandatory)
{
    this->vArgName.push_back("");
    this->vArgDefaultValue.push_back(valueByDefault);
    this->vArgMandatory.push_back(kMandatory);
    this->vArgColor.push_back((kMandatory) ? UIUC::HandboxMsg::kRed : UIUC::HandboxMsg::kNoColor);

    this->bArgOptionLock = true;
    vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(kMandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
            str = str.Strip(TString::kBoth);

    this->vArgColor.pop_back();
    this->vArgColor.push_back(UIUC::HandboxMsg::kGreen);

    if(UIUC::TFileReader::Obj(str).EqualTo("/")) {

        TString objname = UIUC::TFileReader::Obj(valueByDefault);
        if(!objname.EqualTo("/")) {

            TString eMessage = "Argument #" + TString::Itoa(vArgNotUsed[0], 10) + " has no objname.. Default value (=\""+objname+"\") will be used";
            UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);

            str += ":" + objname;
        }
    }

    UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);
    (*value).Init(str);

    TString out, err;
    UIUC::HandboxMsg::GetTTY(__METHOD_NAME__, &out, &err);
    if(!out.EqualTo("") || !err.EqualTo("")) {
        bufferTTY[0].push_back(out);
        bufferTTY[1].push_back(err);
    }

    return true;
}

void UIUC::HandboxUsage::Print(bool kIncrementTab)
{
    this->kPrintUsageOnDestroy = false;
    TString shell_cmd;
    for(int i = 1; i < this->argc; i++) {

        TString str = this->argv[i];

        if((i != 0 && (!str.BeginsWith("-")) || str.Contains(" "))) shell_cmd += "\"";

        if(str.BeginsWith("-")) shell_cmd += UIUC::HandboxMsg::kBlue + str + UIUC::HandboxMsg::kNoColor;
        else shell_cmd += str;

        if((i != 0 && (!str.BeginsWith("-")) || str.Contains(" "))) shell_cmd += "\" ";
        else shell_cmd += " ";
    }

    // if(AskHelp()) return;

    int index = 0;
    bool bWithArgument = false;

    // When there is a parameter starting with "-" from previous instructions, it must have a UIUC::HAndboxMsg::kNoColor tag..
    while((index = shell_cmd.Index(UIUC::HandboxMsg::kBlue, index)) != -1) {

        int subindex = shell_cmd.Index(UIUC::HandboxMsg::kBlue, index+2);

        TString substr = shell_cmd(index-1, subindex-index+1);
        if(substr.Contains("\"")) bWithArgument = true;

        if(bWithArgument) shell_cmd.Replace(index, 0, '\n');
        index += 3;
    }

    // Add some offset after a new line
    TString spacer = UIUC::HandboxMsg::Spacer(TString(gSystem->BaseName(this->argv[0])).Length()+1);
    shell_cmd.ReplaceAll('\n', "\\\n" + spacer);
    shell_cmd.ReplaceAll("\" \"", "\"" + UIUC::HandboxMsg::kNoColor + " \\\n" + spacer + UIUC::HandboxMsg::kPurple + "\"");
    shell_cmd += UIUC::HandboxMsg::kNoColor;

    // Print the remaining error message in the buffer..
    if(!kIncrementTab) UIUC::HandboxMsg::SetSingleSkipIncrementTab();
    UIUC::HandboxMsg::PrintInfo(gSystem->BaseName(this->argv[0]), shell_cmd);
    if(bWithArgument) cout << UIUC::HandboxMsg::Endl();
}