/**
 **********************************************
 *
 * \file HandboxStyle.cc
 * \brief Source code of the HandboxStyle class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <UIUC/HandboxStyle.h>

ClassImp(UIUC::HandboxStyle)

using namespace std;

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

bool UIUC::HandboxStyle::bBuildLegend = true;
bool UIUC::HandboxStyle::bShowLegend = 1;

const int UIUC::HandboxStyle::kDataStyle       = 0;
const int UIUC::HandboxStyle::kMonteCarloStyle = 1;
const int UIUC::HandboxStyle::kBkgStyle        = 2;
const int UIUC::HandboxStyle::kFitStyle        = 3;

int UIUC::HandboxStyle::styleId = -1;
std::vector<TString> UIUC::HandboxStyle::styleName;
std::vector< std::vector<TStyle* > > UIUC::HandboxStyle::styleMap;
std::vector< UIUC::HandboxStyle* > UIUC::HandboxStyle::style;

UIUC::HandboxStyle::HandboxStyle(const char *name, const char *title) : TNamed(name, title)
{
    // Load the Classic root styles
    if(TString(name).EqualTo("Default")) {

        TStyle *s = NULL;
        TIter Next(gROOT->GetListOfStyles());
        while((s = (TStyle*) Next())) {

            UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Classic style named \"" + TString(s->GetName()) + "\" found..");
            vector<TStyle*> vStyle0;
            vStyle0.push_back((TStyle*) s->Clone());
            vStyle0.push_back((TStyle*) s->Clone());
            vStyle0.push_back((TStyle*) s->Clone());
            vStyle0.push_back((TStyle*) s->Clone());

            UIUC::HandboxStyle::style.push_back(this);
            UIUC::HandboxStyle::styleName.push_back(s->GetName());
            UIUC::HandboxStyle::styleMap.push_back(vStyle0);
        }

        // Set Classic style to Modern..
        if(this->styleId == -1) {

            TString style_name;
            if( find(this->styleName.begin(), this->styleName.end(), "Modern") != this->styleName.end() ) style_name = "Modern";
            else style_name = this->styleName.back();

            this->UseStyle(style_name);
        }
    }
}

TString UIUC::HandboxStyle::GetStyle(TString style)
{
    if(style.EqualTo("bkg")) return style;
    else if(style.EqualTo("fit")) return style;
    else if(style.EqualTo("mc")) return style;

    return "data";
}

bool UIUC::HandboxStyle::SetTag(TVirtualPad *pad, TString text)
{
    if(pad == NULL) return 0;
    pad->cd();

    TH1 *h = NULL;

    TObject *obj = NULL;
    TIter Next(pad->GetListOfPrimitives());
    while(( obj = (TObject*) Next()) ) {

        if(obj->InheritsFrom("TH1")) {

            h = (TH1*) obj;
            break;
        }
    }

    double x0 = 0, y0 = 0;
    if(h) {

        x0 = h->GetXaxis()->GetXmax() - h->GetXaxis()->GetXmin();
        x0 = h->GetXaxis()->GetXmin()+x0/4;
        y0 = h->GetMaximum() - h->GetMinimum();
        y0 = h->GetMinimum()+y0/4;
    }

    TLatex *tex = new TLatex(x0, y0, text);
    tex->SetTextColor(17);
    tex->SetTextColorAlpha(kBlack, 0.1);
    tex->SetTextSize(0.09);
    tex->SetTextAngle(30.0);
    tex->SetLineWidth(2);
    tex->Draw();

    return 1;
}

bool UIUC::HandboxStyle::Register(HandboxStyle *fHandboxStyle, std::vector<TStyle*> vStyle) {

    unsigned int index = find(UIUC::HandboxStyle::styleName.begin(), UIUC::HandboxStyle::styleName.end(), fHandboxStyle->GetName()) - UIUC::HandboxStyle::styleName.begin();
    if( index == UIUC::HandboxStyle::styleName.size() ) {

        TString eMessage = (TString) "Handbox style named \"" + fHandboxStyle->GetName() + "\" loaded.. ";
        UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);

        UIUC::HandboxStyle::style.push_back(fHandboxStyle);
        UIUC::HandboxStyle::styleMap.push_back(vStyle);
        UIUC::HandboxStyle::styleName.push_back(fHandboxStyle->GetName());

        return 1;
    }

    TString eMessage = (TString) "Handbox style named \"" + fHandboxStyle->GetName() + "\" already registered.. ";
    UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);
    return 0;
}

bool UIUC::HandboxStyle::UseStyle(TString style_name)
{
    // Classic style in case the one passed as parameter is not found
    if( find(UIUC::HandboxStyle::styleName.begin(), UIUC::HandboxStyle::styleName.end(), style_name) == UIUC::HandboxStyle::styleName.end() ) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Style \"" + style_name + "\" not found..");
        return 0;
    }

    this->styleId = find(UIUC::HandboxStyle::styleName.begin(), UIUC::HandboxStyle::styleName.end(), style_name) - UIUC::HandboxStyle::styleName.begin();
    TStyle *style = UIUC::HandboxStyle::styleMap[this->styleId][0];
    if(style == NULL) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Classic style #0 named \"" + style_name + "\" not found.. aborted");
        return 0;
    }

    UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Loading custom style set named \"" + TString(UIUC::HandboxStyle::styleName[this->styleId]) + "\"");
    gROOT->SetStyle(style->GetName());

    return 1;
}

bool UIUC::HandboxStyle::ApplyStyle(TList* l, TString styleID_name)
{
    bool b = true;

    TIter Next(l);
    TObject *obj = NULL;
    while( (obj = (TObject*) Next())  ) {

        b &= this->ApplyStyle(obj, styleID_name);
    }

    return b;
}
bool UIUC::HandboxStyle::ApplyStyle(TObject* obj, TString styleID_name)
{
    if(obj == NULL) return 0;
    if(obj->InheritsFrom("TVirtualPad")) return 0;

    int styleID = UIUC::HandboxStyle::kDataStyle;
    if(styleID_name == "data") styleID = UIUC::HandboxStyle::kDataStyle;
    else if (styleID_name == "mc") styleID = UIUC::HandboxStyle::kMonteCarloStyle;
    else if (styleID_name == "fit") styleID = UIUC::HandboxStyle::kFitStyle;
    else if (styleID_name == "bkg") styleID = UIUC::HandboxStyle::kBkgStyle;

    return this->ApplyStyle(obj, styleID);
}

bool UIUC::HandboxStyle::ApplyStyle(TObject* obj, int styleID)
{
    if(obj == NULL) return 0;
    if(obj->InheritsFrom("TVirtualPad")) return 0;

    bool bValidStyle = (int) UIUC::HandboxStyle::styleMap.size() <= Instance().styleId || Instance().styleId < 0;
    if(bValidStyle) bValidStyle |= styleID >= (int) UIUC::HandboxStyle::styleMap[Instance().styleId].size() || styleID < 0 || UIUC::HandboxStyle::styleMap[Instance().styleId][styleID] == NULL;
    if(!UIUC::HandboxMsg::Error(bValidStyle, __METHOD_NAME__, "Something is wrong with \"" + Instance().GetCustomStyleName() + "\"")) {

        TString name = TString(obj->IsA()->GetName());
        if(obj->InheritsFrom("TNamed")) name = ((TNamed*) obj)->GetName();
        UIUC::HandboxMsg::PrintMessage(
            __METHOD_NAME__,
            "Applying custom style to \"" + name +
            "\" => \"" + TString(UIUC::HandboxStyle::styleName[Instance().styleId]) + "\"; ID #" + TString::Itoa(styleID,10)
            );

        TStyle *previous_style = gStyle;

        gROOT->SetStyle(UIUC::HandboxStyle::styleMap[Instance().styleId][styleID]->GetName());

        if(gSystem->Getenv("ESCALADE_NOSTATBOX")) gStyle->SetOptStat(0);
        obj->UseCurrentStyle();
        gROOT->SetStyle(previous_style->GetName());

        return 1;
    }

    return 0;
}

bool UIUC::HandboxStyle::BuildCanvas(TString title, TVirtualPad *vPad, TList* lObject, vector<TString> vOption, std::vector<TObjectStyle> vObjectStyle)
{
    TPad *pad = (TPad*) vPad;
    pad->cd();

    if(pad == NULL) return 0;

    if(lObject == NULL) {

        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "List of objects to draw is NULL for pad \"" + vPad->GetName() + "\"..");
        return 0;
    }

    if(!lObject->GetSize()) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, (TString) "List of objects to draw is empty for pad \"" + vPad->GetName() + "\"..");
        return 0;
    }

    if(vObjectStyle.size() != 0) {
        for(int i = 0, N = lObject->GetSize(); i < N; i++) {

            if(i < (int) vObjectStyle.size()) vObjectStyle[i].SetColor(lObject->At(i+1));
        }
    }

    //
    // Custom functions called here
    // * Custom option  => Set the proper option for the object wrt to the custom style..
    // * Custom axis    => Take care to put proper axis
    //
    // * Custom statbox        => Remove if requested and position it in a difference place is requested
    // * Custom title   => Set proper title, to avoid conflict with legend..
    // * Custom legends+others => Update options wrt the default option of the style + shift also because of reference plot..
    // NB: Statbox is used as reference.. positioning
    //
    if(gSystem->Getenv("ESCALADE_NOSTATBOX")) gStyle->SetOptStat(0);
    else gStyle->SetOptStat(1);
    
    gStyle->SetOptTitle(1);

    vector<TString> vOpt = this->SetCustomOptions0(lObject, vOption);
    this->SetCustomAxis0(lObject);

    //
    // Draw reference + object
    //
    TObject *obj = NULL;
    TIter Next(lObject);

    int dimension = 0;
    int nplots = 0;

    TH1* obj0 = NULL;
    for(int i = 0; (obj = Next()); i++) {

        if(TString(obj->GetName()).Contains("Statbox")) {

            if(obj->InheritsFrom("TH3")) dimension = 3;
            else if(obj->InheritsFrom("TH2") || obj->InheritsFrom("TGraph2D")) dimension = 2;
            else if(obj->InheritsFrom("TH1") || obj->InheritsFrom("TGraph")) dimension = 1;

            obj0 = (TH1*) obj;
        }
    }

    bool bFirst = true;
    Next.Reset();
    for(int i = 0; (obj = Next()); i++) {

        if(dimension > 1 && obj == obj0) continue;
        if(dimension > 1 && obj0 != NULL && bFirst) {

            ((TH1*)obj)->SetTitle(obj0->GetTitle());
            ((TH1*)obj)->GetXaxis()->SetTitle(obj0->GetXaxis()->GetTitle());
            ((TH1*)obj)->GetYaxis()->SetTitle(obj0->GetYaxis()->GetTitle());
            ((TH1*)obj)->GetZaxis()->SetTitle(obj0->GetZaxis()->GetTitle());

            vOpt[i].ReplaceAll("SAME", "");
            bFirst = false;
        }

        if(vOpt[i].Contains("HIST")) {

            ((TH1*)obj)->SetFillStyle(1001);
            ((TH1*)obj)->SetFillColor(0);
        }

        vOpt[i] = vOpt[i].ReplaceAll("  ", " ").Strip(TString::kBoth);
        if(!obj->InheritsFrom("TObjString")) {
            obj->Draw(vOpt[i]);
        }

        if(!TString(obj->GetName()).Contains("Statbox")) nplots++;
    }

    if( UIUC::HandboxMsg::Warning(dimension > 1 && nplots > 1,
                                  __METHOD_NAME__, "Histogram might be misdrawn.. because dim > 1 and multiple plots") ) {

        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, title);
        lObject->Print();
    }

    // To refresh all object that must be available for positioning calculation
    // such as setCustomLegend0, SetCustomTitle0.. etc

    if(pad) pad->Update();               // MANDATORY..

    //
    // Build statbox detached from curves..
    //
    TPaveStats *ps = this->SetCustomStatbox0(pad, lObject);
    if(ps) ps->Draw();

    //
    // Build legend block for curves..
    //
    if(bShowLegend)
        this->SetCustomLegend0(pad, lObject);

    //
    // Build title block
    //
    if(gSystem->Getenv("ESCALADE_TITLE") && title.EqualTo("")) {

        title = TString(gSystem->Getenv("ESCALADE_TITLE"));
    }

    if(TString(((TH1*) lObject->First())->GetName()).Contains("Statbox")) {

        title = ((TH1*) lObject->At(0))->GetTitle();
        //((TH1*) lObject->First())->SetTitle("");

    } else if(title.EqualTo("")) title = lObject->First()->GetTitle();

    if(!title.EqualTo("")) {
        TPaveText* pt = this->SetCustomTitle0(vPad, title);
        if(pt) pt->Draw();
    }

    //
    // Build preliminary tag
    //
    if(gSystem->Getenv("ESCALADE_TAG")) this->SetTag(pad, gSystem->Getenv("ESCALADE_TAG"));
    else if(gSystem->Getenv("ESCALADE_PRELIMINARY")) this->SetPreliminaryTag(pad);

    if(pad) pad->Update();
    return 1;
}

bool UIUC::HandboxStyle::BuildCanvas(TString title, TVirtualPad *vPad, TList *lObject, TString option, std::vector<TObjectStyle> vObjectStyle)
{
    vector<TString> vOption(lObject->GetSize(), option);
    return BuildCanvas(title, vPad, lObject, vOption, vObjectStyle);
}

bool UIUC::HandboxStyle::BuildCanvas(TString title, TVirtualPad *vPad, vector<TObject*> vObject, TString option, std::vector<TObjectStyle> vObjectStyle)
{
    vector<TString> vOption(vObject.size(), option);
    return BuildCanvas(title, vPad, vObject, vOption, vObjectStyle);
}

bool UIUC::HandboxStyle::BuildCanvas(TString title, TVirtualPad *vPad, vector<TObject*> vObject, vector<TString> vOption, std::vector<TObjectStyle> vObjectStyle)
{
    TList *lObject = new TList;
    for(int i = 0, N = (int) vObject.size(); i < N; i++) {

        if(vObject[i] == NULL) continue;
        lObject->Add(vObject[i]);
    }

    bool b = UIUC::HandboxStyle::BuildCanvas(title, vPad, lObject, vOption, vObjectStyle);
    lObject->Clear("nodelete");

    return b;
}


bool UIUC::HandboxStyle::BuildCanvas(TVirtualPad *vPad, TList* lObject, vector<TString> vOption, std::vector<TObjectStyle> vObjectStyle)
{
    TString title = gSystem->Getenv("ESCALADE_TITLE");
    return BuildCanvas(title, vPad, lObject, vOption, vObjectStyle);
}

bool UIUC::HandboxStyle::BuildCanvas(TVirtualPad *vPad, TList *lObject, TString option, std::vector<TObjectStyle> vObjectStyle)
{
    TString title = gSystem->Getenv("ESCALADE_TITLE");
    return BuildCanvas(title, vPad, lObject, option, vObjectStyle);
}

bool UIUC::HandboxStyle::BuildCanvas(TVirtualPad *vPad, vector<TObject*> vObject, TString option, std::vector<TObjectStyle> vObjectStyle)
{
    TString title = gSystem->Getenv("ESCALADE_TITLE");
    return BuildCanvas(title, vPad, vObject, option, vObjectStyle);
}

bool UIUC::HandboxStyle::BuildCanvas(TVirtualPad *vPad, vector<TObject*> vObject, vector<TString> vOption, std::vector<TObjectStyle> vObjectStyle)
{
    TString title = gSystem->Getenv("ESCALADE_TITLE");
    return BuildCanvas(title, vPad, vObject, vOption, vObjectStyle);
}


TCanvas* UIUC::HandboxStyle::BuildExample(TString style_name)
{
    TCanvas *c = new TCanvas("c", "c");
    c->UseCurrentStyle();

    TH1D *hData       = new TH1D("data", "", 100, 0, 10);
    hData->Sumw2();

    TH1D *hMonteCarlo = new TH1D("mc",   "", 100, 0, 10);
    hMonteCarlo->Sumw2();

    TH1D *hBkg        = new TH1D("bkg",  "", 100, 0, 10);
    hBkg->Sumw2();

    TF1  *g0    = new TF1("g1","expo",0, 10);
    TF1  *g1    = new TF1("g1","gaus",3, 5);
    TF1  *g2    = new TF1("g2","gaus",5, 7);

    double par[8];
    TF1  *gFit        = new TF1("gFit", "expo(0)+gaus(2)+gaus(5)", 0, 10);

    for (int i = 0; i < 10000; i++) hBkg->Fill(gRandom->Exp(5));

    for (int i = 0; i < 2500; i++) hData->Fill(gRandom->Gaus(4.0, 0.5));
    for (int i = 0; i < 2500; i++) hData->Fill(gRandom->Gaus(6.0, 0.3));
    for (int i = 0; i < 10000; i++) hData->Fill(gRandom->Exp(5));
    hData->Add(hBkg);

    for (int i = 0; i < 2500; i++) hMonteCarlo->Fill(gRandom->Gaus(4.0, 0.5));
    for (int i = 0; i < 2500; i++) hMonteCarlo->Fill(gRandom->Gaus(6.0, 0.3));

    hData->Fit(g0, "RQ0");
    hData->Fit(g1, "RQ0+");
    hData->Fit(g2, "RQ0+");
    g0->GetParameters(&par[0]);
    g1->GetParameters(&par[2]);
    g2->GetParameters(&par[5]);
    gFit->SetParameters(par);

    hData->Fit(gFit, "Q+");

    TList *l = new TList;
    l->Add(hData);
    l->Add(hMonteCarlo);
    l->Add(hBkg);
    l->Add(gFit);

    this->UseStyle(style_name);
    this->ApplyStyle(hData, 0);
    this->ApplyStyle(hMonteCarlo, 1);
    this->ApplyStyle(hBkg, 2);
    this->ApplyStyle(gFit, 3);

    hData->SetTitle("My Data;Label X;Label Y");
    hMonteCarlo->SetTitle("MC data;Label X;Label Y");
    hBkg->SetTitle("Background;Label X;Label Y");
    gFit->SetTitle("My data Fit;Label X;Label Y");

    c->SetTitle("My canvas title");
    this->BuildCanvas(c, l);

    c->UseCurrentStyle();
    c->Update();

    return c;
}