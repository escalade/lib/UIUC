/**
 **********************************************
 *
 * \file Handbox.cc
 * \brief Source code of the Handbox class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <UIUC/HandboxMsg.h>
ClassImp(UIUC::HandboxMsg)

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

using namespace std;

int UIUC ::HandboxMsg::kTabular           = 0;
const char UIUC::HandboxMsg::kTabularChar = '\t';

int UIUC ::HandboxMsg::kDebug          = TString(gSystem->Getenv("ESCALADE_DEBUG")).Atoi();
bool UIUC::HandboxMsg::kQuiet          = (TString(gSystem->Getenv("ESCALADE_QUIET")).Atoi() != 0);
bool UIUC::HandboxMsg::kSuperQuiet     = (TString(gSystem->Getenv("ESCALADE_SUPERQUIET")).Atoi() != 0);
bool UIUC::HandboxMsg::kCarriageReturn = true;

vector<int> UIUC::HandboxMsg::asserts;
int UIUC::HandboxMsg::iTTY       = 0;
int UIUC::HandboxMsg::iFailedTTY = 0;

std::chrono::time_point<std::chrono::system_clock> UIUC::HandboxMsg::progressBarTime = std::chrono::system_clock::now();

int UIUC::HandboxMsg::kStdOut_dup = dup(1);
vector<int> UIUC::HandboxMsg::kStdOut;
vector<TString> UIUC::HandboxMsg::kStdOut_fname;

vector<TString> UIUC::HandboxMsg::kTrash;

int UIUC::HandboxMsg::kStdErr_dup = dup(2);
vector<int> UIUC::HandboxMsg::kStdErr;
vector<TString> UIUC::HandboxMsg::kStdErr_fname;

map<TString, TString> UIUC::HandboxMsg::envlist;
vector<TString> UIUC::HandboxMsg::envhashmap;
vector<double> UIUC::HandboxMsg::envcoeffmap;

bool UIUC::HandboxMsg::bSingleSkipIncrementTab = false;
bool UIUC::HandboxMsg::bSingleCarriageReturn = false;
bool UIUC::HandboxMsg::bShortRedirectTTY = false;

const bool UIUC::HandboxMsg::kIgnoreTab = false;
const bool UIUC::HandboxMsg::kIncrementTab = !UIUC::HandboxMsg::kIgnoreTab;
const TString UIUC::HandboxMsg::kNoColor = "\033[0m";
const TString UIUC::HandboxMsg::kRed = "\033[1;31m";
const TString UIUC::HandboxMsg::kGreen = "\033[1;32m";
const TString UIUC::HandboxMsg::kOrange = "\033[1;33m";
const TString UIUC::HandboxMsg::kBlue = "\033[1;34m";
const TString UIUC::HandboxMsg::kPurple = "\033[1;35m";

const TString UIUC::HandboxMsg::kOrangeBliking = "\033[33;5m";

bool UIUC::HandboxMsg::RunTests(const char* title)
{
	UIUC::HandboxMsg::PrintTests(title);
	return asserts.size() == count(asserts.begin(), asserts.end(), 1);
}

void UIUC::HandboxMsg::PrintTests(const char* title)
{
	int total     = asserts.size();
	int remaining = count(asserts.begin(), asserts.end(),  0);
	int failed    = count(asserts.begin(), asserts.end(), -1);
	int succeeded = count(asserts.begin(), asserts.end(),  1);

	UIUC::HandboxMsg::PrintInfo(title, UIUC::HandboxMsg::kOrange + "Test Summary: " + UIUC::HandboxMsg::kNoColor);
	UIUC::HandboxMsg::DecrementTab();
	UIUC::HandboxMsg::PrintInfo(title, "- There are %d scheduled test(s), %s%d missing assert(s)%s", total, 
		remaining > 1 ? UIUC::HandboxMsg::kRed.Data() : UIUC::HandboxMsg::kNoColor.Data(), remaining, UIUC::HandboxMsg::kNoColor.Data()
	);
	
	UIUC::HandboxMsg::DecrementTab();
	UIUC::HandboxMsg::PrintInfo(title, "- In total %s%d test(s) got executed%s out of %d, %s%d test(s) succeeded%s and %s%d test(s) failed%s", 
		total-remaining > 1 ? UIUC::HandboxMsg::kOrange.Data() : UIUC::HandboxMsg::kNoColor.Data(), total-remaining, UIUC::HandboxMsg::kNoColor.Data(), 
		total,
		      succeeded > 0 ? UIUC::HandboxMsg::kGreen.Data()  : UIUC::HandboxMsg::kNoColor.Data(), succeeded,       UIUC::HandboxMsg::kNoColor.Data(), 
		         failed > 1 ? UIUC::HandboxMsg::kRed.Data()    : UIUC::HandboxMsg::kNoColor.Data(), failed,          UIUC::HandboxMsg::kNoColor.Data());
	UIUC::HandboxMsg::DecrementTab();
}

void UIUC::HandboxMsg::ScheduleTests(int N)
{
	asserts.resize(asserts.size() + N);

	int total     = asserts.size();
	int failed    = count(asserts.begin(), asserts.end(), -1);
	int succeeded = count(asserts.begin(), asserts.end(),  1);

	int remaining = count(asserts.begin(), asserts.end(),  0);
	TString remainingStr = remaining != total ? ", " + TString::Itoa(remaining, 10) + " test(s) remaining" : "";

	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, 
		"You scheduled "   + TString::Itoa(N,10)      + 
		" new test(s) => " + TString::Itoa(total, 10) + " test(s) in total" + remainingStr);
}

void UIUC::HandboxMsg::Assert(const char* title, bool b, int pos)
{
	if(pos < 0) {
	
		pos = find(asserts.begin(), asserts.end(), 0) - asserts.begin();
		if(UIUC::HandboxMsg::Error(pos == asserts.size(), __METHOD_NAME__, "Too many tests.. Please schedule more tests."))
			return;
	}

	if(asserts[pos] != 0) {

		if(UIUC::HandboxMsg::Error(pos == asserts.size(), __METHOD_NAME__, "Assertion #"+TString::Itoa(pos,10)+" already got executed.."))
			return;
	}

	if(UIUC::HandboxMsg::Error(pos > asserts.size() - 1, __METHOD_NAME__, "Unexpected test index received.. Please schedule more tests."))
		return;

	asserts[pos] = b ? 1 : -1;
	UIUC::HandboxMsg::PrintTestBar(title);

	std::cout << flush;
}

TString UIUC::HandboxMsg::GetTestBar(const char* title)
{
	int total     = asserts.size();
	int succeeded = count(asserts.begin(), asserts.end(),  1);

	double percentage = double(succeeded)/double(total);
	int val = (int) (percentage * 100);
	
	TString str     = "";
	for(int i = 0; i < total; i++) {

		if(asserts[i] > 0) str += "o";
		else if(asserts[i] < 0) str += "x";
		else str += ".";
	}
	
	TString head    = (!TString(title).EqualTo("")) ? TString(title) : "";
	TString message = Form("%3d%% [%s] (%d/%d)", val, str.Data(), succeeded, total);
	if (UIUC::HandboxMsg::IsQuiet()) return MakeItShorter(head + message);

	TString os;
	os += UIUC::HandboxMsg::kPurple + MakeItShorter(head) + UIUC::HandboxMsg::kNoColor;
	os += ": Test(s)..  ";
	os += MakeItShorter(message);

	return os;
}


void UIUC::HandboxMsg::PrintTestBar(const char* title)
{
	bool bHoldSingleCarriageReturn = bSingleCarriageReturn;
	TString str = UIUC::HandboxMsg::GetTestBar(title);
	bSingleCarriageReturn = bHoldSingleCarriageReturn;
	if(!str.EqualTo("")) {

		cout << UIUC::HandboxMsg::CarriageReturn(1);
		cout << UIUC::HandboxMsg::Tab();
		cout << str;
		cout << UIUC::HandboxMsg::CarriageReturn();
		cout << flush;
	}

	int remaining = count(asserts.begin(), asserts.end(),  0);
	if(remaining < 1) {

		if(bSingleCarriageReturn) cout << UIUC::HandboxMsg::CarriageReturn(1);
		else cout << endl;

		if(UIUC::HandboxMsg::IsQuiet() && !UIUC::HandboxMsg::IsSuperQuiet()) cout << endl;
	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

void UIUC::HandboxMsg::ResetTab() {
	UIUC::HandboxMsg::kTabular = 0;
}
void UIUC::HandboxMsg::IncrementTab() {
	UIUC::HandboxMsg::kTabular++;
}

void UIUC::HandboxMsg::DecrementTab()
{
	if(UIUC::HandboxMsg::kTabular > 0) UIUC::HandboxMsg::kTabular--;
}

void UIUC::HandboxMsg::PrintProgressBar(const char* title, int event, int total, int refresh)
{
	bool bHoldSingleCarriageReturn = bSingleCarriageReturn;
	TString str = UIUC::HandboxMsg::GetProgressBar(title, event, total, refresh);
	bSingleCarriageReturn = bHoldSingleCarriageReturn;
	if(!str.EqualTo("")) {

		cout << UIUC::HandboxMsg::CarriageReturn(1);
		cout << UIUC::HandboxMsg::Tab();
		cout << str;
		cout << UIUC::HandboxMsg::CarriageReturn();
		cout << flush;
	}

	if(event == total) {

		if(bSingleCarriageReturn) cout << UIUC::HandboxMsg::CarriageReturn(1);
		else cout << endl;

		if(UIUC::HandboxMsg::IsQuiet() && !UIUC::HandboxMsg::IsSuperQuiet()) cout << endl;
	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

void UIUC::HandboxMsg::PrintDebug(int iDebugLevel, const char* title, TVector3 &v3, bool spherical_coordinate)
{
	if(!spherical_coordinate) {

		vector<double> v = {v3.Px(), v3.Py(), v3.Pz()};
		UIUC::HandboxMsg::PrintDebug(iDebugLevel, title, v);

	} else {

		vector<double> v2 = {v3.Mag(), v3.Theta()/TMath::Pi()*180, v3.Phi()/TMath::Pi()*180};
		UIUC::HandboxMsg::PrintDebug(iDebugLevel, title, v2);
	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

void UIUC::HandboxMsg::PrintDebug(int iDebugLevel, const char* title, TLorentzVector &lv, bool spherical_coordinate)
{
	if(!spherical_coordinate) {

		vector<double> v = {lv.Px(), lv.Py(), lv.Pz(), lv.E()};
		UIUC::HandboxMsg::PrintDebug(iDebugLevel, title, v);

	} else {

		vector<double> v2 = {lv.Rho(), lv.Theta()/TMath::Pi()*180, lv.Phi()/TMath::Pi()*180, lv.E()};
		UIUC::HandboxMsg::PrintDebug(iDebugLevel, title, v2);
	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

TString UIUC::HandboxMsg::GetDate(long long timestamp, TString format)
{
	std::time_t unix_timestamp = timestamp;
	char time_buf[80];
	struct tm ts;
	ts = *localtime(&unix_timestamp);
	strftime(time_buf, sizeof(time_buf), format.Data(), &ts);

	return TString(time_buf);
}

TString UIUC::HandboxMsg::GetTime(long long timestamp, TString format)
{
	std::time_t unix_timestamp = timestamp;
	char time_buf[80];
	struct tm ts;
	ts = *gmtime(&unix_timestamp);
	strftime(time_buf, sizeof(time_buf), format.Data(), &ts);

	TString time = TString(time_buf);
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, '0');
			time = (TString) time.Strip(TString::kLeading, 'h');
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, '0');
			time = (TString) time.Strip(TString::kLeading, 'm');
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, 's');
			time = (TString) time.Strip(TString::kBoth, ' ');

	return time;
}

TString UIUC::HandboxMsg::GetProgressBar(const char* title, int event, int total, int refresh)
{
	TString progressBarStr = "";
	if(event == 0) progressBarTime = std::chrono::system_clock::now();
	else 
	{
		std::chrono::duration<double> elapsedTime = std::chrono::system_clock::now() - progressBarTime;
		uint64_t time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		progressBarStr = Form(", dt = %.2fs, %s ETA ~ %s", 
			elapsedTime.count()/event, 
			GetDate(time + elapsedTime.count()/event*(total-event)).Data(),
			GetTime(elapsedTime.count()/event*(total-event)).Data()
		);
	}
	
	if(UIUC::HandboxMsg::Error(refresh == 0, __METHOD_NAME__, "Misconfiguration of the progress bar.. (refresh = 0)")) return "";

	TString PBSTR = "||||||||||";

	double percentage = double(event)/double(total);
	int val = (int) (percentage * 100);
	int lpad = (int) (percentage * PBSTR.Length());
	int rpad = PBSTR.Length() - lpad;

	double ratio = ((double) event) / refresh;
	if(int(ratio) != ratio && event != total) return "";

	TString head = (!TString(title).EqualTo("")) ? TString(title) : "";
	TString message = Form("%3d%% [%.*s%*s] (%d/%d%s)", val, lpad, PBSTR.Data(), rpad, "", event, total, progressBarStr.Data());
	if (UIUC::HandboxMsg::IsQuiet()) return MakeItShorter(head + message);

	TString os;
	os += UIUC::HandboxMsg::kPurple + MakeItShorter(head) + UIUC::HandboxMsg::kNoColor;
	os += ": Progress.. ";
	os += MakeItShorter(message);

	return os;
}

TString UIUC::HandboxMsg::Tab()
{
	if(UIUC::HandboxMsg::IsDebugEnabled()) return "";
	if(UIUC::HandboxMsg::IsQuiet()) return "";

	TString os;
	for(int i = 0; i < UIUC::HandboxMsg::kTabular; i++) os += UIUC::HandboxMsg::kTabularChar;

	return os;
}

TString UIUC::HandboxMsg::Spacer(int length, char c)
{
	TString space = ""; //UIUC::HandboxMsg::Tab();
	for(int i = 0; i < length; i++) space += c;
	return space;
}

TString UIUC::HandboxMsg::ReplaceVariadic(const char* str, va_list aptr)
{
	char buf[16384];

	/* A segfault is still occuring when specifying wrong number of parameters..
	   Cannot really be fixed in a smart way.. you have to provide the number of args.. */
	try {

		vsnprintf(buf, sizeof buf, TString(str).ReplaceAll("_%_", " / ").Data(), aptr); // histogram naming convension.. might be improved..
		va_end(aptr);
		return buf;

	} catch (const std::exception& e) {           // reference to the base of a polymorphic object

		std::cerr << e.what() << endl;         // information in length_error printed
	}

	return str;
}

TString UIUC::HandboxMsg::GetInfo(const char* title, const char* str,...)
{
	if(UIUC::HandboxMsg::IsQuiet()) return "";

	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Info in <" + TString(title) + ">: " : TString(title) + " ";
	TString message = buf.ReplaceAll('\n', '\n' + Spacer(head.Length()));

	TString os;
	os += UIUC::HandboxMsg::kGreen + MakeItShorter(head) +UIUC::HandboxMsg::kNoColor;
	os += MakeItShorter(message);

	return os;
}

void UIUC::HandboxMsg::PrintInfo(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	cout << UIUC::HandboxMsg::CarriageReturn(1);
	cout << UIUC::HandboxMsg::Tab();
	cout << UIUC::HandboxMsg::GetInfo(title, buf);
	cout << UIUC::HandboxMsg::Endl();
	cout << flush;

	if(!bSingleSkipIncrementTab) UIUC::HandboxMsg::IncrementTab();
	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

TString UIUC::HandboxMsg::GetError(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Error in <" + TString(title) + ">: " : TString(title) + " ";
	TString message = buf.ReplaceAll('\n', '\n' + Spacer(head.Length()));

	TString os;
	os += UIUC::HandboxMsg::kRed + MakeItShorter(head) +UIUC::HandboxMsg::kNoColor;
	os += MakeItShorter(message);

	return os;
}

void UIUC::HandboxMsg::PrintError(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	cerr << UIUC::HandboxMsg::CarriageReturn(1);
	cerr << UIUC::HandboxMsg::Tab();
	cerr << UIUC::HandboxMsg::GetError(title, buf);

	if(UIUC::HandboxMsg::IsQuiet()) cerr << endl;
	else cerr << UIUC::HandboxMsg::Endl();

	cerr << flush;

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

bool UIUC::HandboxMsg::Error(bool condition, const char* title, const char* str, ...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	if(!buf.EqualTo("") && condition) UIUC::HandboxMsg::PrintError(title, buf);
	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;

	return condition;
}


TString UIUC::HandboxMsg::GetWarning(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Warning in <" + TString(title) + ">: " : TString(title) + " ";
	TString message = buf.ReplaceAll('\n', '\n' + Spacer(head.Length()));

	TString os;
	os += UIUC::HandboxMsg::kOrange + MakeItShorter(head) +UIUC::HandboxMsg::kNoColor;
	os += MakeItShorter(message);

	return os;
}

void UIUC::HandboxMsg::PrintWarning(const char* title, const char* str, ...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	cerr << UIUC::HandboxMsg::CarriageReturn(1);
	cerr << UIUC::HandboxMsg::Tab();
	cerr << UIUC::HandboxMsg::GetWarning(title, buf);

	if(UIUC::HandboxMsg::IsQuiet()) cerr << endl;
	else cerr << UIUC::HandboxMsg::Endl();

	cerr << flush;

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

bool UIUC::HandboxMsg::Warning(bool condition, const char* title, const char* str, ...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	if(!TString(buf).EqualTo("") && condition) UIUC::HandboxMsg::PrintWarning(title, buf);
	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;

	return condition;
}

TString UIUC::HandboxMsg::GetMessage(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	if(UIUC::HandboxMsg::IsQuiet()) return "";

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Message in <" + TString(title) + ">: " : TString(title) + " "; 
	if(TString(title).EqualTo("")) head = head.Strip(TString::kBoth);

	TString message = TString(buf).ReplaceAll('\n', '\n' + UIUC::HandboxMsg::Tab() + Spacer(head.Length()));

	TString os;
	os += UIUC::HandboxMsg::kBlue + MakeItShorter(head) +UIUC::HandboxMsg::kNoColor;
	os += MakeItShorter(message);

	return os;
}

void UIUC::HandboxMsg::PrintMessage(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	cout << UIUC::HandboxMsg::CarriageReturn(1);
	cout << UIUC::HandboxMsg::Tab();
	cout << UIUC::HandboxMsg::GetMessage(title, buf);

	if(!bSingleCarriageReturn) cout << UIUC::HandboxMsg::Endl();
	else if (!UIUC::HandboxMsg::IsSuperQuiet()) cout << UIUC::HandboxMsg::CarriageReturn();
	else if(UIUC::HandboxMsg::IsQuiet()) cout << endl;
	else cout << UIUC::HandboxMsg::Endl();

	cout << flush;

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

void UIUC::HandboxMsg::PrintDebug(int iDebugLevel, const char* title, const char* str,...)
{
	TString os;

	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	if( UIUC::HandboxMsg::IsDebugEnabled(iDebugLevel) || iDebugLevel == -1) {

		TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Debug (lvl " + TString::Itoa(iDebugLevel,10) + ") in <" + TString(title) + ">: " : TString(title) + " ";
		TString message = TString(buf).ReplaceAll('\n', '\n' + UIUC::HandboxMsg::Tab() + Spacer(head.Length()));

		os += UIUC::HandboxMsg::kPurple + MakeItShorter(head) +UIUC::HandboxMsg::kNoColor;
		os += MakeItShorter(message);

		cout << UIUC::HandboxMsg::CarriageReturn(1);
		cout << UIUC::HandboxMsg::Tab();
		cout << os;
		
		if(UIUC::HandboxMsg::IsQuiet()) cout << endl;
		else cout << UIUC::HandboxMsg::Endl();

		cout << flush;

	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

TString UIUC::HandboxMsg::CreateTemporaryPath(TString format) {

	if(Error(!format.Contains("XXXXXX"), __METHOD_NAME__, "No XXXXXX found in the path \""+format+"\"")) return "";

	int i = 0;
	int imax = 100;
	TString tmpname;
	do {

		tmpname = format;
		int i = tmpname.Index("XXXXXX");
		while(i > -1) {

				TString rnd = UIUC::GetRandomStr(6);
				tmpname = tmpname.Replace(i, 6, rnd);
				i = tmpname.Index("XXXXXX");
		}

	} while( FileExists(tmpname) and i++ < imax);

	if(Error(i > imax, __METHOD_NAME__, "Too many iterations trying to replace XXXXXX for \""+format+"\".. abort")) return "";

	UIUC::HandboxMsg::PrintDebug(100, __METHOD_NAME__, "Create temporary file \"" + tmpname + "\" using the pattern \""+format+"\"");
	return tmpname;
}

void UIUC::HandboxMsg::ReadAndWrite(TString fname, std::vector<TString> regex_pattern, std::vector<TString> replacement)
{
	if(regex_pattern.size() != replacement.size()) {

		int min = TMath::Min(regex_pattern.size(), replacement.size());
		regex_pattern.resize(min);
		replacement.resize(min);
	}

	std::vector<TPRegexp> regex;
	for(int i = 0, N = regex_pattern.size(); i < N; i++)
		regex.push_back(regex_pattern[i]);

	TString bufname = UIUC::HandboxMsg::CreateTemporaryPath();

	ifstream in(fname);
	ofstream out(bufname);

	string buff;
	while (getline(in, buff)) {

		TString line = TString(buff.c_str());
		for(int i = 0, N = regex.size(); i < N; i++)
			regex[i].Substitute(line, replacement[i]);

		out << line.Data() << std::endl;
	}

	in.close();
	out.close();

	gSystem->Rename(bufname, fname);
}

void UIUC::HandboxMsg::PressAnyKeyToContinue(int iDebugLevel, const char* title, const char *msg, ...)
{
	va_list aptr;
	va_start(aptr, msg);
	TString buf = ReplaceVariadic(msg, aptr);

    UIUC::HandboxMsg::PrintDebug(iDebugLevel, title, TString(kOrangeBliking + "Press any key to continue.. " + kNoColor) + buf);
    if(UIUC::HandboxMsg::IsDebugEnabled(iDebugLevel)) {
        cin.ignore(std::numeric_limits<streamsize>::max(),'\n');
		UIUC::HandboxMsg::ErasePreviousLines(2);
	}
}

void UIUC::HandboxMsg::ErasePreviousLines(int previousLines)
{
 	while(previousLines-- > 0) {
		std::cout << "\033[F";
		ClearLine();
	}
}

void UIUC::HandboxMsg::ClearPage()
{
	std::cout << "\x1B[2J\x1B[H";
}

TString UIUC::HandboxMsg::RedirectPrint(TObject* obj, Option_t* option) {

	UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);
	obj->Print(option);

	TString out, err;
	UIUC::HandboxMsg::GetTTY(__METHOD_NAME__, &out, &err);
	if(!err.EqualTo("")) {

		if(!out.EqualTo("")) out += '\n';
		out += err;
	}

	out = UIUC::HandboxMsg::Trim(out);
	return out;
}

void UIUC::HandboxMsg::PrintTTY(const char* title, const char *out0, const char* err0) {

	TString out = out0;
	TString err = err0;

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Redirection TTY in <" + TString(title) + ">: " : TString(title) + " ";
	TString shift = UIUC::HandboxMsg::Spacer(MakeItShorter(head).Length() + 2);
	if(!out.EqualTo("")) {

		cout << UIUC::HandboxMsg::CarriageReturn(1);
		cout << UIUC::HandboxMsg::Tab();

		cout << UIUC::HandboxMsg::kOrange;
		if(UIUC::HandboxMsg::bShortRedirectTTY) cout << MakeItShorter(head);
		else cout << head;
		cout << UIUC::HandboxMsg::kNoColor;

		TObjArray *array = out.Tokenize('\n');
		for (int i = 0, N = array->GetEntries(); i < N; i++) {

			TString out_i = UIUC::HandboxMsg::Trim(((TObjString *)(array->At(i)))->String());
			if(out_i.EqualTo("")) continue;

			if(i == 0) cout << endl;
			cout << UIUC::HandboxMsg::Tab();
			if(UIUC::HandboxMsg::bShortRedirectTTY) cout << '\t' << MakeItShorter(out_i, UIUC::HandboxMsg::kNoColor, 100);
			else cout << '\t' << out_i;

			if(i != N-1) cout << endl;
		}
		cout << endl;

	}

	if(!err.EqualTo("")) {

		cerr << UIUC::HandboxMsg::CarriageReturn(1);
		cerr << UIUC::HandboxMsg::Tab();

		cerr << UIUC::HandboxMsg::kRed;
		if(UIUC::HandboxMsg::bShortRedirectTTY) cerr << MakeItShorter(head);
		else cerr << head;
		cerr << UIUC::HandboxMsg::kNoColor;

		TObjArray *array = err.Tokenize('\n');
		for (int i = 0, N = array->GetEntries(); i < N; i++) {

			TString err_i = UIUC::HandboxMsg::Trim(((TObjString *)(array->At(i)))->String());
			if(err_i.EqualTo("")) continue;

			if(i == 0) cerr << endl;
			cerr << UIUC::HandboxMsg::Tab();
			cerr << '\t' << err_i;
			if(i != N-1) cerr << endl;
		}
		cerr << endl;
	}

	UIUC::HandboxMsg::bShortRedirectTTY = false;
}

void UIUC::HandboxMsg::PrepareEnvList(bool kForce)
{
	if(kForce) {

		envhashmap.clear();
		envlist.clear();
	}

	if(!UIUC::HandboxMsg::envlist.size()) {

		TString env = "";
		TString cmd = "env | awk -F'=' '{print $1} '";

		FILE * stream = NULL;
		const int max_buffer = 2048;
		char buffer[max_buffer];
		stream = popen(cmd.Data(), "r");
		if (stream) {

			while (!feof(stream))
				if (fgets(buffer, max_buffer, stream) != NULL) env += buffer;
			pclose(stream);
		}

		TObjArray *fqfnArray = env.Tokenize('\n');
		for (int i = 0; i < fqfnArray->GetEntries(); i++) {

			TString varname = ((TObjString *)(fqfnArray->At(i)))->String();
			UIUC::HandboxMsg::envlist[varname] = TString(gSystem->Getenv(varname));
		}

		// Create a hashmap in order to replace first the shortest variables, more precisely the one with the smallest ratio varname/value
		map<TString, TString>::iterator it;
		for ( it = UIUC::HandboxMsg::envlist.begin(); it != UIUC::HandboxMsg::envlist.end(); it++ )
		{
			TString name = it->first;
			TString str = it->second;

			double coeff = ((double) name.Length())/str.Length();
			for(int i = 0, N = envcoeffmap.size(); i <= N; i++) {

				//cout << i << endl;
				if(i == N) {

					envcoeffmap.push_back(coeff);
					envhashmap.push_back(name);
					break;
				}

				if(envcoeffmap[i] == coeff) {

					if(envhashmap[i].Length() < name.Length()) {

						envcoeffmap.insert(envcoeffmap.begin()+(i+1), coeff);
						envhashmap.insert(envhashmap.begin()+(i+1), name);

					} else {

						envcoeffmap.insert(envcoeffmap.begin()+i, coeff);
						envhashmap.insert(envhashmap.begin()+i, name);
					}

					break;

				} else if(envcoeffmap[i] > coeff) {

					envcoeffmap.insert(envcoeffmap.begin()+i, coeff);
					envhashmap.insert(envhashmap.begin()+i, name);
					break;
				}
			}
		}

		delete fqfnArray;
		fqfnArray = NULL;
	}
}

TString UIUC::HandboxMsg::ExpandVariables(TString str, bool bRelativePath)
{
	// Get environmental variables
	UIUC::HandboxMsg::PrepareEnvList();

	if(bRelativePath) {

		if (str.BeginsWith("~/"))
			str = str.Replace(0, 2, "$HOME/");
		if (str.BeginsWith("./"))
			str = str.Replace(0, 2, "$PWD/");
		if (str.BeginsWith("../"))
			str = str.Replace(0, 3, "$PWD/../");
	}

	for (int i = 0, N = UIUC::HandboxMsg::envlist.size(); i < N; i++)
	{
		map<TString, TString>::iterator it = UIUC::HandboxMsg::envlist.find(envhashmap[i]);

		str.ReplaceAll("${" + it->first +"}", it->second);
		str.ReplaceAll("$(" + it->first +")", it->second);
		str.ReplaceAll("$"  + it->first +"" , it->second);
	}

	return str;
}

TString UIUC::HandboxMsg::MakeItShorter(TString str, TString color, int length)
{
	str.ReplaceAll("file://", "");

	// Get environmental variables
	UIUC::HandboxMsg::PrepareEnvList();

	if(!UIUC::HandboxMsg::IsQuiet()) {

		for (int i = 0, N = UIUC::HandboxMsg::envlist.size(); i < N; i++)
		{
			map<TString, TString>::iterator it = UIUC::HandboxMsg::envlist.find(envhashmap[i]);
			if(it->second.Length() == 0 || it->second.Length() < it->first.Length()+1 || it->first.Length() < 3) continue;             // Skip if variable is too small..

			str.ReplaceAll(it->second, UIUC::HandboxMsg::kPurple + "${" + it->first +"}"+ color);
		}
	}

	if(UIUC::HandboxMsg::IsDebugEnabled()) return str;

	// Reduce length of the string..
	TString buf = str;
	int diff = str.Length() - length;
	if(length != -1 && diff > 0 && length > 4) {

		buf  = color + str(0, str.Length()/2-diff/2);
		buf += UIUC::HandboxMsg::kRed + "[..]" + color;
		buf += str(str.Length()/2+diff/2, str.Length());
	}
	
	TString pwd = TString(gSystem->pwd()) + "/";
	buf.ReplaceAll(pwd, "./");
	return buf;
}

TString UIUC::HandboxMsg::ReadTTY(TString fname)
{
    TString out = "";

    if(fname.EqualTo("") || fname.EqualTo("/dev/tty") || fname.EqualTo("/dev/pts/0"))
        return "";

    FILE *fp = fopen(fname, "r");
    if(fp == NULL)
        return "";

    char * line = NULL;
    size_t len = 0;

    while(getline(&line, &len, fp) != -1)
        out += TString(line);

    if(fileno(fp) != kStdErr_dup && fileno(fp) != kStdOut_dup) fclose(fp);

    remove(fname);
    return out;
}


bool UIUC::HandboxMsg::ValidTTY(TString fname)
{
    fname = Readlink(fname);
    if(fname.EqualTo("")) return 0;
    if(fname.EqualTo("/dev/tty")) return 0;

    TString fname0;
    fname0 = Readlink(stdout);
    if(fname.EqualTo(fname0)) return 0;

    fname0 = Readlink(stderr);
    if(fname.EqualTo(fname0)) return 0;

    for(int i = 0, N = kStdOut.size(); i < N; i++) {
        fname0 = Readlink(kStdOut[i]);
        if(fname.EqualTo(fname0))
            return 0;
    }

    for(int i = 0, N = kStdErr.size(); i < N; i++) {
        fname0 = Readlink(kStdErr[i]);
        if(fname.EqualTo(fname0))
            return 0;
    }

    return 1;
}

int UIUC::HandboxMsg::OpenTTY(int desc, TString fname)
{
    if(fname.EqualTo("")) {
		
        if(desc == 1) fname = CreateTemporaryPath("/tmp/stdout.XXXXXX");
        else if(desc == 2) fname = CreateTemporaryPath("/tmp/stderr.XXXXXX");
    }

    if(UIUC::HandboxMsg::Warning(!ValidTTY(fname), __METHOD_NAME__, "Invalid TTY-"+TString::Itoa(desc,10)+" name provided.. \""+fname+"\""))
	return -1;

    UIUC::HandboxMsg::PrintDebug(20, __METHOD_NAME__, "Debugging mode detected.. TTY-"+TString::Itoa(desc,10)+" shunted.");
    if(UIUC::HandboxMsg::IsDebugEnabled()) return 0;

    if(desc == 1) {

        int fd = kStdOut.size() ? dup(1) : kStdOut_dup;
        kStdOut.push_back(fd);
        kStdOut_fname.push_back(fname);

        // Prepare the new fd
        FILE *fOut = fopen(fname, "w");
        if(UIUC::HandboxMsg::Error(fOut == NULL, __METHOD_NAME__, "Cannot open \""+fname+"\"")) return -1;
        fd = fileno(fOut);

        // Redirect stream
        fflush(stdout);
		dup2(fd, 1);
        if(fd != kStdOut_dup) fclose(fOut);

        return kStdOut.size();
    }

    if(desc == 2) {

        int fd = kStdErr.size() ? dup(2) : kStdErr_dup;
        kStdErr.push_back(fd);
        kStdErr_fname.push_back(fname);

        // Prepare the new fd
        FILE *fErr = fopen(fname, "w");
        if(UIUC::HandboxMsg::Error(fErr == NULL, __METHOD_NAME__, "Cannot open \""+fname+"\"")) return -1;
        fd = fileno(fErr);

        // Redirect stream
        fflush(stderr);
		dup2(fd, 2);
		if(fd != kStdErr_dup) fclose(fErr);

        return kStdErr.size();
    }

    return -1;
}

TString UIUC::HandboxMsg::CloseTTY(int desc)
{
    TString fname = "";

    UIUC::HandboxMsg::PrintDebug(20, __METHOD_NAME__, "Debugging mode detected.. TTY-"+TString::Itoa(desc,10)+" shunted.");
    if(UIUC::HandboxMsg::IsDebugEnabled()) return "";

    if(desc == 1 && kStdOut.size()) {

        FILE *fp = stdout;
        fname = Readlink(fp);

        int fd = kStdOut.back();
        kStdOut.pop_back();

        fflush(stdout);
        dup2(fd, 1);
        clearerr(stdout);

		fname = kStdOut_fname.back();

		kStdOut_fname.pop_back();
		kTrash.push_back(fname.Data());

        if(fd != kStdOut_dup)
			close(fd);
    }

    if(desc == 2 && kStdErr.size()) {

        FILE *fp = stderr;
        fname = Readlink(fp);

        int fd = kStdErr.back();
        kStdErr.pop_back();

        fflush(stderr);
        dup2(fd, 2);
        clearerr(stderr);

		fname = kStdErr_fname.back();
		kStdErr_fname.pop_back();
		kTrash.push_back(fname.Data());

        if(fd != kStdErr_dup)
			close(fd);
    }

    return fname;
}


TString UIUC::HandboxMsg::ListTTY(int desc)
{
    TString list = "Descriptor #" + TString::Itoa(desc,10) + ":\n";
    if(desc == 1) {

        for(int i = 0, N = kStdOut.size(); i < N; i++) {

            int fd = kStdOut[i];
	    	TString pts = Readlink(fd);
	   		TString fname = kStdOut_fname[i];

            if(fname.EqualTo("")) list += "* tty="+TString::Itoa(i,10)+"; no file (fd = " +TString::Itoa(fd,10) + ")" + "\n";
            else list += "* tty="+TString::Itoa(i,10)+"; "+pts+" --> "+fname+" (fd = " +TString::Itoa(fd,10) + ")" + "\n";
        }

        int fd = fileno(stdout);
		TString pts = Readlink(fd);
        list += "* stdout; "+pts+" (fd = " +TString::Itoa(fd,10) + ")";

    } else if(desc == 2) {

        for(int i = 0, N = kStdErr.size(); i < N; i++) {

            int fd = kStdErr[i];
			TString pts = Readlink(fd);
			TString fname = kStdErr_fname[i];

            if(fname.EqualTo("")) list += "* tty="+TString::Itoa(i,10)+"; no file (fd = " +TString::Itoa(fd,10) + ")" + "\n";
            else list += "* tty="+TString::Itoa(i,10)+"; "+pts+" --> "+fname+" (fd = " +TString::Itoa(fd,10) + ")" + "\n";
        }

        int fd = fileno(stderr);
        TString fname = Readlink(fd);
        list += "* stderr; "+fname+" (fd = " +TString::Itoa(fd,10) + ")";

    } else {

        list += "* nothing found";
    }

    return list;
}

int UIUC::HandboxMsg::RedirectTTY(const char* title, bool make_it_shorter)
{
    UIUC::HandboxMsg::PrintDebug(20, __METHOD_NAME__, "Redirection TTY("+TString::Itoa(iTTY,10)+") starts here");

    iTTY++;
    if(UIUC::HandboxMsg::Warning(OpenTTY(1) < 0, __METHOD_NAME__, "Redirection TTY-1("+TString::Itoa(iTTY,10)+") could not be opened in <"+ TString(title) +">")) {
		iFailedTTY++;
		return iTTY;
    }

    if(UIUC::HandboxMsg::Warning(OpenTTY(2) < 0, __METHOD_NAME__, "Redirection TTY-2("+TString::Itoa(iTTY,10)+") could not be opened in <"+ TString(title) +">")) {
        CloseTTY(1);
		iFailedTTY++;
		return iTTY;
    }

    bShortRedirectTTY = make_it_shorter;
    return iTTY;
}

void UIUC::HandboxMsg::GetTTY(const char *title, TString *out, TString *err)
{
	UNUSED(title);

    if(UIUC::HandboxMsg::iFailedTTY-- > 0) {
		UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Redirection TTY("+TString::Itoa(iTTY,10)+") would have ended here");
		return;
    }

    if(UIUC::HandboxMsg::iTTY <= 0) {
        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Redirection TTY("+TString::Itoa(iTTY,10)+") unexpectedly ended here");
		return;
    }

    TString fOut = CloseTTY(1);
    if(out != NULL) *out = ReadTTY(fOut);
	if(FileExists(fOut)) remove(fOut);

    TString fErr = CloseTTY(2);
    if(err != NULL) *err = ReadTTY(fErr);
	if(FileExists(fErr)) remove(fErr);

    UIUC::HandboxMsg::PrintDebug(20, __METHOD_NAME__, "Redirection TTY("+TString::Itoa(--iTTY,10)+") ended here");
}

void UIUC::HandboxMsg::PrintTTY(const char* title, bool bOut, bool bErr)
{
    TString out,err;
    GetTTY(title, &out, &err);
    if(!bOut) out = "";
    if(!bErr) err = "";

    UIUC::HandboxMsg::PrintTTY(title, out, err);
    bSingleSkipIncrementTab = false;
    bSingleCarriageReturn = false;
}

void UIUC::HandboxMsg::DumpTTY(const char* title)
{
    GetTTY(title, NULL, NULL);
    UIUC::HandboxMsg::PrintDebug(20, __METHOD_NAME__, "Redirection TTY("+TString::Itoa(iTTY,10)+") has been dumped");
}
