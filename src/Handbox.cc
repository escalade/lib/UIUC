/**
 **********************************************
 *
 * \file Handbox.cc
 * \brief Source code of the Handbox class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <UIUC/Handbox.h>

using namespace std;

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

TDirectory *UIUC::Handbox::gRootDirectory = gDirectory;

const bool UIUC::Handbox::kClone = true;
const int  UIUC::Handbox::kDefaultSlot = -1;
const bool UIUC::Handbox::kDirectory = true;
const bool UIUC::Handbox::kSingleFile = !UIUC::Handbox::kDirectory;
const int  UIUC::Handbox::kTextFile = 0;
const int  UIUC::Handbox::kRootFile = 1;
const bool UIUC::Handbox::kRemoveNullPtr = false;
const bool UIUC::Handbox::kKeepNullPtr = true;

TString UIUC::Handbox::RunOption = "";
bool UIUC::Handbox::bProcessLoop = true;

bool UIUC::Handbox::WarmUp(Option_t* option)
{
	TString fOutputPattern = this->GetOutputPattern();

 	int seed = TString(gSystem->Getenv("ESCALADE_SEED")).Atoi() == 0 ? UIUC::E_MICROTIMEPID : TString(gSystem->Getenv("ESCALADE_SEED")).Atoi();
	UIUC::SetRandomSeed(seed);

 	int nThreads = TString(gSystem->Getenv("ESCALADE_NTHREADS")).Atoi() > 1 ? TString(gSystem->Getenv("ESCALADE_NTHREADS")).Atoi() : 1;
	this->SetThreadPoolSize(nThreads);

	// Prepare option acccording to environment.
	// Handbox is using ESCALADE_XXX environment variables.. (Cf. HandboxUsage.h)
	TString opt  = option + TString(gSystem->Getenv("ESCALADE_OPT"));
			opt += !opt.Contains("s") && (!fOutputPattern.EqualTo("") || !fOutputFinalizeStr.EqualTo("")) ? "s" : "";
			opt += !opt.Contains("v") && this->IsDebugEnabled() ? "v" : "";
			opt.ToLower();

	this->RunOption = opt;

	//
	// Set the output filename list in case one has been found..
	//
	if (opt.Contains("0") && UIUC::TFileReader::HasPattern(fOutputPattern)) {

		UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Replacement char is not allowed, saving is only allowed after finalization (0-option found..)");
		return 1;

	} else if(UIUC::TFileReader::HasPattern(fOutputPattern) && !fIdentifiers.size() && opt.Contains("s")) {

		UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Saving option with replacement char found.. But no ID found.. (Use {} in the --output options)");
		return 1;

	} else if(fOutputPattern.EqualTo("") && opt.Contains("s")) {

		TString eMessage = "Saving option used but no output filename specified..(Use --output/--output-finalize options)";
		UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, eMessage);

	} else if(!opt.Contains("s")) {

		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No saving option found.. (Use s-option for that)");

	} else {

		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Saving option found..");
		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "* Step output file: " + fOutputPattern);
		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "* Final output file: " + fOutputFinalizeStr);
	}

	//
	// Set custom style
	//
	if(!opt.Contains("h")) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No custom style not loaded.. (Use h-option to use it)");
	else if(!UIUC::Handbox::InstanceStyle().GetCustomStyleName().EqualTo("")) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Custom style \"" + UIUC::Handbox::InstanceStyle().GetCustomStyleName() + "\" loaded..");

	//
	// Check input ID size VS input size
	//
	if(this->fIdentifiers.size() && this->fIdentifiers.size() != this->fInput.size()) {

		UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "An input ID has been set, but not matching with the number of input specifieds.. abort");
		return 1;
	}

	//
	//  resizing..
	//
	UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Resize all buffer sizes according to input buffer size");
	this->fIdentifiers.resize(this->fInput.size());
	this->ResizeTempo();
	this->ResizeOutput();

	UIUC::Handbox::EnableSignalHandler();
	UIUC::Handbox::EnableBacktraceHandler();
	return 0;
}

void UIUC::Handbox::DisableSignalHandler() {

    UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Disable Ctrl+C catcher..");
    signal(SIGINT, SIG_DFL);
}

void UIUC::Handbox::EnableSignalHandler(){

	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Enable Ctrl+C catcher..");
	struct sigaction sigIntHandler;
	sigIntHandler.sa_handler = UIUC::Handbox::HandlerCTRL_C;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;
	sigaction(SIGINT, &sigIntHandler, NULL);
}

void UIUC::Handbox::DisableBacktraceHandler() {

    UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Disable backtrace catcher..");
    signal(SIGINT, SIG_DFL);
}

void UIUC::Handbox::EnableBacktraceHandler(){

	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Enable backtrace catcher..");
	struct sigaction sigIntHandler;
	sigIntHandler.sa_handler = UIUC::Handbox::HandlerBacktrace;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;
	sigaction(SIGSEGV, &sigIntHandler, NULL);
}

void UIUC::Handbox::HandlerCTRL_C(int s){

	cerr << endl;
	cerr << "** Caught signal " << s << endl;
	cerr << "** You stopped processing pressing CTRL-C.."<< endl;
	cerr << "** Now, going to finish processing last file." << endl;
	cerr << "** Please press CTRL-C again, if you want to exit." << endl;

	UIUC::Handbox::DisableSignalHandler();
	UIUC::Handbox::bProcessLoop = false;
}

void UIUC::Handbox::HandlerBacktrace(int s){

	cerr << endl;
	cerr << "** Caught signal " << s << endl;

	UIUC::Handbox::DisableBacktraceHandler();
	UIUC::Handbox::bProcessLoop = false;

	void *array[10];
	size_t size;

	// get void*'s for all entries on the stack
	size = backtrace(array, 10);

	// print out all the frames to stderr
	fprintf(stderr, "Error: signal %d:\n", s);
	backtrace_symbols_fd(array, size, STDERR_FILENO);
	exit(1);
}

TString UIUC::Handbox::GetOutputPattern()
{
	// Output pattern..
	if(this->fOutputFinalizeStr.EqualTo("")) {

		this->fOutputFinalizeStr = TString(gSystem->Getenv("ESCALADE_HOUTPUT_FINALIZE"));
		if(UIUC::TFileReader::HasPattern(this->fOutputFinalizeStr)) {
			this->fOutputFinalizeStr = UIUC::TFileReader::SubstituteIdentifier(this->fOutputFinalizeStr, "final");
		}

		UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Default ESCALADE_HOUTPUT_OUTPUT has been found and will be used for finalization");
	}

	if(this->fOutputPattern.EqualTo("")) {

		TString fOutputDefaultStr = TString(gSystem->Getenv("ESCALADE_HOUTPUT"));
		UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Default ESCALADE_HOUTPUT has been found and will be used..");

		this->SetOutputPattern(fOutputDefaultStr);
	}

	return this->fOutputPattern;
}

bool UIUC::Handbox::SetOutputPattern(TString pattern) {

	this->fOutputPattern = pattern;
	if (this->fOutputFinalizeStr.EqualTo("")) {

		this->fOutputFinalizeStr = pattern;

		if(UIUC::TFileReader::HasPattern(pattern)) {
	
			this->fOutputFinalizeStr = UIUC::TFileReader::SubstituteIdentifier(this->fOutputFinalizeStr, "final");
			UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Replacement char found in the output.. Outgoing file for finalization will be named \""+this->fOutputFinalizeStr+"\"");
			UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Outgoing step files will be named based on the pattern: \""+pattern+"\"");
			UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Saving option will be called at each step");
		}
	}
	
	return 1;
}

TString UIUC::Handbox::GetInputPattern()
{
	// Input pattern..
	TString fInputDefaultStr = TString(gSystem->Getenv("ESCALADE_HINPUT"));

	if(fInputDefaultStr.EqualTo("")) fInputDefaultStr = (TString) UIUC::TFileReader::kSeparator+"{}";
	// if(fInputPattern.EqualTo(""))
	// 	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Default ESCALADE_HINPUT has been found and will be used.. Variable \"fInputPattern\" is empty..");

	this->SetInputPattern(fInputDefaultStr);
	return this->fInputPattern;
}

bool UIUC::Handbox::SetInputPattern(TString pattern) {

	this->fInputPattern = pattern;
	if (UIUC::TFileReader::HasPattern(pattern))
		UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Input is collected based on the following pattern: \""+pattern+"\"");

	return 1;
}

int UIUC::Handbox::Wrapper(int skip_entries, int N, int i, TString opt, TString className)
{
	TDirectory *gParentDirectory = gDirectory;

	TString eMessage;
	TDirectory *working_directory = UIUC::Handbox::gRootDirectory->mkdir("slot-" + TString::Itoa(i, 10) );
	working_directory->cd();

	// Reset index at 0 after each processing, it will be increamented each time AddOutput is called
	this->iOutputSlot = 0;

	// Check if all tempo objects are loaded !
	bool bTempo = true;
	for(int j = 0; j < fTempo.size(); j++) {

		bTempo = (i < fTempo[j].size() && fTempo[j][i] != NULL);
		if(!bTempo) return 1;
	}

	if(!bProcessLoop) return 1;
	if(fInput[i] == NULL) {

		eMessage = "Input object #" + TString::Itoa(i, 10) + " is a null ptr.. skip";
		UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, eMessage);

	} else if(fInput[i] != (TObject*)(uintptr_t)-1 && !className.EqualTo("") && !fInput[i]->IsA()->InheritsFrom(className)) {

		eMessage = "Input object #" + TString::Itoa(i, 10) + " is not inheriting from \"" + className + "\"..";
		UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);

	} else {

		TObjString *fInputBak = NULL;
		if(fInput[i] == (TObject*)(uintptr_t)-1) {

			eMessage = "Special iteration #" + TString::Itoa(i, 10) + " requested..";
			UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, eMessage);

		} else {

			if(fInput[i]->InheritsFrom("TObjString")) {

				TString fInputPattern = ((TObjString*) fInput[i])->String();
				if(fInputPattern.BeginsWith("TFileReader#")) {

					fInputBak = (TObjString*) fInput[i];
					fInputPattern.ReplaceAll("TFileReader#", "");
					fInput[i] = UIUC::TFileReader::ReadObj(fInputPattern);

					eMessage = "TFileReader detected.. try to load and replace input from handbox";
					UIUC::HandboxMsg::PrintDebug(30, __METHOD_NAME__, eMessage);

					if(UIUC::HandboxMsg::Warning(!fInputPattern.EqualTo("") && fInput[i] == NULL, __METHOD_NAME__, "Cannot load \""+fInputPattern+"\".. skip processing")) return 0;
				}
			}
		}

		vector<TObjString*> fTempoBak(fTempo.size(), NULL);
		for(int j = 0; j < fTempo.size(); j++)
		{
			if(fTempo[j][i] == NULL) continue;
			if(fTempo[j][i]->InheritsFrom("TObjString")) {

				TString fTempoStr = ((TObjString*) fTempo[j][i])->String();
				if(fTempoStr.BeginsWith("TFileReader#")) {

					fTempoBak[j] = (TObjString*) fTempo[j][i];
					fTempoStr.ReplaceAll("TFileReader#", "");
					fTempo[j][i] = UIUC::TFileReader::ReadObj(fTempoStr);

					eMessage = "TFileReader detected.. try to load and replace input from handbox";
					UIUC::HandboxMsg::PrintDebug(30, __METHOD_NAME__, eMessage);

					if(UIUC::HandboxMsg::Warning(!fTempoStr.EqualTo("") && fTempo[j][i] == NULL, __METHOD_NAME__, "Cannot load \""+fTempoStr+"\".. skip processing")) continue;
				}
			}
		}

		// Prepare environment when first element..
		if(this->iOutput == skip_entries) {

			TDirectory *init_directory = UIUC::Handbox::gRootDirectory->mkdir("../init");
			init_directory->cd();

			UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Initialization procedure..");
			if( this->Init() ) {

				eMessage = "Something went wrong while initializing..";
				UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);
				this->sigProcessErr = 1;

				return 1;
			}

			working_directory->cd();
		}

		TObject *input = NULL;
		if(fInput[i] != (TObject*)(uintptr_t)-1 && fInput[i] != NULL) 
			input = fInput[i];

		this->sigProcessErr = this->Process(input, i);

		if(!UIUC::HandboxMsg::IsSuperQuiet())
			cout << UIUC::HandboxMsg::CarriageReturn(1);

		if( this->sigProcessErr > 0 ) {

			UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Error signal returned by the process method is \"" + TString::Itoa(this->sigProcessErr, 10) + "\"");
			if(this->sigProcessErr > 1) {

				eMessage = "Fatal error while processing ID #" + TString::Itoa(i, 10) + ".. abort";
				UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);
				return 2;

			} else {

				eMessage = "Failed to process ID #" + TString::Itoa(i, 10) + ".. abort";
				UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, eMessage);
			}
		}

		if(fInputBak != NULL) {

			eMessage = Form("Input #%d has been transmitted to the outgoing ..\nThis is not allowed.. Please clone the object..", i);
			if( UIUC::HandboxMsg::Error(FindOutput(fInput[i]), __METHOD_NAME__, eMessage) ) return 1;

			if(fInput[i] != NULL && !fInput[i]->InheritsFrom("TCanvas"))
				delete fInput[i];
			
			fInput[i] = fInputBak;
		}

		for(int j = 0; j < fTempo.size(); j++) {

			if(fTempoBak[j] != NULL) {

				eMessage = Form("Tempo #%d.%d has been transmitted to the outgoing ..\nThis is not allowed.. Please clone the object..", j, i);

				if( UIUC::HandboxMsg::Error(FindOutput(fTempo[j][i]), __METHOD_NAME__, eMessage) ) return 1;
				if(fTempo[j][i] != NULL && !fTempo[j][i]->InheritsFrom("TCanvas"))
					delete fTempo[j][i];

				fTempo[j][i] = fTempoBak[j];
			}
		}

		if(opt.Contains("g")) // Wait also here to allow user to modify before writing into PDF
			gSystem->ProcessEvents();

		if( this->RunOption.Contains("0") && this->RunOption.Contains("s")) {

			if(iOutput == skip_entries) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Automatic serial saving method disabled, 0-option detected..");
		}
		else if( this->RunOption.Contains("s") && UIUC::TFileReader::HasPattern(this->fOutputPattern) ) {

			this->Save(this->iOutput);
			if(opt.Contains("g")) // Wait also here to allow user to modify before writing into PDF
				UIUC::HandboxUsage::WaitForGraphicsToBeClosed(opt);             // Wait also here to allow user to modify before writing into PDF

			this->CleanOutput(this->iOutput);
		}
	}

	this->iOutput++;
	gParentDirectory->cd();

	finishedWorkers++;

    if(!UIUC::HandboxMsg::IsSuperQuiet()) {

		if(UIUC::TFileReader::HasPattern(this->fOutputPattern)) UIUC::HandboxMsg::PrintProgressBar("Processing (incl. serial saving).. ", finishedWorkers, N);
		else UIUC::HandboxMsg::PrintProgressBar("Processing.. ", finishedWorkers, N);
	}

	return 0;
}

int UIUC::Handbox::Run(Option_t* option, TString className)
{
	TString eMessage;
	this->iOutputSlot = 0;
	kRunStarted = true;

	this->sigPrepareErr = 0;
	this->sigFinalizeErr = 0;
	this->sigProcessErr = 0;

	TDatime bot;
	UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Begin of time: " + UIUC::HandboxMsg::kGreen + (TString) bot.AsString() +  UIUC::HandboxMsg::kNoColor);
	this->stopwatch = UIUC::microtime();

	// Prepare run
	if( this->WarmUp(option) ) {

		UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to prepare..");
		this->sigPrepareErr = 1;
		return 1;
	}

	TString opt = this->RunOption;
			opt.ToLower();

	//
	// Open graphics from here !
	//

	if(!fInput.size()) {

		eMessage = "No input file to process..";
		this->sigProcessErr = 1;

		UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, eMessage);
		UIUC::HandboxUsage::WaitForGraphicsToBeClosed(opt);

	} else {

		// Start processing..
		int skip_entries = TString(gSystem->Getenv("ESCALADE_SKIPENTRIES")).Atoi();
		if(gSystem->Getenv("ESCALADE_SKIPENTRIES")) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, Form("Skip %d first entries", skip_entries));
		int read_entries = (!gSystem->Getenv("ESCALADE_READENTRIES")) ? fInput.size() : TString(gSystem->Getenv("ESCALADE_READENTRIES")).Atoi();
		if(gSystem->Getenv("ESCALADE_READENTRIES")) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, Form("Only going to read %d entries", read_entries));

		cout << UIUC::HandboxMsg::Tab();
		cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "Running..    ");
		cout << UIUC::HandboxMsg::Endl();
		UIUC::HandboxMsg::IncrementTab();

		std::vector<TCanvas*> fCanvas;
		this->iOutput = skip_entries;

		if(ROOT::GetThreadPoolSize() > 1) { 
		
			// Experimental feature..
			UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Multithreaded handbox is an experimental feature..");
			
			UIUC::HandboxMsg::kDebug = 1; // Mark as level 1 debug.. due to a printing issue (to be investigated..)
			// NB : iOutputSlot is most likely not thread-safe..
			// @TODO: implement.. std::thread::id this_id = std::this_thread::get_id();
		}
		
	    int N = (int) TMath::Min((int) fInput.size(), (int) read_entries+skip_entries);
		for(int i = skip_entries; i < N; i++) {

			if(ROOT::GetThreadPoolSize() > 1) { 
				
				std::thread *t = new std::thread(&UIUC::Handbox::Wrapper, this, skip_entries, N, i, opt, className);
				workers.push_back(t);

				int runningWorkers = workers.size() - finishedWorkers + 1;
				while(runningWorkers >= ROOT::GetThreadPoolSize()) {

					UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Wait for workers to be free.. (currently " + TString::Itoa(runningWorkers,10) + " running workers out of " + TString::Itoa(ROOT::GetThreadPoolSize(), 10) + ")");
					sleep(1);

					runningWorkers = workers.size() - finishedWorkers + 1;
				}

			} else {

				int ret = UIUC::Handbox::Wrapper(skip_entries, N, i, opt, className);
				if(ret == 1) break;
			}
		}

		WaitForWorkersToBeDone();
		
		if(this->iOutput == (int) fInput.size() || this->iOutput == (int) read_entries+skip_entries || !bProcessLoop) {

			UIUC::HandboxMsg::DecrementTab();
			UIUC::HandboxUsage::DisableSignalHandler();

			if(this->FinalizeRun()) return 1;
			UIUC::HandboxMsg::IncrementTab();
		}
	}

	if(UIUC::HandboxMsg::IsSuperQuiet()) return 0;
	UIUC::HandboxMsg::DecrementTab();
	return 0;
}

bool UIUC::Handbox::FinalizeRun()
{
	// In case of debug mode, force to print
	TString opt = this->RunOption;
            opt.ToLower();

	this->iOutputSlot = 0;

	// Finalizing the process
	TDirectory *gParentDirectory = gDirectory;
	TDirectory *working_directory = UIUC::Handbox::gRootDirectory->mkdir("finalize");
	working_directory->cd();

	UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Finalization on going.. ");
	UIUC::HandboxMsg::IncrementTab();

	this->sigFinalizeErr = this->Finalize();
	
	if( UIUC::HandboxMsg::Error(sigFinalizeErr, __METHOD_NAME__, TString("Failed to finalize the procedure..")) ) {

		// If procedure is failing.. reset the usage booleans
		UIUC::HandboxMsg::DecrementTab();
		gParentDirectory->cd();

		return 1;
	}

	// Print method
	if(!opt.Contains("p")) {

		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No print function called.. (Use p-option)");

	} else {

		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Print function called..");
		this->Print();
	}

	if(opt.Contains("g")) UIUC::HandboxUsage::WaitForGraphicsToBeClosed(opt); // Wait also here to allow user to modify before writing into PDF

	// Wait for closing all canvases
	if(!opt.Contains("c")) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No canvas storing option found.. (Use c-option)");
	else {

		TString pdfname = TString(gSystem->Getenv("ESCALADE_PDFNAME"));
		UIUC::TFileReader::WritePDF(pdfname, "UPDATE");
	}

	// Save method
	int outgoing_obj = 0;
	for(int i = 0; i < fOutput.size(); i++) {

		for(int j = 0; j < fOutput[i].size(); j++) {

			outgoing_obj += (fOutput[i][j] == NULL) ? 0 : 1;
		}
	}

	if(!opt.Contains("s")) {

		if(outgoing_obj) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No save option called.. (Use s-option)");
		else UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No save option called.. However there is nothing to save");

		if( !TString(gSystem->Getenv("ESCALADE_PDFNAME")).EqualTo("") ) cout << "Target: Output saved into pdf file.. " << endl;
		else {

			if(UIUC::HandboxMsg::IsDebugEnabled() || !UIUC::HandboxMsg::IsQuiet()) UIUC::HandboxMsg::Warning(outgoing_obj, __METHOD_NAME__, "Object(s) found in outgoing directory.. but not saved.. (Use --output/--output-finalize options)");
			else cout << "Target: No output specified.. outgoing object(s) cannot be saved" << endl;
		}
	}

	UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__,
	                               Form("%sSummary report :%s %s%d processed object(s)%s out of %s%d object(s)%s",
	                                    UIUC::HandboxMsg::kOrange.Data(), UIUC::HandboxMsg::kNoColor.Data(),
	                                    UIUC::HandboxMsg::kGreen.Data(), this->iOutput, UIUC::HandboxMsg::kNoColor.Data(),
	                                    UIUC::HandboxMsg::kRed.Data(), (int) this->fInput.size(), UIUC::HandboxMsg::kNoColor.Data()
	                                    ));

	// Check processing if there are outgoing objects
	if(opt.Contains("s")) {

		if(!outgoing_obj) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No output object to save..");
		else {

			UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__,
			                               Form("%sData saved :%s %s%d outgoing object(s)%s detected over %s%d slot(s)%s.. (into %s)",
			                                    UIUC::HandboxMsg::kOrange.Data(), UIUC::HandboxMsg::kNoColor.Data(),
			                                    UIUC::HandboxMsg::kGreen.Data(), outgoing_obj, UIUC::HandboxMsg::kNoColor.Data(),
			                                    UIUC::HandboxMsg::kRed.Data(), (int) fOutput.size(), UIUC::HandboxMsg::kNoColor.Data(),
			                                    UIUC::TFileReader::FileName(this->fOutputPattern).Data())
			                               );

			UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Save option called..");
			this->Save();
		}
	}

	//
	// Dump option
	int dump_obj = 0;
	for(int i = 0; i < gDirectory->GetList()->GetSize(); i++) {
		if(!this->FindOutput(gDirectory->GetList()->At(i))) dump_obj += 1;
	}

	if(!opt.Contains("d")) {

		if(!dump_obj) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "No dump option called and nothing to dump.. (Use d-option)");
		else {

			UIUC::HandboxMsg::PrintMessage(
				__METHOD_NAME__, Form("%s%d object(s)%s detected in the finalization directory.. but no dump option called..",
				                      UIUC::HandboxMsg::kRed.Data(), dump_obj, UIUC::HandboxMsg::kNoColor.Data()
				                      )
				);
		}

	} else {

		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Dump option called..");
		UIUC::HandboxMsg::IncrementTab();
		if(!dump_obj) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Nothing to dump ! ");
		else {

			TFile *f = NULL;
			TString filename = this->PrefixDumpFile + ".root";
			UIUC::TFileReader::Open(filename, f, "UPDATE");

			if(f == NULL) UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Failed to dump finalization into \"" + filename + "\".. no file opened");
			else {

				UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__,
				                               "Dump " + UIUC::HandboxMsg::kGreen + TString::Itoa(dump_obj,10) + " object(s)" + UIUC::HandboxMsg::kNoColor + " into \"" + filename
				                               );

				TObject *obj = NULL;
				TIter Next(working_directory->GetList());
				while ( (obj = (TObject*) Next()) ) {
					if(this->FindOutput(obj)) continue;
					else if(!obj->InheritsFrom("TDirectory")) gDirectory->WriteTObject(obj);
					else UIUC::TFileReader::CopyDirectory((TDirectory*) obj, gDirectory);
				}
			}
		}
		UIUC::HandboxMsg::DecrementTab();
	}

	UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Finished ! Everything has been processed.");
	UIUC::HandboxMsg::DecrementTab();
	gParentDirectory->cd();

	return (this->CleanOutput() == 0);
}

bool UIUC::Handbox::FindOutput(TObject *obj, int index_exception, int slot_exception)
{
	if(obj == NULL) return false;
	for(int k = 0; k < this->fOutput.size(); k++) {

		if(k == slot_exception && index_exception == -1) continue;

		for(int l = 0; l < this->fOutput[k].size(); l++) {

			if(l == index_exception) continue;
			if( this->fOutput[k][l] == NULL) continue;

			if(obj == this->fOutput[k][l]) return true;
			else if( this->fOutput[k][l]->InheritsFrom("TDirectory") ) {

				// In case your have this directory in a nested directory structure..
				TDirectory *dir = (TDirectory*) this->fOutput[k][l];

				TString eMessage = "Object \""+TString(obj->GetName())+"\" already inside the directory \""+TString(dir->GetName())+"\" in output .. skip";
				if( UIUC::HandboxMsg::Warning(dir->FindObject(obj), __METHOD_NAME__, eMessage) ) return true;
			}
		}
	}

	return false;
}

bool UIUC::Handbox::FindTempo(TObject *obj, int index_exception, int slot_exception)
{
	for(int k = 0; k < this->fTempo.size(); k++) {

		if(k == slot_exception && index_exception == -1) continue;

		for(int l = 0; l < this->fTempo[k].size(); l++) {

			if(l == index_exception) continue;
			if( this->fTempo[k][l] == NULL) continue;
			if( this->fTempo[k][l]->InheritsFrom("TDirectory") ) {

				TDirectory *dir = (TDirectory*) this->fTempo[k][l];
				TString eMessage = "An object has been found in a nested structure of a directory registered as tempo.. skip";
				if( UIUC::HandboxMsg::Warning(dir->FindObject(obj), __METHOD_NAME__, eMessage) ) return true;

			} else if(obj == this->fTempo[k][l]) return true;
		}
	}

	return false;
}

bool UIUC::Handbox::FindInput(TObject *obj, int index_exception)
{
	for(int l = 0; l < this->fInput.size(); l++) {

		if(l == index_exception) continue;
		if( this->fInput[l] == NULL) continue;
		if(fInput[l] == (TObject*)(uintptr_t)-1) continue;
		if( this->fInput[l]->InheritsFrom("TDirectory") ) {

			TDirectory *dir = (TDirectory*) this->fInput[l];
			TString eMessage = "An object has been found in a nested structure of a directory registered as input.. skip";
			if( UIUC::HandboxMsg::Warning(dir->FindObject(obj), __METHOD_NAME__, eMessage) ) return true;

		} else if(obj == this->fInput[l]) return true;
	}

	return false;
}

bool UIUC::Handbox::CleanOutput(int index, int slot)
{
	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Cleaning output ..");

	for(int j = 0; j < GetNumberOfOutputSlot(); j++) {

		if(slot != -1 && slot != j) continue;
		for(int i = 0; i < this->fOutput[j].size(); i++) {

			if(index != -1 && index != i) continue;
			uintptr_t addr = reinterpret_cast<uintptr_t>(this->fOutput[j][i]);
			TString eMessage = "Remove pointer from the output list #" + TString::Itoa(j, 10) + "."  + TString::Itoa(i, 10) + " (at: \"0x" + TString::LLtoa(addr, 16) + ")\"..";

			if(this->fOutput[j][i] == NULL) continue;
			UIUC::HandboxMsg::PrintDebug(20, __METHOD_NAME__, eMessage);

			if(this->fOutputDeleteSafely[j][i]) delete this->fOutput[j][i];
			this->fOutput[j][i] = NULL;
		}
	}

	return true;
}

bool UIUC::Handbox::CleanTempo(int index, int slot)
{
	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Cleaning tempo ..");

	for(int j = 0; j < GetNumberOfTempoSlot(); j++) {

		if(slot != -1 && slot != j) continue;
		for(int i = 0; i < GetN(); i++) {

			if(index != -1 && index != i) continue;
			if(this->fTempo[j][i] == NULL) continue;

			if( find(this->fTempo[j].begin()+i+1, this->fTempo[j].end(), this->fTempo[j][i]) == this->fTempo[j].end() ) {
				if(!UIUC::Handbox::FindTempo(this->fTempo[j][i], -1, j)) {
					if(!UIUC::Handbox::FindInput(this->fTempo[j][i], i)) {

						// Deleting the tempo object
						uintptr_t addr = reinterpret_cast<uintptr_t>(this->fTempo[j][i]);
						TString eMessage = "Delete tempo #" + TString::Itoa(j, 10) + "."  + TString::Itoa(i, 10) + " (at: \"0x" + TString::LLtoa(addr, 16) + ")\"..";
						UIUC::HandboxMsg::PrintDebug(20, __METHOD_NAME__, eMessage);

						// If file is depending on a key
						if(this->fTempo[j][i]->InheritsFrom("TCanvas")) continue;
						if(this->fTempo[j][i]->IsA()->InheritsFrom("TKey")) {
							if ( ((TKey*) this->fTempo[j][i])->GetFile() != NULL ) continue;                                         // If link with a file, do not delete.. closing the file is enough
						}


						delete this->fTempo[j][i];
					}
				}
			}

			//If used in fInput or fTempo, just replace to null pointer; it will be delete later at the destruction
			this->fTempo[j][i] = NULL;
		}
	}
	return true;
}

bool UIUC::Handbox::ResizeOutput(int slot)
{
	if(slot >= 0) {

		this->fOutput.resize(slot+1);
		this->fOutputDeleteSafely.resize(slot+1);
		this->fOutputDir.resize(slot+1);
	}

	if(this->fOutput.size() == 0) return 1;

	// Add one entry if finalize step..
	int N = this->fInput.size();
	if(this->iOutput == GetN()) N++;

	for(int j = 0; j < GetNumberOfOutputSlot(); j++) {

		if(slot != -1 && slot != j) continue;

		this->fOutput[j].resize(N, NULL);
		this->fOutputDeleteSafely[j].resize(N, false);
		this->fOutputDir[j].resize(N, "");
	}

	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Resizing output buffer.. New size = %d", fOutput.size());
	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Resizing entries.. New size = %d", fOutput[fOutput.size()-1].size());
	return true;
}

bool UIUC::Handbox::ResizeTempo(int slot)
{
	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Resizing tempo buffer..");
	for(int j = 0; j < GetNumberOfTempoSlot(); j++) {

		if(slot != -1 && slot != j) continue;
		this->fTempo[j].resize(this->fInput.size());
	}

	return true;
}

bool UIUC::Handbox::CleanInput(int index)
{
	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Cleaning input ..");

	for(int i = 0; i < this->fInput.size(); i++) {

		if(index != -1 && index != i) continue;

		if(this->fInput[i] == NULL) continue;
		if(fInput[i] == (TObject*)(uintptr_t)-1) continue;
		if(this->fInput[i]->InheritsFrom("TKey") && ((TKey*) this->fInput[i])->GetFile() != NULL) continue;                 // If link with a file, do not delete.. closing the file is enough

		if( find(this->fInput.begin()+i+1, this->fInput.end(), this->fInput[i]) == this->fInput.end() ) {

			uintptr_t addr = reinterpret_cast<uintptr_t>(this->fInput[i]);
			TString eMessage = "Delete input #" + TString::Itoa(i, 10) + " (at: \"0x" + TString::LLtoa(addr, 16) + ")\"..";
			UIUC::HandboxMsg::PrintDebug(20, __METHOD_NAME__, eMessage);

			if(!this->fInput[i]->InheritsFrom("TCanvas")) continue;
			delete this->fInput[i];
		}

		//If used in fInput or fTempo, just replace to null pointer; it will be delete later at the destruction
		this->fInput[i] = NULL;
	}

	return true;
}

bool UIUC::Handbox::SetOutputSlot(int slot)
{
	TString eMessage = "Cannot set a negative index";
	if( UIUC::HandboxMsg::Warning(slot < 0, __METHOD_NAME__, eMessage) ) return false;

	this->iOutputSlot = slot;
	eMessage = "Output slot selected is " + TString::Itoa(slot, 10) + ".. !";
	UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);

	return true;
}

TFile *UIUC::Handbox::GetOutputFile(int index)
{
	TString fOutputFullname = this->fOutputPattern;
	if(fOutputFullname.EqualTo("")) return NULL;

	if(UIUC::TFileReader::HasPattern(fOutputFullname))
		fOutputFullname = UIUC::TFileReader::SubstituteIdentifier(fOutputFullname, this->fIdentifiers[index]);	

	outputFileCall = true;
	return TFile::Open(fOutputFullname, "UPDATE");
}

bool UIUC::Handbox::AddInput(int iteration) {

	std::lock_guard<std::mutex> guard(workerMutex);

	UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Special iteration mode called.. N = %d", iteration);
	for(int i = 0; i < iteration; i++) {

		fInput.push_back((TObject*)(uintptr_t) -1);
		fIdentifiers.push_back(vector<TString>(fIdentifiers.size() + i));
	}

	return true;
}

bool UIUC::Handbox::AddInput(TObject* obj, vector<TString> identifier)
{
	std::lock_guard<std::mutex> guard(workerMutex);

	if(obj == NULL) return false;

	fInput.push_back(obj);
	fIdentifiers.push_back(identifier);

	return true;
}

bool UIUC::Handbox::AddInput(vector<TObject*> obj, vector<vector<TString>> identifier)
{
	bool ret = identifier.size();
	for(int i = 0, N = identifier.size() > 0 ? identifier.size() : obj.size(); i < N; i++) {

		if(obj[i] == NULL) continue;
		ret &= this->AddInput(obj[i], identifier.size() ? identifier[i] : vector<TString>());
	}

	return ret;
}

bool UIUC::Handbox::AddInput(vector<TString> vStr, vector<vector<TString>> identifier)
{
	vector<TObject*> vObjStr;
	for(int i = 0; i < vStr.size(); i++)
		vObjStr.push_back(new TObjString(vStr[i]));

	return AddInput(vObjStr, identifier);
}

bool UIUC::Handbox::AddInput(vector<TKey*> k, vector<vector<TString>> identifier)
{
	vector<TObject*> v(k.begin(), k.end());             // Cast vector<TKey*> into vector<TObject*>
	return AddInput(v, identifier);
}

bool UIUC::Handbox::AddInput(TFileReader &input, bool bChain)
{
	if(this->GetOutputPattern().Contains("{}")) {

		vector<TString> vInput      = input.GetFqfn();	
		vector<vector<TString>> vIdentifiers;

		vIdentifiers = input.ParseIdentifiers(this->GetInputPattern());
		if(vIdentifiers.size() == 0) {

			for(int i = 0; i < vInput.size(); i++) 
				vIdentifiers.push_back({TString::Itoa(i+1, 10)});
		}

		for(int i = 0; i < vInput.size(); i++)
			vInput[i] = "TFileReader#" + vInput[i];

		return AddInput(vInput, vIdentifiers);

	} else if(bChain) {

		vector<TObject *> vInput      = input.ChainObjects();
		
		vector<vector<TString>> vIdentifiers;
		for(int i = 0; i < vInput.size(); i++)
			vIdentifiers.push_back({TString::Itoa(i+1, 10)});

		return AddInput(vInput, vIdentifiers);
	
	} else {

		vector<vector<TString>> vIdentifiers = input.ParseIdentifiers(this->GetInputPattern());
		if(vIdentifiers.size() == 0) return false;

		vector<TString> vInput      = input.GetFqfn();
		for(int i = 0; i < vInput.size(); i++)
			vInput[i] = "TFileReader#" + vInput[i];

		return AddInput(vInput, vIdentifiers);
	}

	return false;
}

bool UIUC::Handbox::AddInput(TFileReader *input, bool bChain)
{
	if(input == NULL) return false;
	return this->AddInput(*input, bChain);
}

int UIUC::Handbox::AllocateTempoSlot(int slot)
{
	std::lock_guard<std::mutex> guard(workerMutex);

	if(slot < 0) slot = this->fTempo.size();
	while(slot >= (int) this->fTempo.size()) {

		this->fTempo.push_back(vector<TObject*>(this->fInput.size()));

		TString eMessage = "New slot #"+TString::Itoa(slot, 10) + " created for tempo";
		UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);
	}

	return slot;
}
bool UIUC::Handbox::AddTempo(TObject* obj, int slot, int index)
{
	TString eMessage;
	slot = this->AllocateTempoSlot(slot);

	eMessage = "Too many entries in slot #" + TString::Itoa(slot, 10) + " compared to the number of input .. something wrong.. skip";
	if( UIUC::HandboxMsg::Warning(index >= (int) this->fTempo[slot].size(), __METHOD_NAME__, eMessage)) return false;

	eMessage = "Slot #" + TString::Itoa(slot, 10) + " entry #" + TString::Itoa(index, 10) + " is already used.. something wrong.. skip";
	if( UIUC::HandboxMsg::Warning(this->fTempo[slot][index] != NULL, __METHOD_NAME__, eMessage)) return false;

	fTempo[slot][index] = obj;
	return true;
}

bool UIUC::Handbox::AddTempo(vector<TObject*> l, int slot)         // Slot is not mentionned, every call is a new slot !
{
	slot = this->AllocateTempoSlot(slot);
	for(int i = 0; i < l.size(); i++)
		this->AddTempo(l[i], slot, i);

	return 1;
}

bool UIUC::Handbox::AddTempo(vector<TKey*> k, int slot)
{
	vector<TObject*> v(k.begin(), k.end());             // Cast vector<TKey*> into vector<TObject*>

	slot = this->AllocateTempoSlot(slot);
	return UIUC::Handbox::AddTempo(v, slot);
}

bool UIUC::Handbox::AddTempo(TFileReader &tempo, int slot)
{
	vector<TString> vTempo = tempo.GetFqfn();
	slot = this->AllocateTempoSlot(slot);

	for(int i = 0; i < vTempo.size(); i++)
		vTempo[i] = "TFileReader#" + vTempo[i];

	return AddTempo(vTempo, slot);
}

bool UIUC::Handbox::AddTempo(TFileReader *tempo, int slot)
{
	if(tempo == NULL) return false;
	return AddTempo(*tempo, slot);
}

bool UIUC::Handbox::AddTempo(vector<TString> vStr, int slot)
{
	vector<TObject*> vObjStr;
	slot = this->AllocateTempoSlot(slot);

	for(int i = 0; i < vStr.size(); i++)
		vObjStr.push_back(new TObjString(vStr[i]));

	return AddTempo(vObjStr, slot);
}

vector<vector<TObject*> > UIUC::Handbox::GetOutputBuffer(bool keep_nullptr)
{
	vector<vector<TObject*> > v = this->fOutput;
	if(keep_nullptr) return v;

	for(int i = 0; i < this->fOutput.size(); i++)
		v[i].erase(std::remove(begin(v[i]), end(v[i]), nullptr), end(v[i]));

	return v;
}

vector<TObject*> UIUC::Handbox::GetOutputBuffer(int slot, bool keep_nullptr)
{
	if(slot < 0 || slot >= (int) this->fOutput.size()) return {};

	vector<TObject*> v = this->fOutput[slot];
	if(keep_nullptr) return v;

	v.erase(std::remove(begin(v), end(v), nullptr),
	end(v));

	return v;
}

TObject* UIUC::Handbox::GetOutput(int slot, int index)
{
	if(index < 0) index = this->iOutput;

	if(slot < 0 || slot >= (int) this->fOutput.size()) return {};
	if(index < 0 || index >= (int) this->fOutput[slot].size()) return {};

	return this->fOutput[slot][index];
}

vector<vector<TObject*> > UIUC::Handbox::GetTempoBuffer(bool keep_nullptr)
{
	vector<vector<TObject*> > v = this->fTempo;
	if(keep_nullptr) return v;

	for(int i = 0; i < this->fTempo.size(); i++) {

		v[i].erase(std::remove(begin(v[i]), end(v[i]), nullptr),
		           end(v[i]));
	}

	return v;
}

vector<TObject*> UIUC::Handbox::GetTempoBuffer(int slot, bool keep_nullptr)
{
	if(slot < 0 || slot >= (int) this->fTempo.size()) return {};

	vector<TObject*> v = this->fTempo[slot];
	if(keep_nullptr) return v;

	v.erase(std::remove(begin(v), end(v), nullptr),
	        end(v));

	return v;
}

TObject* UIUC::Handbox::GetTempo(int slot, int index)
{
	if(index < 0) index = this->iOutput;

	if(slot < 0 || slot >= (int) this->fTempo.size()) return {};
	if(index < 0 || index >= (int) this->fTempo[slot].size()) return {};
	return this->fTempo[slot][index];
}

TString UIUC::Handbox::GetGlobalIdentifier(vector<TString> identifier)
{
		int N = identifier.size();
		if (N < 1) return "";

		TString globalIdentifier = identifier[0];
		for(int i = 1; i < N; i++)
			globalIdentifier += UIUC::TFileReader::kSeparator + identifier[i];

		return  globalIdentifier;
}

std::vector<TString> UIUC::Handbox::FirstIdentifier()
{
	std::vector<std::pair<TString, int>> vGlobalIdentifiers;
	for(int i = 0, N = this->fIdentifiers.size(); i < N; i++)
		vGlobalIdentifiers.push_back(std::make_pair(this->GetGlobalIdentifier(this->fIdentifiers[i]), i));

	std::sort(vGlobalIdentifiers.begin(),vGlobalIdentifiers.end(), UIUC::Handbox::sortIdentifiers);

	return this->fIdentifiers[vGlobalIdentifiers[0].second];
}

std::vector<TString> UIUC::Handbox::LastIdentifier()
{
	std::vector<std::pair<TString, int>> vGlobalIdentifiers;
	for(int i = 0, N = this->fIdentifiers.size(); i < N; i++)
		vGlobalIdentifiers.push_back(std::make_pair(this->GetGlobalIdentifier(this->fIdentifiers[i]), i));

	std::sort(vGlobalIdentifiers.begin(),vGlobalIdentifiers.end(), UIUC::Handbox::sortIdentifiers);

	return this->fIdentifiers[vGlobalIdentifiers[vGlobalIdentifiers.size()-1].second];
}

int UIUC::Handbox::GetOutputSize(int slot)
{
	if(slot < 0 || slot >= (int) this->fOutput.size()) return -1;

	int iFirst = 0;
	int iLast = this->fOutput[slot].size();

	int N = 0;
	for(int i = iFirst; i < iLast; i++) {

		if(this->fOutput[slot][i] == NULL) continue;
		N++;
	}

	return N;
}

int UIUC::Handbox::GetTempoSize(int slot)
{
	if(slot < 0 || slot >= (int) this->fTempo.size()) return -1;

	int iFirst = 0;
	int iLast = this->fTempo[slot].size();

	int N = 0;
	for(int i = iFirst; i < iLast; i++) {

		if(this->fTempo[slot][i] == NULL) continue;
		N++;
	}

	return N;
}

int UIUC::Handbox::GetInputSize()
{
	return this->fInput.size();
}

void UIUC::Handbox::Print(int slot)
{
	TString opt = this->RunOption;
	opt.ToLower();

	if (opt.Contains("v")) {

		// Loop over all objects
		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "------------------------------");
		for(int i = 0; i < GetN(); i++) {

			bool kWarning = false;

			// Print the input
			cout << UIUC::HandboxMsg::Tab() << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "* Input #" + TString::Itoa(i, 10) + ": ");
			if(fInput[i] != NULL) {

				if(fInput[i]->InheritsFrom("TKey")) {

					TKey *key = (TKey*) fInput[i];
					TString keypath = (TString) key->GetMotherDir()->GetPath() + "/" + key->GetName();
					keypath = UIUC::TFileReader::ApplyCorrection(keypath);

					cout << UIUC::HandboxMsg::MakeItShorter(TFileReader::Url(keypath));
					cout << UIUC::HandboxMsg::Endl();

					if(!this->fInput[i]->InheritsFrom("TDirectory")) {

						cout << UIUC::HandboxMsg::Tab();
						cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "  Object directory: " + (TString) gSystem->DirName(TFileReader::Obj(keypath)));
						cout << UIUC::HandboxMsg::Endl();
					}

				} else cout << UIUC::HandboxMsg::Endl();

				cout << UIUC::HandboxMsg::Tab();
				if(!this->fInput[i]->InheritsFrom("TDirectory")) cout <<  UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, TString("  ") + UIUC::HandboxMsg::MakeItShorter(UIUC::HandboxMsg::RedirectPrint(fInput[i])));
				else cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, TString("  ") + TString(fInput[i]->GetName()) + "; Number of entries inside: " + TString::Itoa(((TDirectory*)fInput[i])->GetList()->GetSize(), 10));
				cout << UIUC::HandboxMsg::Endl();

			} else {

				cout << "NULL" << UIUC::HandboxMsg::Endl();
				kWarning = true;
			}

			UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "");

			// Print all tempo files
			for(int j = 0; j < GetNumberOfTempoSlot(); j++) {

				if(slot != -1 && slot != j) continue;
				cout << UIUC::HandboxMsg::Tab() << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "  Tempo #" + TString::Itoa(j, 10) + "."  + TString::Itoa(i, 10) + ": ");

				if(fTempo[j][i] != NULL) {

					if(fTempo[j][i]->InheritsFrom("TKey")) {

						TKey *key = (TKey*) fTempo[j][i];
						TString keypath = (TString) key->GetMotherDir()->GetPath() + "/" + key->GetName();
						keypath = UIUC::TFileReader::ApplyCorrection(keypath);

						cout << UIUC::HandboxMsg::MakeItShorter(TFileReader::Url(keypath));
						cout << UIUC::HandboxMsg::Endl();

						if(!this->fTempo[j][i]->InheritsFrom("TDirectory")) {

							cout << UIUC::HandboxMsg::Tab();
							cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "  Object directory: " + (TString) gSystem->DirName(TFileReader::Obj(keypath)));
							cout << UIUC::HandboxMsg::Endl();
						}
					} else cout << UIUC::HandboxMsg::Endl();

					cout << UIUC::HandboxMsg::Tab();
					if(!this->fTempo[j][i]->InheritsFrom("TDirectory")) cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, TString("  ") +  UIUC::HandboxMsg::MakeItShorter(UIUC::HandboxMsg::RedirectPrint(fTempo[j][i])));
					else cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "  " + TString(fTempo[j][i]->GetName()) + "; Number of entries inside: " + TString::Itoa(((TDirectory*)fTempo[j][i])->GetList()->GetSize(),10));
					cout << UIUC::HandboxMsg::Endl();

				} else {

					cout << "NULL" << UIUC::HandboxMsg::Endl();
					//kWarning = true;
				}

				UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "");
			}

			for(int j = 0; j < GetNumberOfOutputSlot(); j++) {

				if(slot != -1 && slot != j) continue;
				cout << UIUC::HandboxMsg::Tab() << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "  Output #" + TString::Itoa(j, 10) + "."  + TString::Itoa(i, 10) + ": ");

				if(fOutput[j][i] != NULL) {

					if(this->fOutputPattern.EqualTo("")) cout << "N/A" << UIUC::HandboxMsg::Endl();
					else {
						TString url = UIUC::TFileReader::Url(UIUC::TFileReader::SubstituteIdentifier(this->fOutputPattern, this->fIdentifiers[i]));
						TString dirname = UIUC::TFileReader::Obj(this->fOutputPattern);
						TString keypath = UIUC::TFileReader::ApplyCorrection(url + ":/" + dirname + "/" + this->fOutput[j][i]->GetName());

						cout << UIUC::HandboxMsg::MakeItShorter(TFileReader::Url(keypath));
						cout << UIUC::HandboxMsg::Endl();

						if(!this->fOutput[j][i]->InheritsFrom("TDirectory")) {

							cout << UIUC::HandboxMsg::Tab();
							cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "  Object directory: " + (TString) gSystem->DirName(TFileReader::Obj(keypath)));
							cout << UIUC::HandboxMsg::Endl();
						}
					}

					cout << UIUC::HandboxMsg::Tab();
					if(!this->fOutput[j][i]->InheritsFrom("TDirectory")) cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, TString("  ") + UIUC::HandboxMsg::MakeItShorter(UIUC::HandboxMsg::RedirectPrint(fOutput[j][i])));
					else cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "  Directory name : " + TString(fOutput[j][i]->GetName()) + "; Number of entries inside: " + TString::Itoa(((TDirectory*)fOutput[j][i])->GetList()->GetSize(),10));
					cout << UIUC::HandboxMsg::Endl();

				} else {

					cout << "NULL" << UIUC::HandboxMsg::Endl();
				}

				if((unsigned int) j != fOutput.size()-1) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "");
			}

			TString eMessage = "Invalid couple of input/tempo objects provided.. skip";
			if( UIUC::HandboxMsg::Warning(kWarning, __METHOD_NAME__, eMessage) ) continue;

			UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "------------------------------");
		}

		return;
	}

	// Print all output
	for(int j = 0; j < GetNumberOfOutputSlot(); j++) {

		for(int i = 0; i < this->fOutput[j].size(); i++) {

			if(slot != -1 && slot != j) continue;
			if(fOutput[j][i] != NULL) {

				uintptr_t addr = reinterpret_cast<uintptr_t>(this->fOutput[j][i]);
				cout << UIUC::HandboxMsg::Tab();
				cout << UIUC::HandboxMsg::GetMessage(
					__METHOD_NAME__,
					"Output #" + TString::Itoa(j, 10) + "."  + TString::Itoa(i, 10) + " (at: \"0x" + TString::LLtoa(addr, 16) + ")\")")
				     << UIUC::HandboxMsg::Endl();

				cout << UIUC::HandboxMsg::Tab();
				if(!this->fOutput[j][i]->InheritsFrom("TDirectory")) cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, TString("* ") + UIUC::HandboxMsg::MakeItShorter(UIUC::HandboxMsg::RedirectPrint(fOutput[j][i])));
				else cout << UIUC::HandboxMsg::GetMessage(__METHOD_NAME__, "  Directory : " + TString(fOutput[j][i]->GetName()) + "; Number of entries inside: " + TString::Itoa(((TDirectory*)fOutput[j][i])->GetList()->GetSize(),10));
				cout << UIUC::HandboxMsg::Endl();
			}
		}
	}
}

void UIUC::Handbox::Save(int index)
{
	TString eMessage = "Nothing to save..";
	if( UIUC::HandboxMsg::Warning(this->fOutput.size() == 0, __METHOD_NAME__, eMessage)) return;

	eMessage = "Manual saving [Handbox::GetOutputFile()] called.. skip saving.";
	if( UIUC::HandboxMsg::Warning(this->outputFileCall, __METHOD_NAME__, eMessage)) return;

	eMessage = "No savelist found in the handbox.. cannot save outgoing objects";
	if((UIUC::HandboxMsg::IsDebugEnabled()) && this->fOutputPattern.EqualTo("")) cout << "Target: No output specified.. outgoing object(s) cannot be saved" << endl;
	else if( UIUC::HandboxMsg::Warning(this->fOutputPattern.EqualTo(""), __METHOD_NAME__, eMessage)) return;

	UIUC::HandboxMsg::ClearLine();
	if(index != -1) UIUC::HandboxMsg::SetSingleCarriageReturn(); 

	if(index != -1) {

		TString fOutputFullname  = UIUC::TFileReader::SubstituteIdentifier(this->fOutputPattern, this->fIdentifiers[index]);	
		UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "Saving outgoing object(s) into \""+fOutputFullname+"\".. ");
	}

    int I = this->GetNumberOfOutputSlot();
	for(int i = 0; i < I; i++) {

		if(UIUC::TFileReader::IsTextFile(this->fOutputPattern)) {

			TString fOutputFullname0 = UIUC::TFileReader::BaseName(this->fOutputPattern, ".txt") + "." + UIUC::NumberingFormat(i,this->fOutput.size()) + ".XXX.txt";
			UIUC::HandboxMsg::PrintProgressBar("Saving outgoing object(s) into \""+fOutputFullname0+"\".. ", i+1, I);
		}

		for(int j = 0; j < this->fOutput[i].size(); j++) {

			// In case quiet mode is enabled.. it is at least showing the progress bar..
			// NB: I removed jLast+1 from the display of the progress bar.. because jLast+1 refers to the finalization step.. this might be misleading when some N files and it displays N+1 files..
			if(index != -1 && index != j) continue;

			eMessage = "Nothing to save..";
			if( UIUC::HandboxMsg::Warning(this->fOutput[i].size() == 0, __METHOD_NAME__, eMessage)) continue;

			eMessage = "Output object #" + TString::Itoa(i, 10) + "."  + TString::Itoa(j, 10) + " is NULL.. skip";
			if(this->fOutput[i][j] == NULL) UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);
			if(this->fOutput[i][j] == NULL) continue;

			TString fOutputFullname = "";
			if(j == this->GetN()) { // N+1 = finalization step
			
				if(fOutputFinalizeStr.EqualTo("")) fOutputFullname = this->fOutputPattern;
				else fOutputFullname = fOutputFinalizeStr; 

			} else if(UIUC::TFileReader::HasPattern(this->fOutputPattern)) {

				fOutputFullname  = UIUC::TFileReader::SubstituteIdentifier(this->fOutputPattern, this->fIdentifiers[j]);	
			
			} else {

				fOutputFullname  = this->fOutputPattern;
			}

			fOutputFullname += !fOutputFullname.Contains(":") ? ":/" : "";
			fOutputFullname += "/" + this->fOutputDir[i][j];

			TString name0 = this->fOutput[i][j]->GetName();
					name0.ReplaceAll(" ", "_");

			if(this->fOutput[i][j]->InheritsFrom("TNamed")) ((TNamed*) this->fOutput[i][j])->SetName(name0);

			TString keypath = TString(fOutputFullname + "/" + this->fOutput[i][j]->GetName());
			keypath = UIUC::TFileReader::ApplyCorrection(keypath);

			if(UIUC::TFileReader::IsTextFile(fOutputFullname)) {

				TString fOutputFullname0 = UIUC::TFileReader::BaseName(fOutputFullname, ".txt") + "." + UIUC::NumberingFormat(i,this->fOutput.size()) +"."+ UIUC::NumberingFormat(j,this->fOutput[i].size()) + ".txt";
				if(this->fOutput[i][j]->InheritsFrom("TH1")) {

					UIUC::TFactory::SaveIntoASCII(fOutputFullname0, ios::out | ios::trunc, (TH1*) this->fOutput[i][j]);

				} else if(this->fOutput[i][j]->InheritsFrom("TGraph")) {

					UIUC::TFactory::SaveIntoASCII(fOutputFullname0, ios::out | ios::trunc, (TGraph*)this->fOutput[i][j]);

				} else if(this->fOutput[i][j]->InheritsFrom("TTree")) {

					UIUC::HandboxMsg::PrintDebug(1,__METHOD_NAME__,"Writing TTree into a ASCII file require some special variable.. Please do it inline.");

				} else {
					UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Cannot save %s, %s is a (not a TH1, TGraph nor TTree..)", fOutputFullname0.Data(), this->fOutput[i][j]->IsA()->GetName(), ((TH1*) this->fOutput[i][j])->GetName());
					continue;
				}
				
				eMessage = "Outgoing object #" + TString::Itoa(i, 10) + "."  + TString::Itoa(j, 10) + " saved here \"" + fOutputFullname0 + "\"";
				UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, eMessage);

			} else if( UIUC::TFileReader::Write(this->fOutput[i][j], UIUC::TFileReader::ObjDirName(keypath), "UPDATE") ) {

				eMessage = "Outgoing object #" + TString::Itoa(i, 10) + "."  + TString::Itoa(j, 10) + " saved here \"" + keypath + "\"";
				UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, eMessage);
					
			} else {

				eMessage = "Failed to save the object #" + TString::Itoa(i, 10) + "."  + TString::Itoa(j, 10) + " here \"" + keypath + "\"";
				UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);
			}
		}
	}

	return;
}

bool UIUC::Handbox::SerialProcessing() {

	return !this->fOutputPattern.EqualTo("") && UIUC::TFileReader::HasPattern(this->fOutputPattern);
}

TString UIUC::Handbox::GetOutputPath(int i)
{
	if(!UIUC::TFileReader::HasPattern(this->fOutputPattern)) return this->fOutputPattern;
	return UIUC::TFileReader::SubstituteIdentifier(this->fOutputPattern, this->fIdentifiers[i]);
}

bool UIUC::Handbox::AddOutput(TObject* obj, TString obj_dir, int slot)
{
	std::lock_guard<std::mutex> guard(workerMutex);

	TString eMessage;

	if(obj != NULL && obj->InheritsFrom("TNamed")) {

		TString str = ((TNamed*)obj)->GetName();
		if(str.Contains("%") && !str.Contains("_%_")) {

			UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Object contains a \\% character without underscore around.. cannot add to the output object. Please change name..");
			return 0;
		}
	}

	if(obj != NULL && obj->InheritsFrom("TList")) {

		Bool_t b = true;

		TList *list = (TList*) obj;
		eMessage = "TList detected with " + TString::Itoa(list->GetSize(),10) + " element(s)";
		UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);

		if(UIUC::HandboxMsg::IsDebugEnabled()) {
			UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);
			list->ls();
			UIUC::HandboxMsg::PrintTTY(__METHOD_NAME__);
		}

		TIter Next(list);
		TObject *item = NULL;
		while ((item = (TKey*) Next())) {

			if(item->InheritsFrom("TList")) {

				UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "TList found in the outgoing list \""+TString(list->GetName())+"\".. skip");
				continue;
			}

			if(item->IsFolder()) {

				UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "TDirectory found in the outgoing list \""+TString(list->GetName())+"\".. skip");
				continue;
			}

			b &= AddOutput(item, obj_dir, slot);
		}

		return b;
	}

	if(slot >= 0) this->iOutputSlot = slot;
	while((int) this->fOutput.size() < this->iOutputSlot+1) {

		eMessage = "New slot #" + TString::Itoa(this->iOutputSlot, 10) + " created in output ";
		UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);

		ResizeOutput(this->iOutputSlot);
	}

	// Add one entry in case AddOutput is called at the finalization step..
	if(this->iOutput == this->GetN()) ResizeOutput();

	eMessage = "Slot #" + TString::Itoa(this->iOutputSlot, 10) + ", entry #" + TString::Itoa(this->iOutput, 10) + " is already used.. something wrong.. skip";
	if( UIUC::HandboxMsg::Warning(this->fOutput[this->iOutputSlot][this->iOutput] != NULL, __METHOD_NAME__, eMessage)) return false;

	uintptr_t addr = reinterpret_cast<uintptr_t>(obj);
	if( obj != NULL) {

		eMessage = "Object \"0x" + TString::LLtoa(addr, 16) + "\", named \""+TString(obj->GetName())+"\", already found in the output ..\nPlease clone if you want to add it.. Potential memory leak";
		if( UIUC::HandboxMsg::Warning(UIUC::Handbox::FindOutput(obj), __METHOD_NAME__, eMessage) ) return false;

		if(obj->InheritsFrom("TDirectory")) {

			TDirectory *dir = (TDirectory*) obj;
			eMessage = "Object \"0x" + TString::LLtoa(addr, 16) + "\" is the default root directory \"root:/\".. skip (please create a subdirectory and add it)";
			if( UIUC::HandboxMsg::Warning(TString(dir->GetPath()).EqualTo("root:/"), __METHOD_NAME__, eMessage)) return false;
			eMessage = "Object \"0x" + TString::LLtoa(addr, 16) + "\" is the default root directory \"root:/..\".. skip (please create a subdirectory and add it)";
			if( UIUC::HandboxMsg::Warning(TString(dir->GetPath()).EqualTo("root:/.."), __METHOD_NAME__, eMessage)) return false;
		}
	}

	eMessage = "Object \"0x" + TString::LLtoa(addr, 16) + "\" has been added to slot #" + TString::Itoa(this->iOutputSlot, 10) + ", entry #" + TString::Itoa(this->iOutput, 10);
	if(obj == NULL) UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, eMessage);
	else UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);

	this->fOutput[this->iOutputSlot][this->iOutput] = obj;
	this->fOutputDir[this->iOutputSlot][this->iOutput] = obj_dir;

	// Handbox is deleting the object only if..
	// - it is not in the input
	// - nor the tempo
	// - nor linked to a TFile already open
	// NB: THERE IS AN ISSUE WHEN USING TOBJECT COMING FROM AN OPEN TFILE... TFILE is deleting itself the object........ TO BE IMPROVED
	this->fOutputDeleteSafely[this->iOutputSlot][this->iOutput] =
	(
		!this->FindInput(obj) &&
		!this->FindTempo(obj) &&
		!(obj != NULL && obj->IsA()->InheritsFrom("TKey") && ((TKey*) obj)->GetFile() == NULL)
	);

	if(slot == -1) this->iOutputSlot++;
	return true;
}

bool UIUC::Handbox::RemoveOutput(TObject* obj, int slot)
{
	TString eMessage;
	if(obj == NULL) return false;
	if(slot >= (int) this->fOutput.size()) {

		eMessage = "Cannot proceed, slot #" + TString::Itoa(this->iOutputSlot, 10) + " doesn't exist";
		UIUC::HandboxMsg::PrintError(__METHOD_NAME__, eMessage);
	}

	int slot0 = (slot < 0) ? 0 : slot;
	int slotN = (slot < 0) ? this->fOutput.size() : slot+1;

	bool bFound = false;
	while(slot0 < slotN) {

		unsigned int index = find(this->fOutput[slot0].begin(), this->fOutput[slot0].end(), obj) - this->fOutput[slot0].begin();
		if( index == this->fOutput[slot0].size() ) {
			slot0++;
			continue;
		}
		this->fOutput[slot0][index] = NULL;
		this->fOutputDir[slot0][index] = "";
		this->fOutputDeleteSafely[slot0][index] = 0;

		eMessage = "Object \""+TString(obj->GetName())+"\" found and removed from the output ..";
		UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, eMessage);

		bFound = true;
		slot0++;
	}

	if(!bFound) {

		uintptr_t addr = reinterpret_cast<uintptr_t>(obj);
		eMessage = "Object \"0x" + TString::LLtoa(addr, 16) + "\", named \""+TString(obj->GetName())+"\", not found in the output ..";
		UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, eMessage);
		return false;
	}

	return true;
}