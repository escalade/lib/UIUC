/**
 * @Author: Marco Meyer <meyerma>
 * @Date:   2019-03-27T01:29:31-05:00
 * @Email:  marco.meyer@cern.ch
 * @Last modified by:   meyerma
 * @Last modified time: 2019-03-30T15:41:05-05:00
 */



/**
 * *********************************************
 *
 * \file TVarexp.cc
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \brief Source code of the TVarexp class
 *
 * ********************************************* */

#include <time.h>
#include <UIUC/TVarexp.h>
ClassImp(UIUC::TVarexp)
#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

using namespace UIUC;
using namespace std;

const bool UIUC::TVarexp::kWithBinning = true;
const bool UIUC::TVarexp::kWithoutBinning = !kWithBinning;
const bool UIUC::TVarexp::kSwap = true;

TString UIUC::TVarexp::StripBranchName(TString str) {

        TPRegexp r1("([a-zA-Z0-9_]*\\.)");
        while(r1.Substitute(str, "")) {};

        str.ReplaceAll("()", "");
        return str;
}

TString UIUC::TVarexp::GetTFormulaHistogramName(TH1 *h0, TString suffix, bool bBinning)
{
        if(h0 == NULL) return "";
        if(bBinning != UIUC::TVarexp::kWithBinning) h0->GetName();

        TString hpar = "";
        TString hname = h0->GetName();

        if(h0->InheritsFrom("TH3")) {

                hpar = Form("(%d,%f,%f, %d,%f,%f, %d,%f,%f)", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                            h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                            h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax() );

        } else if(h0->InheritsFrom("TH2")) {

                hpar = Form("(%d,%f,%f, %d,%f,%f)", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                            h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax() );

        } else if(h0->InheritsFrom("TH1")) {

                hpar = Form("(%d,%f,%f)", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax() );
        }

        return TString(hname + suffix + hpar);
}

TString UIUC::TVarexp::GetHistogramName(TString hname, TString suffix, bool bSwap, bool bNoBinning)
{
        if(hname.EqualTo("")) hname = UIUC::TFactory::GetRandomName("h");
        hname = TVarexp(hname).RemoveBinning();
        hname.ReplaceAll("/", "_%_");
        hname.ReplaceAll("(", "<");
        hname.ReplaceAll(")", ">");

        unsigned int count = 0;
        TString hpar = "";

        vector<TString> vArray = this->Tokenize();
        for(int i = 0, N = vArray.size(); i < N; i++) {

                if(UIUC::TVarexp::HasVariableBinningInformation(vArray[i])) {

                        if(UIUC::HandboxMsg::Error(bNoBinning != UIUC::TVarexp::kWithBinning,
                                                   __METHOD_NAME__, "Variable binning is not supported here.. name without binning returned..")) return hname;

                } else {

                        TString varname_str = vArray[i];
                        TString nbins_str   = vArray[i];
                        TString binmin_str  = vArray[i];
                        TString binmax_str  = vArray[i];

                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$1");
                        r.Substitute(nbins_str, "$2");
                        r.Substitute(binmin_str, "$3");
                        r.Substitute(binmax_str, "$4");

                        if(varname_str != nbins_str && varname_str != binmin_str && varname_str != binmax_str) {

                                TString hpar0 = Form("%d,%f,%f", (int) InterpretFormula(nbins_str.Data()), InterpretFormula(binmin_str.Data()), InterpretFormula(binmax_str.Data()));
                                if(!hpar.EqualTo("")) {

                                        if(bSwap) hpar = hpar + ",";
                                        else hpar = "," + hpar;
                                }

                                if(bSwap) hpar = hpar + hpar0;
                                else hpar = hpar0 + hpar;
                                count++;
                        }
                }
        }

        UIUC::HandboxMsg::Error(vArray.size() != count && count != 0, __METHOD_NAME__, "You have to properly specify all bin ranges..");
        UIUC::HandboxMsg::Error(vArray.size() != count && count != 0, __METHOD_NAME__, "If you want your histogram within your expected range");

        if(!hpar.EqualTo("")) hpar = "(" + hpar + ")";
        if(!bNoBinning) hpar = "";

        return TString(hname + suffix + hpar);
}

TString UIUC::TVarexp::RemoveBinning()
{
        TString hpar = "";

        vector<TString> vArray = this->Tokenize(':', kWithoutBinning);
        for(int i = 0, N = vArray.size(); i < N; i++) {

                if(i != 0) hpar += ":";
                hpar += vArray[i];
        }

        return hpar;
}

int UIUC::TVarexp::GetN()
{
        int N = 0;
        TPRegexp regex("\\[[0-9]*\\]\\.");
        Ssiz_t length;

        int index = 0;
        while((index = this->varexp.Index(regex, &length, index)) != -1) {

                index += length;
                N++;
        }

        return N;
}

int UIUC::TVarexp::GetNdimensions()
{
        if(this->Get().EqualTo("")) return 0;

        vector<TString> vArray = this->Tokenize();
        return vArray.size();
}

bool UIUC::TVarexp::HasParameters(TString varexp0, TString pattern)
{
        TPRegexp regex(pattern);
        return varexp0.Contains(regex);
}

int UIUC::TVarexp::GetNparameters(TString varexp0, TString pattern)
{
        varexp0 = TVarexp(varexp0).Get(); // TVarexp::Get() removes space..
        if(varexp0.EqualTo("")) return 0;

        int N = 0;
        TPRegexp regex(pattern);
        Ssiz_t length;

        int index = 0;
        while((index = varexp0.Index(regex, &length, index)) != -1) {

                int i = 4;
                if(N > 10) i++;
                TString catch0 = varexp0(index, i);
                varexp0.ReplaceAll(catch0, "");

                N++;
        }

        return N;
}

bool UIUC::TVarexp::HasBinningInformation(TString varexp0)
{
        if(UIUC::TVarexp::HasConstantBinningInformation(varexp0)) return 1;
        if(UIUC::TVarexp::HasVariableBinningInformation(varexp0)) return 1;

        return 0;
}

bool UIUC::TVarexp::HasConstantBinningInformation(TString varexp0)
{
        TPRegexp regex("\\[[^,]*,[^,]*,[^,]*\\]");
        return varexp0.Contains(regex);
}

bool UIUC::TVarexp::HasVariableBinningInformation(TString varexp0)
{
        TPRegexp regex("\\{.*\\}");
        return varexp0.Contains(regex);
}

vector<vector<double> > UIUC::TVarexp::GetConstantBinning(TString varexp0)
{
        vector<vector<double> > vv;

        vector<TString> vArray = TVarexp(varexp0).Tokenize();
        for(int i = 0, N = vArray.size(); i < N; i++) {

                vector<TString> v = {
                        vArray[i],
                        vArray[i],
                        vArray[i],
                        vArray[i]
                };

                TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                r.Substitute(v[0], "$1");
                r.Substitute(v[1], "$2");
                r.Substitute(v[2], "$3");
                r.Substitute(v[3], "$4");

                if(v[0] != v[1] && v[0] != v[2] && v[0] != v[3]) {

                        int nbins = UIUC::InterpretFormula(v[1]);
                        double xmin = UIUC::InterpretFormula(v[2]);
                        double xmax = UIUC::InterpretFormula(v[3]);
                        double delta = (xmax-xmin)/nbins;

                        vector<double> v0;
                        for(int i = 0; i < nbins+1; i++)
                                v0.push_back(xmin+i*delta);

                        vv.push_back(v0);
                }

                else vv.push_back({});
        }

        return vv;
}

vector<vector<double> > UIUC::TVarexp::GetVariableBinning(TString varexp0)
{
        vector<vector<double> > vv;
        vector<TString> vArray = TVarexp(varexp0).Tokenize(':', UIUC::TVarexp::kWithBinning);

        for(int i = 0, N = vArray.size(); i < N; i++) {

                TString varexp0;

                TString varname_str = vArray[i];
                int brackleft = varname_str.First('{');
                if(brackleft != -1) {

                        int brackright = varname_str.Last('}');
                        if(brackright != -1) {

                                int length = brackright-brackleft-1;
                                if(length >= 0) varexp0 = varname_str(brackleft+1,length);
                        }
                }

                vector<double> v0;
                vector<TString> v = TVarexp(varexp0).Tokenize(',');
                for(int i = 0, N = v.size(); i < N; i++)
                        v0.push_back(UIUC::InterpretFormula(v[i]));

                vv.push_back(v0);
        }

        return vv;
}

TString UIUC::TVarexp::GetVariableBinningStr(bool bBrackets)
{
        unsigned int count = 0;
        TString hpar = "";

        vector<vector<double> > vArray = this->GetVariableBinning();
        for(int i = 0, N = vArray.size(); i < N; i++) {

                if(i > 0) hpar += ":";

                if(bBrackets) hpar += "{";

                for(int j = 0, J = vArray[i].size(); j < J; j++) {

                        if(j > 0) hpar += ",";
                        hpar += Form("%f", vArray[i][j]);
                }

                if(bBrackets) hpar += "}";
        }

        UIUC::HandboxMsg::Warning(
                vArray.size() != count && count != 0, __METHOD_NAME__,
                "You have to properly specify all bin ranges.."
                );

        UIUC::HandboxMsg::Warning(
                vArray.size() != count && count != 0, __METHOD_NAME__,
                "If you want your histogram within your expected range"
                );

        return hpar;
}

TString UIUC::TVarexp::GetConstantBinningStr(bool bBrackets)
{
        unsigned int count = 0;
        TString hpar = "";

        vector<TString> vArray = this->Tokenize();
        for(int i = 0, N = vArray.size(); i < N; i++) {

                if(HasVariableBinningInformation(vArray[i]))

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "This function handles only constant bins..");
                else {

                        //
                        // Constant binning case
                        //
                        TString varname_str = vArray[i];
                        TString nbins_str   = vArray[i];
                        TString binmin_str  = vArray[i];
                        TString binmax_str  = vArray[i];

                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$1");
                        r.Substitute(nbins_str, "$2");
                        r.Substitute(binmin_str, "$3");
                        r.Substitute(binmax_str, "$4");

                        if(varname_str != nbins_str && varname_str != binmin_str && varname_str != binmax_str) {

                                if(!hpar.EqualTo("")) hpar = "," + hpar;
                                hpar = Form("%d,%f,%f", (int) InterpretFormula(nbins_str.Data()), InterpretFormula(binmin_str.Data()), InterpretFormula(binmax_str.Data())) + hpar;
                                count++;
                        }
                }
        }

        UIUC::HandboxMsg::Warning(
                vArray.size() != count && count != 0, __METHOD_NAME__,
                "You have to properly specify all bin ranges.."
                );

        UIUC::HandboxMsg::Warning(
                vArray.size() != count && count != 0, __METHOD_NAME__,
                "If you want your histogram within your expected range"
                );

        if(bBrackets) return "[" + hpar + "]";
        return hpar;
}

TString UIUC::TVarexp::Expand(vector<TTree*> vTree, bool bBinning)
{
        // Replace encoded parameters..
        TString varexp_expanded = "";
        //if(UIUC::HandboxMsg::Error(!vTree.size() || vTree[0] == NULL, __METHOD_NAME__, "First tree of the list cannot be NULL")) gSystem->Abort();

        if(!vTree.size()) return varexp;
        if(!vTree[0]) return varexp;

        if(!vTree[0]->InheritsFrom("TTree")) {
                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Can only expand tree objects.. (index #0 is not..)");
                return varexp;
        }

        vector<TString> vArray = this->Tokenize(':', bBinning);
        for(int i = 0, N = vArray.size(); i < N; i++) {

                TString varexp0 = vArray[i];

                // Replace default branchname from tree[0] if not bracket found
                if(vTree.size() && vTree[0] != NULL) {

                        if(!vTree[0]->InheritsFrom("TTree")) {
                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Can only expand tree objects.. (index #%d is not..)", i);
                                return "";
                        }

                        TList *l = (TList*) vTree[0]->GetListOfBranches();
                        for(int j = 0, N = l->GetSize(); j < N; j++) {

                                if(l->At(j) == NULL) break;
                                TString branchname = l->At(j)->GetName();
                                if(branchname.EndsWith(".") && varexp0.BeginsWith(branchname)) varexp0 = "[0]." + varexp0;
                                else if(varexp0.EqualTo(branchname)) varexp0 = "[0]." + varexp0;
                        }
                }

                // Replace bracket by treename..
                for(int j = 0, J = vTree.size(); j < J; j++) {

                        if(vTree[j] == NULL) continue;
                        varexp0.ReplaceAll(Form("[%d].",j), (TString) vTree[j]->GetName() + ".");
                }

                if(i==0) varexp_expanded = varexp0;
                else varexp_expanded += ":" + varexp0;
        }

        // Check if there is still an encoded parameter
        if(HasParameters((varexp_expanded))) {

                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, Form("The expression \"%s\" still have encoded parameters..", varexp_expanded.Data()));
                UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, Form("Only %d were taken into account.. Something might be wrong there..", (int) vTree.size()));
                gSystem->Abort();
        }

        //Look for lonely branches and put the first treename in front of it.
        if(vTree.size() && vTree[0]) {

                TList *l = (TList*) vTree[0]->GetListOfBranches();
                for(int i = 0, N = l->GetSize(); i < N; i++) {

                        if(l->At(i) == NULL) break;
                        TString branchname = l->At(i)->GetName();
                        Ssiz_t length;

                        int index = 0;
                        while((index = varexp_expanded.Index(branchname, &length, (Ssiz_t) index)) != -1) {

                                int index0 = index;
                                TString before = (index > 0) ? (TString) varexp_expanded(index-1,1) : "";
                                TString after = (TString) varexp_expanded(index+length,1);

                                TString word = (TString) varexp_expanded(index,varexp_expanded.Length());

                                index += branchname.Length();

                                //cout << branchname << " " << word << endl;
                                //cout << "before : \""<<before << "\"" << endl;
                                //cout << "after : \""<<after << "\"" << endl;
                                if(before.EqualTo(".")) continue;

                                TPRegexp r1("[a-zA-Z0-9]");
                                if(before.Contains(r1)) continue;
                                if(!branchname.EndsWith(".") && after.Contains(r1)) continue;

                                TString replacement = (TString) vTree[0]->GetName() + ".";
                                varexp_expanded.Replace(index0, 0, replacement);
                                index += replacement.Length();

                                //cout << varexp_expanded << endl << endl;
                        }

                        // In case we are looking at the regular variable without operations..
                        //        if(varexp_expanded.BeginsWith("^" + branchname + "[^a-zA-Z0-9]")) varexp_expanded = (TString) vTree[0]->GetName() + "." + varexp_expanded;
                }
        }

        UIUC::HandboxMsg::PrintDebug(30, __METHOD_NAME__, "[Before] : \"" + this->Get() + "\"");
        UIUC::HandboxMsg::PrintDebug(30, __METHOD_NAME__, "[After]  : \"" + varexp_expanded + "\"");
        return varexp_expanded;
}

TString UIUC::TVarexp::GetTFormulaHistogramBinning(TH1 *h0, int axis)
{
        if(h0 == NULL) return "";

        TString hname = h0->GetName();
        TString hpar = "";

        if(h0->InheritsFrom("TH3")) {

                if (axis == 1) hpar = Form("%d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax());
                else if (axis == 2) hpar = Form("%d,%f,%f", h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax());
                else if (axis == 3) hpar = Form("%d,%f,%f", h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax());
                else {

                        hpar = Form("%d,%f,%f, %d,%f,%f, %d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax(),
                                    h0->GetNbinsZ(), h0->GetZaxis()->GetXmin(), h0->GetZaxis()->GetXmax() );
                }

        } else if(h0->InheritsFrom("TH2")) {

                if (axis == 1) hpar = Form("%d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax());
                else if (axis == 2) hpar = Form("%d,%f,%f", h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax());
                else {

                        hpar = Form("%d,%f,%f, %d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax(),
                                    h0->GetNbinsY(), h0->GetYaxis()->GetXmin(), h0->GetYaxis()->GetXmax() );
                }

        } else if(h0->InheritsFrom("TH1")) hpar = Form("%d,%f,%f", h0->GetNbinsX(), h0->GetXaxis()->GetXmin(), h0->GetXaxis()->GetXmax() );

        return hpar;
}


TString UIUC::TVarexp::GetTFormulaHistogramBinning(TH1 *h0, TString axis) {

        if(axis.EqualTo("x")) return GetTFormulaHistogramBinning(h0, 1);
        else if(axis.EqualTo("y")) return GetTFormulaHistogramBinning(h0, 2);
        else if(axis.EqualTo("z")) return GetTFormulaHistogramBinning(h0, 3);
        else return GetTFormulaHistogramBinning(h0);
}

// DO NOT CONFUSE WITH BINNING INFORMATION FROM TVAREXP.. TVAREXP IS [x,y,z] while Histogram are (x,y,z)
TString UIUC::TVarexp::RemoveTFormulaHistogramBinning(TString hname)
{
        TPRegexp r3("(.*)\\(([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*)\\)");
        if(r3.Substitute(hname, "$1")) return hname;

        TPRegexp r2("(.*)\\(([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),([^,]*)\\)");
        if(r2.Substitute(hname, "$1")) return hname;

        TPRegexp r1("(.*)\\(([^,]*),([^,]*),([^,]*)\\)");
        if(r1.Substitute(hname, "$1")) return hname;

        return hname;
}

bool UIUC::TVarexp::HasTFormulaBinningInformation(TString hname)
{
        bool b = false;

        //1D histogram
        TPRegexp regex1("\\([^,]*,[^,]*,[^,]*\\)");
        b = hname.Contains(regex1);
        if(b) return 1;

        //2D histogram
        TPRegexp regex2("\\([^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*\\)");
        b = hname.Contains(regex2);
        if(b) return 1;

        //3D histogram;
        TPRegexp regex3("\\([^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*,[^,]*\\)");
        b = hname.Contains(regex3);
        if(b) return 1;

        return 0;
}

TH1* UIUC::TVarexp::GetHistogramStruct(TString hname, TString htitle)
{
        if(hname.EqualTo("")) hname = UIUC::TFactory::GetRandomName("h");
        hname.ReplaceAll(":", "_");

        vector<TString> vArray = this->Tokenize(':');
        int ndims = vArray.size();

        if(UIUC::HandboxMsg::Error(ndims > 3,__METHOD_NAME__, "Too many dimensions..")) return NULL;

        vector<vector<double> > vv;
        for(int i = 0; i < ndims; i++) {

                TString varexp = vArray[i];

                vector<vector<double> > v;
                if(UIUC::TVarexp::HasBinningInformation(varexp)) {
                        v = UIUC::TVarexp::GetBinning(varexp);
                } else {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Binning information unknown.. abort");
                        return NULL;
                }

                if(UIUC::HandboxMsg::Error(
                           v.size() != 1, __METHOD_NAME__,
                           "Unexpected error.. cannot compute histogram.. abort")) return NULL;

                vv.push_back(v[0]);
        }

        TH1* h0 = NULL;
        if(ndims == 1) h0 = new TH1D(hname, htitle, vv[0].size()-1, &(vv[0][0]));
        else if(ndims == 2) h0 = new TH2D(hname, htitle, vv[0].size()-1, &(vv[0][0]), vv[1].size()-1, &(vv[1][0]));
        else if(ndims == 3) h0 = new TH3D(hname, htitle, vv[0].size()-1, &(vv[0][0]), vv[1].size()-1, &(vv[1][0]), vv[2].size()-1, &(vv[2][0]));

        if(h0) h0->SetDirectory(gDirectory);
        return h0;
}


TFormula* UIUC::TVarexp::GetFormula(TString name)
{
        if(name.EqualTo("")) name = UIUC::TFactory::GetRandomName("formula");
        name.ReplaceAll(":", "_");

        return new TFormula(name, this->varexp);
}

std::vector<double> UIUC::TVarexp::Eval(TFormula *formula, std::vector<double> params, std::vector<double> sigmas, double x, double y, double z, double t)
{
        int nParams = TMath::Max(params.size(), sigmas.size());
        params.resize(nParams, NAN);
        sigmas.resize(nParams, 0);
       
        if(formula == NULL) {

                params.push_back(NAN);
                return params;
        }
 
        for(int i = 0, N = params.size(); i < N; i++) {
        
                if(!UIUC::EpsilonEqualTo(sigmas[i], 0))
                        params[i] = gRandom->Gaus(params[i], sigmas[i]);
        }

        formula->SetParameters(&params[0]);

        double value = 0;
        if(!UIUC::IsNaN(t)) value = formula->Eval(x,y,z,t);
        if(!UIUC::IsNaN(z)) value = formula->Eval(x,y,z);
        if(!UIUC::IsNaN(y)) value = formula->Eval(x,y);
        if(!UIUC::IsNaN(x)) value = formula->Eval(x);

        params.push_back(value);
        return params;
}

TGraph* UIUC::TVarexp::GetGraph(TString name, vector<TGraph*> vGraph, TString varexp0_errl, TString varexp0_errh)
{
        //
        // Prepare the outgoing histogram...
        //
        if(name.EqualTo("")) name = UIUC::TFactory::GetRandomName("g");
        if(vGraph.size() == 0 || vGraph.size() == 0) return NULL;

        TGraph *g0 = (TGraph*) vGraph[0]->Clone(name);
        if(varexp0_errh.EqualTo("")) varexp0_errh = varexp0_errl;

        //
        // Check structure of other histograms
        //
        TString e2 = "[y0.e]*[y0.e]";
        for(int i = 1; i < (int) vGraph.size(); i++) {

                e2 += Form(" + [y%d.e]*[y%d.e]", i, i);
                TString eMessage = Form("Structure of histogram #%d is different than #0", i+1);
                if(UIUC::HandboxMsg::Warning(vGraph[i] != NULL && !UIUC::TFactory::CompareStruct(g0, vGraph[i]), __METHOD_NAME__, eMessage)) return NULL;
        }

        //
        // Check ci,ei indexes and prepare fContent..
        //
        TString varexp0 = TVarexp(this->varexp).Get();
        if(varexp0.EqualTo("")) return NULL;

        // Check number of ci, ei parameters
        int xi, yi, ni;
        ni = this->GetNparameters(varexp0, "\\[n[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(ni > (int) vGraph.size(), __METHOD_NAME__, "More n-like parameters than histograms provided for main formula..")) return NULL;
        xi = this->GetNparameters(varexp0, "\\[x[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(xi > (int) vGraph.size(), __METHOD_NAME__, "More x-like parameters than histograms provided for main formula..")) return NULL;
        yi = this->GetNparameters(varexp0, "\\[y[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(yi > (int) vGraph.size(), __METHOD_NAME__, "More y-like parameters than histograms provided for main formula..")) return NULL;

        // Check ci,ei indexes and prepare fContentErr..
        ni = this->GetNparameters(varexp0_errl, "\\[n[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(ni > (int) vGraph.size(), __METHOD_NAME__, "More n-like parameters than histograms provided for main formula..")) return NULL;
        xi = this->GetNparameters(varexp0_errl, "\\[x[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(xi > (int) vGraph.size(), __METHOD_NAME__, "More x-like parameters than histograms provided for error formula..")) return NULL;
        yi = this->GetNparameters(varexp0_errl, "\\[y[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(yi > (int) vGraph.size(), __METHOD_NAME__, "More y-like parameters than histograms provided for error formula..")) return NULL;

        ni = this->GetNparameters(varexp0_errh, "\\[n[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(ni > (int) vGraph.size(), __METHOD_NAME__, "More n-like parameters than histograms provided for main formula..")) return NULL;
        xi = this->GetNparameters(varexp0_errh, "\\[x[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(xi > (int) vGraph.size(), __METHOD_NAME__, "More x-like parameters than histograms provided for error formula..")) return NULL;
        yi = this->GetNparameters(varexp0_errh, "\\[y[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(yi > (int) vGraph.size(), __METHOD_NAME__, "More y-like parameters than histograms provided for error formula..")) return NULL;

        varexp0.ReplaceAll("#__#", "::");
        varexp0_errl.ReplaceAll("#__#", "::");
        varexp0_errh.ReplaceAll("#__#", "::");

        TFormula *fContent = new TFormula("tvarexp_content", varexp0);
        TFormula *fContentErrLow = new TFormula("tvarexp_content_errl", varexp0_errl);
        TFormula *fContentErrHigh = new TFormula("tvarexp_content_errh", varexp0_errh);

        // Check if there is remaining [c0-9] or [e0-9] parameters
        for(int ibin = 0, nbins = g0->GetN(); ibin < nbins; ibin++) {

                // Prepare list of vars
                std::vector<double> v, vErrLow, vErrHigh;

                //
                // TFORMULA IS USING ALPHANUMERIC INDEX !!!
                // SO [c0] < [c1] < [c2] < ... < [e0] < [e1] < [e2] ..
                for(int i = 0; i < (int) vGraph.size(); i++) {

                        if(vGraph[i] == NULL) continue;

                        if(varexp0.Contains("[n"+TString::Itoa(i,10)+"]")) v.push_back(vGraph[i]->GetN());
                        if(varexp0_errl.Contains("[n"+TString::Itoa(i,10)+"]")) vErrLow.push_back(vGraph[i]->GetN());
                        if(varexp0_errh.Contains("[n"+TString::Itoa(i,10)+"]")) vErrHigh.push_back(vGraph[i]->GetN());

                        if(vGraph[i]->GetX()) {
                                if(varexp0.Contains("[x"+TString::Itoa(i,10)+"]")) v.push_back(vGraph[i]->GetX()[ibin]);
                                if(varexp0_errl.Contains("[x"+TString::Itoa(i,10)+"]")) vErrLow.push_back(vGraph[i]->GetX()[ibin]);
                                if(varexp0_errh.Contains("[x"+TString::Itoa(i,10)+"]")) vErrHigh.push_back(vGraph[i]->GetX()[ibin]);
                        }
                        if(vGraph[i]->GetEXhigh()) {
                                if(varexp0.Contains("[x"+TString::Itoa(i,10)+".eh]")) v.push_back(vGraph[i]->GetEXhigh()[ibin]);
                                if(varexp0_errl.Contains("[x"+TString::Itoa(i,10)+".eh]")) vErrLow.push_back(vGraph[i]->GetEXhigh()[ibin]);
                                if(varexp0_errh.Contains("[x"+TString::Itoa(i,10)+".eh]")) vErrHigh.push_back(vGraph[i]->GetEXhigh()[ibin]);
                        }
                        if(vGraph[i]->GetEXlow()) {
                                if(varexp0.Contains("[x"+TString::Itoa(i,10)+".el]")) v.push_back(vGraph[i]->GetEXlow()[ibin]);
                                if(varexp0_errl.Contains("[x"+TString::Itoa(i,10)+".el]")) vErrLow.push_back(vGraph[i]->GetEXlow()[ibin]);
                                if(varexp0_errh.Contains("[x"+TString::Itoa(i,10)+".el]")) vErrHigh.push_back(vGraph[i]->GetEXlow()[ibin]);
                        }

                        if(vGraph[i]->GetY()) {
                                if(varexp0.Contains("[y"+TString::Itoa(i,10)+"]")) v.push_back(vGraph[i]->GetY()[ibin]);
                                if(varexp0_errl.Contains("[y"+TString::Itoa(i,10)+"]")) vErrLow.push_back(vGraph[i]->GetY()[ibin]);
                                if(varexp0_errh.Contains("[y"+TString::Itoa(i,10)+"]")) vErrHigh.push_back(vGraph[i]->GetY()[ibin]);
                        }
                        if(vGraph[i]->GetEYlow() && vGraph[i]->GetEYhigh()) {
                                if(varexp0_errl.Contains("[y"+TString::Itoa(i,10)+".e]")) vErrLow.push_back(vGraph[i]->GetEYlow()[ibin]);
                                if(varexp0_errh.Contains("[y"+TString::Itoa(i,10)+".e]")) vErrHigh.push_back(vGraph[i]->GetEYhigh()[ibin]);
                        } else if(vGraph[i]->GetEY()) {
                                if(varexp0_errl.Contains("[y"+TString::Itoa(i,10)+".e]")) vErrLow.push_back(vGraph[i]->GetEY()[ibin]);
                                if(varexp0_errh.Contains("[y"+TString::Itoa(i,10)+".e]")) vErrHigh.push_back(vGraph[i]->GetEY()[ibin]);
                        }

                }

                fContent->SetParameters(&v[0]);
                fContentErrLow->SetParameters(&vErrLow[0]);
                fContentErrHigh->SetParameters(&vErrHigh[0]);

                // replace dcontent..
                double dContent = fContent->Eval(0);
                if(UIUC::IsNormal(dContent) && g0->GetY())
                        g0->GetY()[ibin] = dContent;

                if(g0->GetEY()) g0->GetEY()[ibin] = 0;
                if(g0->GetEYlow()) g0->GetEYlow()[ibin] = 0;
                if(g0->GetEYhigh()) g0->GetEYhigh()[ibin] = 0;

                double dContentErrLow = fContentErrLow->Eval(0);
                double dContentErrHigh = fContentErrHigh->Eval(0);

                if(!varexp0_errl.EqualTo("")) {
                        if(UIUC::IsNormal(dContentErrLow) && g0->GetEYlow()) g0->GetEYlow()[ibin] = dContentErrLow;
                        else if(UIUC::IsNormal(dContentErrLow) && g0->GetEY()) g0->GetEY()[ibin] = dContentErrLow;

                }
                if(!varexp0_errh.EqualTo(""))
                        if(UIUC::IsNormal(dContentErrHigh) && g0->GetEYhigh()) g0->GetEYhigh()[ibin] = dContentErrHigh;


                //cout << dContent << " " << dContentErr << endl;
                UIUC::HandboxMsg::SetSingleCarriageReturn();
                UIUC::HandboxMsg::PrintProgressBar("Processing.. ", ibin, nbins, 64);
        }

        delete fContent;
        delete fContentErrLow;
        delete fContentErrHigh;

        return g0;
}

TH1* UIUC::TVarexp::GetHistogram(TString hname, vector<TH1*> vHist, TString varexp0_err)
{
        if(hname.EqualTo("")) hname = UIUC::TFactory::GetRandomName("h");

        //
        // Prepare the outgoing histogram...
        //
        if(vHist.size() == 0 || vHist[0] == NULL) return NULL;

        TH1 *h0 = NULL;
        if(h0 == NULL) h0 = UIUC::TFactory::CopyStruct(vHist[0], hname);

        //
        // Check structure of other histograms
        //
        for(int i = 1; i < (int) vHist.size(); i++) {

                if(vHist[i] == NULL) return NULL;

                TString eMessage = Form("Structure of histogram #%d is different than #0", i+1);
                if(UIUC::HandboxMsg::Warning(!UIUC::TFactory::CompareStruct(h0, vHist[i]), __METHOD_NAME__, eMessage)) return NULL;
        }

        //
        // Check ci,ei indexes and prepare fContent..
        //
        TString varexp0 = TVarexp(this->varexp).Get();
        if(varexp0.EqualTo("")) return NULL;

        varexp0.ReplaceAll("#__#", "::");

        // Check number of ci, ei parameters
        int vi, ci, ni, ei;
        ci = this->GetNparameters(varexp0, "\\[c[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(ci > (int) vHist.size(), __METHOD_NAME__, "More content-like parameters than histograms provided for main formula..")) return NULL;
        ei = this->GetNparameters(varexp0, "\\[e[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(ei > (int) vHist.size(), __METHOD_NAME__, "More error-like parameters than histograms provided for main formula..")) return NULL;
        ni = this->GetNparameters(varexp0, "\\[n[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(ni > (int) vHist.size(), __METHOD_NAME__, "More n-like parameters than histograms provided for main formula..")) return NULL;
        vi = this->GetNparameters(varexp0, "\\[v[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(vi > (int) vHist.size(), __METHOD_NAME__, "More center-like parameters than histograms provided for main formula..")) return NULL;

        TFormula *fContent = new TFormula("tvarexp_content", varexp0);

        // Check ci,ei indexes and prepare fContentErr..
        varexp0_err = TVarexp(varexp0_err).Get();
        if(!varexp0.EqualTo("") && varexp0_err.EqualTo("")) varexp0_err = Form("[e0]*(%s)/[c0]", this->varexp.Data());
        if(varexp0_err.EqualTo("")) return NULL;
        varexp0_err.ReplaceAll("#__#", "::");

        ci = this->GetNparameters(varexp0_err, "\\[c[0-9.]*\\]");
        ei = this->GetNparameters(varexp0_err, "\\[e[0-9.]*\\]");
        ni = this->GetNparameters(varexp0_err, "\\[n[0-9.]*\\]");
        vi = this->GetNparameters(varexp0_err, "\\[v[0-9.]*\\]");
        if( UIUC::HandboxMsg::Warning(vi > (int) vHist.size(), __METHOD_NAME__, "More value-like parameters than histograms provided for error formula..")) return NULL;
        if( UIUC::HandboxMsg::Warning(ci > (int) vHist.size(), __METHOD_NAME__, "More content-like parameters than histograms provided for error formula..")) return NULL;
        if( UIUC::HandboxMsg::Warning(ni > (int) vHist.size(), __METHOD_NAME__, "More n-like parameters than histograms provided for error formula..")) return NULL;
        if( UIUC::HandboxMsg::Warning(ei > (int) vHist.size(), __METHOD_NAME__, "More error-like parameters than histograms provided for error formula..")) return NULL;

        TFormula *fContentErr = new TFormula("tvarexp_content_err", varexp0_err);

        // Check if there is remaining [c0-9] or [e0-9] parameters
        int nbins = h0->GetNcells();
        for(int ix = 1, xbins = h0->GetNbinsX(); ix <= xbins; ix++) {
                for(int iy = 1, ybins = h0->GetNbinsY(); iy <= ybins; iy++) {
                        for(int iz = 1, zbins = h0->GetNbinsZ(); iz <= zbins; iz++) {

                                int ibin = h0->GetBin(ix,iy,iz);

                                // Prepare list of vars
                                std::vector<double> v, vErr;

                                //
                                // TFORMULA IS USING ALPHANUMERIC INDEX !!! So variables MUST be replaced in the same order..
                                // SO [c0] < [c1] < [c2] < ... < [e0] < [e1] < [e2] .. < [v0] < ...

                                for(int i = 0; i < (int) vHist.size(); i++) {

                                        if(varexp0.Contains("[c"+TString::Itoa(i,10)+"]")) v.push_back(vHist[i]->GetBinContent(ibin));
                                        if(varexp0_err.Contains("[c"+TString::Itoa(i,10)+"]")) vErr.push_back(vHist[i]->GetBinContent(ibin));
                                }

                                for(int i = 0; i < (int) vHist.size(); i++) {

                                        if(varexp0.Contains("[e"+TString::Itoa(i,10)+"]")) v.push_back(vHist[i]->GetBinError(ibin));
                                        if(varexp0_err.Contains("[e"+TString::Itoa(i,10)+"]")) vErr.push_back(vHist[i]->GetBinError(ibin));
                                }

                                for(int i = 0; i < (int) vHist.size(); i++) {

                                        if(varexp0.Contains("[n"+TString::Itoa(i,10)+"]")) v.push_back(vHist[i]->GetEntries());
                                        if(varexp0_err.Contains("[n"+TString::Itoa(i,10)+"]")) vErr.push_back(vHist[i]->GetEntries());
                                }

                                for(int i = 0; i < (int) vHist.size(); i++) {

                                        if(varexp0.Contains("[v"+TString::Itoa(i,10)+"]")) v.push_back(vHist[i]->GetXaxis()->GetBinCenter(ix));
                                        if(varexp0_err.Contains("[v"+TString::Itoa(i,10)+"]")) vErr.push_back(vHist[i]->GetXaxis()->GetBinCenter(ix));
                                        if(varexp0.Contains("[v"+TString::Itoa(i,10)+".0]")) v.push_back(vHist[i]->GetXaxis()->GetBinCenter(ix));
                                        if(varexp0_err.Contains("[v"+TString::Itoa(i,10)+".0]")) vErr.push_back(vHist[i]->GetXaxis()->GetBinCenter(ix));
                                        if(varexp0.Contains("[v"+TString::Itoa(i,10)+".1]")) v.push_back(vHist[i]->GetYaxis()->GetBinCenter(iy));
                                        if(varexp0_err.Contains("[v"+TString::Itoa(i,10)+".1]")) vErr.push_back(vHist[i]->GetYaxis()->GetBinCenter(iy));
                                        if(varexp0.Contains("[v"+TString::Itoa(i,10)+".2]")) v.push_back(vHist[i]->GetZaxis()->GetBinCenter(iz));
                                        if(varexp0_err.Contains("[v"+TString::Itoa(i,10)+".2]")) vErr.push_back(vHist[i]->GetZaxis()->GetBinCenter(iz));
                                }
                                fContent->SetParameters(&v[0]);
                                fContentErr->SetParameters(&vErr[0]);

                                // replace dcontent..
                                double dContent = fContent->Eval(0);
                                double dContentErr = fContentErr->Eval(0);

                                if(UIUC::IsNormal(dContent) && UIUC::IsNormal(dContentErr)) {

                                    h0->SetBinContent(ibin, dContent);
                                    h0->SetBinError(ibin, dContentErr);
                                }

                                //cout << dContent << " " << dContentErr << endl;
                                UIUC::HandboxMsg::SetSingleCarriageReturn();
                                UIUC::HandboxMsg::PrintProgressBar("Processing.. ", ibin, nbins, 64);
                        }
                }
        }

        delete fContent;
        delete fContentErr;

        return h0;
}

TString UIUC::TVarexp::FlipExpression(TString varexp0, char delimiter) {

        vector<TString> v0 = TVarexp(varexp0).Tokenize(delimiter);
        vector<TString> v = UIUC::TVarexp::FlipExpression(v0);

        TString varexp;
        for(int i = 0, N = v.size(); i < N; i++)
                varexp += (i == 0) ? v[0] : delimiter + v[i];

        return varexp;
}

vector<TString> UIUC::TVarexp::FlipExpression(vector<TString> v)
{
        std::reverse(v.begin(),v.end());
        return v;
}

bool UIUC::TVarexp::AppendToHistogram(TH1*h0, TTree*tree, TString selection_str)
{
        if(h0 == NULL) return 0;

        TString hname = h0->GetName();
        TH1 *h = (TH1*) UIUC::TFactory::CopyStruct(h0,hname);
        GetHistogram(hname, tree, selection_str);

        h0->Add(h);

        delete h;
        h = NULL;
        return 1;
}

TH1* UIUC::TVarexp::GetHistogram(TH1*h0, TTree*tree, TString selection_str, int iEntry)
{
        if(h0 == NULL) {

                TString hname = UIUC::TFactory::GetRandomName("h");
                return GetHistogram(hname, tree, selection_str, iEntry);
        }

        TString hname = h0->GetName();
        return GetHistogram(hname, tree, selection_str, iEntry);
}

TH1* UIUC::TVarexp::GetHistogram(TString hname, TTree* tree, TString selection_str, int iEntry)
{
        if(tree == NULL) return NULL;
        if(tree->GetEntries() == 0) return NULL;

        TString varexp0 = this->RemoveBinning();
        varexp0 = UIUC::TVarexp::FlipExpression(varexp0);

        UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);
        tree->SetEntries(tree->GetEntriesFast());
        tree->Draw(varexp0 + ">>"+hname, selection_str, "goff", ((iEntry < 0) ? TTree::kMaxEntries : 1), ((iEntry < 0) ? 0 : iEntry));

        TString out, err;
        UIUC::HandboxMsg::GetTTY(__METHOD_NAME__, &out, &err); // IF ERROR DETECTED RETURN NULL..
        if( !err.EqualTo("") ) {

                UIUC::HandboxMsg::PrintTTY(__METHOD_NAME__, "", err);
                return NULL;
        }

        int par = hname.First('(');
        int len = hname.Length();
        if(par != -1) hname.Remove(par,len-par);

        TH1 *h = (TH1*) gDirectory->Get(hname);
        if(h == NULL) return NULL;

        gErrorIgnoreLevel = kBreak;
        int N = h->GetEntries();
        gErrorIgnoreLevel = kInfo;

        if(N == 0) UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Tree subset is empty");
        //return h;

        if(h != NULL) {
                vector<TString> label = this->TokenizeLabel();
                label = UIUC::TVarexp::FlipExpression(label);
                if(label.size() > 0) h->GetXaxis()->SetTitle(label[label.size()-1]);
                if(label.size() > 1) h->GetYaxis()->SetTitle(label[label.size()-2]);
                if(label.size() > 2) h->GetZaxis()->SetTitle(label[label.size()-3]);
        }

        return h;
}

TString UIUC::TVarexp::Detokenize(std::vector<double> vStr, char separator) {

        TString str;
        for(int i = 0, N = vStr.size(); i < N; i++) {

                if(i) str += separator;
                str += Form("%f", vStr[i]);
        }

        return str;
}

TString UIUC::TVarexp::Detokenize(std::vector<int> vStr, char separator) {

        TString str;
        for(int i = 0, N = vStr.size(); i < N; i++) {

                if(i) str += separator;
                str += Form("%d", vStr[i]);
        }

        return str;
}

vector<TString> UIUC::TVarexp::Tokenize(char separator, bool bBinning) {

        vector<TString> vArray;

        if(this->varexp.EqualTo("")) return {};

        vector<int> vIndex = {0};
        for(int i = 0, N = this->varexp.Length(); i < N; i++) {

                if(TString(this->varexp[i]).EqualTo(separator))
                        vIndex.push_back(i+1);
        }

        vIndex.push_back(this->varexp.Length()+1);
        for(int i = 0, N = vIndex.size()-1; i < N; i++) {

                TString varexp0 = this->varexp(vIndex[i], vIndex[i+1]-vIndex[i]-1);
                varexp0.ReplaceAll("#__#", "::");
                //varexp0.ReplaceAll(" ", "");

                if(bBinning != UIUC::TVarexp::kWithBinning) {

                        TString varname_str;

                        //
                        // Remove constant binning
                        //
                        varname_str = varexp0;
                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$1");
                        varexp0 = varname_str;

                        //
                        // Remove variable binning
                        //
                        varname_str = varexp0;
                        int brackleft = varname_str.First('{');
                        if(brackleft != -1) {

                                int brackright = varname_str.Last('}');
                                if(brackright != -1) {

                                        int length = brackright-brackleft-1;
                                        if(length >= 0) varexp0 = varname_str(0,brackleft);
                                }
                        }
                }

                vArray.push_back(varexp0);
        }

        if(!vArray.size()) vArray.push_back("");
        return vArray;
}


vector<TString> UIUC::TVarexp::TokenizeLabel(char separator) {

        return Tokenize(separator, kWithoutBinning);
}

vector<int> UIUC::TVarexp::TokenizeNbins(char separator) {

        vector<int> vArray;

        if(this->varexp.EqualTo("")) return {};

        TObjArray *varexp_array = this->varexp.Tokenize(separator);
        for(int i = 0, N = varexp_array->GetEntries(); i < N; i++) {

                TString varexp0 = ((TObjString *)(varexp_array->At(i)))->String();
                varexp0.ReplaceAll("#__#", "::");
                //varexp0.ReplaceAll(" ", "");

                if(HasVariableBinningInformation(varexp0)) {

                        //
                        // Remove variable binning
                        //

                        TString varname_str = varexp0;
                        int brackleft = varname_str.First('{');
                        if(brackleft != -1) {

                                int brackright = varname_str.Last('}');
                                if(brackright != -1) {

                                        int length = brackright-brackleft-1;
                                        if(length >= 0) {

                                                TString str0 = varname_str(brackleft+1,length);
                                                vector<TString> v = TVarexp(str0).Tokenize();
                                                varexp0 = TString::Itoa(v.size()-1,10);
                                        }
                                }
                        }

                } else {

                        //
                        // Constant binning case
                        //

                        TString varname_str = varexp0;
                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$2");
                        varexp0 = varname_str;
                }

                vArray.push_back((int) InterpretFormula(varexp0.Data()));
        }

        delete varexp_array;
        varexp_array = NULL;

        if(!vArray.size()) vArray.push_back(0);
        return vArray;
}

vector<double> UIUC::TVarexp::TokenizeXmin(char separator) {

        vector<double> vArray;

        if(this->varexp.EqualTo("")) return {};

        TObjArray *varexp_array = this->varexp.Tokenize(separator);
        for(int i = 0, N = varexp_array->GetEntries(); i < N; i++) {

                TString varexp0 = ((TObjString *)(varexp_array->At(i)))->String();
                varexp0.ReplaceAll("#__#", "::");
                //varexp0.ReplaceAll(" ", "");

                if(HasVariableBinningInformation(varexp0)) {

                        //
                        // Remove variable binning
                        //

                        TString varname_str = varexp0;
                        int brackleft = varname_str.First('{');
                        if(brackleft != -1) {

                                int brackright = varname_str.Last('}');
                                if(brackright != -1) {

                                        int length = brackright-brackleft-1;
                                        if(length >= 0) {

                                                TString str0 = varname_str(brackleft+1,length);
                                                vector<TString> v = TVarexp(str0).Tokenize();
                                                varexp0 = v[0];
                                        }
                                }
                        }
                } else {

                        //
                        // Constant binning case
                        //

                        TString varname_str = varexp0;
                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$2");
                        varexp0 = varname_str;
                }

                vArray.push_back(InterpretFormula(varexp0.Data()));
        }

        delete varexp_array;
        varexp_array = NULL;

        if(!vArray.size()) vArray.push_back(NAN);
        return vArray;
}

vector<double> UIUC::TVarexp::TokenizeXmax(char separator) {

        vector<double> vArray;

        if(this->varexp.EqualTo("")) return {};

        TObjArray *varexp_array = this->varexp.Tokenize(separator);
        for(int i = 0, N = varexp_array->GetEntries(); i < N; i++) {

                TString varexp0 = ((TObjString *)(varexp_array->At(i)))->String();
                varexp0.ReplaceAll("#__#", "::");
                //varexp0.ReplaceAll(" ", "");

                if(HasVariableBinningInformation(varexp0)) {

                        //
                        // Remove variable binning
                        //

                        TString varname_str = varexp0;
                        int brackleft = varname_str.First('{');
                        if(brackleft != -1) {

                                int brackright = varname_str.Last('}');
                                if(brackright != -1) {

                                        int length = brackright-brackleft-1;
                                        if(length >= 0) {

                                                TString str0 = varname_str(brackleft+1,length);
                                                vector<TString> v = TVarexp(str0).Tokenize();
                                                varexp0 = v[v.size()-1];
                                        }
                                }
                        }

                } else {

                        //
                        // Constant binning case
                        //

                        TString varname_str = varexp0;
                        TPRegexp r("(.*)\\[([^,]*),([^,]*),([^,]*)\\]");
                        r.Substitute(varname_str, "$2");
                        varexp0 = varname_str;
                }

                vArray.push_back(InterpretFormula(varexp0.Data()));
        }

        delete varexp_array;
        varexp_array = NULL;

        if(!vArray.size()) vArray.push_back(NAN);
        return vArray;
}

vector< vector<double> > UIUC::TVarexp::GetSelection(TTree* tree, TString selection_str, Long64_t nEntries, int firstEntry, int downsample)
{
        vector< vector<double> > selection;
        vector<TString> branches = this->Tokenize();

        int size = TMath::Max((int) branches.size(), 1);
        selection.resize(size);

        if(tree == NULL) return selection;

        // Use RDataFrame..
        // Fixme: handle dynamic branches
        // if(selection_str.EqualTo("")) selection_str = "true";
        // if(tree->GetCurrentFile() != NULL) {

        //         ROOT::RDataFrame df(*tree);                         
        //         for(int i = 0, N = branches.size(); i < N; i++) 
        //         {
        //                 auto v = df.Filter(selection_str.Data()).Take<double>(branches[i]).GetValue();
        //                 if(downsample == 1) selection.push_back(v);
        //                 else {

        //                         for(int j = 0, N = v.size(); j < N/downsample; j++)
        //                                 selection[i].push_back(v[j*downsample]);
        //                 }
        //         }

        //         return selection;
        // }

        int makeClass = tree->GetMakeClass();
        tree->SetMakeClass(0);
        tree->SetEstimate(_selectionBuffer);

        Long64_t nTotalEntries = nEntries < 0 ? tree->GetEntries() : nEntries;

        Long64_t iEntry = nEntries < 0 ? _selectionBuffer : TMath::Min(_selectionBuffer, nEntries);
        while(firstEntry < nTotalEntries) {

                UIUC::HandboxMsg::RedirectTTY(__METHOD_NAME__);
                tree->Draw(this->varexp, selection_str, "goff", iEntry, firstEntry);
                TString out, err;
                UIUC::HandboxMsg::GetTTY(__METHOD_NAME__, &out, &err);

                if(err.Contains("The selected TTree subset is empty")) err = "";
                if(!out.EqualTo("")) UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, out);
                if(!err.EqualTo("")) {

                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, err);
                        return selection;
                }

                for(int i = 0, N = branches.size(); i < N; i++) {
                        
                        double *array = tree->GetVal(i);
                        if(array) {

                                for(int j = 0, N = tree->GetSelectedRows(); j < N/downsample; j++) {
                                        selection[i].push_back(array[j*downsample]);
                                }
                        }
                }

                firstEntry += iEntry;
        }


        tree->SetMakeClass(makeClass);

        return selection;
}
