# Use the rootproject/root image as the base image
FROM rootproject/root:latest

# Update package lists and install necessary packages
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV NPROC=8

ENV LD_LIBRARY_PATH=/usr/local/lib

# Update package lists and install necessary packages
COPY Dockerfile.packages packages

RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get -y install $(cat packages | grep -v '#') && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf packages
