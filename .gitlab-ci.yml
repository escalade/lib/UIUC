variables:  # Override OS information from "Settings > CI/CD > Variables"
  GIT_SUBMODULE_STRATEGY: recursive
  CI_IMAGE_BUILDER: "k8s-default-runners"
  CI_RUNNER_CVMFS: "false"
  CI_RUNNER_ARCH: ""

stages:
  - image
  - staging
  - configure
  - build
  - tests
  - documentation

arch:
  stage: image
  allow_failure: true
  tags:
    - $CI_IMAGE_BUILDER
  script:
    - >
      if [ -z "$CI_RUNNER_ARCH" ]; then
        echo "Warning CI_RUNNER_ARCH is not set. Consider overriding default value under 'Settings > CI/CD > Variables'."
        exit 1
      fi
  rules:
    - exists: 
        - $CI_COMMIT_TAG
      when: always
    - changes:
      - Dockerfile
      - Dockerfile.packages
      - .gitlab-ci.yml
      when: always

#
# Overview of the gitlab environment loaded.
kaniko:
  stage: image
  tags:
    - $CI_IMAGE_BUILDER
    - $CI_RUNNER_ARCH
  image:
     # The kaniko debug image is recommended because it has a shell, and a shell is required for an image to be used with GitLab CI/CD.
     name: gcr.io/kaniko-project/executor:debug
     entrypoint: [""]
  variables:
    CI_REGISTRY_AUTH: "{\"auths\":{\"'$CI_REGISTRY'\":{\"username\":\"'$CI_REGISTRY_USER'\",\"password\":\"'$CI_REGISTRY_PASSWORD'\"}}}"
  script:
    # Prepare Kaniko configuration file
    - echo "${CI_REGISTRY_AUTH}" > /kaniko/.docker/config.json
    # Build and push the image from the Dockerfile at the root of the project.
    - /kaniko/executor 
      --context $CI_PROJECT_DIR 
      --dockerfile $CI_PROJECT_DIR/Dockerfile 
      --destination ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
      --destination ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH:-$(uname -m)}
    # Print the full registry path of the pushed image
    - echo "Image pushed successfully to ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME} for [noarch|${CI_RUNNER_ARCH:-$(uname -m)}] architecture."
  rules:
    - exists: 
        - $CI_COMMIT_TAG
      when: always
    - changes:
      - Dockerfile
      - Dockerfile.packages
      - .gitlab-ci.yml
      when: always

# ###
# Environment: return list of variables, load container from registry, and check if specific architecture is provided
# ### ###
.env: &env
  stage: staging
  tags:
    - $CI_RUNNER_ARCH
  script:
    - env
    - which root # ROOT Configuration
    - root-config --version
    - root-config --features

# Fallback in case a specific computer architecture is required
env:
  <<: *env
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH}
  rules:
    - if: $CI_RUNNER_ARCH != ""
      when: always

# Fallback in case no specific computer architecture is required
env:noarch:
  <<: *env
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
  rules:
    - if: $CI_RUNNER_ARCH == ""
      when: always


# ###
# CVMFS: check on demand if CVMFS is setup. (a specific tag on runners is requested on these machines where CVMFS is available)
# ### ###
.cmvfs: &cvmfs
  stage: staging
  allow_failure: true
  tags:
    - $CI_RUNNER_ARCH
    - cvmfs
  script:
    - ls -ls /cvmfs/
    - ls -ls /cvmfs/sft.cern.ch/lcg/app/releases/
    - ls -ls /cvmfs/software.igwn.org/

# Fallback in case a specific computer architecture is required
cmvfs:
  <<: *cvmfs
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH}
  rules:
    - if: $CI_RUNNER_CVMFS != "true"
      when: never
    - if: $CI_RUNNER_ARCH != ""
      when: manual

# Fallback in case no specific computer architecture is required
cmvfs:noarch:
  <<: *cvmfs
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
  rules:
    - if: $CI_RUNNER_CVMFS != "true"
      when: never
    - if: $CI_RUNNER_ARCH == ""
      when: manual


# ###
# Cmake configuration
# ### ###
.cmake: &cmake
  stage: configure
  tags:
    - $CI_RUNNER_ARCH
  artifacts:
    paths:
      - build/
  script:
    - mkdir -p build
    - cd build
    - cmake .. -DCMAKE_BUILD_TYPE=Debug

# Fallback in case a specific computer architecture is required
cmake:
  <<: *cmake
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH}
  rules:
    - if: $CI_RUNNER_ARCH != ""
      when: always
  needs:
    - env

# Fallback in case no specific computer architecture is required
cmake:noarch:
  <<: *cmake
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
  rules:
    - if: $CI_RUNNER_ARCH == ""
      when: always
  needs:
    - env:noarch

# ###
# Build library
# ### ###
.library: &library
  stage: build
  tags:
    - $CI_RUNNER_ARCH
  script:
    - cd build
    - make -j lib
  artifacts:
    paths:
      - build

# Fallback in case a specific computer architecture is required
library:
  <<: *library
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH}
  rules:
    - if: $CI_RUNNER_ARCH != ""
      when: always
  needs:
    - job: cmake
      artifacts: true

# Fallback in case no specific computer architecture is required
library:noarch:
  <<: *library
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
  rules:
    - if: $CI_RUNNER_ARCH == ""
      when: always
  needs:
    - job: cmake:noarch
      artifacts: true


# ###
# Tests
# ### ###
.tests: &tests
  stage: tests
  tags:
    - $CI_RUNNER_ARCH
  script:
    - cd build
    - make -j tests
  artifacts:
    paths:
      - build

# Fallback in case a specific computer architecture is required
tests:
  <<: *tests
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH}
  rules:
    - if: $CI_RUNNER_ARCH != ""
      when: always
  needs:
    - job: library
      artifacts: true

# Fallback in case no specific computer architecture is required
tests:noarch:
  <<: *tests
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
  rules:
    - if: $CI_RUNNER_ARCH == ""
      when: always
  needs:
    - job: library:noarch
      artifacts: true


# ###
# Examples
# ### ###
.examples: &examples
  stage: build
  tags:
    - $CI_RUNNER_ARCH
  script:
    - cd build
    - make -j examples
  artifacts:
    paths:
      - build

# Fallback in case a specific computer architecture is required
examples:
  <<: *examples
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH}
  rules:
    - if: $CI_RUNNER_ARCH != ""
      when: always
  needs:
    - job: library
      artifacts: true

# Fallback in case no specific computer architecture is required
examples:noarch:
  <<: *examples
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
  rules:
    - if: $CI_RUNNER_ARCH == ""
      when: always
  needs:
    - job: library:noarch
      artifacts: true


# ###
# Additional scripts
# ### ###
.scripts: &scripts
  stage: build
  tags:
    - $CI_RUNNER_ARCH
  script:
    - cd build
    - make -j scripts
  artifacts:
    paths:
      - build

# Fallback in case a specific computer architecture is required
scripts:
  <<: *scripts
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH}
  rules:
    - if: $CI_RUNNER_ARCH != ""
      when: always
  needs:
    - job: library
      artifacts: true

# Fallback in case no specific computer architecture is required
scripts:noarch:
  <<: *scripts
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
  rules:
    - if: $CI_RUNNER_ARCH == ""
      when: always
  needs:
    - job: library:noarch
      artifacts: true



# ###
# Documentation
# ### ###
.pages: &pages
  stage: documentation
  tags:
    - $CI_RUNNER_ARCH
  script:
    - cd build
    - make -j pages
    - mv public ..
  artifacts:
    paths:
    - public


# Fallback in case a specific computer architecture is required
pages:
  <<: *pages
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_RUNNER_ARCH}
  rules:
    - if: $CI_RUNNER_ARCH != ""
      when: always
    - exists:
        - $CI_COMMIT_TAG
  needs:
    - job: tests
      artifacts: true

# Fallback in case no specific computer architecture is required
pages:noarch:
  <<: *pages
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:noarch
  rules:
    - if: $CI_RUNNER_ARCH == ""
      when: always
  needs:
    - job: tests:noarch
      artifacts: true
