#! /bin/bash

[[ $_ != $0 ]] && REALPATH=`dirname $(readlink -f ${BASH_SOURCE[0]})` || REALPATH=`dirname $(readlink -f $0)`

if [[ ! -z "$1" || ! -d "$REALPATH/build" ]]; then

  mkdir -p $REALPATH/build
  cd $REALPATH/build

  cmake $REALPATH -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=$REALPATH/install
  [ $? -eq 1 ] && exit 1
fi

cd $REALPATH/build

[[ ! -z `which nproc` ]] && make install -j$(($(nproc --all)-1)) || make install -j
[ $? -eq 1 ] && exit 1

cd - > /dev/null 2> /dev/null
