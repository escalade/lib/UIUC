/**
 * *********************************************
 *
 * \file HandboxUsage.h
 * \brief Header of the HandboxUsage class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 * *********************************************
 *
 * \class HandboxUsage
 * \brief ..
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date August 11th, 2017
 *
 * *********************************************

 */

#ifndef HandboxUsage_H
#define HandboxUsage_H

#include <signal.h>

#include <Riostream.h>
#include <vector>
#include <TROOT.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TApplication.h>
#include <TObject.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>
#include <TBrowser.h>
#include <TCanvas.h>
#include <TVector.h>
#include <TVector2.h>
#include <TVector3.h>

#include <TTree.h>
#include <UIUC/HandboxMsg.h>
#include <UIUC/HandboxStyle.h>
#include <UIUC/TFileReader.h>
#include <UIUC/MathUtils.h>

namespace UIUC {

/*
 * Avoid to start root and create a TBrowser session
 */
        class HandboxBrowser: public TBrowser {

                public:
                HandboxBrowser() : TBrowser() {}
                virtual ~HandboxBrowser() { gApplication->Terminate(); }
        };

        class HandboxUsage
        {

                private:

                HandboxUsage(int *newArgc, char **newArgv, Option_t *newOption) { Init(newArgc, newArgv, newOption); }
                HandboxUsage(const HandboxUsage&) = delete;
                HandboxUsage& operator=(const HandboxUsage&) = delete;

                protected:

                int iarg, argc;
                std::vector<TString> argv;

                bool kUsage;
                bool kPrintUsageOnDestroy;
                TString usage_msg;

                Option_t *option;

                std::vector<TString> vTitle;
                std::vector<int> vTitlePos;

                bool bArgOptionLock;
                std::vector<TString>  vArgName;
                std::vector<TString>  vArgColor;
                std::vector<bool>  vArgMandatory;
                std::vector<TString> vArgDefaultValue;

                std::vector<int>  vArgAlreadyUsed;

                std::vector<TString> bufferTTY[2];
                TString WrongUsageStr;

        public:
                enum class Verbosity {VERBOSITY_QUIET = -1, VERBOSITY_NORMAL = 0, VERBOSITY_VERBOSE = 1, VERBOSITY_VERY_VERBOSE = 10, VERBOSITY_DEBUG = 100};
                static void enum2str() { std::invalid_argument("Invalid argument converting enum to string"); }
                static TString enum2str(Verbosity verbosity) {

                        switch(verbosity) {

                                case Verbosity::VERBOSITY_QUIET: return "Quiet";
                                case Verbosity::VERBOSITY_NORMAL: return "Normal";
                                case Verbosity::VERBOSITY_VERBOSE: return "Verbose";
                                case Verbosity::VERBOSITY_VERY_VERBOSE: return "Very verbose";
                                case Verbosity::VERBOSITY_DEBUG: return "Debugging";
                                default: enum2str();
                        }
                }

                static int iPadTitle;
                static int iApplyGraphics;
                static TApplication *fApp;
                static TBrowser *fBrowser;
                static bool bGraphicsLoop;
                static bool bOpenGraphics;

                static bool bCallTrees;
                static bool bCallHistos;
                static bool bCallGraphics;

                static void EnableGraphicsHandler();
                static void DisableSignalHandler();
                static void HandlerGRAPHICS(int);

                std::vector<TObject*> ApplyGraphicsOptions(TVirtualPad *vpad, std::vector<TObject*> vObject = {})
                {
                        return ApplyGraphicsOptions(vpad, NULL, vObject);
                }
                std::vector<TObject*> ApplyGraphicsOptions(TVirtualPad *vpad, TObject *obj)
                {
                        return ApplyGraphicsOptions(vpad, NULL, obj);
                }
                std::vector<TObject*> ApplyGraphicsOptions(TVirtualPad *vpad, TList* lObject)
                {
                        return ApplyGraphicsOptions(vpad, NULL, lObject);
                }


                std::vector<TObject*> ApplyGraphicsOptions(TVirtualPad *,     TString*, std::vector<TObject*> = {});
                std::vector<TObject*> ApplyGraphicsOptions(TVirtualPad *vpad, TString* option, TList* lObject)
                {
                        std::vector<TObject*> vObject;
                        for(int i = 0, N = lObject->GetSize(); i < N; i++)
                                vObject.push_back(lObject->At(i));

                        return UIUC::HandboxUsage::ApplyGraphicsOptions(vpad, option, vObject);
                }

                std::vector<TObject*> ApplyGraphicsOptions(TVirtualPad *vpad, TString *option, TObject *obj) {
                        return ApplyGraphicsOptions(vpad, option, std::vector<TObject*>{obj});
                }

                TString ApplyGraphicsOptions(TVirtualPad *, TList*, Option_t* = "");
                void ApplyHistosOptions(TH1 *);
                void ApplyTreesOptions(TTree *);

                static bool HasGraphics(Option_t *opt = "");
                static void OpenGraphics(Option_t *opt = "g");
                static void WaitForGraphicsToBeClosed(Option_t *opt = "");
                static void CloseGraphics();

                static const bool kReverseLogic;
                static const bool kMandatory;
                static const bool kOptional;
                static const bool kUseDefault;
                static const bool kNotUseDefault;
                
                // Constructor, destructor
                static UIUC::HandboxUsage& Instance(int *newArgc = 0, char **newArgv = NULL, Option_t *newOption = "") {
            
                        static UIUC::HandboxUsage _Instance(newArgc, newArgv, newOption);
                        if(_Instance.argc == 0) _Instance.Init(newArgc, newArgv, newOption);
                        return _Instance;
                };

                virtual ~HandboxUsage() {

                    if(this->kUsage) {

                        if(!bCallGraphics) UIUC::HandboxMsg::PrintDebug(100, __METHOD_NAME__, "Graphics options expected, but UIUC::HandboxMsg::ApplyGraphicsOptions never called..");
                        if(!bCallHistos)   UIUC::HandboxMsg::PrintDebug(100, __METHOD_NAME__, "Hist options expected, but UIUC::HandboxMsg::ApplyHistosOptions never called..");
                        if(!bCallHistos)   UIUC::HandboxMsg::PrintDebug(100, __METHOD_NAME__, "Tree options expected, but UIUC::HandboxMsg::ApplyTreesOptions never called..");
                    }

                        UIUC::HandboxMsg::EmptyTrash();
                }

                void Init(int *, char **, Option_t *);

                // Usage functions and arg manager
                void AddTitle(TString);
                void AddMessage(TString);
                bool Usage(bool = 0);
                void WrongUsage(TString str = "") {

                        if(!str.EqualTo("")) WrongUsageStr += UIUC::HandboxMsg::kRed + str + UIUC::HandboxMsg::kNoColor + '\n';
                        kUsage = false;
                }

                bool IsArgAlreadyDefined(TString);

                inline bool GetStatusUsage() {
                        return kUsage;
                }
                bool AskHelp(int = 0);
                bool CheckArg(int);

                static TString GetRootVersion() {
                        return gROOT->GetVersion();
                };

                static int GetRootVersionMajor() {
                        TString str =  gROOT->GetVersion();

                        TObjArray *var_array = str.Tokenize(".");
                        int major = ((TObjString *)(var_array->At(0)))->String().Atoi();

                        delete var_array;
                        return major;
                };

                static int GetRootVersionMinor() {
                        TString str =  gROOT->GetVersion();

                        TObjArray *var_array = str.Tokenize(".");
                        TString subversion = ((TObjString *)(var_array->At(1)))->String();
                        delete var_array;

                        var_array = subversion.Tokenize("/");
                        int minor = ((TObjString *)(var_array->At(0)))->String().Atoi();
                        delete var_array;
                        var_array = NULL;

                        return minor;
                };

                static int GetRootVersionPatch() {

                        TString str =  gROOT->GetVersion();

                        TObjArray *var_array = str.Tokenize("/");
                        int patch = ((TObjString *)(var_array->At(1)))->String().Atoi();
                        delete var_array;
                        var_array = NULL;

                        return patch;
                };

                inline static TString GetOutput() { return gSystem->Getenv("ESCALADE_HOUTPUT"); }
                inline static TString GetOutputTitle() { return gSystem->Getenv("ESCALADE_HOUTPUT_TITLE"); }
                inline static TString GetOutputFinalize() { return gSystem->Getenv("ESCALADE_HOUTPUT_FINALIZE"); }

                std::vector<int> GetArgNotUsed();
                int GetArgIndex(TString);
                bool AddArgument(bool*, TString, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(int*, TString, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(double*, TString, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(TString*, TString, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(TFileReader*, TString, bool = UIUC::HandboxUsage::kMandatory);

                bool AddArgument(std::vector<bool>&, TString, char = ' ', int = -1, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(std::vector<int>&, TString, char = ' ', int = -1, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(std::vector<float>&, TString, char = ' ', int = -1, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(std::vector<double>&, TString, char = ' ', int = -1, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(std::vector<TString>&, TString, char = ' ', int = -1, bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(std::vector<TFileReader>&, TString, TClass *cl = NULL, char = ' ', int = -1, bool = UIUC::HandboxUsage::kMandatory);

                bool AddArgument(TVector2*, TString, char = ' ', bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(TVector3*, TString, char = ' ', bool = UIUC::HandboxUsage::kMandatory);
                bool AddArgument(std::map<TString, double>*, TString, char = ';', char = ':', bool = UIUC::HandboxUsage::kMandatory);

                bool AddOption(TString, TString, bool *, bool = !UIUC::HandboxUsage::kReverseLogic);
                bool AddOption(TString, TString, std::vector<bool>&, std::vector<bool>, char = ' ', int = -1);

                bool AddOption(TString, TString, int *, double = INFINITY);
                bool AddOption(TString, TString, int *, TString);
                bool AddOption(TString, TString, std::vector<int>&, TString = "", char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<int>&, std::vector<int>, char = ' ', int = -1);

                bool AddOption(TString, TString, float *, double = INFINITY);
                bool AddOption(TString, TString, float *, TString);
                bool AddOption(TString, TString, std::vector<float>&, TString = "", char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<float>&, std::vector<float>, char = ' ', int = -1);

                bool AddOption(TString, TString, double *, double = INFINITY);
                bool AddOption(TString, TString, double *, TString);
                bool AddOption(TString, TString, std::vector<double>&, TString = "", char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<double>&, std::vector<double>, char = ' ', int = -1);

                bool AddOption(TString, TString, char *, char = '\0');
                bool AddOption(TString, TString, TString *, TString = "");
                bool AddOption(TString, TString, std::vector<TString>&, std::vector<TString>, char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<TString>&, TString = "", char = ' ', int = -1);

                bool AddOption(TString, TString, TFileReader *, TString = "");
                bool AddOption(TString, TString, std::vector<TFileReader>&, std::vector<TString>, TClass *cl = NULL, char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<TFileReader>&, TString, TClass *cl = NULL, char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<TFileReader>&, TClass *cl = NULL, TString = "", char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<TFileReader>&, TClass *cl, std::vector<TString>, char = ' ', int = -1);

                bool AddOption(TString, TString, TVector2 *, TVector2   , char = ' ');
                bool AddOption(TString, TString, TVector2 *, std::vector<double>, char = ' ');
                bool AddOption(TString, TString, TVector2 *, TString = "", char = ' ');
                bool AddOption(TString, TString, TVector3 *, TVector3   , char = ' ');
                bool AddOption(TString, TString, TVector3 *, std::vector<double>, char = ' ');
                bool AddOption(TString, TString, TVector3 *, TString = "", char = ' ');

                bool AddOption(TString, TString, std::map<TString, double> *, std::map<TString, double>, char = '\n', char = ':');
                bool AddOption(TString, TString, std::map<TString, double> *, TString = "", char = '\n', char = ':');

                int GetArgc() {
                        return argc;
                }

                int CheckNArg(int argi) {

                        return !(argc <= argi);
                }

                static TString GetCurrentTime(TString = "%Y-%m-%d at %H:%M:%S");

                void Print(bool = false);
        };
}
#endif
