/**
 * @Author: Marco Meyer <meyerma>
 * @Date:   2019-03-27T01:29:35-05:00
 * @Email:  marco.meyer@cern.ch
 * @Last modified by:   meyerma
 * @Last modified time: 2019-03-30T15:33:13-05:00
 */



/**
 * *********************************************
 *
 * \file HandboxStyle.h
 * \brief Header of the HandboxStyle class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 * *********************************************
 *
 * \class HandboxStyle
 * \brief ..
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date August 11th, 2017
 *
 * *********************************************

 */

#ifndef HandboxStyle_H
#define HandboxStyle_H

#include <vector>

#include <TSystem.h>
#include <TROOT.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TClass.h>

#include <TApplication.h>
#include <TObject.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>
#include <UIUC/HandboxMsg.h>
#include <TPaveStats.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TFrame.h>
#include <TRandom.h>
#include <TPaletteAxis.h>
#include <TLatex.h>
#include <TStyle.h>

typedef struct TObjectStyle {

        Color_t line;
        Color_t marker;

        double fillalpha;
        Color_t fill;

        void SetColor(TObject *obj)
        {
                SetLineColor(obj);
                SetMarkerColor(obj);
                SetFillColorAlpha(obj);
        }

        void SetLineColor(TObject *obj)
        {
                if(line == -1) return;
                if(obj == NULL) return;
                if(obj->InheritsFrom("TH1")) {

                        TH1 *h = (TH1*)obj;
                        h->SetLineColor(line);

                } else if (obj->InheritsFrom("TF1")) {

                        TF1 *f = (TF1*)obj;
                        f->SetLineColor(line);

                } else if (obj->InheritsFrom("TGraph")) {

                        TGraph *g = (TGraph*)obj;
                        g->SetLineColor(line);

                } else if (obj->InheritsFrom("TGraph2D")) {

                        TGraph2D *g = (TGraph2D*)obj;
                        g->SetLineColor(line);

                } else {

                        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Object passed as argument is not supported.. (to set custom color)");
                }
        }

        void SetMarkerColor(TObject *obj)
        {
                if(marker == -1) return;
                if(obj == NULL) return;
                if(obj->InheritsFrom("TH1")) {

                        TH1 *h = (TH1*)obj;
                        h->SetMarkerColor(marker);

                } else if (obj->InheritsFrom("TF1")) {

                        TF1 *f = (TF1*)obj;
                        f->SetMarkerColor(marker);

                } else if (obj->InheritsFrom("TGraph")) {

                        TGraph *g = (TGraph*)obj;
                        g->SetMarkerColor(marker);

                } else if (obj->InheritsFrom("TGraph2D")) {

                        TGraph2D *g = (TGraph2D*)obj;
                        g->SetMarkerColor(marker);

                } else {

                        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Object passed as argument is not supported.. (to set custom color)");
                }
        }

        void SetFillColorAlpha(TObject *obj)
        {
                if(fill == -1) return;
                if(fillalpha == -1) fillalpha = 1;

                if(obj == NULL) return;
                if(obj->InheritsFrom("TH1")) {

                        TH1 *h = (TH1*)obj;
                        if(fillalpha != 0) h->SetFillColorAlpha(fill, fillalpha);

                } else if (obj->InheritsFrom("TF1")) {

                        TF1 *f = (TF1*)obj;
                        if(fillalpha != 0) f->SetFillColorAlpha(fill, fillalpha);

                } else if (obj->InheritsFrom("TGraph")) {

                        TGraph *g = (TGraph*)obj;
                        if(fillalpha != 0) g->SetFillColorAlpha(fill, fillalpha);

                } else if (obj->InheritsFrom("TGraph2D")) {

                        TGraph2D *g = (TGraph2D*)obj;
                        if(fillalpha != 0) g->SetFillColorAlpha(fill, fillalpha);

                } else {

                        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Object passed as argument is not supported.. (to set custom color)");
                }
        }

        void CopyColor(TObject *obj)
        {
                if(obj == NULL) return;
                if(obj->InheritsFrom("TH1")) {

                        TH1 *h = (TH1*)obj;
                        line = h->GetLineColor();
                        marker = h->GetMarkerColor();
                        fill = h->GetFillColor();

                } else if (obj->InheritsFrom("TF1")) {

                        TF1 *f = (TF1*)obj;
                        line = f->GetLineColor();
                        marker = f->GetMarkerColor();
                        fill = f->GetFillColor();

                } else if (obj->InheritsFrom("TGraph")) {

                        TGraph *g = (TGraph*)obj;
                        line = g->GetLineColor();
                        marker = g->GetMarkerColor();
                        fill = g->GetFillColor();

                } else if (obj->InheritsFrom("TGraph2D")) {

                        TGraph2D *g = (TGraph2D*)obj;
                        line = g->GetLineColor();
                        marker = g->GetMarkerColor();
                        fill = g->GetFillColor();

                } else {

                        UIUC::HandboxMsg::PrintWarning(__METHOD_NAME__, "Object passed as argument is not supported.. (to set custom color)");
                }
        }

} TObjectStyle;


namespace UIUC {
        class HandboxStyle: public TNamed
        {
                protected:
                static int styleId;
                static std::vector< UIUC::HandboxStyle* > style;
                static std::vector< std::vector<TStyle* >> styleMap;
                static std::vector<TString> styleName;

                static bool Register(HandboxStyle *, std::vector<TStyle*>);
                HandboxStyle(const char* name, const char* title);

                public:
                static const int kDataStyle      ;
                static const int kMonteCarloStyle;
                static const int kBkgStyle       ;
                static const int kFitStyle       ;

                static bool bBuildLegend;
                static bool bShowLegend;

                static UIUC::HandboxStyle& Instance() {

                        static UIUC::HandboxStyle _MainInstance("Default", "");
                        return _MainInstance;
                };

                // Constructor, destructor
                virtual ~HandboxStyle() {

                        // std::vector<TString> defaultStyles = {
                        //         "Plain",  "Bold","Video","Pub",
                        //         "Classic","Default","Modern","ATLAS",
                        //         "BELLE2"}; // Issue with clone ? ROOT internal removal ?

                        // for(int i = 0, I = styleMap.size(); i < I; i++) {

                        //         unsigned int index = find(defaultStyles.begin(), defaultStyles.end(), styleName[i]) - defaultStyles.begin();
                        //         if( index != defaultStyles.size() ) {

                        //                 styleMap[i].clear();
                        //                 continue;
                        //         }

                        //         for(int j = 0, J = styleMap[i].size(); i < J; j++)
                        //                 styleMap[i][j]->Delete();

                        //         styleMap[i].clear();
                        // }

                        // styleMap.clear();
                }

                // Canvas layout
                inline HandboxStyle* GetCustomStyle() {
                        if(this->styleName.size() <= (unsigned int) this->styleId || this->styleId < 0) return NULL;
                        return this->style[this->styleId];
                }

                inline TString GetCustomStyleName() {
                        if(this->styleName.size() <= (unsigned int) this->styleId || this->styleId < 0) return "";
                        return this->styleName[this->styleId];
                }

                bool UseStyle(TString = "");
                bool ApplyStyle(TObject*, int = 0);
                bool ApplyStyle(TObject*, TString);

                bool SetTag(TVirtualPad*, TString);
                inline bool SetPreliminaryTag(TVirtualPad*vpad, TString prelim = "Preliminary") {
                        return SetTag(vpad, prelim);
                };

                virtual std::vector<TObjectStyle> GetDefaultObjectStyle(TList* lObject) {

                        UNUSED(lObject);
                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Default color options will be used..");
                        return {};
                };

                std::vector<TObjectStyle> GetDefaultObjectStyle0(TList* lObject) {

                        HandboxStyle* hStyle = this->GetCustomStyle();
                        if(hStyle == NULL) return {};

                        return hStyle->GetDefaultObjectStyle(lObject);
                }

                std::vector<TObjectStyle> GetDefaultObjectStyle0(TObject* obj) {

                        TList *lObject = new TList;
                        lObject->Add(obj);

                        std::vector<TObjectStyle> v = GetDefaultObjectStyle0(lObject);

                        delete lObject;
                        lObject = NULL;

                        return v;
                };

                virtual bool SetCustomAxis(TList* lObject) {

                        UNUSED(lObject);
                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Default axis options used..");
                        return 1;
                }

                bool SetCustomAxis0(TObject* obj) {

                        TList *lObject = new TList;
                        lObject->Add(obj);

                        bool b = SetCustomAxis0(lObject);

                        delete lObject;
                        lObject = NULL;

                        return b;
                };

                bool ApplyStyle(TList*, TString);

                bool SetCustomAxis0(TList* lObject) {

                        HandboxStyle* hStyle = this->GetCustomStyle();
                        if(hStyle == NULL) return 0;

                        bool b = hStyle->SetCustomAxis(lObject);

                        double xmin = TString(gSystem->Getenv("ESCALADE_XMIN")).Atof();
                        double xmax = TString(gSystem->Getenv("ESCALADE_XMAX")).Atof();
                        double ymin = TString(gSystem->Getenv("ESCALADE_YMIN")).Atof();
                        double ymax = TString(gSystem->Getenv("ESCALADE_YMAX")).Atof();
                        double zmin = TString(gSystem->Getenv("ESCALADE_ZMIN")).Atof();
                        double zmax = TString(gSystem->Getenv("ESCALADE_ZMAX")).Atof();

                        for(int i = 0, N = lObject->GetSize(); i < N; i++) {

                                if(lObject->At(i) == NULL) continue;

                                if( ((TH1*)lObject->At(i))->InheritsFrom("TH1") ) {

                                        if(gSystem->Getenv("ESCALADE_XMIN") && gSystem->Getenv("ESCALADE_XMAX") && ((TH1*)lObject->At(i))->GetXaxis()) ((TH1*)lObject->At(i))->GetXaxis()->SetRangeUser(xmin, xmax);
                                        if(gSystem->Getenv("ESCALADE_YMIN") && gSystem->Getenv("ESCALADE_YMAX") && ((TH1*)lObject->At(i))->GetYaxis()) ((TH1*)lObject->At(i))->GetYaxis()->SetRangeUser(ymin, ymax);
                                        if(gSystem->Getenv("ESCALADE_ZMIN") && gSystem->Getenv("ESCALADE_ZMAX") && ((TH1*)lObject->At(i))->GetZaxis()) ((TH1*)lObject->At(i))->GetZaxis()->SetRangeUser(zmin, zmax);

                                } else if( ((TH1*)lObject->At(i))->InheritsFrom("TGraph") ) {

                                        if(gSystem->Getenv("ESCALADE_XMIN") && gSystem->Getenv("ESCALADE_XMAX") && ((TGraph*)lObject->At(i))->GetXaxis()) ((TGraph*)lObject->At(i))->GetXaxis()->SetRangeUser(xmin, xmax);
                                        if(gSystem->Getenv("ESCALADE_YMIN") && gSystem->Getenv("ESCALADE_YMAX") && ((TGraph*)lObject->At(i))->GetYaxis()) ((TGraph*)lObject->At(i))->GetYaxis()->SetRangeUser(ymin, ymax);
                                }
                        }

                        return b;
                };

                virtual TPaveStats* SetCustomStatbox(TVirtualPad* pad, TList* lObject) {

                        UNUSED(lObject);
                        UNUSED(pad);

                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Default statbox used..");
                        return (TPaveStats*) pad->GetPrimitive("stats");
                }

                TPaveStats* SetCustomStatbox0(TVirtualPad* pad, TObject* obj) {

                        TList *lObject = new TList;
                        lObject->Add(obj);

                        TPaveStats* b = SetCustomStatbox0(pad, lObject);

                        delete lObject;
                        lObject = NULL;

                        return b;
                };

                TPaveStats* SetCustomStatbox0(TVirtualPad* pad, TList* lObject) {

                        HandboxStyle* hStyle = this->GetCustomStyle();
                        if(hStyle == NULL) return 0;

                        if(gSystem->Getenv("ESCALADE_NOSTATBOX"))
                                ((TH1*)lObject->First())->SetStats(0);

                        //if( !(TString(((TNamed*) lObject->First())->GetName()).BeginsWith("Statbox") && lObject->GetSize() > 2) || lObject->GetSize() < 2 ) return 0;
                        //else if ( !(lObject->GetSize() > 1) ) return 0;

                        return hStyle->SetCustomStatbox(pad, lObject);
                };



                bool SetCustomLegend0(TVirtualPad* pad, TList* lObject) {

                        HandboxStyle* hStyle = this->GetCustomStyle();
                        if(hStyle == NULL) return 0;

                        // Remove the reference clone "StatboxXXX", it is an empty clone of h0
                        if(lObject == NULL) return 0;
                        lObject = (TList*) lObject->Clone();
                        if ( TString(((TNamed*) lObject->First())->GetName()).BeginsWith("Statbox") ) lObject->Remove(lObject->First());
                        if(lObject->GetEntries() < 2) return 0;

                        for(int i = 0; i < lObject->GetEntries(); i++) {

                                TNamed *objnamed = (TNamed*) lObject->At(i);
                                if(objnamed == NULL) continue;

                                if(TString(objnamed->GetTitle()).EqualTo("")) objnamed->SetTitle(objnamed->GetName());
                        }
                        return hStyle->SetCustomLegend(pad, lObject);
                };

                virtual bool SetCustomLegend(TVirtualPad* pad, TList* lObject) {

                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Default axis options used..");
                        if(pad == NULL) return 0;
                        if(lObject->GetSize() == 0) return 0;

                        TObject *obj = NULL;
                        TIter Next(lObject);

                        int nString = 1; // = 1, because of the title..
                        int nCurves = 1; // = 1, because of the title..
                        for(int i = 0; (obj = Next()); i++) {

                                if(obj->InheritsFrom("TObjString")) nString++;
                                else nCurves++;
                        }
                        pad->Update();

                        TH1 *h = NULL;
                        TIter Next2(pad->GetListOfPrimitives());
                        while(( obj = (TObject*) Next2()) ) {

                                if(h == NULL && obj->InheritsFrom("TH1")) h = (TH1*) obj;
                        }

                        TPaveStats *st=NULL;
                        if(h != NULL) st = (TPaveStats*) h->FindObject("stats");
                        int nStats = (!st) ? 0 : st->GetListOfLines()->GetSize();
                        int nStatLines = 4;
                        //if(obj0->InheritsFrom("TGraph3D") || obj0->InheritsFrom("TH3")) nStatLines = 4;
                        //else if(obj0->InheritsFrom("TGraph2D") || obj0->InheritsFrom("TH2")) nStatLines = 6;
                        //else if(obj0->InheritsFrom("TGraph") || obj0->InheritsFrom("TH1")) nStatLines = 8;

                        double line_height = gStyle->GetStatH()/nStatLines;     // Classic value.. (4 lines is the Classic value defined in gStyle whatever the option)
                        double spacer = 0.01;
                        double x1,y1,x2,y2;

                        double offset = 0;
                        if(!st) offset = 0.016;
                        if(nCurves > 1) {

                                x2 = gStyle->GetStatX();
                                x1 = gStyle->GetStatX()-gStyle->GetStatW();
                                y2 = gStyle->GetStatY()-nStats*line_height-(nStats>0)*spacer + offset;
                                y1 = gStyle->GetStatY()-nStats*line_height-(nStats>0)*spacer-nCurves*line_height + offset;

                                TLegend *fLegend = new TLegend(x1, y1, x2, y2);

                                Next.Reset();
                                for(int i = 0; (obj = Next()); i++) {

                                        if(!obj->InheritsFrom("TObjString")) fLegend->AddEntry(obj);
                                }
                                fLegend->Draw();
                        }

                        //Build legend for extra information..
                        if(nString > 1) {

                                x2 = gStyle->GetStatX();
                                x1 = gStyle->GetStatX()-gStyle->GetStatW();
                                y2 = gStyle->GetStatY()-nStats*line_height-nCurves*line_height-(nStats>0)*spacer-(nCurves>1)*spacer + offset;
                                y1 = gStyle->GetStatY()-nStats*line_height-nCurves*line_height-(nStats>0)*spacer-(nCurves>1)*spacer-nString*line_height + offset;

                                TLegend *fLegend2 = new TLegend(x1, y1, x2, y2);
                                fLegend2->SetHeader("Additional :");

                                Next.Reset();
                                for(int i = 0; (obj = Next()); i++) {

                                        if(i > 0 && obj->InheritsFrom("TObjString")) fLegend2->AddEntry((TObject*) NULL, ((TObjString*) obj)->GetString(), "");
                                }
                                fLegend2->Draw();
                        }

                        return 1;
                };

                TPaveText* SetCustomTitle0(TVirtualPad *vpad, TObject *obj)
                {
                        HandboxStyle* hStyle = this->GetCustomStyle();
                        if(hStyle == NULL) return 0;

                        return hStyle->SetCustomTitle(vpad, obj->GetTitle());
                };

                TPaveText* SetCustomTitle0(TVirtualPad *vpad, TString title)
                {
                        HandboxStyle* hStyle = this->GetCustomStyle();
                        if(hStyle == NULL) return 0;

                        return hStyle->SetCustomTitle(vpad, title);
                };

                virtual TPaveText* SetCustomTitle(TVirtualPad* vpad, TString title) {

                        UNUSED(vpad);
                        TPaveText *pt = (TPaveText*)(vpad->GetPrimitive("title"));
                        if(pt == NULL) return NULL;

                        TPaveText* ptn = (TPaveText*) pt->Clone();
                        ptn->SetName("title0");
                        ptn->Clear();
                        ptn->AddText(title);
                        ptn->SetTextSize(0.035);
                        ptn->SetTextFont(143);

                        //gStyle->SetOptTitle(0);
                        //pt->Clear();
                        return ptn;
                }

                std::vector<TString> SetCustomOptions0(TList *lObject, std::vector<TString> vOption = {})
                {
                        std::vector<TString> vOpt;
                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Default options used..");

                        if( TString(((TNamed*) lObject->First())->GetName()).BeginsWith("Statbox") && lObject->GetSize() > (int) vOption.size()) {
                                if(vOption.size() > 0) vOption.insert(vOption.begin(), "");
                        }

                        HandboxStyle* hStyle = this->GetCustomStyle();
                        if(hStyle == NULL) vOpt.resize(lObject->GetSize());
                        else {

                                vOption.resize(lObject->GetSize());
                                vOpt = hStyle->SetCustomOptions(lObject, vOption);
                                if(UIUC::HandboxMsg::Error((int) vOpt.size() != lObject->GetEntries(), __METHOD_NAME__,
                                                           "List of object size != Returned option vector size")) return {};
                        }

                        for(int i = 0, N = lObject->GetSize(); i < N; i++) {

                                vOpt[i].ReplaceAll("SAME", "");

                                if(lObject->At(i)->InheritsFrom("TGraph")) vOpt[i] += (i == 0) ? " AP " : " P ";
                                else vOpt[i] += (i == 0) ? "" : " SAME ";
                        }

                        return vOpt;
                };

                virtual std::vector<TString> SetCustomOptions(TList *lObject, std::vector<TString> vOption = {}) {

                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Default options used..");

                        std::vector<TString> vOpt(lObject->GetSize(), "");
                        for(int i = 0, N = lObject->GetSize(); i < N; i++) {

                                TString opt = TString(vOption[i]);
                                vOpt[i].ReplaceAll(opt, "");
                                vOpt[i] += opt;
                        }

                        return vOpt;
                }

                static TString GetStyle(TString style);

                static void ShowLegend(bool b = 1)
                {
                        bShowLegend = b;
                        if(gSystem->Getenv("ESCALADE_NOLEGEND")) bShowLegend = 0;
                }

                virtual bool BuildCanvas(TString, TVirtualPad*, TList*, std::vector<TString> = {}, std::vector<TObjectStyle> = {});
                virtual bool BuildCanvas(TString, TVirtualPad*, std::vector<TObject*>, std::vector<TString> = {}, std::vector<TObjectStyle> = {});
                virtual bool BuildCanvas(TString, TVirtualPad*, TList*, TString, std::vector<TObjectStyle> = {});
                virtual bool BuildCanvas(TString, TVirtualPad*, std::vector<TObject*>, TString, std::vector<TObjectStyle> = {});

                virtual bool BuildCanvas(TVirtualPad*, TList*, std::vector<TString> = {}, std::vector<TObjectStyle> = {});
                virtual bool BuildCanvas(TVirtualPad*, std::vector<TObject*>, std::vector<TString> = {}, std::vector<TObjectStyle> = {});
                virtual bool BuildCanvas(TVirtualPad*, TList*, TString, std::vector<TObjectStyle> = {});
                virtual bool BuildCanvas(TVirtualPad*, std::vector<TObject*>, TString, std::vector<TObjectStyle> = {});

                virtual TCanvas* BuildExample(TString);

                ClassDef(UIUC::HandboxStyle,1);
        };
}

#endif

