/**
 * @Author: Marco Meyer <meyerma>
 * @Date:   2019-03-27T01:29:35-05:00
 * @Email:  marco.meyer@cern.ch
 * @Last modified by:   meyerma
 * @Last modified time: 2019-03-30T15:33:55-05:00
 */



/**
 * *********************************************
 *
 * \file Handbox.h
 * \brief Header of the Handbox class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 * *********************************************
 *
 * \class Handbox
 * \brief ..
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date August 11th, 2017
 *
 * *********************************************

 */


#ifndef Handbox_H
#define Handbox_H

#include <Riostream.h>
#include <vector>
#include <TROOT.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TApplication.h>
#include <TPDF.h>
#include <TObject.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>
#include <TBrowser.h>
#include <TCanvas.h>

#include <TTree.h>

#include <UIUC/HandboxStyle.h>
#include <UIUC/HandboxUsage.h>
#include <UIUC/HandboxMsg.h>
#include <UIUC/TFileReader.h>

#include <thread>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

namespace UIUC {

/*
 * Main class..
 */

        class Handbox
        {
                protected:

                bool kRunStarted;

                std::vector<TString> vArg;
                std::vector<bool>  vArgMandatory;
                std::vector<TString> vArgOption;

                std::vector<TObject*> fInput;
                std::vector<TCanvas*> fCanvas;
                std::vector<std::vector<TString>> fIdentifiers;
                std::vector<std::vector<TObject* >> fTempo;
                std::vector<std::vector<TObject* >> fOutput;
                std::vector<std::vector<bool>> fOutputDeleteSafely;
                std::vector<std::vector<TString>> fOutputDir;
                std::vector<TString> fOutputfqfn;

                std::vector<TFile*> vDumpFile;
                TString PrefixDumpFile;

                TString fInputPattern;
                TString fOutputPattern;
                TString fOutputFinalizeStr;

                int iFinalize;
                int iOutputSlot;
                int iOutput;

                double stopwatch = 0;
                static TString RunOption;

                int sigPrepareErr  = 0;
                int sigFinalizeErr = 0;
                int sigProcessErr  = 0;

                public:

                static TDirectory *gRootDirectory;
                static bool bProcessLoop;

                // Main variables
                static const bool kClone;
                static const int kDefaultSlot;
                
                static const bool kDirectory;
                static const bool kSingleFile;
                static const int kTextFile;
                static const int kRootFile;
                
                static const bool kRemoveNullPtr;
                static const bool kKeepNullPtr;
                
                static UIUC::HandboxStyle& InstanceStyle() {
                        return UIUC::HandboxStyle::Instance();
                };

                static UIUC::HandboxUsage& InstanceUsage() {
                        return UIUC::HandboxUsage::Instance(0,NULL,"");
                }

                Handbox(int quiet = 0, int debug = 0) {

                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Instanciate handbox..");

                        TString eMessage;
                        UIUC::HandboxMsg::kDebug = TMath::Max(UIUC::HandboxMsg::kDebug, debug);
                        if(!UIUC::HandboxMsg::IsDebugEnabled()) {

                                UIUC::HandboxMsg::kQuiet |= quiet;
                                if(quiet > 1) UIUC::HandboxMsg::kSuperQuiet |= quiet;
                        }

                        PrefixDumpFile = "escalade-dump";

                        fInputPattern = "";
                        fOutputPattern = "";

                        fOutputFinalizeStr = "";
                        kRunStarted = false;
                        iOutputSlot = 0;
                        iOutput = 0;
                        
                        UIUC::Handbox::gRootDirectory = gDirectory;
                        UIUC::Handbox::InstanceUsage();
                        UIUC::Handbox::InstanceStyle();
                };

                int nThreads = 1;
                int finishedWorkers = 0;
                std::mutex workerMutex;
                std::vector<std::thread *> workers;
                void SetThreadPoolSize(int _nThreads)
                {
                        if(_nThreads > 1) ROOT::EnableImplicitMT( TMath::Min(ROOT::GetThreadPoolSize(), std::thread::hardware_concurrency()) );
                        else if(nThreads != 1) ROOT::DisableImplicitMT();

                        nThreads = _nThreads;
                }

                void CleanWorkers()
                {
                        while(workers.size() > 0) {
                        
                                std::thread *t = workers[workers.size() - 1];
                                workers.pop_back();

                                delete t;
                        }        
                }

                bool WaitForWorkersToBeDone()
                {
                        for(auto t : workers)
                                t->join();
        
                        return 1;
                }
        
                virtual ~Handbox()
                {
                        if(this->sigPrepareErr || this->sigProcessErr || this->sigFinalizeErr) {

                                UIUC::HandboxUsage::bCallGraphics = true;
                                UIUC::HandboxUsage::bCallHistos = true;
                        }

                        this->CleanTempo();
                        this->CleanInput();
                        this->CleanOutput();

                        this->CleanWorkers();

                        UNUSED(this->vDumpFile.empty());

                        UIUC::HandboxMsg::DecrementTab();
                        if(this->InstanceUsage().GetStatusUsage()) {

                                if(!kRunStarted) UIUC::HandboxMsg::PrintDebug(10, __METHOD_NAME__, "Handbox exited without starting..");
                                else {

                                        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "DOOOM ! (Please wait while cleaning memory..)");
                                        TDatime eot;

                                        double lap = UIUC::microtime() - this->stopwatch;
                                        
                                        UIUC::HandboxMsg::PrintMessage(__METHOD_NAME__, "End of time: " + UIUC::HandboxMsg::kPurple + (TString) eot.AsString() + UIUC::HandboxMsg::kOrange + " " + Form("(execution time ~ %.2fs",lap) + ")" + UIUC::HandboxMsg::kNoColor);
                                }
                        }

                        UIUC::HandboxMsg::EmptyTrash();
                };

                bool FindInput(TObject*, int = UIUC::Handbox::kDefaultSlot);
                bool FindTempo(TObject*, int = UIUC::Handbox::kDefaultSlot, int = UIUC::Handbox::kDefaultSlot);
                bool FindOutput(TObject*, int = UIUC::Handbox::kDefaultSlot, int = UIUC::Handbox::kDefaultSlot);

                bool SerialProcessing();

                static void EnableSignalHandler();
                static void DisableSignalHandler();

                static void EnableBacktraceHandler();
                static void DisableBacktraceHandler();

                bool CleanInput(int index = -1);
                bool CleanTempo(int slot = -1, int index= -1);
                bool CleanOutput(int slot = -1, int index= -1);

                bool ResizeTempo(int slot = -1);
                bool ResizeOutput(int slot = -1);

                int  GetInputSize();
                inline int GetN() { return GetInputSize(); }

                int  GetOutputSize(int slot = -1);
                int  GetTempoSize(int slot = -1);

                TObject* GetOutput(int slot, int index = -1);
                std::vector<TObject* > GetOutputBuffer(int slot , bool = UIUC::Handbox::kKeepNullPtr);
                std::vector<std::vector<TObject* >> GetOutputBuffer(bool = UIUC::Handbox::kKeepNullPtr);

                TObject* GetTempo(int, int = -1);
                std::vector<TObject* > GetTempoBuffer(int slot, bool = UIUC::Handbox::kKeepNullPtr);
                std::vector<std::vector<TObject* >> GetTempoBuffer(bool = UIUC::Handbox::kKeepNullPtr);

                TString GetInputPattern();
                bool SetInputPattern(TString);
                TString GetOutputPattern();
                bool SetOutputPattern(TString);
                bool SetOutputSlot(int);

                bool outputFileCall = false;
                TFile *GetOutputFile(int index = -1);

                int  GetNumberOfTempoSlot() { return this->fTempo.size(); };
                int  GetNumberOfOutputSlot() { return this->fOutput.size(); };

                inline bool IsDebugEnabled(int level = 1) { return UIUC::HandboxMsg::IsDebugEnabled(level); };
                inline static bool DeleteSafelyObject(TObject* &obj) { return UIUC::Handbox::DeleteSafelyObject(obj); }
                inline static bool DeleteSafelyObjectPtr(TObject* obj) { return UIUC::Handbox::DeleteSafelyObjectPtr(obj); }

                static void HandlerCTRL_C(int);
                static void HandlerBacktrace(int);
                TString GetRunOption() { return this->RunOption; };

                bool AddInput(TFileReader*, bool bChain = false);
                bool AddInput(TFileReader&, bool bChain = false);

                bool AddInput(int);
                bool AddInput(TObject*, std::vector<TString> = std::vector<TString>());
                bool AddInput(std::vector<TKey*>, std::vector<std::vector<TString>> = std::vector<std::vector<TString>>());
                bool AddInput(std::vector<TString>, std::vector<std::vector<TString>> = std::vector<std::vector<TString>>());
                bool AddInput(std::vector<TObject*>, std::vector<std::vector<TString>> = std::vector<std::vector<TString>>());

                inline TObject* GetInput(int i = -1) {
                        if(i < 0) i = this->iOutput;
                        return (i < (int) this->fInput.size()) ? this->fInput[i] : NULL;
                }

                TString GetGlobalIdentifier(std::vector<TString>);
                inline TString GetGlobalIdentifier(int i = -1)
                {
                        if(i < 0) i = this->iOutput;
                        return this->GetGlobalIdentifier(this->fIdentifiers[i]);
                }

	        inline static bool sortIdentifiers (std::pair<TString,int> a, std::pair<TString,int> b) { return a.first < b.first; };
                std::vector<TString> FirstIdentifier();
                std::vector<TString> LastIdentifier();

                inline std::vector<TString> GetIdentifier(int i = -1)
                {
                        if(i < 0) i = this->iOutput;
                        return (i < (int) this->fIdentifiers.size()) ? this->fIdentifiers[i] : std::vector<TString>();
                }

                inline bool AddOutput(TObject* obj, int slot = UIUC::Handbox::kDefaultSlot) { return AddOutput(obj, "", slot); }
                inline bool AddOutput(std::vector<TObject*> vObj, int slot = UIUC::Handbox::kDefaultSlot) {
                        std::vector<TString> dir(vObj.size(), "");
                        return AddOutput(vObj, dir, slot);
                }

                inline bool AddOutput(std::vector<TObject*> vObj, TString dir, int slot = UIUC::Handbox::kDefaultSlot) {
                        std::vector<TString> vDir(vObj.size(), dir);
                        return AddOutput(vObj, vDir, slot);
                }

                inline bool AddOutput(std::vector<TObject*> vObj, std::vector<TString> vDir, int slot = UIUC::Handbox::kDefaultSlot) {

                        bool b = true;

                        vDir.resize(vObj.size(), "");
                        for(int i = 0, N = vObj.size(); i < N; i++) {
                                b &= AddOutput(vObj[i], vDir[i], slot);
                        }
                        return b;
                }

                bool AddOutput(TObject*, TString, int = UIUC::Handbox::kDefaultSlot);
                bool RemoveOutput(TObject*,int = UIUC::Handbox::kDefaultSlot);

                TString GetOutputPath(int i);

                bool AddTempo(TObject*, int slot, int id);
                bool AddTempo(std::vector<TString>, int slot = -1);
                bool AddTempo(std::vector<TKey*>, int slot = -1);
                bool AddTempo(std::vector<TObject*>, int slot = -1);
                bool AddTempo(TFileReader &, int slot = -1);
                bool AddTempo(TFileReader *, int slot = -1);
                int AllocateTempoSlot(int slot = UIUC::Handbox::kDefaultSlot);

                inline int Run(Option_t *option = "", TClass *cl = NULL) {
                        return this->Run(option, (cl != NULL) ? cl->GetName() : "");
                };

                int Run(Option_t*, TString);

                bool WarmUp(Option_t *);
                bool FinalizeRun();

                virtual bool Init()
                {
                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Nothing to do..");
                        return 0;
                }

                int Wrapper(int, int, int, TString, TString);
                virtual int Process(TObject*, int)
                {
                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Nothing to do..");
                        return 0;
                }

                virtual int Finalize()
                {
                        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Nothing to do..");
                        return 0;
                }

                virtual void Print(int = UIUC::Handbox::kDefaultSlot);
                virtual void Save(int = -1);

                inline void SetPrefixDumpFile(TString prefix) {
                        this->PrefixDumpFile = prefix;
                }
        };
}

#endif
