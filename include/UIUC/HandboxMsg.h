/**
 * @Author: Marco Meyer <meyerma>
 * @Date:   2019-03-27T01:29:35-05:00
 * @Email:  marco.meyer@cern.ch
 * @Last modified by:   meyerma
 * @Last modified time: 2019-03-30T15:33:12-05:00
 */



/**
 * *********************************************
 *
 * \file Handbox.h
 * \brief Header of the Handbox class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 * *********************************************
 *
 * \class Handbox
 * \brief ..
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date August 11th, 2017
 *
 * *********************************************

 */

#ifndef HandboxMsg_H
#define HandboxMsg_H

#include <Riostream.h>

#include <sys/stat.h>
#include <map>
#include <sstream>
#include <vector>

#include <TSystem.h>
#include <TH1.h>
#include <TGraph.h>

#include <TObject.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>
#include <TROOT.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <unistd.h>
#include <sstream>
#include <UIUC/MathUtils.h>
#include <sys/ioctl.h>

// Add a defined function to get the method name
inline TString MethodName(const TString& prettyFunction)
{
        TString function_name = prettyFunction;
        TString function_args = prettyFunction;
        int colons = function_name.First('(');

        function_name = function_name(0, colons);
        int space = function_name.Last(' ');
        function_name = function_name(space+1, function_name.Length()-space+1);

        function_args = function_args(colons, function_args.Length()-colons+1);

        function_name = (TString) function_name.Strip(TString::kBoth, '*');
        return function_name.EqualTo("main") ? "Main" : function_name;
        //return function_name + function_args;
}

#define __METHOD_NAME__ MethodName(__PRETTY_FUNCTION__)
#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

namespace UIUC {
        class HandboxMsg
        {
            private:

                static int     OpenTTY(int desc, TString fname = "");
                static TString CloseTTY(int desc);
                static bool    ValidTTY(TString fname);
                static TString ReadTTY(TString fname);

            public:

                static int iTTY;
                static int iFailedTTY;
                static int kStdOut_dup;
                static int kStdErr_dup;
                static std::vector<int> kStdOut;
                static std::vector<TString> kStdOut_fname;
                static std::vector<int> kStdErr;
                static std::vector<TString> kStdErr_fname;

                static std::vector<TString> kTrash;

                static std::vector<int> asserts;
                static TString CreateTemporaryPath(TString format = "/tmp/tmp.XXXXXX");
                
                inline static void ReadAndWrite(TString fname, TString regex_pattern, TString replacement) { UIUC::HandboxMsg::ReadAndWrite(fname, std::vector<TString>({regex_pattern}), std::vector<TString>({replacement})); }
                       static void ReadAndWrite(TString fname, std::vector<TString> regex_pattern, std::vector<TString> replacement);
                
		inline static bool FileExists (const char *name) {
			struct stat buffer;
			return (stat (name, &buffer) == 0);
		}

                // Main variables
                static int kDebug;
                static bool kQuiet, kSuperQuiet;
                static bool kCarriageReturn;

                static bool bShortRedirectTTY;
                static const bool kIgnoreTab;
                static const bool kIncrementTab;
                
                static const TString kNoColor;
                static const TString kRed;
                static const TString kGreen;
                static const TString kOrange;
                static const TString kBlue;
                static const TString kPurple;

                static const TString kOrangeBliking;

                // Constructor, destructor
                virtual ~HandboxMsg() {};


                static void EmptyTrash() {

                        while(kTrash.size() > 0) {

                                if(FileExists(kTrash.back())) remove(kTrash.back());
                                kTrash.pop_back();
                        }
                }

                static TString Trim(const char* str) {

                        TString str0 = str;
                        while(str0.BeginsWith("\n") || str0.BeginsWith(" ") || str0.BeginsWith("\r") || str0.BeginsWith("\t")) str0 = str0(1, str0.Length());
                        while(str0.EndsWith("\n") || str0.EndsWith(" ") || str0.EndsWith("\r") || str0.EndsWith("\t")) str0 = str0(0, str0.Length()-1);

                        return str0;
                }

                static int kTabular;
		static const char kTabularChar;

                static bool IsQuiet() {
                        return UIUC::HandboxMsg::kQuiet || UIUC::HandboxMsg::kSuperQuiet;
                }
                static bool IsSuperQuiet() {
                        return UIUC::HandboxMsg::kSuperQuiet;
                }

                static TString Endl()
                {
                        return ( UIUC::HandboxMsg::IsQuiet() ) ? "" : TString('\n');
                }

                template <typename T>
                static TString Address(T *ptr)
                {
                        const void * address = static_cast<const void*>(ptr);
                        std::stringstream ss; ss << address;  
                        return TString(ss.str()); 
                }
                
                static TString CarriageReturn(bool bClean = false)
                {
                        
                        if(bClean) {
                
                                printf("%c[2K", 27);
                                return "";
                        }

                        return TString('\r');
                }


                static TString Readlink(TString fname) {

                    char buff[1024], buff2[1024];
                    for(int i = 0, N = fname.Length(); i < N; i++)
                        buff[i] = fname[i];
		    buff[fname.Length()] = '\0';

                    memset(buff2, 0, sizeof(buff2));
                    if(readlink(buff, buff2, sizeof(buff2)-1) < 0) strcpy(buff2,buff);

                    realpath(buff2, buff);
                    return TString(buff);
                }

                static TString Readlink(int fd) {

                    if(fd < 0) return "";

                    char buff[1024];
                    snprintf(buff, 1024, "/proc/self/fd/%d", fd);

                    TString fname = TString(buff);
                    return Readlink(fname);
                }

                static TString Readlink(FILE *f) {

                    return Readlink(fileno(f));
                }

                static bool bSingleSkipIncrementTab;
                static void SetSingleSkipIncrementTab() {
                        bSingleSkipIncrementTab = true;
                }
                static bool bSingleCarriageReturn;
                static void SetSingleCarriageReturn() {
                        bSingleCarriageReturn = true;
                }

                static void ClearLine() {

                        cout << UIUC::HandboxMsg::CarriageReturn(1) << flush;
                        SetSingleCarriageReturn();
                }

                static void PressAnyKeyToContinue(int, const char*, const char* = "", ...);
                
                static std::chrono::time_point<std::chrono::system_clock> progressBarTime;
                static TString GetProgressBar(const char*, int, int, int = 1);
                static void  PrintProgressBar(const char*, int, int, int = 1);
                
                static TString GetDate(long long timestamp, TString format = "%H:%M:%S");
                static TString GetTime(long long timestamp, TString format = "%Hh %Mm %Ss");

                static TString GetTestBar(const char*);
                static void  PrintTestBar(const char*);
                static bool  RunTests    (const char*);
                static void  PrintTests  (const char*);
                static void  ScheduleTests(int N);
                static void  Assert(const char*, bool b, int pos = -1);

                static void ResetTab();
                static void IncrementTab();
                static void DecrementTab();
                static TString Tab();
                static TString Spacer(int, char = ' ');

                static int CharCount(int N) { return CharCount(TString::Itoa(N, 10)); }
                static int CharCount(TString str) { return str.Length(); }

                static TString GetInfo(const char*, const char*, ...);
                static void PrintInfo(const char*, const char*, ...);

                static TString GetMessage(const char*, const char*, ...);
                static void PrintMessage(const char*, const char*, ...);

                inline static bool IsDebugEnabled(int level = 1) {
                        return UIUC::HandboxMsg::kDebug >= level;
                };

                static void PrintDebug(const char* title, TLorentzVector& lv, bool b = true) { return PrintDebug(1, title, lv, b); };
                static void PrintDebug(const char* title, TVector3& v3, bool b = true) { return PrintDebug(1, title, v3, b); };
                static void PrintDebug(const char* title, const char* str0,...)
                {
                        va_list aptr;
                        va_start(aptr, str0);
                        TString str = ReplaceVariadic(str0, aptr);

                        return PrintDebug(1, title, str);
                }

                static void PrintDebug(int, const char*, const char*, ...);
                static void PrintDebug(int, const char*, TLorentzVector&, bool = true);
                static void PrintDebug(int, const char*, TVector3&, bool = true);

                template <typename T>
                static void PrintDebug(int, const char*, std::vector<T>);
                template <typename T>
                static void PrintDebug(int, const char*, std::vector< std::vector<T>>);

                template <typename T>
                static void PrintDebug(const char* title, std::vector<T> v);
                template <typename T>
                static void PrintDebug(const char* title, std::vector< std::vector<T>> vv);

                static TString ReplaceVariadic(const char*, va_list);
                static TString GetError(const char*, const char*, ...);
                static void PrintError(const char*, const char*, ...);
                static bool Error(bool, const char*, const char*, ...);

                static TString GetWarning(const char*, const char*, ...);
                static void PrintWarning(const char*, const char*, ...);
                static bool  Warning(bool, const char*, const char*, ...);

                static TString ListTTY(int desc);
                static void GetTTY(const char *, TString *, TString *);
                static int RedirectTTY(const char*, bool = true);
                static void DumpTTY(const char*);

                static void PrintTTY(const char*, bool = true, bool = true);
                static void PrintTTY(const char* title, const char*, const char* = "");

                static void ClearPage();
                static void ErasePreviousLines(int previousLines);
                static void ErasePreviousLine() { ErasePreviousLines(1); }
                
                static TString RedirectPrint(TObject*, Option_t* = "");

                static std::vector<double> envcoeffmap;
                static std::vector<TString> envhashmap;
                static std::map<TString, TString> envlist;

                static TString MakeItShorter(TString, TString = UIUC::HandboxMsg::kNoColor, int = -1);
                static TString ExpandVariables(TString, bool = true);
                static void PrepareEnvList(bool = false);

                ClassDef(UIUC::HandboxMsg, 1);
        };
}


template <typename T>
void UIUC::HandboxMsg::PrintDebug(const char* title, std::vector<T> v) {
        return PrintDebug(1, title, v);
}

template <typename T>
void UIUC::HandboxMsg::PrintDebug(const char* title, std::vector< std::vector<T>> vv) {
        return PrintDebug(1, title, vv);
}

template <typename T>
void UIUC::HandboxMsg::PrintDebug(int iDebugLevel, const char* title, std::vector<T> a)
{
        if(!a.size()) {

                UIUC::HandboxMsg::PrintDebug(iDebugLevel, title, "Vector is empty.. skip");
                return;
        }

        TString os;
        if( UIUC::HandboxMsg::IsDebugEnabled(iDebugLevel) || iDebugLevel == -1) {

                TString head = (!TString(title).EqualTo("")) ? "Debug (lvl " + TString::Itoa(iDebugLevel,10) + ") from <" + TString(title) + ">: " : "";

                if(TString(title).EqualTo("")) std::cout << UIUC::HandboxMsg::Tab();
                else std::cout << UIUC::HandboxMsg::Tab() << UIUC::HandboxMsg::kPurple + MakeItShorter(head) +UIUC::HandboxMsg::kNoColor;

                std::cout << Form("v.size() = %d; simple vector\n%sv = (", (int) a.size(), TString(Spacer(head.Length())).Data());
                for(int i = 0, N = a.size(); i < N; i++) {

                        std::stringstream ss;
                        ss << std::setw(8);
                        ss << std::left << std::fixed << std::setprecision(2);
                        if(i != (int) a.size()-1) ss << ((TString)"\"" + a[i] + "\"") << ", ";
                        else ss << ((TString)"\"" + a[i] + "\"");

                        std::cout << ss.str();
                        if(i!=(int) a.size()-1 && (i+1)%8 == 0) std::cout << "\n" << Spacer(head.Length()) << "     ";

                }
                std::cout << ")" << std::endl;
        }
}

template <typename T>
void UIUC::HandboxMsg::PrintDebug(int iDebugLevel, const char* title, std::vector< std::vector<T>> a)
{
        if(!a.size()) {

                UIUC::HandboxMsg::PrintDebug(iDebugLevel, title, "Vector is empty.. skip");
                return;
        }

        TString os;
        if( UIUC::HandboxMsg::IsDebugEnabled(iDebugLevel) || iDebugLevel == -1) {

                TString head = (!TString(title).EqualTo("")) ? "Debug (lvl " + TString::Itoa(iDebugLevel,10) + ") from <" + TString(title) + ">: " : "";

                if(TString(title).EqualTo("")) std::cout << UIUC::HandboxMsg::Tab() << std::endl;
                else std::cout << UIUC::HandboxMsg::Tab() << UIUC::HandboxMsg::kPurple + MakeItShorter(head) +UIUC::HandboxMsg::kNoColor << std::endl;

                std::cout << UIUC::HandboxMsg::Tab() << Form("v.size() = %d; matrix", (int) a.size()) << std::endl;
                std::cout << UIUC::HandboxMsg::Tab() << "v = (";
                for(int i = 0, N = a.size(); i < N; i++) {

                        std::cout << std::endl << UIUC::HandboxMsg::Tab() << "       (";

                        for(int j = 0, J = a[i].size(); j < J; j++) {

                                std::stringstream ss;
                                ss << std::setw(10);
                                ss << std::fixed << std::setprecision(2) << std::left;
                                if(j != (int) a[i].size()-1) ss << ((TString)"\"" + a[i][j]) << ", ";
                                else ss << ((TString)"\"" + a[i][j] + "\" ");

                                std::cout << ss.str();
                        }

                        if(i != (int) a.size()-1) std::cout << "),";
                        else std::cout << ")";
                }
                std::cout << std::endl << UIUC::HandboxMsg::Tab() << ")" << std::endl;
        }
}

#endif

