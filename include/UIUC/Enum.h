
#ifndef Enum_H
#define Enum_H

namespace UIUC {

        template< typename T >
        class Enum
        {
                public:
                class Iterator
                {
                        public:
                                Iterator( int value ) :
                                        m_value( value )
                                { }

                                T operator*( void ) const
                                {
                                        return (T)m_value;
                                }

                                void operator++( void )
                                {
                                        ++m_value;
                                }

                                bool operator!=( Iterator rhs )
                                {
                                        return m_value != rhs.m_value;
                                }
                                
                        private:
                                int m_value;
                };

                static std::vector<T> All() { 

                        std::vector<T> enums;

                        for( auto e: Enum<T>() ) {
                                if(e != T::None) enums.push_back(e);
                        }
                        return enums;
                }
        };

        template< typename T >
        typename Enum<T>::Iterator begin( Enum<T> ) { return typename Enum<T>::Iterator( (int)T::First ); }
        template< typename T >
        typename Enum<T>::Iterator end( Enum<T> ) { return typename Enum<T>::Iterator( ((int)T::Last) + 1 ); }
}
#endif