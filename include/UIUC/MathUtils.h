#ifndef UIUC_MathUtils_H
#define UIUC_MathUtils_H

#include <Riostream.h>
#include <algorithm>
#include <TMath.h>
#include <TFormula.h>
#include <TSystem.h>
#include <cmath>

#include <vector>
#include <TMatrixD.h>
#include <TRandom.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <sys/time.h>
#include <unistd.h>
#include <limits.h>
#include <stdlib.h>
#include <random>
#include <iomanip>
#include <sstream>

#include <regex>
#include <chrono>

#include <TColor.h>
#include <TROOT.h>
#include <TObjArray.h>
#include <TObjString.h>

#include <iostream>
#include <regex>

using namespace std;

namespace UIUC
{
        inline bool IsPrintable(const char *str, size_t length)
        {
                for(int i = 0; i < (int) length; i++)
                        if(!isprint(str[i])) return false;

                return true;
        }

        inline static std::time_t Time(const std::string& str, bool is_dst = false, const std::string& format = "%Y-%b-%d %H:%M:%S")
        {
                std::tm t{};
                t.tm_isdst = is_dst ? 1 : 0;
                std::istringstream ss(str);
                ss >> std::get_time(&t, format.c_str());
                return mktime(&t);
        }

        inline void HexDump(unsigned char* buffer, unsigned int length)
        {        
                unsigned int b=0, c=0;
                int s, rem;

                // b is a counter for the number of bytes (half the number of hex digits)

                std::cout << std::endl << "     PAYLOAD HEXDUMP:" << std::endl;
                while (b < length) {

                        std::cout << std::endl << "     x:" << b;
                        for (; (b%16<15) && (b<length); b++) {
                                if (0 == b % 2) printf(" ");
                                std::cout << std::hex << buffer[b];
                        }

                        if (b < length) std::cout << std::hex << buffer[b++];
                        else { // print a number of spaces to align the remaining text
                                rem = b % 16;
                                for (s=0; s < 44 - ((rem*2) + (rem/2) + 1); s++)
                                        std::cout << " ";
                        }

                        for (;(c%16<15) && (c<length); c++) if (isprint(buffer[c])) std::cout << buffer[c];
                        if (c<length && isprint(buffer[c])) std::cout << buffer[c++];
                }
        }

        inline Color_t Invert(const Color_t& kColor)
        {
                TColor *color = gROOT->GetColor(kColor);
                return TColor::GetColor(1 - color->GetRed(),  1 - color->GetGreen(),  1 - color->GetBlue());
        }

        // inline const TString base64_chars =
        //      "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        //      "abcdefghijklmnopqrstuvwxyz"
        //      "0123456789+/";

        // inline bool is_base64(BYTE c) {
        //         return (isalnum(c) || (c == '+') || (c == '/'));
        // }

        // inline TString base64_encode(BYTE const* buf, unsigned int bufLen) {

        //         TString ret;
        //         int i = 0;
        //         int j = 0;

        //         BYTE char_array_3[3];
        //         BYTE char_array_4[4];

        //         while (bufLen--) {

        //                 char_array_3[i++] = *(buf++);
        //                 if (i == 3) {
                                
        //                         char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        //                         char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        //                         char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        //                         char_array_4[3] = char_array_3[2] & 0x3f;

        //                         for(i = 0; (i <4) ; i++)
        //                                 ret += base64_chars[char_array_4[i]];
                        
        //                         i = 0;
        //                 }
        //         }

        //         if (i)
        //         {
        //                 for(j = i; j < 3; j++)
        //                         char_array_3[j] = '\0';

        //                 char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        //                 char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        //                 char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        //                 char_array_4[3] = char_array_3[2] & 0x3f;

        //                 for (j = 0; (j < i + 1); j++)
        //                         ret += base64_chars[char_array_4[j]];

        //                 while((i++ < 3))
        //                 ret += '=';
        //         }

        //         return ret;
        // }

        // inline TString base64_encode(std::vector<BYTE> buf) { return base64_encode(&buf[0], buf.size()); }

        // inline std::vector<BYTE> base64_decode(TString &encoded_string) {

        //         int in_len = encoded_string.Length();
        //         int i = 0;
        //         int j = 0;
        //         int in_ = 0;

        //         BYTE char_array_4[4], char_array_3[3];
        //         std::vector<BYTE> ret;

        //         while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
                        
        //                 char_array_4[i++] = encoded_string[in_]; in_++;
        //                 if (i ==4) {
                        
        //                         for (i = 0; i <4; i++)
        //                                 char_array_4[i] = base64_chars.Index(char_array_4[i]);

        //                         char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        //                         char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        //                         char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

        //                         for (i = 0; (i < 3); i++)
        //                                 ret.push_back(char_array_3[i]);
                        
        //                         i = 0;
        //                 }
        //         }

        //         if (i) {

        //                 for (j = i; j <4; j++) char_array_4[j] = 0;
        //                 for (j = 0; j <4; j++) char_array_4[j] = base64_chars.Index(char_array_4[j]);

        //                 char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        //                 char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        //                 char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

        //                 for (j = 0; (j < i - 1); j++) ret.push_back(char_array_3[j]);
        //         }

        //         return ret;
        // }

        // inline TString base64_decode(std::vector<BYTE> base64) {
                
        //         std::ostringstream out;
        //         for (BYTE c: base64) out << c;
                
        //         return TString(out.str());
        // }
        
        inline double microtime(){
                return (double(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) / double(1000000));
        }

        template<typename T>
        inline T Sign(T a) { return TMath::Sign(T(1), a); }

        inline TString Implode(const char *delim, std::vector<TString> v)
        {
                if(v.size() == 0) return "";
                if(v.size() == 1) return v[0];
                
                TString x;
                for(int i = 0, N = v.size(); i < N; i++)
                {
                        x += (TString) ((i > 0) ? delim : "");
                        x += v[i];
                }

                return x;
        }

        inline const char* Implode(const TString delim, std::vector<const char *> x) 
        { 
                std::vector<TString> X(x.size());
                for(int i = 0, N = X.size(); i < N; i++) 
                        X[i] = TString(x[i]);

                return Implode(delim, X).Data();
        }

        inline TString Ucfirst(TString str) 
        {
                TString c = str(0, 1);
                        c.ToUpper();

                return c + TString(str(1,str.Length()-1));
        }

        inline std::vector<TString> Explode(const TString delim, TString x)
        {
                std::vector<TString> v;

                for(int pos = -1; (pos = x.Index(delim)) != -1;) {

                        v.push_back(x(0, pos));
                        x = x(pos+delim.Length(), x.Length());
                }

                v.push_back(x);

                return v;
        }

        inline void RemoveEmpty(std::vector<TString> &vec)
        {
                vec.erase(std::remove(vec.begin(), vec.end(), TString("")), vec.end());
        }

        template<typename T>
        inline void Swap(T* &A, T* &B)
        {
                T* C = A;
                A = B;
                B = C;
        }

        inline std::string to_upper(std::string s){
                for(int i=0, N = s.length(); i < N; i++) {s[i]=toupper(s[i]);}
                return s;
        }

        template <typename T>
        std::vector<int> sort_indexes(const std::vector<T> &v) {

                // initialize original index locations
                std::vector<int> idx(v.size());
                std::iota(idx.begin(), idx.end(), 0);

                // sort indexes based on comparing values in v
                std::sort(idx.begin(), idx.end(),
                        [&v](size_t i1, size_t i2) {
                                return v[i1] < v[i2];
                        });

                return idx;
        }

        inline bool compare_natural_order(const char* aa, const char* bb){

                std::string a = aa;
                std::string b = bb;

                if (a.empty())
                        return true;
                if (b.empty())
                        return false;
                if (std::isdigit(a[0]) && !std::isdigit(b[0]))
                        return true;
                if (!std::isdigit(a[0]) && std::isdigit(b[0]))
                        return false;
                if (!std::isdigit(a[0]) && !std::isdigit(b[0]))
                {
                        if (a[0] == b[0])
                                return compare_natural_order(a.substr(1).c_str(), b.substr(1).c_str());
                        return (to_upper(a) < to_upper(b));
                        //to_upper() is a function to convert a std::string to uppercase.
                }

                // Both strings begin with digit --> parse both numbers
                std::istringstream issa(a);
                std::istringstream issb(b);
                int ia, ib;
                issa >> ia;
                issb >> ib;
                if (ia != ib)
                        return ia < ib;

                // Numbers are the same --> remove numbers and recurse
                std::string anew, bnew;
                std::getline(issa, anew);
                std::getline(issb, bnew);
                return (compare_natural_order(anew.c_str(), bnew.c_str()));
        }

        template <typename T>
        bool IsNaN(const T x)
        {
        #if __cplusplus >= 201103L
                using std::isnan;
        #endif
                return isnan(x);
        }


        inline double Round(double d, double precision = NAN) {

                if(IsNaN(precision)) {

                        precision = TMath::Power(
                                10, ceil(TMath::Abs(TMath::Log10(
                                                        std::numeric_limits<double>::epsilon()
                                                        )))
                                );
                }

                return round(d * precision) / precision;
        }

        inline TString NumberingFormat(const int number, const int max) {

                TString smax = TString::Itoa(max,10);

                std::stringstream ss;
                ss << std::setw(smax.Length()) << std::setfill('0') << number;
                return ss.str();
        }

        template <typename T>
        bool IsInf(const T x)
        {
                #if __cplusplus >= 201103L
                       using std::isinf;
                #endif
                
                return isinf(x);
        }

        template <typename T>
        bool IsNormal(const T x)
        {
                return std::isnormal(x);
        }

        template <typename T>
        inline T StripNaN( const T& a, T replacement = 0 ) { return UIUC::IsNaN(a) ? replacement : a; }
        template <typename T>
        inline std::vector<T> StripNaN( const std::vector<T>& v, T replacement = 0 )
        {
                std::vector<T> result;
                for(int i = 0, N = v.size(); i < N; i++) {
                
                        result.push_back(UIUC::IsNaN(v[i]) ? replacement : v[i]);
                }

                return result;
        }
        template <typename T>
        inline std::vector<std::vector<T>> StripNaN( const std::vector<std::vector<T>>& v, T replacement = 0 )
        {
                std::vector<std::vector<T>> result;
                for(int i = 0, I = v.size(); i < I; i++) {

                        result.push_back({});
                        for(int j = 0, J = v.size(); j < J; j++) {

                                result[i].push_back(UIUC::IsNaN(v[i][j]) ? replacement : v[i][j]);
                        }
                }

                return result;
        }
        template <typename T>
        inline std::vector<std::vector<std::vector<T>>> StripNaN( const std::vector<std::vector<std::vector<T>>>& v, T replacement = 0 )
        {
                std::vector<std::vector<std::vector<T>>> result;
                for(int i = 0, I = v.size(); i < I; i++) {

                        result.push_back({});
                        for(int j = 0, J = v.size(); j < J; j++) {

                                result[i].push_back({});
                                for(int k = 0, K = v.size(); k < K; k++) {

                                        result[i][j].push_back(UIUC::IsNaN(v[i][j][k]) ? replacement : v[i][j][k]);
                                }
                        }
                }

                return result;
        }
        
        template <typename T>
        inline T StripInf( const T& a, T replacement = 0 ) { return UIUC::IsInf(a) ? replacement : a; }
        template <typename T>
        inline std::vector<T> StripInf( const std::vector<T>& v, T replacement = 0 )
        {
                std::vector<T> result;
                for(int i = 0, N = v.size(); i < N; i++) {
                        result.push_back(UIUC::IsInf(v[i]) ? replacement : v[i]);
                }

                return result;
        }
        
        template <typename T>
        inline std::vector<std::vector<T>> StripInf( const std::vector<std::vector<T>>& v, T replacement = 0 )
        {
                std::vector<std::vector<T>> result;
                for(int i = 0, I = v.size(); i < I; i++) {

                        result.push_back({});
                        for(int j = 0, J = v.size(); j < J; j++) {

                                result[i].push_back(UIUC::IsInf(v[i][j]) ? replacement : v[i][j]);
                        }
                }

                return result;
        }
        template <typename T>
        inline std::vector<std::vector<std::vector<T>>> StripInf( const std::vector<std::vector<std::vector<T>>>& v, T replacement = 0 )
        {
                std::vector<std::vector<std::vector<T>>> result;
                for(int i = 0, I = v.size(); i < I; i++) {

                        result.push_back({});
                        for(int j = 0, J = v.size(); j < J; j++) {

                                result[i].push_back({});
                                for(int k = 0, K = v.size(); k < K; k++) {

                                        result[i][j].push_back(UIUC::IsInf(v[i][j][k]) ? replacement : v[i][j][k]);
                                }
                        }
                }

                return result;
        }

        template <typename T>
        double SumX(std::vector<T> a) {

                double N = 0;
                for(int i = 0; i < a.size(); i++) {

                        N += a[i];
                }

                return N;
        }

        inline bool IsAdditionSafe(int a, int x) {

                if ((x > 0) && (a > INT_MAX - x)) return 0;
                if ((x < 0) && (a < INT_MIN - x)) return 0;

                return 1;
        }

        inline bool IsMultipicationSafe(int a, int x) {

                if (a > INT_MAX / x) return 0;
                if ((a < INT_MIN / x)) return 0;

                if ((a == -1) && (x == INT_MIN)) return 0;
                if ((x == -1) && (a == INT_MIN)) return 0;

                return 1;
        }

        inline bool IsSubtractionSafe(int a, int x) {

                if ((x < 0) && (a > INT_MAX + x)) return 0;
                if ((x > 0) && (a < INT_MIN + x)) return 0;

                return 1;
        }

        inline bool IsUnsignedAdditionSafe(unsigned int a, unsigned int b) {

                if((a + b) < a) return 0;
                return 1;
        }

        inline bool IsUnsignedSubtractionSafe(unsigned int a, unsigned int b) {

                if((a - b) > a) return 0;
                return 1;
        }

        template <typename T>
        double SumXY(std::vector<T> a, std::vector<T> b) {

                if(a.size() != b.size()) return NAN;

                double N = 0;
                for(int i = 0; i < a.size(); i++)
                        N += a[i] * b[i];

                return N;
        }

        template <typename T>
        double SumX2(std::vector<T> a) { return SumXY(a,a); }

        static int nInterpretFormula = 0;
        inline double InterpretFormula(TString scale_str, std::vector<TString> v0 = {},  std::vector<TString> e0 = {})
        {
                // THIS FUNCTION SHOULD NOT BE USED IN A LOOP :(
                // It create a TFormula each time it is called. Although it is deleted in the end.. ROOT does not like it and slow down step by step.
                // It might not be a big deal when used in a small loop, but if N > 10000.. it becomes problematic.

                TString str0 = scale_str;
                str0.ToLower();
                if(str0.EqualTo("nan")) return NAN;
                if(str0.EqualTo("")) return 1;

                /*if(nInterpretFormula == 20000) {

                        std::cerr << "You called UIUC::InterpretFormula about 20000 times." << std::endl;
                        std::cerr << "This command is creating and deleting a TFormula each time.." << std::endl;
                        std::cerr << "ROOT performances might be reduced after creating too many TObject.." << std::endl;
                }*/

                // Replace possible variables..
                int G = 0;
                for(int i = 0, N = v0.size(); i < N; i++) {

                        scale_str.ReplaceAll("[c"+TString::Itoa(i,10)+"]", v0[i]);
                        scale_str.ReplaceAll("["+TString::Itoa(G,10)+"]", v0[i]);
                        G++;
                }

                for(int i = 0, N = e0.size(); i < N; i++) {

                        scale_str.ReplaceAll("[e"+TString::Itoa(i,10)+"]", e0[i]);
                        scale_str.ReplaceAll("["+TString::Itoa(G,10)+"]", e0[i]);
                        G++;
                }


                gErrorIgnoreLevel = kBreak;
                TFormula *scale = new TFormula(scale_str, scale_str);
                nInterpretFormula++;
                gErrorIgnoreLevel = kPrint;

                if(!scale->IsValid()) {

                        std::cerr << "Formula to interpret is not valid.. \"" << scale_str << "\"" << std::endl;
                        return NAN;
                }
                double newscale = scale->Eval(0);
                delete scale;
                scale = NULL;


                return newscale;
        }
        inline double InterpretFormula(TString scale_str, std::vector<double> v0,  std::vector<double> e0) {

                std::vector<TString> v, e;
                for(int i = 0; i < (int) v0.size(); i++) v.push_back(Form("%f", v0[i]));
                for(int i = 0; i < (int) e0.size(); i++) e.push_back(Form("%f", e0[i]));
                return UIUC::InterpretFormula(scale_str, v, e);
        }


        inline std::string& rtrim(std::string& s, const char* t = " \t\n\r\f\v")
        {
                s.erase(s.find_last_not_of(t) + 1);
                return s;
        }

        inline std::string& ltrim(std::string& s, const char* t = " \t\n\r\f\v")
        {
                s.erase(0, s.find_first_not_of(t));
                return s;
        }

        inline std::string& trim(std::string& s, const char* t = " \t\n\r\f\v")
        {
                return ltrim(rtrim(s, t), t);
        }

        inline std::vector<std::string> explode(std::string const & s, char delim)
        {
                std::vector<std::string> result;
                std::istringstream iss(s);

                for (std::string token; std::getline(iss, token, delim); )
                        result.push_back(std::move(token));

                return result;
        }

        // Mathematic functions
        inline double MathMod(double a, double b) {
                return a - b * TMath::Floor(a / b);
        }
        inline double Quotient(double a, double b) { return TMath::Floor(a / b); }
        inline double Rest(double a, double b) { return MathMod(a,b); }

        inline bool EpsilonEqualTo(double a, double b, double epsilon = std::numeric_limits<double>::epsilon()) {
                return TMath::Abs(a - b) <= epsilon * TMath::Max(TMath::Abs(a), TMath::Abs(b)) || TMath::Abs(a - b) <= std::numeric_limits<double>::min();
        }

        inline bool EpsilonEqualTo(const TMatrixD& M, double b, double epsilon = std::numeric_limits<double>::epsilon())
        {
                for (int i = 0; i < M.GetNrows(); i++) {

                        for (int j = 0; j < M.GetNcols(); j++) {
                        
                                if(!UIUC::EpsilonEqualTo(M(i, j), b, epsilon)) return false;
                        }
                }

                return true;
        }

        template <typename T>
        bool IsInteger(const T x)
        {
                return UIUC::EpsilonEqualTo((int) x, x);
        }
        
        inline double Epsilon() {
                
                return std::numeric_limits<double>::epsilon();
        }

        inline std::vector<double>::iterator find_epsilon(std::vector<double>::iterator vbegin, std::vector<double>::iterator vend, double d) {

                for(std::vector<double>::iterator it = vbegin; it != vend; it++)
                        if(UIUC::EpsilonEqualTo(*it, d)) return it;

                return vend;
        }


        enum CustomSeed {E_DEFAULT = -1, E_TIME = -2, E_MICROTIME = -3, E_TIMEPID = -4, E_MICROTIMEPID = -5, E_URANDOM = -6, E_RANDOMD = -7};
        inline unsigned int read_urandom()
        {
                union {
                        int value;
                        char cs[sizeof(unsigned int)];
                } u;

                std::ifstream rfin("/dev/urandom");
                rfin.read(u.cs, sizeof(u.cs));
                rfin.close();

                return u.value;
        }

        inline std::vector<unsigned int> random_device(int i)
        {
                std::random_device r;
                std::seed_seq seq{r(), r(), r(), r(), r(), r(), r(), r()};
                std::vector<std::uint32_t> seeds(i);
        
                seq.generate(seeds.begin(), seeds.end());
                return seeds;
        }

        static std::atomic<int> randomseed_type      = E_DEFAULT;
        static std::atomic<long> randomseed = -1;

        inline bool SetRandomSeed(long type = E_DEFAULT) {

                if(randomseed < 0) {

                        int seed = TString(gSystem->Getenv("ESCALADE_SEED")).Atoi();
                        type = seed ? seed : UIUC::E_RANDOMD;
                }
                
                if(type == E_DEFAULT) {
                        return 0;
                }

                if(randomseed_type != type) {

                        timeval t1;
                        gettimeofday(&t1, NULL);

                        randomseed_type = type;
                        switch(randomseed_type) {
                        
                                case E_TIME:
                                randomseed = time(NULL);
                                break;
                                
                                case E_TIMEPID:
                                randomseed = time(NULL)*getpid();
                                break;
                                
                                case E_MICROTIME:
                                randomseed = t1.tv_usec*t1.tv_sec;
                                break;
                                
                                case E_MICROTIMEPID:
                                randomseed = t1.tv_usec*t1.tv_sec*getpid();
                                break;

                                case E_URANDOM:
                                randomseed = read_urandom();
                                break;
                                
                                case E_RANDOMD:
                                randomseed = random_device(1)[0];
                                break;
      
                                default:
                                randomseed = TMath::Abs(type); // Seed >=0
                        }

                        gRandom->SetSeed(randomseed);
                        srand(randomseed);

                        return 1;
                }

                return 0;
        }

        inline unsigned int GetRandomSeed() {

                SetRandomSeed();
                return randomseed;
        }

        template <typename T>
        inline int CountChars(T value)
        {
                std::stringstream ss;
                ss << value;
                return ss.str().size();
        }

        inline TString GetZeroLead(const int value, const unsigned int leadingZeros = 0)
        {
                std::stringstream ss;
                ss << std::setw(leadingZeros) << std::setfill('0') << value;
                return TString(ss.str().c_str());
        }

        inline static TString GetRandomStr( size_t length = 6, int type = E_DEFAULT)
        {
                UIUC::SetRandomSeed(type);
                auto randchar = []() -> char
                {
                        const char charset[] =
                        "0123456789"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                        "abcdefghijklmnopqrstuvwxyz";
                        const size_t max_index = (sizeof(charset) - 1);
                        return charset[ rand() % max_index ];
                };

                std::string str(length,0);
                std::generate_n( str.begin(), length, randchar );
                return TString(str);
        }

        inline TString CamelToSnake(TString camel, char separator = '_')
        {
                // Empty String
                string snake = "";
                
                // Append first character(in lower case)
                // to snake string
                char c = tolower(camel[0]);
                snake += (char(c));
                
                // Traverse the string from
                // ist index to last index
                for (int i = 1; i < camel.Length(); i++) {
                
                        char ch = camel[i];
                
                        // Check if the character is upper case
                        // then append '_' and such character
                        // (in lower case) to snake string
                        if (isupper(ch)) {
                        snake  =  snake + separator;
                        snake += char(tolower(ch));
                        }
                
                        // If the character is lower case then
                        // add such character into snake string
                        else {
                        snake = snake + ch;
                        }
                }
                
                // return the snake
                return TString(snake.c_str());
        }
        
        inline TString SnakeToCamel(TString _snake)
        {
                std::string snake = _snake.Data();
                for (auto it = snake.begin(); it != snake.end(); it++)
                {
                        if (*it == '-' || *it == '_') {

                                it = snake.erase(it);
                                *it = toupper(*it);
                        }
                }

                return TString(snake.c_str());
        }
        
        inline double GetRandom(double scale = 1, int type = E_DEFAULT) {

                UIUC::SetRandomSeed(type);
                return rand()/double(RAND_MAX) * scale;
        }

        inline unsigned long long GetRandomULL(unsigned long long scale, int type = E_MICROTIMEPID) {

                UIUC::SetRandomSeed(type);
                return (((unsigned long long) (unsigned int)rand() << 32) + (unsigned long long)(unsigned int) rand()) % scale;
        }

        inline bool MinSorting (double i, double j) {
                return (i<j);
        }

        template <typename T>
        inline T GCD (T A, T B, double eps = 1e0) {
                int precision = TMath::Power(10, TMath::Abs(TMath::Log10(eps)));
                return std::gcd((int) (A*precision), (int) (B*precision))/precision;
                
        }

        template <typename T>
        inline T LCM (T A, T B, double eps=1e0) {
                int precision = TMath::Power(10, TMath::Abs(TMath::Log10(eps)));
                return std::lcm((int) (A * precision), (int) (B * precision))/precision;
                
        }

        inline double Microtime()
        {
                struct timeval tv;
                gettimeofday(&tv,NULL);
                return (1000000*tv.tv_sec) + tv.tv_usec;
        }

        inline double Millitime() {
                return round( UIUC::Microtime() / 1000 );
        }

        // std::vector search
        template <typename T>
        T GetMinMaxSubstr(std::vector<T> a)
        {
                if(!a.size()) return 0;
                return *(std::max_element(a.begin(), a.end())) - *(std::min_element(a.begin(), a.end()));
        }

        template <typename T>
        T GetMinMaxSubstr(T a[2])
        {
                return a[1]-a[0];
        }

        template <typename T>
        T GetMin(std::vector<T> a) {
                if(!a.size()) return 0;
                return *(std::min_element(a.begin(), a.end()));
        }

        template<class InputIt, class T, class BinaryOperation>
        T accumulate(InputIt first, InputIt last, T init,
                BinaryOperation op)
        {
                for (; first != last; ++first) {
                        init = op(std::move(init), *first); // std::move since C++20
                }
                return init;
        }

        template <typename T>
        T GetAverage(std::vector<T> a) {
                return (a.size()) ? UIUC::accumulate(a.begin(), a.end(), 0)/a.size() : 0;
        }

        template <typename T>
        int GetIndexMin(std::vector<T> a) {
                return std::min_element(a.begin(), a.end()) - a.begin();
        }

        template <typename T>
        T GetMax(std::vector<T> a) {
                if(!a.size()) return 0;
                return *(std::max_element(a.begin(), a.end()));
        }

        template <typename T>
        int GetIndexMax(std::vector<T> a)
        {
                if(!a.size()) return 0;
                return std::max_element(a.begin(), a.end()) - a.begin();
        }

        template <typename T>
        std::vector<T> ApplyVectorMask(std::vector<T> a, std::vector<T> mask)
        {
                std::vector<T> b;
                if(a.size() != mask.size()) return {{}};
                b.resize(a.size());

                for(int i = 0; i < (int) a.size(); i++) {

                        if(mask[i]) b[i] = a[i];
                }

                return b;
        }

        template <typename T>
        std::vector< std::vector<T> > ApplyMatrixMask(std::vector< std::vector<T> > a, std::vector< std::vector<T> > mask)
        {
                std::vector< std::vector<T> > b;
                if(a.size() != mask.size()) return {{}};
                for(int i = 0; i < (int) a.size(); i++) {

                        if(a[i].size() != mask[i].size()) return {{}};
                        b.push_back(std::vector<T>(a[i].size()));
                }

                for(int i = 0; i < a.size(); i++) {
                        for(int j = 0; j < (int) a[i].size(); j++) {

                                if(mask[i][j]) b[i][j] = a[i][j];
                        }
                }

                return b;
        }

        inline void ReduceRotationalAmbiguity(TLorentzVector&lv) {

                // THOSE FUNCTIONS ARE IMPORTANT...
                // AFTER APPLYING ROTATION MATRICES SOME QUANTITIES MIGHT BE AMBIGUOUS
                // WHEN THETA=0, PHI IS NOT VERY WELL DEFINED
                // WHEN THETA IS CLOSE TO ZERO (BELOW THE COMPUTER PRECISION..) THIS LEADS TO SOME AMBIGUOUS VALUES OF PHI
                // IN SUCH CASES ONE CAN TRY TO REAPPLY A ROTATIONAL MATRIX... UNFORTUNATELY RESULT MIGHT STILL BE WRONG..
                // THEREFORE, IN SUCH CASES WHEN X,Y,Z,E,Theta,Phi ~ 0 (BELOW COMPUTER PRECISION) => I SET THE VALUE TO 0
                if(UIUC::EpsilonEqualTo(lv.X(), 0)) lv.SetX(0);
                if(UIUC::EpsilonEqualTo(lv.Y(), 0)) lv.SetY(0);
                if(UIUC::EpsilonEqualTo(lv.Z(), 0)) lv.SetZ(0);
                if(UIUC::EpsilonEqualTo(lv.E(), 0)) lv.SetE(0);

                if(UIUC::EpsilonEqualTo(lv.Theta(), 0)) lv.SetTheta(0);
                if(UIUC::EpsilonEqualTo(lv.Phi(), 0)) lv.SetPhi(0);
        }

        inline void ReduceRotationalAmbiguity(TVector3 &v) {

                if(UIUC::EpsilonEqualTo(v.X(), 0)) v.SetX(0);
                if(UIUC::EpsilonEqualTo(v.Y(), 0)) v.SetY(0);
                if(UIUC::EpsilonEqualTo(v.Z(), 0)) v.SetZ(0);

                if(UIUC::EpsilonEqualTo(v.Theta(), 0)) v.SetTheta(0);
                if(UIUC::EpsilonEqualTo(v.Phi(), 0)) v.SetPhi(0);
        }

        inline bool AzimutalAmbiguity(TVector3 v)
        {
                UIUC::ReduceRotationalAmbiguity(v);
                return (UIUC::MathMod(v.Theta(),TMath::Pi()) == 0);
        }

        inline bool AzimutalAmbiguity(TLorentzVector lv)
        {
                UIUC::ReduceRotationalAmbiguity(lv);
                return (UIUC::MathMod(lv.Theta(), TMath::Pi()) == 0);
        }

        template<typename T>
        std::vector<T> ReadPtr(std::vector<T*> vPtr)
        {
                std::vector<T> v(vPtr.size(), NAN);
                for(int i = 0; i < vPtr.size(); i++) {

                        if(vPtr[i] != NULL) v[i] = *vPtr[i];
                }

                return v;
        }
}

#endif
