/**
 * *********************************************
 *
 * \file TFactory.h
 * \brief Header of the class TFactory
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 * *********************************************
 */

#ifndef TFactory_H
#define TFactory_H

#include <Riostream.h>
#include <TSpectrum.h>

#include <stdlib.h>
#include <complex>
#include <ctime>

#include <TTree.h>
#include <TObject.h>
#include <TString.h>
#include <TAxis.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TGraph2D.h>
#include <TList.h>
#include <TPRegexp.h>
#include <TRegexp.h>
#include <TClass.h>
#include <TSystem.h>
#include <fstream>
#include <TFile.h>
#include <TMath.h>
#include <TFormula.h>
#include <TLine.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TVirtualFFT.h>
#include <TPolyMarker.h>

#include <UIUC/TVarexp.h>
#include <UIUC/HandboxMsg.h>
#include <UIUC/TFileReader.h>
#include <UIUC/MathUtils.h>

#include <TVector.h>
#include <TMatrixD.h>
#include <Math/GSLIntegrator.h>
#include <Math/WrappedTF1.h>

namespace UIUC {

        class TFactory: public TObject
        {
                private:
                TList* hList;
                bool SameObject(TRegexp);

                public:
                TFactory() { this->hList = new TList(); };

                ~TFactory() { hList->Clear(); delete hList; hList = NULL; };

                // Histogram methods
                int Load(TString);
                bool Add(TH1*);
                TH1* Get(TString);
                bool Remove(TString);

                static std::pair<double,double> GetRangeContent(TList* l, bool bLog = false);
                static std::pair<double,double> GetRangeContent(std::vector<TObject*> v, bool bLog = false);

                static TObject* SetAutomaticRange(TObject*, std::vector<TObject*>, std::pair<double, double> margin = {0.10, 0.10}, bool bLog = false);

                static void SetAutomaticRangeUser(TObject* obj, std::pair<double, double> margin = {0.10, 0.10}, bool bLog = false)
                {
                        return SetAutomaticRangeUser(std::vector<TObject*>{obj}, margin, bLog);
                }
                static void SetAutomaticRangeUser(TList *l, std::pair<double, double> margin = {0.10, 0.10}, bool bLog = false)
                {
                        std::vector<TObject*> vObj;
                        for(int i = 0, N = l->GetSize(); i < N; i++) vObj.push_back(l->At(i));

                        SetAutomaticRangeUser(vObj, margin, bLog);
                }


                static std::vector<double> GetVectorAxis(TH1* h, Option_t *axis);
                static std::vector<double> GetVectorAxis(TH1* h, int iAxis = 0);

                static void SetAutomaticRangeUser(std::vector<TObject*> v, std::pair<double, double> margin = {0.10, 0.10}, bool bLog = false);

                static int RemoveNaNFromGraph(TGraph *g);

                // Methods acting with TList
                void Print(Option_t *option) const;
                TList* GetList();
                void WriteTH1();

                enum Notation {E_SCIENTIFIC, E_INT, E_ROUNDED, E_FLOAT};

                static const int kSimpleAxis;
                static const int kRepetitionAxis;
                
                static const int kUseSimpleAverage;
                static const int kUseSimpleAverageNoZero;
                static const int kUseWeightedAverage;
                static const int kUseWeightedAverageAndStdDevError;
                static const int kUseSimpleAverageAndStdDevError;
                static const int kUseSum;
                
                static const int kScaleUnit;
                
                static const bool kUseWeight;
                static const bool kNotUseWeight;
                
                static const bool kUseWidth;
                static const bool kNotUseWidth;
                
                static const bool kModulo;
                static const bool kNoModulo;
                
                static const bool kRenormalizeFFT;
                
                static TString MultipleLines(TString);
                static TMatrixD GetMatrix(TH1 *);
                static TVectorD GetFqfn(TH1D *);

                static void AddBinError(TH1* h, double err, Option_t* = "");

                static TH1* RemoveError(TH1*);
                static TH1* RemoveContent(TH1*);
                static TGraph* RemoveError(TGraph*);
                static TGraph* RemoveContent(TGraph*);
                static bool HasCompatibleBinning(TH1*, TH1*);
                static bool HasCompatibleBinning(TAxis *, double, double);
                static void RemoveAmbiguousBins(TH1*);

                static TString GetRandomName(TString, TString = "");
                static TString GetTitle(TString, int = 0);

                static bool HasVariableBinWidth(TAxis *a0, double epsilon = 1e-4);
                static bool HasVariableBinWidth(TGraph *g0, double epsilon = 1e-4);
                static bool HasVariableBinWidth(TGraph2D *g0, double epsilon = 1e-4);
                static bool HasVariableBinWidth(TH1 *h0, double epsilon = 1e-4);

                static TGraph* GetFillArea(int, double, double, TF1*, TF1* = NULL);
                static TLine* GetMeanLine(TGraph*);
                static TLine* GetMeanLine(TF1*);
                static TLine* GetMeanLine(TH1*);

                static TH1* RemoveBackground(TH1*, int = 20);
                static TH1* RemoveEmptySpills(TH1*);

                static TString AddSuffix(TString str, TString suffix);
                static TString RemoveUnits(TString str);
                static TString KeepOnlyUnits(TString str);

                static TString AddPrefixAndSuffix(TString prefix, TString str, TString suffix);
                static TString AddPrefix(TString str, TString prefix);

                static TH1* Normalize(TH1*, double, int = UIUC::TFactory::kSimpleAxis);
                static TH1* Normalize(TH1*, TString, int = UIUC::TFactory::kSimpleAxis);
                //static TH1* Normalize(TH3D*, TH2D*, Option_t*);
                static TH1* Normalize(TH2D*, TH1D*, Option_t*);

                static TH1* NormalizeByBinWidth(TH1 *);
                static TH1* NormalizeByEntries(TH1*);
                static TH1* NormalizeByMax(TH1*);
                static TH1* NormalizeByIntegral(TH1*);
                static TH1* NormalizeByBin(TH1*, int);
    
                inline static TH1* Scale (TH1 *h, double scaleY, double scaleX = 1, Option_t *option = "x") { return ScaleY(ScaleX(h, scaleX, option), scaleY); }
                       static TH1* ScaleX(TH1 *h, double scaleX, Option_t *option = "x");
                       static TH1* ScaleY(TH1 *h, double scaleY);

                static TH1* Abs   (TH1 *h);
                inline static TH1* Sqrt  (TH1 *h) { return UIUC::TFactory::PowerX(h, 1/2.); }
                inline static TH1* Power2(TH1 *h) { return UIUC::TFactory::PowerX(h, 2); }
                       static TH1* PowerX(TH1 *h, double X);
                
                static TH1* Smearing(TH1* h0, double sX = 0, double sY = 0, double sZ = 0);

                static TH1* FindFirstHistogram(TList* lObject)
                {
                        std::vector<TObject*> vObject;
                        for(int i = 0, N = lObject->GetSize(); i < N; i++)
                                vObject.push_back(lObject->At(i));

                        return FindFirstHistogram(vObject);
                }

                static TH1* FindFirstHistogram(std::vector<TObject*>);

                static TH1* NormalizeWithError(TH1 *, TString, TString);
                static TH1* NormalizeWithError(TH1 *, double, double);

                static bool Filter(TH1*, double = NAN, double = NAN);

                static bool FillWithError(TH1*, double, double, double);
                static bool FillWithError(TH2*, double, double, double, double);
                static bool FillWithError(TH3*, double, double, double, double, double);

                static std::pair<double,double> Integral(TH1*, double = NAN, double = NAN, double = NAN, double = NAN, double = NAN, double = NAN);
                static double Integral(TGraph *g, double *err_min = NULL, double *err_max = NULL, double xmin = NAN, double xmax = NAN);

                static bool IsEmpty(TH1*);
                static bool IsCompatible(TGraph*, TGraph*);

                static TGraph* Scale(TGraph*, double);
                static TGraph* Multiply(TGraph*, TGraph*, double = 1, double = 1);
                static TGraph* Divide(TGraph*, TGraph*, double = 1, double = 1);
                static TGraph* Add(TGraph*, TGraph*, double = 1, double = 1);
                static TGraph* Subtract(TGraph*, TGraph*, double = 1, double = 1);

                static TH1* Divide(TH1*, TH1*, double = 1, double = 1);

                static TGraph* HistToGraph(TString, TString, TH1D*);
                static TGraphErrors* HistToGraphErrors(TString, TString, TH1D*);
                static TGraphAsymmErrors* HistToGraphAsymmErrors(TString, TString, TH1D*);
                inline static TGraph* HistToGraph(TString name, TH1D*h){
                        return (h == NULL) ? NULL : HistToGraph(name, h->GetTitle(), h);
                };
                inline static TGraphErrors* HistToGraphErrors(TString name, TH1D*h){
                        return (h == NULL) ? NULL : HistToGraphErrors(name, h->GetTitle(), h);
                };
                inline static TGraphAsymmErrors* HistToGraphAsymmErrors(TString name, TH1D*h){
                        return (h == NULL) ? NULL : HistToGraphAsymmErrors(name, h->GetTitle(), h);
                };
                inline static TGraph* HistToGraph(TH1D*h){
                        return (h == NULL) ? NULL : HistToGraph((TString) h->GetName() + ":graph", h->GetTitle(), h);
                };
                inline static TGraphErrors* HistToGraphErrors(TH1D*h){
                        return (h == NULL) ? NULL : HistToGraphErrors((TString) h->GetName() + ":graph", h->GetTitle(), h);
                };
                inline static TGraphAsymmErrors* HistToGraphAsymmErrors(TH1D*h){
                        return (h == NULL) ? NULL : HistToGraphAsymmErrors((TString) h->GetName() + ":graph", h->GetTitle(), h);
                };

                static TH1D* GraphToHist(TString, TString, TGraph*g);
                inline static TH1D* GraphToHist(TString name, TGraph*g) {
                        return (g == NULL) ? NULL : GraphToHist(name, g->GetTitle(), g);
                };
                inline static TH1D* GraphToHist(TGraph*g) {
                        return (g == NULL) ? NULL : GraphToHist((TString) g->GetName() + ":hist", g->GetTitle(), g);
                };

                static TGraphAsymmErrors* GetGraphWithHessianErrors(TGraphAsymmErrors*, std::vector<TGraphAsymmErrors*>, int);
                static TGraphAsymmErrors* GetGraphWithMinMaxErrors(TGraphAsymmErrors*, std::vector<TGraphAsymmErrors*>);
                static TGraphAsymmErrors* GetGraphWithWeightedSamplesErrors(TGraphAsymmErrors*, std::vector<TGraphAsymmErrors*>);
                static TGraphAsymmErrors* GetGraphWithSumMaxErrors(TGraphAsymmErrors *, std::vector<TGraphAsymmErrors*>, std::vector<TGraphAsymmErrors*>, int);

                static TH1* MultiplyWithoutErrorPropagation(TH1*, TH1*);
                static TH1* DivideWithoutErrorPropagation(TH1*, TH1*);

                // static TTree* TreeFromStruct(name, title, const char *classname, TClass* ptrClass)
                // {
                //         TTree *fTree = new TTree(name, title);

                //         static struct TFrProcData procData;
                //         fTree->Branch("frame.", "TFrProcData", &procData);
                //         // Int_t TTree::Branch(const char* foldername, Int_t bufsize /* = 32000 */, Int_t splitlevel /* = 99 */)
                //         // {
                //         // TObject* ob = gROOT->FindObjectAny(foldername);
                //         // if (!ob) {
                //         // return 0;
                //         // }
                //         // if (ob->IsA() != TFolder::Class()) {
                //         // return 0;
                //         // }
                //         // Int_t nbranches = GetListOfBranches()->GetEntries();
                //         // TFolder* folder = (TFolder*) ob;
                //         // TIter next(folder->GetListOfFolders());
                //         // TObject* obj = 0;
                //         // char* curname = new char[1000];
                //         // char occur[20];
                //         // while ((obj = next())) {
                //         // snprintf(curname,1000, "%s/%s", foldername, obj->GetName());
                //         // if (obj->IsA() == TFolder::Class()) {
                //         //         Branch(curname, bufsize, splitlevel - 1);
                //         // } else {
                //         //         void* add = (void*) folder->GetListOfFolders()->GetObjectRef(obj);
                //         //         for (Int_t i = 0; i < 1000; ++i) {
                //         //         if (curname[i] == 0) {
                //         //         break;
                //         //         }
                //         //         if (curname[i] == '/') {
                //         //         curname[i] = '.';
                //         //         }
                //         //         }
                //         //         Int_t noccur = folder->Occurence(obj);
                //         //         if (noccur > 0) {
                //         //         snprintf(occur,20, "_%d", noccur);
                //         //         strlcat(curname, occur,1000);
                //         //         }
                //         //         TBranchElement* br = (TBranchElement*) Bronch(curname, obj->ClassName(), add, bufsize, splitlevel - 1);
                //         //         if (br) br->SetBranchFolder();
                //         // }
                //         // }
                //         // delete[] curname;
                //         // return GetListOfBranches()->GetEntries() - nbranches;
                //         // }
                // }

                static TH1* DivideBinContent(TH1*, TH1*);
                static TH1* DivideBinError(TH1*, TH1*);

                static bool SetAllBinContent(TH1*, double);
                static bool SetAllBinError(TH1*, double);

                static TH1* Offset(TH1 *, int, int, int, bool = UIUC::TFactory::kNoModulo);
                static TH1* Empty(TH1 *);
                static TGraph* Empty(TGraph *);
                static void PrintWithAxis(TH1 *);

                static TH1* Flip(TH1*, Option_t* = "");
                static TH1* SetOrigin(TH1*, double x0 = NAN, double y0 = NAN, double z0 = NAN);
                
                static TH1* CopyPaste(TGraph*, TGraph*, Option_t* = "");
                static TH1* CopyPaste(TH1*, TH1*, Option_t* = "");
                static TH1* CopyStruct(TH1*, TString = "", TString = "", std::vector<int> = {});
                static TGraph* CopyStruct(TGraph*, TString = "", TString = "");

                static bool CompareData(TH1*, TH1*, double = std::numeric_limits<double>::epsilon(), bool = false);
                static bool CompareStruct(TH1*, TH1*);
                static bool CompareData(TGraph*, TGraph*, double = std::numeric_limits<double>::epsilon());
                static bool CompareStruct(TGraph*, TGraph*);
                static bool CompareBinWidth(TH1*, TH1*);

                // Static methods
                static TH1D* PullDistribution(TH1*, int);
                static TH1D* PullDistribution(std::vector<TH1*>, int);
                static TH1D* PullDistribution(TList*, int);

                static TH1* CumulativeHistogram(TH1 *, TH1* = NULL);
                static TH2D* Transpose(TH2 *);

                static TH1* FFT(TH1*, Option_t*, bool, std::vector<double> * = NULL, std::vector<double> * = NULL);
                static TH1* FFT(TH1*, Option_t*, std::vector<double> * = NULL, std::vector<double> * = NULL);
                static TH1* BackwardFFT(TString, TString, bool, std::vector<double> &, std::vector<double> &, double = 0, double = 0, TDirectory * = gDirectory);
                static TH1* BackwardFFT(TString, TString, bool, int, double*, double*, double = 0, double = 0, TDirectory * = gDirectory);

                static TSpectrum* Spectrum(TH1*, int);

                static bool ComputeMeanAndSV(std::vector<double>&, std::vector<double>, std::vector<double>, std::vector<double>, bool = false);
                static bool ComputeMeanAndSV(std::vector<double>&, Option_t*, std::vector<double>, std::vector<double> = {}, std::vector<double> = {}, std::vector<double> = {});

                static double GetMeanContent(TGraph *, Option_t* = "0");
                static double GetStdDevContent(TGraph *, Option_t* = "0");
                static double GetMean(TGraph *, Option_t* = "0");
                static double GetStdDev(TGraph *, Option_t* = "0");

                static double GetMeanContent(TH1 *, Option_t* = "0");
                static double GetStdDevContent(TH1 *, Option_t* = "0");
                static double GetMean(TH1 *, Option_t* = "0");
                static double GetStdDev(TH1 *, Option_t* = "0");

                static TH1* Sum(TList*);
                static TH1* Rebin(TH1*, std::vector<int>, Option_t* = "");

                static TGraph* Sort(TGraph*, Option_t * = "x");
                static TGraph* Rebin(TGraph*, int = 1, Option_t * = "average x");

                static TH1* Average(TH1*, Option_t*);
                static TH1D* Average(TH2*, Option_t*);
                static TH2D* Average(TH3*, Option_t*);

                static TH1* AverageBinContent(TList*, Option_t* = "", Option_t* = "");
                static TH1* AverageBinContent(TH1*, Option_t*, Option_t* = "", Option_t* = "");
                static TH1D* AverageBinContent(TH2*, Option_t*, Option_t* = "", Option_t* = "");
                static TH2D* AverageBinContent(TH3*, Option_t*, Option_t* = "", Option_t* = "");

                static TH1* Integrate(TH1*, Option_t*);
                static TH1D* Integrate(TH2*, Option_t*);
                static TH2D* Integrate(TH3*, Option_t*);

                static TH1* Projection(TH1*, Option_t*);
                static TH1D* Projection(TH2*, Option_t*);
                static TH2D* Projection(TH3*, Option_t*);

                static TH1* Profile(TList*, Option_t *);
                static TH1* Profile(TH1*, Option_t *);
                static TProfile* Profile(TH2*, Option_t *);
                static TProfile2D* Profile(TH3*, Option_t *);

                static TList* Modulate(TH1*, Option_t*);
                static TList* Modulate(TH1*, Option_t*, int, int);
                static TList* Modulate(TH2*, Option_t*, int, int);
                static TList* Modulate(TH3*, Option_t*, int, int);

                static TH1 * FrequencyDistribution(TH1*, Option_t*, int, double, double);
                static TH2D* FrequencyDistribution(TH2*, Option_t*, int, double, double);
                static TH3D* FrequencyDistribution(TH3*, Option_t*, int, double, double);

                static TH1 *Invert(TH1 *h);
                
                static TH1 *Roll(TH1 *h, Option_t* = "x", int nbins = 0);
                static TH1 *Roll(TH2 *h, Option_t* = "x", int nbins = 0);
                static TH1 *Roll(TH3 *h, Option_t* = "x", int nbins = 0);

                static TH1 *Shift(TH1 *h, Option_t* = "x", double shift = 0);
                static TGraph *Shift(TGraph *g, double x0 = 0);

                static std::vector<double> GetVectorContent(TH1* h);
                static std::vector<double> GetVectorContent(TH1D* h);
                static std::vector<float>  GetVectorContent(TH1F* h);

                static std::vector<double> GetVectorError(TH1* h);
                static std::vector<double> GetVectorError(TH1D* h);
                static std::vector<float>  GetVectorError(TH1F* h);

                template<typename T>
                static TH1* SetVectorError(TH1* h, std::vector<T> v, int first = 0)
                {
                        for(int i = first, j = 0, N = h->GetNcells(); i < N; i++) {

                                if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                                if(j > (int) v.size()-1) break;

                                h->SetBinError(i, v[j++]);
                        }

                        return h;
                }

                template<typename T>
                static TH1* SetVectorContent(TH1* h, std::vector<T> v, int first = 0)
                {
                        int nEntries = h->GetEntries();
                        for(int i = first, j = 0, N = h->GetNcells(); i < N; i++) {

                                if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                                if(j > (int) v.size()-1) break;

                                h->SetBinContent(i, v[j++]);
                        }

                        h->SetEntries(nEntries);
                        return h;
                }

                template<typename T>
                static TH1* AddVectorContent(TH1* h, std::vector<T> v, int c1 = 1)
                {        
                        if(h == NULL) return NULL;
                        
                        int nEntries = h->GetEntries();
                        for(int i = 0, I = v.size(); i < I; i++) {

                                int bin = h->GetBin(i+1);
                                h->SetBinContent(bin, h->GetBinContent(bin) + c1*v[i]);
                        }

                        h->SetEntries(nEntries);
                        return h;
                }

                template<typename T>
                static TH1* AddVectorContent(TH1* h, std::vector<std::vector<T>> v, int c1 = 1)
                {
                        if(h == NULL) return NULL;
                        
                        if(h->InheritsFrom("TH3")) {
                                
                                UIUC::HandboxMsg::PrintError(__METHOD_NAME__, "Narrowing of 2D histogram not implemented");
                                return NULL;

                        } else if(h->InheritsFrom("TH2")) {

                                for(int i = 0, I = v.size(); i < I; i++) {

                                        for(int j = 0, J = v[i].size(); j < J; j++) {
                                        
                                                int bin = h->GetBin(i+1, j+1);
                                                h->SetBinContent(bin, h->GetBinContent(bin) + c1*v[i][j]);
                                        }
                                }

                        } else {
                        
                                for(int i = 0, I = v.size(); i < I; i++)
                                        AddVectorContent(h, v[i], c1);
                        }

                        return h;
                }
                static TH1* AppendToHistogram(TString, TTree*, TString, TString);
                static bool AppendToHistogram(TH1*&,   TTree*, TString, TString);

                TList * Search(TRegexp, int = 0);

                static TH1* MakeCorrelation(TH1*, TH1*, Option_t*, int, int, double = NAN, double = NAN, double = NAN, double = NAN);

                static TGraph* SetRange(TGraph*, Option_t*, double, double);

                static TH1*  SetRange(TH1* , Option_t*, double, double);
                static TH1D* SetRange(TH1D*, double, double);
                static TH2D* SetRange(TH2D*, Option_t *, double, double);
                static TH3D* SetRange(TH3D*, Option_t *, double, double);

                static TH1D* Pad(TH1D*, double, double);

                static double Length(TH1* , Option_t*);

                template<typename T>
                        static TH1* Histogram1D(TString name, TString title, std::vector<double> x, std::vector<T> content, std::vector<T> contentErr = std::vector<T>())
                        {
                                if(x.size() == 0) return NULL;
                                if(x.size()-1 != content.size()) {
                                        
                                        TString hintStr = "[#x=`"+TString::Itoa(x.size(),10)+"`,#content=`"+TString::Itoa(content.size(),10)+"`] ";
                                        if(x.size() == content.size()) hintStr += "(did you added x range upper bound?)";

                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector axis size.. cannot compute histogram `"+name+"` %s", hintStr.Data());
                                        return NULL;
                                }

                                if(contentErr.size() > 0 && content.size() != contentErr.size()) {
                                        
                                        TString hintStr = "[#x=`"+TString::Itoa(x.size(),10)+"`,#content=`"+TString::Itoa(content.size(),10)+"`,#sigma`"+TString::Itoa(contentErr.size(),10)+"`]";
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong uncertainty vector size.. cannot compute histogram `"+name+"` %s", hintStr.Data());
                                        return NULL;
                                }
                                
                                TH1* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH1*) new TH1C(name, title, x.size()-1, &x[0]);
                                if(std::is_same<T,short>::value)
                                        h = (TH1*) new TH1S(name, title, x.size()-1, &x[0]);
                                if(std::is_same<T,int>::value)
                                        h = (TH1*) new TH1I(name, title, x.size()-1, &x[0]);
                                if(std::is_same<T,float>::value)
                                        h = (TH1*) new TH1F(name, title, x.size()-1, &x[0]);
                                if(std::is_same<T,double>::value)
                                        h = (TH1*) new TH1D(name, title, x.size()-1, &x[0]);
                                
                                if(h == NULL) return NULL;

                                h->Sumw2(false);

                                content = UIUC::StripNaN(UIUC::StripInf(content));
                                contentErr = UIUC::StripNaN(UIUC::StripInf(contentErr));
                                for(int i = 1, N = h->GetNbinsX()+1, contentErrN = contentErr.size(); i < N; i++) 
                                {
                                        int idx = i-1;
                                        h->SetBinContent(i, content[idx]);
                                        if(contentErrN > 0) h->SetBinError(i, contentErr[idx]);
                                }

                                return h;
                        }

                template<typename T>
                        static TH2* Histogram2D(TString name, TString title, std::vector<double> x, std::vector<double> y, std::vector<T> content, std::vector<T> contentErr = std::vector<T>())
                        {
                                if(x.size() == 0) return NULL;
                                if(x.size()-1 != content.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. x.size() != content.size()+1 (did you added x range upper bound?)");
                                if(contentErr.size() > 0 && content.size() != contentErr.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. content.size() != contentErr.size()");

                                TH2* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH2*) new TH2C(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                                if(std::is_same<T,short>::value)
                                        h = (TH2*) new TH2S(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                                if(std::is_same<T,int>::value)
                                        h = (TH2*) new TH2I(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                                if(std::is_same<T,float>::value)
                                        h = (TH2*) new TH2F(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                                if(std::is_same<T,double>::value)
                                        h = (TH2*) new TH2D(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                                
                                if(h == NULL) return NULL;

                                h->Sumw2(false);

                                content = UIUC::StripNaN(UIUC::StripInf(content));
                                contentErr = UIUC::StripNaN(UIUC::StripInf(contentErr));
                                for(int i = 1, I = h->GetNbinsX()+1, contentErrN = contentErr.size(); i < I; i++) 
                                {
                                        for(int j = 1, J = h->GetNbinsY()+1; j < J; j++) 
                                        {
                                                int idx = (i-1) + (j-1)*I;
                                                h->SetBinContent(i, j, content[idx]);
                                                if(contentErrN > 0) h->SetBinError(i, j, contentErr[idx]);
                                        }
                                }

                                return h;
                        }

                template<typename T>
                        static TH3* Histogram3D(TString name, TString title, std::vector<double> x, std::vector<double> y, std::vector<double> z, std::vector<T> content, std::vector<T> contentErr = std::vector<T>())
                        {
                                if(x.size() == 0) return NULL;
                                if(x.size()-1 != content.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. x.size() != content.size()+1 (did you added x range upper bound?)");
                                if(contentErr.size() > 0 && content.size() != contentErr.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. content.size() != contentErr.size()");

                                TH3* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH3*) new TH3C(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                                if(std::is_same<T,short>::value)
                                        h = (TH3*) new TH3S(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                                if(std::is_same<T,int>::value)
                                        h = (TH3*) new TH3I(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                                if(std::is_same<T,float>::value)
                                        h = (TH3*) new TH3F(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                                if(std::is_same<T,double>::value)
                                        h = (TH3*) new TH3D(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                                
                                if(h == NULL) return NULL;

                                h->Sumw2(false);

                                content = UIUC::StripNaN(UIUC::StripInf(content));
                                contentErr = UIUC::StripNaN(UIUC::StripInf(contentErr));
                                for(int i = 1, I = h->GetNbinsX()+1, contentErrN = contentErr.size(); i < I; i++) 
                                {
                                        for(int j = 1, J = h->GetNbinsY()+1; j < J; j++) 
                                        {
                                                for(int k = 1, K = h->GetNbinsZ()+1; k < K; k++) 
                                                {
                                                        int idx = (i-1) + (j-1) * I + (k-1) * I * J;
                                                        h->SetBinContent(i, j, k, content[idx]);
                                                        if(contentErrN > 0) h->SetBinError(i, j, k, contentErr[idx]);
                                                }
                                        }
                                }

                                return h;
                        }

                template<typename T>
                inline  static TH1* Histogram  (TString name, TString title, std::vector<T> content, std::vector<T> contentErr = std::vector<T>()) { return Histogram(name, title, 1, content, contentErr); }
                template<typename T>
                        static TH1* Histogram  (TString name, TString title, double scaleX, std::vector<T> content, std::vector<T> contentErr = std::vector<T>())
                        {
                                if(content.size() == 0) return NULL;
                                if(contentErr.size() > 0 && content.size() != contentErr.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. content.size() != contentErr.size()");

                                TH1* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH1*) new TH1C(name, title, content.size(), 0, content.size()*scaleX);
                                if(std::is_same<T,short>::value)
                                        h = (TH1*) new TH1S(name, title, content.size(), 0, content.size()*scaleX);
                                if(std::is_same<T,int>::value)
                                        h = (TH1*) new TH1I(name, title, content.size(), 0, content.size()*scaleX);
                                if(std::is_same<T,float>::value)
                                        h = (TH1*) new TH1F(name, title, content.size(), 0, content.size()*scaleX);
                                if(std::is_same<T,double>::value)
                                        h = (TH1*) new TH1D(name, title, content.size(), 0, content.size()*scaleX);
                                
                                if(h == NULL) return NULL;
                                h->Sumw2(false);

                                content = UIUC::StripNaN(UIUC::StripInf(content));
                                contentErr = UIUC::StripNaN(UIUC::StripInf(contentErr));
                                for(int i = 1, I = content.size()+1, contentErrN = contentErr.size(); i < I; i++)
                                {
                                        h->SetBinContent(i, content[i-1]);
                                        if(contentErrN > 0) h->SetBinError(i, contentErr[i-1]);
                                }

                                return h;
                        }
                        

                template<typename T>
                inline  static TH2* Histogram  (TString name, TString title, std::vector<std::vector<T>> content, std::vector<std::vector<T>> contentErr = std::vector<std::vector<T>>()) { return Histogram(name, title, 1, 1, content, contentErr); }
                template<typename T>
                        static TH2* Histogram  (TString name, TString title, double scaleX, double scaleY, std::vector<std::vector<T>> content, std::vector<std::vector<T>> contentErr = std::vector<std::vector<T>>())
                        {
                                if(content.size() < 1) return NULL;
                                if(content[0].size() < 1) return NULL;

                                if(contentErr.size() > 0 && content.size() != contentErr.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. content.size() != contentErr.size()");

                                TH2* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH2*) new TH2C(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                                if(std::is_same<T,short>::value)
                                        h = (TH2*) new TH2S(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                                if(std::is_same<T,int>::value)
                                        h = (TH2*) new TH2I(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                                if(std::is_same<T,float>::value)
                                        h = (TH2*) new TH2F(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                                if(std::is_same<T,double>::value)
                                        h = (TH2*) new TH2D(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                                
                                if(h == NULL) return NULL;
                                h->Sumw2(false);
                                
                                content = UIUC::StripNaN(UIUC::StripInf(content));
                                contentErr = UIUC::StripNaN(UIUC::StripInf(contentErr));
                                for(int i = 1, I = content.size()+1, contentErrN = contentErr.size(); i < I; i++)
                                {
                                        for(int j = 1, J = content[i].size()+1, contentErrN = contentErr[i].size(); j < J; j++)
                                        {
                                                h->SetBinContent(i, j, content[i-1][j-1]);
                                                if(contentErrN > 0) h->SetBinError(i, j, contentErr[i-1][j-1]);
                                        }
                                }

                                return h;
                        }

                template<typename T>
                inline  static TH3* Histogram  (TString name, TString title, std::vector<std::vector<std::vector<T>>> content, std::vector<std::vector<std::vector<T>>> contentErr = std::vector<std::vector<std::vector<T>>>()) { return Histogram(name, title, 1, 1, 1, content, contentErr); }
                template<typename T>
                        static TH3* Histogram  (TString name, TString title, double scaleX, double scaleY, double scaleZ, std::vector<std::vector<std::vector<T>>> content, std::vector<std::vector<std::vector<T>>> contentErr = std::vector<std::vector<std::vector<T>>>())
                        {
                                if(content.size() < 1) return NULL;
                                if(content[0].size() < 1) return NULL;

                                if(contentErr.size() > 0 && content.size() != contentErr.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. content.size() != contentErr.size()");

                                TH3* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH3*) new TH3C(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                                if(std::is_same<T,short>::value)
                                        h = (TH3*) new TH3S(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                                if(std::is_same<T,int>::value)
                                        h = (TH3*) new TH3I(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                                if(std::is_same<T,float>::value)
                                        h = (TH3*) new TH3F(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                                if(std::is_same<T,double>::value)
                                        h = (TH3*) new TH3D(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                                
                                if(h == NULL) return NULL;
                                h->Sumw2(false);

                                content = UIUC::StripNaN(UIUC::StripInf(content));
                                contentErr = UIUC::StripNaN(UIUC::StripInf(contentErr));
                                for(int i = 1, I = content.size()+1, contentErrN = contentErr.size(); i < I; i++)
                                {
                                        for(int j = 1, J = content[i].size()+1, contentErrN = contentErr[i].size(); j < J; j++)
                                        {
                                                for(int k = 1, K = content[i][j].size()+1, contentErrN = contentErr[i][j].size(); k < K; k++)
                                                {
                                                        h->SetBinContent(i, j, k, content[i-1][j-1][k-1]);
                                                        if(contentErrN > 0) h->SetBinError(i, j, k, contentErr[i-1][j-1][k-1]);
                                                }
                                        }
                                }

                                return h;
                        }

                template<typename T>
                        static TH1* Histogram(TString name, TString title, int nbinsx, double xmin, double xmax, std::vector<T> x, std::vector<T> w = {})
                        {
                                if(w.size() > 0 && w.size() != x.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. x.size() != w.size()");

                                TH1* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH1*) new TH1C(name, title, nbinsx, xmin, xmax);
                                if(std::is_same<T,short>::value)
                                        h = (TH1*) new TH1S(name, title, nbinsx, xmin, xmax);
                                if(std::is_same<T,int>::value)
                                        h = (TH1*) new TH1I(name, title, nbinsx, xmin, xmax);
                                if(std::is_same<T,float>::value)
                                        h = (TH1*) new TH1F(name, title, nbinsx, xmin, xmax);
                                if(std::is_same<T,double>::value)
                                        h = (TH1*) new TH1D(name, title, nbinsx, xmin, xmax);
                                
                                if(h == NULL) return NULL;

                                h->Sumw2(w.size());
                                for(int i = 0, N = x.size(); i < N; i++) {
                                        h->Fill(x[i], w.size() ? w[i] : 1);
                                }

                                return h;
                        }

                template<typename T>
                        static TH2* Histogram(TString name, TString title, int nbinsx, double xmin, double xmax, int nbinsy, double ymin, double ymax, std::vector<T> x, std::vector<T> y, std::vector<T> w = {})
                        {
                                if(y.size() != x.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. x.size() != y.size()");
                                if(w.size() > 0 && w.size() != x.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. x.size() != w.size()");

                                TH2* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH2*) new TH2C(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                                if(std::is_same<T,short>::value)
                                        h = (TH2*) new TH2S(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                                if(std::is_same<T,int>::value)
                                        h = (TH2*) new TH2I(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                                if(std::is_same<T,float>::value)
                                        h = (TH2*) new TH2F(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                                if(std::is_same<T,double>::value)
                                        h = (TH2*) new TH2D(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                                
                                if(h == NULL) return NULL;

                                h->Sumw2(w.size());
                                for(int i = 0, N = x.size(); i < N; i++) {
                                        h->Fill(x[i],y[i], w.size() ? w[i] : 1);
                                }

                                return h;
                        }

                template<typename T>
                        static TH3* Histogram(TString name, TString title, int nbinsx, double xmin, double xmax, int nbinsy, double ymin, double ymax, int nbinsz, double zmin, double zmax, std::vector<T> x, std::vector<T> y, std::vector<T> z, std::vector<T> w = {})
                        {
                                if(y.size() != x.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. x.size() != y.size()");
                                if(z.size() != x.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. x.size() != z.size()");
                                if(w.size() > 0 && w.size() != x.size())
                                        UIUC::HandboxMsg::PrintError(__METHOD_NAME__, (TString) "Wrong vector size.. x.size() != w.size()");

                                TH3* h = NULL;
                                if(std::is_same<T,char>::value)
                                        h = (TH3*) new TH3C(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);
                                if(std::is_same<T,short>::value)
                                        h = (TH3*) new TH3S(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);
                                if(std::is_same<T,int>::value)
                                        h = (TH3*) new TH3I(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);
                                if(std::is_same<T,float>::value)
                                        h = (TH3*) new TH3F(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);
                                if(std::is_same<T,double>::value)
                                        h = (TH3*) new TH3D(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);

                                if(h == NULL) return NULL;
                                h->Sumw2(w.size());

                                for(int i = 0, N = x.size(); i < N; i++) {
                                        h->Fill(x[i],y[i],z[i], w.size() ? w[i] : 1);
                                }

                                return h;
                        }

                static std::vector<double> ContentVector(TH1* , int axis = -1);
                static std::vector<float>  ContentVector(TH1F*, int axis = -1);
                static std::vector<double> ContentVector(TH1D*, int axis = -1);
                static std::vector<double> ErrorVector(TH1* );
                static std::vector<double> ErrorVector(TH1D*);
                static std::vector<float>  ErrorVector(TH1F*);

                static std::vector<double> InverseTransformSampling(int N, TF1 *pdf);
                inline static std::vector<double> RandomVector(int N, TF1 *pdf) { return InverseTransformSampling(N, pdf); }
                       static std::vector<double> RandomVector(int N, TString pdfFormula, std::vector<double> pdfParams, double xMin, double xMax);
                
                // TODO: Variable bin length if required
                // static TH1*  SetRange(TH1* , Option_t *, vector<double>);
                // static TH1D* SetRange(TH1D*, vector<double>);
                // static TH2D* SetRange(TH2D*, Option_t *, vector<double>);
                // static TH3D* SetRange(TH3D*, Option_t *, vector<double>);

                static TH1*  NormalizeRange(TH1* , Option_t *);
                static TH1D* NormalizeRange(TH1D*);
                static TH2D* NormalizeRange(TH2D*, Option_t *);
                static TH3D* NormalizeRange(TH3D*, Option_t *);

                static TH1*  DenormalizeRange(TH1* , Option_t *, double, double);
                static TH1D* DenormalizeRange(TH1D*, double, double);
                static TH2D* DenormalizeRange(TH2D*, Option_t *, double, double);
                static TH3D* DenormalizeRange(TH3D*, Option_t *, double, double);

                // TODO: Variable bin length if required
                // static TH1*  DenormalizeRange(TH1* , Option_t *, vector<double>);
                // static TH1D* DenormalizeRange(TH1D*, vector<double>);
                // static TH2D* DenormalizeRange(TH2D*, Option_t *, vector<double>);
                // static TH3D* DenormalizeRange(TH3D*, Option_t *, vector<double>);

                static TH1D* CreateTH1chain(TString, TString, TList*);
                TH1D* CreateTH1chain(TString, TString, TRegexp = ".*", int = 0);
                static TH2D* CreateTH2chain(TString, TString, TList*, std::vector<int> = {});
                TH2D* CreateTH2chain(TString, TString, TRegexp = ".*", int = 0);
                static TH3D* CreateTH3chain(TString, TString, TList*, std::vector<int> = {});
                TH3D* CreateTH3chain(TString, TString, TRegexp = ".*", int = 0);

                static void PrintASCII(TH1*, Notation = E_FLOAT);
                //static void PrintASCII(TGraph*, Notation = E_FLOAT);
                static void PrintLatex(TH1*, Notation = E_FLOAT);
                //static void PrintLatex(TGraph, Notation = E_FLOAT);

                static TGraph*                           LoadGraphFromASCII(TString);
                static TH1*                              LoadHistFromASCII(TString);
                static TMatrixD*                         LoadMatrixFromASCII(TString);
                static TTree*                            LoadTreeFromASCII(TString, TString = "", char = ' ');
                static std::vector<double>               LoadVectorFromASCII(TString);
                static std::vector<std::complex<double>> LoadComplexVectorFromASCII(TString);
                static std::vector<std::vector<TString>> LoadStringFromASCII(TString);

                static bool SaveIntoASCII(TString, std::ios_base::openmode, TGraph *);
                static bool SaveIntoASCII(TString, std::ios_base::openmode, TH1 *);
                static bool SaveIntoASCII(TString, std::ios_base::openmode, TTree *, TString, TString = "");
                static bool SaveIntoASCII(TString, std::ios_base::openmode, TMatrixD *, TString);
                static bool SaveIntoASCII(TString, std::ios_base::openmode, const std::vector<std::complex<double>> &, int = 64);
                static bool SaveIntoASCII(TString, std::ios_base::openmode, const std::vector<double>  &, int = 64);
                static bool SaveIntoASCII(TString, std::ios_base::openmode, const std::vector<std::vector<double>> &, int = 64);
                static bool SaveIntoASCII(TString, std::ios_base::openmode, const std::vector<std::vector<TString>> &);

                static std::vector<double> Range(TH1D *h);
                static std::vector<double> Range(TAxis *x);
                static std::vector<double> Range(int nBins, double xmin, double xmax);
                static bool ExtendBinRange(std::vector<double>&, double, double, double = 1e10);
                static bool ExtendBinRangeByStep(std::vector<double> &x, double x0, double x1, double dx);

                static std::vector<double> GetBinWidth(TH1*, Option_t*);

                static std::vector<std::pair<double, double>> PeakScan(TH1 *h, Option_t *option = "", int nPeaks = 1, double sigma = 2, double threshold = 0.05);
                static void DrawPeak(TH1 *h, Option_t *option, std::vector<std::pair<double, double>>, Color_t color = kRed);

                ClassDef(UIUC::TFactory,1);
        };
}

#endif
