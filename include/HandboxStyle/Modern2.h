// This header file is used in case there is no user-defined HandboxStyle.
// So this will avoid errors during compilation time
#ifndef Modern2_H
#define Modern2_H

// Uncomment to include this custom handbox style in the $LIBUIUC/include/UIUC/Handbox.h
// #define MYHANDBOX_STYLE

#include <UIUC/HandboxStyle.h>
#include <UIUC/TFactory.h>

namespace UIUC {

class Modern2 : public UIUC::HandboxStyle
{
public:

        Modern2(const char* name = "Modern2", const char* title = "") : UIUC::HandboxStyle(name, title) {

        this->SetName(name);

        TStyle *s = (TStyle*) gROOT->GetStyle("Modern");
        TStyle *sData = new TStyle("Modern2","");
        s->Copy(*sData);

        sData->SetLegendFont(132);
        sData->SetLabelFont(132, "");
        sData->SetTitleFont(132, "");
        sData->SetLabelFont(132, "XYZ");
        sData->SetTitleFont(132, "XYZ");
        sData->SetTextSize(0.035);
        sData->SetLabelSize(0.035, "XYZ");
        sData->SetTitleSize(0.035, "XYZ");

        sData->SetTitleXOffset(1.5);
        sData->SetTitleYOffset(1.7);
        //sData->SetTitleZOffset(2.0);

        sData->SetStatFont(132);
        sData->SetStatX(.8990995);
        sData->SetStatW(0.26);
        //sData->SetStatY(0.95);
        //sData->SetStatH(0.26);

        sData->SetPadTopMargin(0.075);
        sData->SetPadRightMargin(0.150);
        sData->SetPadLeftMargin(0.150);
        sData->SetPadBottomMargin(0.125);

        TStyle *sMC = new TStyle("Modern2MC","");
        sData->Copy(*sMC);
        TStyle *sBkg = new TStyle("Modern2Bkg","");
        sData->Copy(*sBkg);
        TStyle *sFit = new TStyle("Modern2Fit","");
        sData->Copy(*sFit);

        std::vector<TStyle*> vStyle;
        vStyle.push_back(sData);
        vStyle.push_back(sMC);
        vStyle.push_back(sBkg);
        vStyle.push_back(sFit);


        UIUC::HandboxStyle::Register(this, vStyle);
}

~Modern2() {
};

std::vector<TObjectStyle> GetDefaultObjectStyle(TList* listOfObjects) {

        std::vector<TObjectStyle> vObjStyle;

        std::vector<Color_t> color = {
                Color_t(kBlue+2), Color_t(kRed+2), Color_t(kGreen+2), Color_t(kOrange+2),
                Color_t(kPink+2), Color_t(kViolet+2), Color_t(kMagenta+2), Color_t(kTeal+2),
                Color_t(kSpring+2), kBlack
        };

        std::vector<Color_t> fillcolor = {
                Color_t(kBlue+2), Color_t(kRed+2), Color_t(kGreen+2), Color_t(kOrange+2),
                Color_t(kPink+2), Color_t(kViolet+2), Color_t(kMagenta+2),
                Color_t(kTeal+2), Color_t(kSpring+2), kBlack
        };

        std::vector<double> fillalpha = {0,0,0,0,0,0,0,0,0,0};

        for(int i = 0, N = listOfObjects->GetSize(); i < N; i++) {

                TObjectStyle obj_style;

                if(i < (int) color.size()) obj_style.line = color[i];
                else obj_style.line = color[0];

                if(i < (int) color.size()) obj_style.marker = color[i];
                else obj_style.marker = color[0];

                if(i < (int) fillcolor.size()) {

                        obj_style.fill = fillcolor[i];
                        if(i < (int) fillalpha.size()) obj_style.fillalpha = fillalpha[i];

                } else {

                        obj_style.fill = -1;
                        obj_style.fillalpha = -1;
                }

                vObjStyle.push_back(obj_style);
        }

        return vObjStyle;
}

bool SetCustomLegend(TVirtualPad* pad, TList* lObject) {

        UIUC::HandboxMsg::PrintDebug(__METHOD_NAME__, "Default axis options used..");
        if(pad == NULL) return 0;
        if(lObject->GetSize() == 0) return 0;

        TObject *obj = NULL;
        TIter Next(lObject);

        int nString = 1; // = 1, because of the title..
        int nCurves = 1; // = 1, because of the title..
        for(int i = 0; (obj = Next()); i++) {

                if(obj->InheritsFrom("TObjString")) nString++;
                else nCurves++;
        }
        pad->Update();

        TH1 *h = NULL;
        TIter Next2(pad->GetListOfPrimitives());
        while(( obj = (TObject*) Next2()) ) {

                if(h == NULL && obj->InheritsFrom("TH1")) h = (TH1*) obj;
        }

        TPaveStats *st=NULL;
        if(h != NULL) st = (TPaveStats*) h->FindObject("stats");
        int nStats = (!st) ? 0 : st->GetListOfLines()->GetSize();
        int nStatLines = 4;
        //if(obj0->InheritsFrom("TGraph3D") || obj0->InheritsFrom("TH3")) nStatLines = 4;
        //else if(obj0->InheritsFrom("TGraph2D") || obj0->InheritsFrom("TH2")) nStatLines = 6;
        //else if(obj0->InheritsFrom("TGraph") || obj0->InheritsFrom("TH1")) nStatLines = 8;

        double line_height = gStyle->GetStatH()/nStatLines; // Classic value.. (4 lines is the Classic value defined in gStyle whatever the option)
        double spacer = 0.01;
        double x1,y1,x2,y2;

        double offset = 0;
        if(!st) offset = -0.010;
        if(nCurves > 1) {

                x2 = 0.8493948;
                x1 = 0.5497047;
                y2 = gStyle->GetStatY()-nStats*line_height-(nStats>0)*spacer + offset;
                y1 = gStyle->GetStatY()-nStats*line_height-(nStats>0)*spacer-nCurves*line_height + offset;

                TLegend *fLegend = new TLegend(x1, y1, x2, y2);

                Next.Reset();
                for(int i = 0; (obj = Next()); i++) {

                        if(!obj->InheritsFrom("TObjString")) fLegend->AddEntry(obj);
                }
                fLegend->Draw();
        }

        //Build legend for extra information..
        if(nString > 1) {

                x2 = 0.8493948;
                x1 = 0.5497047;
                y2 = gStyle->GetStatY()-nStats*line_height-nCurves*line_height-(nStats>0)*spacer-(nCurves>1)*spacer + offset;
                y1 = gStyle->GetStatY()-nStats*line_height-nCurves*line_height-(nStats>0)*spacer-(nCurves>1)*spacer-nString*line_height + offset;

                TLegend *fLegend2 = new TLegend(x1, y1, x2, y2);
                fLegend2->SetHeader("Additional :");

                Next.Reset();
                for(int i = 0; (obj = Next()); i++) {

                        if(i > 0 && obj->InheritsFrom("TObjString")) fLegend2->AddEntry((TObject*) NULL, ((TObjString*) obj)->GetString(), "");
                }
                fLegend2->Draw();
        }

        return 1;
}

TPaveStats* SetCustomStatbox(TVirtualPad* vpad, TList *lObject) {

        UNUSED(lObject);

        TPaveStats *st = (TPaveStats*)(vpad->GetPrimitive("stats"));
        if(!st) return NULL;

        st->SetX1NDC(0.6382158);
        st->SetY1NDC(0.7639598);
        st->SetX2NDC(0.8498055);
        st->SetY2NDC(0.9251701);

        vpad->Modified();

        return NULL;
}

TPaveText* SetCustomTitle(TVirtualPad* vpad, TString title) {

        UNUSED(vpad);
        TPaveText *pt0 = (TPaveText*)(vpad->GetPrimitive("title"));

        TPaveText *pt = new TPaveText(0.1739957,0.7966698,0.6330332,0.8924739,"blNDC");
        pt->SetName("title0");
        pt->Clear();
        pt->AddText(UIUC::TFactory::MultipleLines(title));

        pt->SetBorderSize(0);
        pt->SetFillColor(0);
        pt->SetFillStyle(0);
        pt->SetTextAlign(12);
        pt->SetTextFont(132);
        pt->SetTextSize(0.035);

        if(pt0 != NULL) pt0->Clear();
        return pt;
}

std::vector<TString> SetCustomOptions(TList *lObject, std::vector<TString> vOption = {}) {

        std::vector<TString> vOpt;

        TObject *obj = NULL;
        TIter Next(lObject);
        for(int i = 0; (obj = Next()); i++) {

                TString opt = TString(vOption[i]);

                if(obj->InheritsFrom("TH2")) opt += "";
                else if(obj->InheritsFrom("TH1")) {

                        TH1 *h = ((TH1*) obj);

                        if(h->GetFillColor() != 0) opt += "BAR";
                        else opt += "F";

                        if(opt.Contains("HIST") || opt.Contains("E3")) h->SetMarkerStyle(0);
                        else {
                                opt += "E1";
                                //h->SetMarkerStyle(20);
                                //    h->SetMarkerSize(0.4);
                        }

                } else if(obj->InheritsFrom("TGraph") || obj->InheritsFrom("TGraphErrors") || obj->InheritsFrom("TGraphAsymmErrors")) {

                        TGraph *g = ((TGraph*) obj);

                        if(opt.Contains("E3")) {

                                g->SetMarkerStyle(0);
                                g->SetFillColor(g->GetMarkerColor());
                                g->SetFillStyle(3002);

                        } else {
                                opt += "E1";
                                //g->SetMarkerStyle(20);
                                //    g->SetMarkerSize(0.4);
                        }
                }

                vOpt.push_back(opt);
        }

        return vOpt;
}

bool SetCustomAxis(TList* listOfObjects) {

        double yMin = NAN;
        double yMax = NAN;

        for(int i = 0; i < listOfObjects->GetSize(); i++) {

                TObject *obj0 = listOfObjects->At(i);

                TAxis *x = NULL;
                TAxis *y = NULL;
                TAxis *z = NULL;

                double yMin0;
                double yMax0;
                if(obj0->InheritsFrom("TGraph")) {

                        if(((TGraph*) obj0)->GetN() == 0) continue;
                        yMin0 = ((TGraph*) obj0)->GetHistogram()->GetMinimum(0);
                        yMax0 = ((TGraph*) obj0)->GetHistogram()->GetMaximum();

                        x = ((TGraph*) obj0)->GetHistogram()->GetXaxis();
                        y = ((TGraph*) obj0)->GetHistogram()->GetYaxis();

                } else if(obj0->InheritsFrom("TF1")) {

                        yMin0 = ((TF1*) obj0)->GetHistogram()->GetMinimum(0);
                        yMax0 = ((TF1*) obj0)->GetHistogram()->GetMaximum();

                        x = ((TF1*) obj0)->GetHistogram()->GetXaxis();
                        y = ((TF1*) obj0)->GetHistogram()->GetYaxis();

                } else if(obj0->InheritsFrom("TH1")) {

                        if(((TH1D*) obj0)->GetEntries() == 0) continue;
                        yMin0 = ((TH1D*) obj0)->GetMinimum(0);
                        yMax0 = ((TH1D*) obj0)->GetMaximum();

                        x = ((TH1*) obj0)->GetXaxis();
                        y = ((TH1*) obj0)->GetYaxis();
                        z = ((TH1*) obj0)->GetZaxis();
                }

                // Center title
                if(x) x->CenterTitle();
                if(y) y->CenterTitle();
                if(z) z->CenterTitle();

                if(x) x->SetMaxDigits(5);
                if(y) y->SetMaxDigits(3);
                if(z) z->SetMaxDigits(3);

                // To be better implemented..
                if(gSystem->Getenv("ESCALADE_TSIZE")) {

                        //        ->SetTextSize(gSystem->Getenv("ESCALADE_TSIZE").Atof());
                        //      ->SetTitleSize(gSystem->Getenv("ESCALADE_TSIZE").Atof(), "XYZ");
                        if(x) x->SetLabelSize(TString(gSystem->Getenv("ESCALADE_TSIZE")).Atof());
                        if(y) y->SetLabelSize(TString(gSystem->Getenv("ESCALADE_TSIZE")).Atof());
                        if(z) z->SetLabelSize(TString(gSystem->Getenv("ESCALADE_TSIZE")).Atof());

                        if(x) x->SetTitleSize(TString(gSystem->Getenv("ESCALADE_TSIZE")).Atof());
                        if(y) y->SetTitleSize(TString(gSystem->Getenv("ESCALADE_TSIZE")).Atof());
                        if(z) z->SetTitleSize(TString(gSystem->Getenv("ESCALADE_TSIZE")).Atof());
                }

                // To be better implemented..
                if(gSystem->Getenv("ESCALADE_TOFFSET")) {

                        if(x) x->SetTitleOffset(TString(gSystem->Getenv("ESCALADE_TOFFSET")).Atof());
                        if(y) y->SetTitleOffset(TString(gSystem->Getenv("ESCALADE_TOFFSET")).Atof());
                        if(z) z->SetTitleOffset(TString(gSystem->Getenv("ESCALADE_TOFFSET")).Atof());
                }

                if(yMin0 < yMin || UIUC::IsNaN(yMin)) yMin = yMin0;
                if(yMax0 > yMax || UIUC::IsNaN(yMax)) yMax = yMax0;
        }

        if(yMin >= yMax) return 0;
        yMax += 0.35*yMax;

        TObject *obj0 = listOfObjects->At(0);
        if(UIUC::IsNaN(yMin) || UIUC::IsNaN(yMax)) return 0;

        if(obj0->InheritsFrom("TGraph")) ((TGraph*) obj0)->GetYaxis()->SetRangeUser(yMin,yMax);
        else if(obj0->InheritsFrom("TF1")) ((TF1*) obj0)->GetYaxis()->SetRangeUser(yMin,yMax);
        else if(obj0->InheritsFrom("TH1D")) ((TH1*) obj0)->GetYaxis()->SetRangeUser(yMin,yMax);

        return 1;
}
};
}

#endif
