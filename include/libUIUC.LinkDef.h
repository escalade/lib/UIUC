/**
 * @Author: Marco Meyer <meyerma>
 * @Date:   2019-03-27T01:29:35-05:00
 * @Email:  marco.meyer@cern.ch
 * @Last modified by:   meyerma
 * @Last modified time: 2019-03-30T15:33:21-05:00
 */



/**
 **********************************************
 *
 * \file libUIUC.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all functions;

#pragma link C++ namespace UIUC+;

#pragma link C++ class UIUC::Handbox+;
#pragma link C++ class UIUC::HandboxMsg+;
#pragma link C++ class UIUC::HandboxStyle+;
#pragma link C++ class UIUC::HandboxUsage+;

#pragma link C++ class UIUC::TFactory+;

#pragma link C++ class UIUC::TFileReader+;
#pragma link C++ class UIUC::TVarexp+;

#endif
